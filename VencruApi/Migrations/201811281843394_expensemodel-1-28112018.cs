namespace VencruApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class expensemodel128112018 : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.ExpenseModels", "receiptimageurl");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ExpenseModels", "receiptimageurl", c => c.String());
        }
    }
}
