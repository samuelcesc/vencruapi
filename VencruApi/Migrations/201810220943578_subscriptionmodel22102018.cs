namespace VencruApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class subscriptionmodel22102018 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.SubscriptionModels",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        planid = c.String(),
                        userid = c.String(maxLength: 128),
                        date_created = c.DateTime(nullable: false),
                        status = c.Boolean(nullable: false),
                        plan_id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.PlanModels", t => t.plan_id)
                .ForeignKey("dbo.AspNetUsers", t => t.userid)
                .Index(t => t.userid)
                .Index(t => t.plan_id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SubscriptionModels", "userid", "dbo.AspNetUsers");
            DropForeignKey("dbo.SubscriptionModels", "plan_id", "dbo.PlanModels");
            DropIndex("dbo.SubscriptionModels", new[] { "plan_id" });
            DropIndex("dbo.SubscriptionModels", new[] { "userid" });
            DropTable("dbo.SubscriptionModels");
        }
    }
}
