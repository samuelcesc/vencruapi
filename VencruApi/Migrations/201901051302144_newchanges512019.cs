namespace VencruApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class newchanges512019 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.BusinessModels", "companyname", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.BusinessModels", "companyname", c => c.String());
        }
    }
}
