namespace VencruApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class waitlistedit2018094 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.WaitListModels", "Date_added", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.WaitListModels", "Date_added");
        }
    }
}
