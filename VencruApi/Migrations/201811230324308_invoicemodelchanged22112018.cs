namespace VencruApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class invoicemodelchanged22112018 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ProductModelInvoiceModels", "ProductModel_id", "dbo.ProductModels");
            DropForeignKey("dbo.ProductModelInvoiceModels", "InvoiceModel_id", "dbo.InvoiceModels");
            DropIndex("dbo.ProductModelInvoiceModels", new[] { "ProductModel_id" });
            DropIndex("dbo.ProductModelInvoiceModels", new[] { "InvoiceModel_id" });
            AddColumn("dbo.ItemsModels", "productid", c => c.Int(nullable: false));
            CreateIndex("dbo.ItemsModels", "productid");
            AddForeignKey("dbo.ItemsModels", "productid", "dbo.ProductModels", "id", cascadeDelete: false);
            DropTable("dbo.ProductModelInvoiceModels");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.ProductModelInvoiceModels",
                c => new
                    {
                        ProductModel_id = c.Int(nullable: false),
                        InvoiceModel_id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.ProductModel_id, t.InvoiceModel_id });
            
            DropForeignKey("dbo.ItemsModels", "productid", "dbo.ProductModels");
            DropIndex("dbo.ItemsModels", new[] { "productid" });
            DropColumn("dbo.ItemsModels", "productid");
            CreateIndex("dbo.ProductModelInvoiceModels", "InvoiceModel_id");
            CreateIndex("dbo.ProductModelInvoiceModels", "ProductModel_id");
            AddForeignKey("dbo.ProductModelInvoiceModels", "InvoiceModel_id", "dbo.InvoiceModels", "id", cascadeDelete: true);
            AddForeignKey("dbo.ProductModelInvoiceModels", "ProductModel_id", "dbo.ProductModels", "id", cascadeDelete: true);
        }
    }
}
