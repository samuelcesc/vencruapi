namespace VencruApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class clientandproductlistchange3 : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.ClientsModels", new[] { "BusinessId" });
            //DropIndex("dbo.ProductListModels", new[] { "BusinessId" });
            AddColumn("dbo.ClientsModels", "isdeleted", c => c.Boolean(nullable: false));
            //AddColumn("dbo.ProductListModels", "isdeleted", c => c.Boolean(nullable: false));
            CreateIndex("dbo.ClientsModels", "businessid");
            //CreateIndex("dbo.ProductListModels", "businessid");
        }
        
        public override void Down()
        {
            //DropIndex("dbo.ProductListModels", new[] { "businessid" });
            DropIndex("dbo.ClientsModels", new[] { "businessid" });
            //DropColumn("dbo.ProductListModels", "isdeleted");
            DropColumn("dbo.ClientsModels", "isdeleted");
            //CreateIndex("dbo.ProductListModels", "BusinessId");
            CreateIndex("dbo.ClientsModels", "BusinessId");
        }
    }
}
