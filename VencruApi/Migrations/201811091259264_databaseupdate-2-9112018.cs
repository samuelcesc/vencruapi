namespace VencruApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class databaseupdate29112018 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PromoCodeModels", "created_by", c => c.String());
            AddColumn("dbo.SubscriptionModels", "promocode", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.SubscriptionModels", "promocode");
            DropColumn("dbo.PromoCodeModels", "created_by");
        }
    }
}
