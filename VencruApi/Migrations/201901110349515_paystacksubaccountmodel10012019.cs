namespace VencruApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class paystacksubaccountmodel10012019 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PayStackSubAccountModels",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        userid = c.String(nullable: false),
                        businessid = c.Int(nullable: false),
                        subaccountcode = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.BusinessModels", t => t.Id)
                .Index(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PayStackSubAccountModels", "Id", "dbo.BusinessModels");
            DropIndex("dbo.PayStackSubAccountModels", new[] { "Id" });
            DropTable("dbo.PayStackSubAccountModels");
        }
    }
}
