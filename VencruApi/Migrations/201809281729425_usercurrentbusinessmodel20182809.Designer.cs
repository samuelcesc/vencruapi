// <auto-generated />
namespace VencruApi.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class usercurrentbusinessmodel20182809 : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(usercurrentbusinessmodel20182809));
        
        string IMigrationMetadata.Id
        {
            get { return "201809281729425_usercurrentbusinessmodel20182809"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
