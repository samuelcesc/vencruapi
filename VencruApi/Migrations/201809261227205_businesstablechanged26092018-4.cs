namespace VencruApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class businesstablechanged260920184 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.BusinessModels", "UsersId", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.BusinessModels", "UsersId");
        }
    }
}
