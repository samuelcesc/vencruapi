namespace VencruApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class invoiceguid30102018 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.InvoiceModels", "invoiceguid", c => c.Guid(nullable: false));
            AddColumn("dbo.InvoiceModels", "paymentlink", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.InvoiceModels", "paymentlink");
            DropColumn("dbo.InvoiceModels", "invoiceguid");
        }
    }
}
