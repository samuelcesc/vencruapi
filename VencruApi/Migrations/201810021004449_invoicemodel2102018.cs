namespace VencruApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class invoicemodel2102018 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.InvoiceModels", "BusinessModel_Id", "dbo.BusinessModels");
            DropIndex("dbo.InvoiceModels", new[] { "BusinessModel_Id" });
            RenameColumn(table: "dbo.InvoiceModels", name: "BusinessModel_Id", newName: "businessid");
            AlterColumn("dbo.InvoiceModels", "businessid", c => c.Int(nullable: false));
            CreateIndex("dbo.InvoiceModels", "businessid");
            AddForeignKey("dbo.InvoiceModels", "businessid", "dbo.BusinessModels", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.InvoiceModels", "businessid", "dbo.BusinessModels");
            DropIndex("dbo.InvoiceModels", new[] { "businessid" });
            AlterColumn("dbo.InvoiceModels", "businessid", c => c.Int());
            RenameColumn(table: "dbo.InvoiceModels", name: "businessid", newName: "BusinessModel_Id");
            CreateIndex("dbo.InvoiceModels", "BusinessModel_Id");
            AddForeignKey("dbo.InvoiceModels", "BusinessModel_Id", "dbo.BusinessModels", "Id");
        }
    }
}
