namespace VencruApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class clientandproductlistmodelchanged27092018 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ClientsModels", "UserId", c => c.String(nullable: false));
            AddColumn("dbo.ProductListModels", "UserId", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ProductListModels", "UserId");
            DropColumn("dbo.ClientsModels", "UserId");
        }
    }
}
