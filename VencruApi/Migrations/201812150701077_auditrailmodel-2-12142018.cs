namespace VencruApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class auditrailmodel212142018 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AuditTrailModels", "userid", c => c.String());
            AddColumn("dbo.AuditTrailModels", "datamodel", c => c.String());
            AddColumn("dbo.AuditTrailModels", "valuesbefore", c => c.String());
            DropColumn("dbo.AuditTrailModels", "actionmodel");
            DropColumn("dbo.AuditTrailModels", "valuebefore");
        }
        
        public override void Down()
        {
            AddColumn("dbo.AuditTrailModels", "valuebefore", c => c.String());
            AddColumn("dbo.AuditTrailModels", "actionmodel", c => c.String());
            DropColumn("dbo.AuditTrailModels", "valuesbefore");
            DropColumn("dbo.AuditTrailModels", "datamodel");
            DropColumn("dbo.AuditTrailModels", "userid");
        }
    }
}
