namespace VencruApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class auditrailmodel12142018 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AuditTrailModels",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        keyfieldid = c.Int(nullable: false),
                        auditactiontypeenum = c.Int(nullable: false),
                        action_date = c.String(),
                        actionmodel = c.String(),
                        changes = c.String(),
                        valuebefore = c.String(),
                        valueafter = c.String(),
                    })
                .PrimaryKey(t => t.id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.AuditTrailModels");
        }
    }
}
