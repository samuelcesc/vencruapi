namespace VencruApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _4onboardingfieldschanged15092018 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "city", c => c.String());
            AddColumn("dbo.AspNetUsers", "companyname", c => c.String());
            AddColumn("dbo.AspNetUsers", "country", c => c.String());
            AddColumn("dbo.AspNetUsers", "firstname", c => c.String());
            AddColumn("dbo.AspNetUsers", "industry", c => c.String());
            AddColumn("dbo.AspNetUsers", "isincorporated", c => c.Int(nullable: false));
            AddColumn("dbo.AspNetUsers", "lastname", c => c.String());
            AddColumn("dbo.AspNetUsers", "state", c => c.String());
            AddColumn("dbo.AspNetUsers", "address", c => c.String());
            AddColumn("dbo.AspNetUsers", "employeesize", c => c.String());
            AddColumn("dbo.AspNetUsers", "currency", c => c.String());
            AddColumn("dbo.AspNetUsers", "onlinepayment", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "onlinepayment");
            DropColumn("dbo.AspNetUsers", "currency");
            DropColumn("dbo.AspNetUsers", "employeesize");
            DropColumn("dbo.AspNetUsers", "address");
            DropColumn("dbo.AspNetUsers", "state");
            DropColumn("dbo.AspNetUsers", "lastname");
            DropColumn("dbo.AspNetUsers", "isincorporated");
            DropColumn("dbo.AspNetUsers", "industry");
            DropColumn("dbo.AspNetUsers", "firstname");
            DropColumn("dbo.AspNetUsers", "country");
            DropColumn("dbo.AspNetUsers", "companyname");
            DropColumn("dbo.AspNetUsers", "city");
        }
    }
}
