namespace VencruApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class businesstablechanged260920183 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.BusinessModels", "date_created", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.BusinessModels", "date_created");
        }
    }
}
