namespace VencruApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class isincorporatedchanged19092018 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.AspNetUsers", "onlinepayment", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.AspNetUsers", "onlinepayment", c => c.String());
        }
    }
}
