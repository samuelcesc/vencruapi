namespace VencruApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class invoicepaymentmodel26102018 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ItemsModels",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        description = c.String(),
                        price = c.Int(nullable: false),
                        quantity = c.Int(nullable: false),
                        amount = c.Int(nullable: false),
                        invoiceid = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.InvoiceModels", t => t.invoiceid, cascadeDelete: true)
                .Index(t => t.invoiceid);
            
            CreateTable(
                "dbo.InvoicePaymentModels",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        amount = c.Int(nullable: false),
                        paidwith = c.String(),
                        date_paid = c.String(nullable: false),
                        date_created = c.String(),
                        invoiceid = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.InvoiceModels", t => t.invoiceid, cascadeDelete: true)
                .Index(t => t.invoiceid);
            
            AddColumn("dbo.InvoiceModels", "subtotal", c => c.Int(nullable: false));
            AddColumn("dbo.InvoiceModels", "discount", c => c.Int(nullable: false));
            AddColumn("dbo.InvoiceModels", "amountdue", c => c.Int(nullable: false));
            AddColumn("dbo.InvoiceModels", "deposit", c => c.Int(nullable: false));
            AddColumn("dbo.InvoiceModels", "paymenttype", c => c.String());
            AddColumn("dbo.InvoiceModels", "notes", c => c.String());
            AddColumn("dbo.InvoiceModels", "invoicestyle", c => c.Int(nullable: false));
            AddColumn("dbo.InvoiceModels", "color", c => c.String());
            AddColumn("dbo.InvoiceModels", "font", c => c.String());
            AddColumn("dbo.InvoiceModels", "sendstyle", c => c.String());
            AddColumn("dbo.InvoiceModels", "personalmessage", c => c.String());
            AddColumn("dbo.InvoiceModels", "invoicestatus", c => c.String());
            AddColumn("dbo.InvoiceModels", "clientid", c => c.Int(nullable: false));
            CreateIndex("dbo.InvoiceModels", "clientid");
            AddForeignKey("dbo.InvoiceModels", "clientid", "dbo.ClientsModels", "id", cascadeDelete: false);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.InvoicePaymentModels", "invoiceid", "dbo.InvoiceModels");
            DropForeignKey("dbo.ItemsModels", "invoiceid", "dbo.InvoiceModels");
            DropForeignKey("dbo.InvoiceModels", "clientid", "dbo.ClientsModels");
            DropIndex("dbo.InvoicePaymentModels", new[] { "invoiceid" });
            DropIndex("dbo.ItemsModels", new[] { "invoiceid" });
            DropIndex("dbo.InvoiceModels", new[] { "clientid" });
            DropColumn("dbo.InvoiceModels", "clientid");
            DropColumn("dbo.InvoiceModels", "invoicestatus");
            DropColumn("dbo.InvoiceModels", "personalmessage");
            DropColumn("dbo.InvoiceModels", "sendstyle");
            DropColumn("dbo.InvoiceModels", "font");
            DropColumn("dbo.InvoiceModels", "color");
            DropColumn("dbo.InvoiceModels", "invoicestyle");
            DropColumn("dbo.InvoiceModels", "notes");
            DropColumn("dbo.InvoiceModels", "paymenttype");
            DropColumn("dbo.InvoiceModels", "deposit");
            DropColumn("dbo.InvoiceModels", "amountdue");
            DropColumn("dbo.InvoiceModels", "discount");
            DropColumn("dbo.InvoiceModels", "subtotal");
            DropTable("dbo.InvoicePaymentModels");
            DropTable("dbo.ItemsModels");
        }
    }
}
