namespace VencruApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class banksettingsmodel317102018 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.BankSettingsModels", "accountname", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.BankSettingsModels", "accountname");
        }
    }
}
