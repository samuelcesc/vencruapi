namespace VencruApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addedtypetoinvoicemodel512019 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.InvoiceModels", "invoicetype", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.InvoiceModels", "invoicetype");
        }
    }
}
