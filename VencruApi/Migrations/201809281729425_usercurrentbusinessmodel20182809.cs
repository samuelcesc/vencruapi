namespace VencruApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class usercurrentbusinessmodel20182809 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UserCurrentBusinessModels",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        BusinessId = c.Int(nullable: false),
                        UserId = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.UserCurrentBusinessModels");
        }
    }
}
