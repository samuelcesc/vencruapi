namespace VencruApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class costofgoods11102018 : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.ProductModels", "costofitem");

        }
        
        public override void Down()
        {
            AddColumn("dbo.ProductModels", "costofitem", c => c.Int(nullable: false));

        }
    }
}
