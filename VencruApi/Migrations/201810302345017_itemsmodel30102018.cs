namespace VencruApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class itemsmodel30102018 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.InvoiceModels", "image", c => c.String());
            AddColumn("dbo.InvoiceModels", "userid", c => c.String());
            AddColumn("dbo.InvoiceModels", "due_date", c => c.String());
            AddColumn("dbo.ItemsModels", "date_created", c => c.DateTime(nullable: false));
            AlterColumn("dbo.InvoicePaymentModels", "date_created", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.InvoicePaymentModels", "date_created", c => c.String());
            DropColumn("dbo.ItemsModels", "date_created");
            DropColumn("dbo.InvoiceModels", "due_date");
            DropColumn("dbo.InvoiceModels", "userid");
            DropColumn("dbo.InvoiceModels", "image");
        }
    }
}
