namespace VencruApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class planandfeebackmodel22102018 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.SubscriptionModels", "planid", "dbo.PlanModels");
            DropIndex("dbo.SubscriptionModels", new[] { "planid" });
            DropColumn("dbo.SubscriptionModels", "planid");

            DropPrimaryKey("dbo.SubscriptionModels");
            DropColumn("dbo.SubscriptionModels", "Id");

            DropPrimaryKey("dbo.PlanModels");
            DropColumn("dbo.PlanModels", "id");

            DropPrimaryKey("dbo.FeedbackModels");
            DropColumn("dbo.FeedbackModels", "Id");

            CreateTable(
                "dbo.ReviewsModels",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        fullname = c.String(nullable: false),
                        reviewcomment = c.String(nullable: false),
                        isapproved = c.Boolean(nullable: false),
                        date_created = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
            AddColumn("dbo.SubscriptionModels", "Id", c => c.Int(nullable: false, identity: true));
            AddColumn("dbo.SubscriptionModels", "planid", c => c.Int(nullable: false));
            AddColumn("dbo.PlanModels", "id", c => c.Int(nullable: false, identity: true));
            AddColumn("dbo.FeedbackModels", "id", c => c.Int(nullable: false, identity: true));
            AddPrimaryKey("dbo.SubscriptionModels", "Id");
            AddPrimaryKey("dbo.PlanModels", "id");
            AddPrimaryKey("dbo.FeedbackModels", "id");
            CreateIndex("dbo.SubscriptionModels", "planid");
            AddForeignKey("dbo.SubscriptionModels", "planid", "dbo.PlanModels", "id", cascadeDelete: true);
            DropColumn("dbo.BusinessModels", "image");
        }
        
        public override void Down()
        {
            AddColumn("dbo.BusinessModels", "image", c => c.String());
            DropForeignKey("dbo.SubscriptionModels", "planid", "dbo.PlanModels");
            DropIndex("dbo.SubscriptionModels", new[] { "planid" });
            DropPrimaryKey("dbo.FeedbackModels");
            DropPrimaryKey("dbo.PlanModels");
            DropPrimaryKey("dbo.SubscriptionModels");
            AlterColumn("dbo.FeedbackModels", "id", c => c.Guid(nullable: false));
            AlterColumn("dbo.PlanModels", "id", c => c.Guid(nullable: false));
            AlterColumn("dbo.SubscriptionModels", "planid", c => c.Guid(nullable: false));
            AlterColumn("dbo.SubscriptionModels", "Id", c => c.Guid(nullable: false));
            DropTable("dbo.ReviewsModels");
            AddPrimaryKey("dbo.FeedbackModels", "id");
            AddPrimaryKey("dbo.PlanModels", "id");
            AddPrimaryKey("dbo.SubscriptionModels", "Id");
            CreateIndex("dbo.SubscriptionModels", "planid");
            AddForeignKey("dbo.SubscriptionModels", "planid", "dbo.PlanModels", "id", cascadeDelete: true);
        }
    }
}
