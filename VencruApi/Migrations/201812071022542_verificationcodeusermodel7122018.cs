namespace VencruApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class verificationcodeusermodel7122018 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "verificationcode", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "verificationcode");
        }
    }
}
