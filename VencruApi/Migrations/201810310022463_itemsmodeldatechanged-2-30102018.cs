namespace VencruApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class itemsmodeldatechanged230102018 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.InvoiceModels", "userid", c => c.String(nullable: false));
            AlterColumn("dbo.InvoiceModels", "date_created", c => c.String());
            AlterColumn("dbo.ItemsModels", "date_created", c => c.String());
            AlterColumn("dbo.InvoicePaymentModels", "date_created", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.InvoicePaymentModels", "date_created", c => c.DateTime(nullable: false));
            AlterColumn("dbo.ItemsModels", "date_created", c => c.DateTime(nullable: false));
            AlterColumn("dbo.InvoiceModels", "date_created", c => c.DateTime(nullable: false));
            AlterColumn("dbo.InvoiceModels", "userid", c => c.String());
        }
    }
}
