namespace VencruApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class productlistmodels04102018 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.InvoiceModels", "date_created", c => c.DateTime(nullable: false));
            AddColumn("dbo.InvoiceModels", "utime", c => c.Int(nullable: false));
            //AddColumn("dbo.ProductListModels", "date_created", c => c.DateTime(nullable: false));
            AlterColumn("dbo.ExpenseModels", "Date_Created", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.ExpenseModels", "Date_Created", c => c.String());
            //DropColumn("dbo.ProductListModels", "date_created");
            DropColumn("dbo.InvoiceModels", "utime");
            DropColumn("dbo.InvoiceModels", "date_created");
        }
    }
}
