namespace VencruApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class utimeaddedtomodels04102018 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.BusinessModels", "utime", c => c.Int(nullable: false));
            AddColumn("dbo.ClientsModels", "utime", c => c.Int(nullable: false));
            AddColumn("dbo.ExpenseModels", "utime", c => c.Int(nullable: false));
            AddColumn("dbo.ProductListModels", "utime", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ProductListModels", "utime");
            DropColumn("dbo.ExpenseModels", "utime");
            DropColumn("dbo.ClientsModels", "utime");
            DropColumn("dbo.BusinessModels", "utime");
        }
    }
}
