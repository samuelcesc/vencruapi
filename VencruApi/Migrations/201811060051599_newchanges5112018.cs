namespace VencruApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class newchanges5112018 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ExpenseModels", "image", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.ExpenseModels", "image");
        }
    }
}
