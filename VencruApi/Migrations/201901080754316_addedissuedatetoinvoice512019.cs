namespace VencruApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addedissuedatetoinvoice512019 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.InvoiceModels", "issue_date", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.InvoiceModels", "issue_date");
        }
    }
}
