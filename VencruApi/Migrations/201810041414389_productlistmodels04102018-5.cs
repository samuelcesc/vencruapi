namespace VencruApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class productlistmodels041020185 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.BusinessModels", "date_created", c => c.DateTime(nullable: false));
            AlterColumn("dbo.ClientsModels", "date_created", c => c.DateTime(nullable: false));
            AlterColumn("dbo.ExpenseModels", "Date_Created", c => c.DateTime(nullable: false));
            //AlterColumn("dbo.ProductListModels", "date_created", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            //AlterColumn("dbo.ProductListModels", "date_created", c => c.String());
            AlterColumn("dbo.ExpenseModels", "Date_Created", c => c.String());
            AlterColumn("dbo.ClientsModels", "date_created", c => c.String());
            AlterColumn("dbo.BusinessModels", "date_created", c => c.String());
        }
    }
}
