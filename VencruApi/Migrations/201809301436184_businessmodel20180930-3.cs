namespace VencruApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class businessmodel201809303 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.BusinessModels", "firstname", c => c.String());
            AddColumn("dbo.BusinessModels", "lastname", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.BusinessModels", "lastname");
            DropColumn("dbo.BusinessModels", "firstname");
        }
    }
}
