namespace VencruApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class expensemodel405102018 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ExpenseModels", "business_Id", "dbo.BusinessModels");
            DropIndex("dbo.ExpenseModels", new[] { "business_Id" });
            DropColumn("dbo.ExpenseModels", "businessid");
            RenameColumn(table: "dbo.ExpenseModels", name: "business_Id", newName: "businessid");
            AlterColumn("dbo.ExpenseModels", "businessid", c => c.Int(nullable: false));
            AlterColumn("dbo.ExpenseModels", "businessid", c => c.Int(nullable: false));
            CreateIndex("dbo.ExpenseModels", "businessid");
            AddForeignKey("dbo.ExpenseModels", "businessid", "dbo.BusinessModels", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ExpenseModels", "businessid", "dbo.BusinessModels");
            DropIndex("dbo.ExpenseModels", new[] { "businessid" });
            AlterColumn("dbo.ExpenseModels", "businessid", c => c.Int());
            AlterColumn("dbo.ExpenseModels", "businessid", c => c.String());
            RenameColumn(table: "dbo.ExpenseModels", name: "businessid", newName: "business_Id");
            AddColumn("dbo.ExpenseModels", "businessid", c => c.String());
            CreateIndex("dbo.ExpenseModels", "business_Id");
            AddForeignKey("dbo.ExpenseModels", "business_Id", "dbo.BusinessModels", "Id");
        }
    }
}
