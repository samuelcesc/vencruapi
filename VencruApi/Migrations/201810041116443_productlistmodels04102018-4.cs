namespace VencruApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class productlistmodels041020184 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.ExpenseModels", "Date_Created", c => c.String());
            //AlterColumn("dbo.ProductListModels", "date_created", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.ProductListModels", "date_created", c => c.DateTime(nullable: false));
            //AlterColumn("dbo.ExpenseModels", "Date_Created", c => c.DateTime(nullable: false));
        }
    }
}
