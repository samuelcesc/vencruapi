namespace VencruApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class databaseupdate9112018 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PromoCodeModels",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        code = c.String(nullable: false),
                        discount = c.Int(nullable: false),
                        limit = c.Int(nullable: false),
                        date_created = c.String(),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.PromoCodeModelApplicationUsers",
                c => new
                    {
                        PromoCodeModel_id = c.Int(nullable: false),
                        ApplicationUser_Id = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.PromoCodeModel_id, t.ApplicationUser_Id })
                .ForeignKey("dbo.PromoCodeModels", t => t.PromoCodeModel_id, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.ApplicationUser_Id, cascadeDelete: true)
                .Index(t => t.PromoCodeModel_id)
                .Index(t => t.ApplicationUser_Id);
            
            AlterColumn("dbo.SubscriptionModels", "date_expire", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PromoCodeModelApplicationUsers", "ApplicationUser_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.PromoCodeModelApplicationUsers", "PromoCodeModel_id", "dbo.PromoCodeModels");
            DropIndex("dbo.PromoCodeModelApplicationUsers", new[] { "ApplicationUser_Id" });
            DropIndex("dbo.PromoCodeModelApplicationUsers", new[] { "PromoCodeModel_id" });
            AlterColumn("dbo.SubscriptionModels", "date_expire", c => c.String());
            DropTable("dbo.PromoCodeModelApplicationUsers");
            DropTable("dbo.PromoCodeModels");
        }
    }
}
