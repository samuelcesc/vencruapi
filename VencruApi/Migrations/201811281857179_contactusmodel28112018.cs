namespace VencruApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class contactusmodel28112018 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ContactUsModels",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        emailaddress = c.String(nullable: false),
                        subject = c.String(),
                        question = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.ContactUsModels");
        }
    }
}
