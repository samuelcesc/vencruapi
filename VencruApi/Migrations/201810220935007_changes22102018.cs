namespace VencruApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changes22102018 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.BusinessModels", "image", c => c.String());
            AddColumn("dbo.FeedbackModels", "emailaddress", c => c.String(nullable: false));
            AlterColumn("dbo.FeedbackModels", "details", c => c.String(nullable: false));
            AlterColumn("dbo.PlanModels", "name", c => c.String(nullable: false));
            AlterColumn("dbo.PlanModels", "plantype", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.PlanModels", "plantype", c => c.String());
            AlterColumn("dbo.PlanModels", "name", c => c.String());
            AlterColumn("dbo.FeedbackModels", "details", c => c.String());
            DropColumn("dbo.FeedbackModels", "emailaddress");
            DropColumn("dbo.BusinessModels", "image");
        }
    }
}
