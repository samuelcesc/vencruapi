namespace VencruApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class planandfeedbackmodel20102018 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.FeedbackModels",
                c => new
                    {
                        id = c.Guid(nullable: false),
                        netpromoterscore = c.Int(nullable: false),
                        details = c.String(),
                        date_created = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.PlanModels",
                c => new
                    {
                        id = c.Guid(nullable: false),
                        price = c.Int(nullable: false),
                        name = c.String(),
                        plantype = c.String(),
                        date_created = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.PlanModels");
            DropTable("dbo.FeedbackModels");
        }
    }
}
