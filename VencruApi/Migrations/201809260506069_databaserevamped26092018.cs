namespace VencruApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class databaserevamped26092018 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.BusinessModels",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        city = c.String(),
                        companyname = c.String(),
                        country = c.String(),
                        industry = c.String(),
                        isincorporated = c.Byte(nullable: false),
                        state = c.String(),
                        address = c.String(),
                        employeesize = c.String(),
                        currency = c.String(),
                        onlinepayment = c.Byte(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ExpenseModels",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ExpenseNumber = c.String(nullable: false),
                        Description = c.String(),
                        Category = c.String(),
                        Date_Created = c.String(),
                        Vendor = c.String(),
                        PaidWith = c.String(),
                        TotalAmount = c.String(),
                        BusinessId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.BusinessModels", t => t.BusinessId, cascadeDelete: true)
                .Index(t => t.BusinessId);
            
            CreateTable(
                "dbo.InvoiceModels",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(),
                        BusinessModel_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.BusinessModels", t => t.BusinessModel_Id)
                .Index(t => t.BusinessModel_Id);
            
            //CreateTable(
            //    "dbo.ProductListModels",
            //    c => new
            //        {
            //            Id = c.Int(nullable: false, identity: true),
            //            StockNumber = c.String(nullable: false),
            //            ProductName = c.String(nullable: false),
            //            Description = c.String(),
            //            UnitPrice = c.Int(nullable: false),
            //            BusinessId = c.Int(nullable: false),
            //        })
            //    .PrimaryKey(t => t.Id)
            //    .ForeignKey("dbo.BusinessModels", t => t.BusinessId, cascadeDelete: true)
            //    .Index(t => t.BusinessId);
            
            CreateTable(
                "dbo.ApplicationUserBusinessModels",
                c => new
                    {
                        ApplicationUser_Id = c.String(nullable: false, maxLength: 128),
                        BusinessModel_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.ApplicationUser_Id, t.BusinessModel_Id })
                .ForeignKey("dbo.AspNetUsers", t => t.ApplicationUser_Id, cascadeDelete: true)
                .ForeignKey("dbo.BusinessModels", t => t.BusinessModel_Id, cascadeDelete: true)
                .Index(t => t.ApplicationUser_Id)
                .Index(t => t.BusinessModel_Id);
            
            AddColumn("dbo.ClientsModels", "CompanyName", c => c.String(nullable: false));
            AddColumn("dbo.ClientsModels", "Country", c => c.String());
            AddColumn("dbo.ClientsModels", "BusinessId", c => c.Int(nullable: false));
            CreateIndex("dbo.ClientsModels", "BusinessId");
            AddForeignKey("dbo.ClientsModels", "BusinessId", "dbo.BusinessModels", "Id", cascadeDelete: true);
            DropColumn("dbo.ClientsModels", "Company");
            DropColumn("dbo.ClientsModels", "Companystate");
            DropColumn("dbo.ClientsModels", "Userid");
            DropColumn("dbo.ClientsModels", "Zipcode");
            DropColumn("dbo.AspNetUsers", "city");
            DropColumn("dbo.AspNetUsers", "companyname");
            DropColumn("dbo.AspNetUsers", "country");
            DropColumn("dbo.AspNetUsers", "industry");
            DropColumn("dbo.AspNetUsers", "isincorporated");
            DropColumn("dbo.AspNetUsers", "state");
            DropColumn("dbo.AspNetUsers", "address");
            DropColumn("dbo.AspNetUsers", "employeesize");
            DropColumn("dbo.AspNetUsers", "currency");
            DropColumn("dbo.AspNetUsers", "onlinepayment");
        }
        
        public override void Down()
        {
            AddColumn("dbo.AspNetUsers", "onlinepayment", c => c.Int(nullable: false));
            AddColumn("dbo.AspNetUsers", "currency", c => c.String());
            AddColumn("dbo.AspNetUsers", "employeesize", c => c.String());
            AddColumn("dbo.AspNetUsers", "address", c => c.String());
            AddColumn("dbo.AspNetUsers", "state", c => c.String());
            AddColumn("dbo.AspNetUsers", "isincorporated", c => c.Int(nullable: false));
            AddColumn("dbo.AspNetUsers", "industry", c => c.String());
            AddColumn("dbo.AspNetUsers", "country", c => c.String());
            AddColumn("dbo.AspNetUsers", "companyname", c => c.String());
            AddColumn("dbo.AspNetUsers", "city", c => c.String());
            AddColumn("dbo.ClientsModels", "Zipcode", c => c.String());
            AddColumn("dbo.ClientsModels", "Userid", c => c.String(nullable: false));
            AddColumn("dbo.ClientsModels", "Companystate", c => c.String());
            AddColumn("dbo.ClientsModels", "Company", c => c.String(nullable: false));
            DropForeignKey("dbo.ApplicationUserBusinessModels", "BusinessModel_Id", "dbo.BusinessModels");
            DropForeignKey("dbo.ApplicationUserBusinessModels", "ApplicationUser_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.ProductListModels", "BusinessId", "dbo.BusinessModels");
            DropForeignKey("dbo.InvoiceModels", "BusinessModel_Id", "dbo.BusinessModels");
            DropForeignKey("dbo.ExpenseModels", "BusinessId", "dbo.BusinessModels");
            DropForeignKey("dbo.ClientsModels", "BusinessId", "dbo.BusinessModels");
            DropIndex("dbo.ApplicationUserBusinessModels", new[] { "BusinessModel_Id" });
            DropIndex("dbo.ApplicationUserBusinessModels", new[] { "ApplicationUser_Id" });
            DropIndex("dbo.ProductListModels", new[] { "BusinessId" });
            DropIndex("dbo.InvoiceModels", new[] { "BusinessModel_Id" });
            DropIndex("dbo.ExpenseModels", new[] { "BusinessId" });
            DropIndex("dbo.ClientsModels", new[] { "BusinessId" });
            DropColumn("dbo.ClientsModels", "BusinessId");
            DropColumn("dbo.ClientsModels", "Country");
            DropColumn("dbo.ClientsModels", "CompanyName");
            DropTable("dbo.ApplicationUserBusinessModels");
            //DropTable("dbo.ProductListModels");
            DropTable("dbo.InvoiceModels");
            DropTable("dbo.ExpenseModels");
            DropTable("dbo.BusinessModels");
        }
    }
}
