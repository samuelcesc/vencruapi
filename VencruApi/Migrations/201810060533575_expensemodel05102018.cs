namespace VencruApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class expensemodel05102018 : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.ExpenseModels", new[] { "BusinessId" });
            AddColumn("dbo.ExpenseModels", "receiptimageurl", c => c.String());
            AddColumn("dbo.ExpenseModels", "userid", c => c.String(nullable: false));
            AlterColumn("dbo.ExpenseModels", "date_created", c => c.DateTime(nullable: false));
            AlterColumn("dbo.ExpenseModels", "totalamount", c => c.Int(nullable: false));
            CreateIndex("dbo.ExpenseModels", "businessid");
        }
        
        public override void Down()
        {
            DropIndex("dbo.ExpenseModels", new[] { "businessid" });
            AlterColumn("dbo.ExpenseModels", "totalamount", c => c.String());
            AlterColumn("dbo.ExpenseModels", "date_created", c => c.String());
            DropColumn("dbo.ExpenseModels", "userid");
            DropColumn("dbo.ExpenseModels", "receiptimageurl");
            CreateIndex("dbo.ExpenseModels", "BusinessId");
        }
    }
}
