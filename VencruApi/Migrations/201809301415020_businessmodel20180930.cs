namespace VencruApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class businessmodel20180930 : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.BusinessModels", "firstname");
            DropColumn("dbo.BusinessModels", "lastname");
        }
        
        public override void Down()
        {
            AddColumn("dbo.BusinessModels", "lastname", c => c.String());
            AddColumn("dbo.BusinessModels", "firstname", c => c.String());
        }
    }
}
