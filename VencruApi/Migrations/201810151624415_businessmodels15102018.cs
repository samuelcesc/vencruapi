namespace VencruApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class businessmodels15102018 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.BusinessModels", "facebookurl", c => c.String());
            AddColumn("dbo.BusinessModels", "twitterurl", c => c.String());
            AddColumn("dbo.BusinessModels", "linkdinurl", c => c.String());
            AddColumn("dbo.BusinessModels", "instagramurl", c => c.String());
            AddColumn("dbo.BusinessModels", "logourl", c => c.String());
            AddColumn("dbo.BusinessModels", "service", c => c.String());
            DropColumn("dbo.BusinessModels", "facebook");
            DropColumn("dbo.BusinessModels", "twitter");
            DropColumn("dbo.BusinessModels", "linkdin");
            DropColumn("dbo.BusinessModels", "imagekey");
            DropColumn("dbo.BusinessModels", "services");
        }
        
        public override void Down()
        {
            AddColumn("dbo.BusinessModels", "services", c => c.String());
            AddColumn("dbo.BusinessModels", "imagekey", c => c.String());
            AddColumn("dbo.BusinessModels", "linkdin", c => c.String());
            AddColumn("dbo.BusinessModels", "twitter", c => c.String());
            AddColumn("dbo.BusinessModels", "facebook", c => c.String());
            DropColumn("dbo.BusinessModels", "service");
            DropColumn("dbo.BusinessModels", "logourl");
            DropColumn("dbo.BusinessModels", "instagramurl");
            DropColumn("dbo.BusinessModels", "linkdinurl");
            DropColumn("dbo.BusinessModels", "twitterurl");
            DropColumn("dbo.BusinessModels", "facebookurl");
        }
    }
}
