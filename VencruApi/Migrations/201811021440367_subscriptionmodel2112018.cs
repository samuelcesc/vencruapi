namespace VencruApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class subscriptionmodel2112018 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.SubscriptionModels", "referencenumber", c => c.String());
            AddColumn("dbo.SubscriptionModels", "date_expire", c => c.String());
            AddColumn("dbo.SubscriptionModels", "istrial", c => c.Boolean(nullable: false));
            AddColumn("dbo.SubscriptionModels", "trialdays", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.SubscriptionModels", "trialdays");
            DropColumn("dbo.SubscriptionModels", "istrial");
            DropColumn("dbo.SubscriptionModels", "date_expire");
            DropColumn("dbo.SubscriptionModels", "referencenumber");
        }
    }
}
