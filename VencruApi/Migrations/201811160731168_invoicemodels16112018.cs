namespace VencruApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class invoicemodels16112018 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ProductModelInvoiceModels",
                c => new
                    {
                        ProductModel_id = c.Int(nullable: false),
                        InvoiceModel_id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.ProductModel_id, t.InvoiceModel_id })
                .ForeignKey("dbo.ProductModels", t => t.ProductModel_id, cascadeDelete: false)
                .ForeignKey("dbo.InvoiceModels", t => t.InvoiceModel_id, cascadeDelete: false)
                .Index(t => t.ProductModel_id)
                .Index(t => t.InvoiceModel_id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ProductModelInvoiceModels", "InvoiceModel_id", "dbo.InvoiceModels");
            DropForeignKey("dbo.ProductModelInvoiceModels", "ProductModel_id", "dbo.ProductModels");
            DropIndex("dbo.ProductModelInvoiceModels", new[] { "InvoiceModel_id" });
            DropIndex("dbo.ProductModelInvoiceModels", new[] { "ProductModel_id" });
            DropTable("dbo.ProductModelInvoiceModels");
        }
    }
}
