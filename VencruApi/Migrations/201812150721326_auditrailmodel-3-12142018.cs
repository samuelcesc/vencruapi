namespace VencruApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class auditrailmodel312142018 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AuditTrailModels", "businessid", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.AuditTrailModels", "businessid");
        }
    }
}
