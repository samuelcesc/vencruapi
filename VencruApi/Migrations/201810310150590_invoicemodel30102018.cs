namespace VencruApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class invoicemodel30102018 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.InvoiceModels", "invoicenumber", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.InvoiceModels", "invoicenumber");
        }
    }
}
