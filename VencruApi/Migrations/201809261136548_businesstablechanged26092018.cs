namespace VencruApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class businesstablechanged26092018 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.BusinessModels", "firstname", c => c.String());
            AddColumn("dbo.BusinessModels", "lastname", c => c.String());
            DropColumn("dbo.AspNetUsers", "firstname");
            DropColumn("dbo.AspNetUsers", "lastname");
        }
        
        public override void Down()
        {
            AddColumn("dbo.AspNetUsers", "lastname", c => c.String());
            AddColumn("dbo.AspNetUsers", "firstname", c => c.String());
            DropColumn("dbo.BusinessModels", "lastname");
            DropColumn("dbo.BusinessModels", "firstname");
        }
    }
}
