namespace VencruApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class rolesadded220102018 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.BusinessModels", "userid", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.BusinessModels", "userid", c => c.String());
        }
    }
}
