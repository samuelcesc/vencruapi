namespace VencruApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class banksettingsmodel17102018 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.BankSettingsModels",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        bankname = c.String(nullable: false),
                        accountnumber = c.Int(nullable: false),
                        accounttype = c.String(),
                        isdefault = c.Byte(nullable: false),
                        userid = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.userid)
                .Index(t => t.userid);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.BankSettingsModels", "userid", "dbo.AspNetUsers");
            DropIndex("dbo.BankSettingsModels", new[] { "userid" });
            DropTable("dbo.BankSettingsModels");
        }
    }
}
