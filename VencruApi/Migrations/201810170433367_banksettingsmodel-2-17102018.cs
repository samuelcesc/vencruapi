namespace VencruApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class banksettingsmodel217102018 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.BankSettingsModels", "userid", "dbo.AspNetUsers");
            DropIndex("dbo.BankSettingsModels", new[] { "userid" });
            AddColumn("dbo.ExpenseModels", "expensedate", c => c.String());
            AddColumn("dbo.BankSettingsModels", "date_created", c => c.String());
            AddColumn("dbo.BankSettingsModels", "businessid", c => c.Int(nullable: false));
            AlterColumn("dbo.BankSettingsModels", "userid", c => c.String());
            CreateIndex("dbo.BankSettingsModels", "businessid");
            AddForeignKey("dbo.BankSettingsModels", "businessid", "dbo.BusinessModels", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.BankSettingsModels", "businessid", "dbo.BusinessModels");
            DropIndex("dbo.BankSettingsModels", new[] { "businessid" });
            AlterColumn("dbo.BankSettingsModels", "userid", c => c.String(maxLength: 128));
            DropColumn("dbo.BankSettingsModels", "businessid");
            DropColumn("dbo.BankSettingsModels", "date_created");
            DropColumn("dbo.ExpenseModels", "expensedate");
            CreateIndex("dbo.BankSettingsModels", "userid");
            AddForeignKey("dbo.BankSettingsModels", "userid", "dbo.AspNetUsers", "Id");
        }
    }
}
