namespace VencruApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class teammembermodel23102018 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.TeamMemberModels",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        memberemail = c.String(),
                        firstname = c.String(),
                        lastname = c.String(),
                        roles = c.String(),
                        status = c.Boolean(nullable: false),
                        businessid = c.Int(nullable: false),
                        userid = c.String(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.BusinessModels", t => t.businessid, cascadeDelete: true)
                .Index(t => t.businessid);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.TeamMemberModels", "businessid", "dbo.BusinessModels");
            DropIndex("dbo.TeamMemberModels", new[] { "businessid" });
            DropTable("dbo.TeamMemberModels");
        }
    }
}
