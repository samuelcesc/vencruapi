namespace VencruApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class userstable30092018 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "firstname", c => c.String());
            AddColumn("dbo.AspNetUsers", "lastname", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "lastname");
            DropColumn("dbo.AspNetUsers", "firstname");
        }
    }
}
