namespace VencruApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class subscriptionmodel222102018 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.SubscriptionModels", "plan_id", "dbo.PlanModels");
            DropIndex("dbo.SubscriptionModels", new[] { "plan_id" });
            DropColumn("dbo.SubscriptionModels", "planid");
            RenameColumn(table: "dbo.SubscriptionModels", name: "plan_id", newName: "planid");
            AlterColumn("dbo.SubscriptionModels", "planid", c => c.Guid(nullable: false));
            AlterColumn("dbo.SubscriptionModels", "planid", c => c.Guid(nullable: false));
            CreateIndex("dbo.SubscriptionModels", "planid");
            AddForeignKey("dbo.SubscriptionModels", "planid", "dbo.PlanModels", "id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SubscriptionModels", "planid", "dbo.PlanModels");
            DropIndex("dbo.SubscriptionModels", new[] { "planid" });
            AlterColumn("dbo.SubscriptionModels", "planid", c => c.Guid());
            AlterColumn("dbo.SubscriptionModels", "planid", c => c.String());
            RenameColumn(table: "dbo.SubscriptionModels", name: "planid", newName: "plan_id");
            AddColumn("dbo.SubscriptionModels", "planid", c => c.String());
            CreateIndex("dbo.SubscriptionModels", "plan_id");
            AddForeignKey("dbo.SubscriptionModels", "plan_id", "dbo.PlanModels", "id");
        }
    }
}
