namespace VencruApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class businessmodels14112018 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.BusinessModels", "monthlytarget", c => c.Int(nullable: false));
            AddColumn("dbo.BusinessModels", "estimatedmonthlyrevenue", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.BusinessModels", "estimatedmonthlyrevenue");
            DropColumn("dbo.BusinessModels", "monthlytarget");
        }
    }
}
