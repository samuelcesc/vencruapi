namespace VencruApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class banksettingsmodel517102018 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.BankSettingsModels", "isdefault", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.BankSettingsModels", "isdefault", c => c.Byte(nullable: false));
        }
    }
}
