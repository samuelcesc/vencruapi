namespace VencruApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class productlistmodels0410201810 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ProductModels",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        stocknumber = c.String(nullable: false),
                        productname = c.String(nullable: false),
                        description = c.String(),
                        unitprice = c.Int(nullable: false),
                        utime = c.Int(nullable: false),
                        userid = c.String(nullable: false),
                        date_created = c.String(),
                        isdeleted = c.Boolean(nullable: false),
                        businessid = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.BusinessModels", t => t.businessid, cascadeDelete: true)
                .Index(t => t.businessid);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ProductModels", "businessid", "dbo.BusinessModels");
            DropIndex("dbo.ProductModels", new[] { "businessid" });
            DropTable("dbo.ProductModels");
        }
    }
}
