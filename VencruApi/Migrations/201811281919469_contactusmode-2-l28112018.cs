namespace VencruApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class contactusmode2l28112018 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ContactUsModels", "date_added", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.ContactUsModels", "date_added");
        }
    }
}
