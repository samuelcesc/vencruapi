namespace VencruApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class expensemode2l05102018 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ExpenseModels", "businessid", "dbo.BusinessModels");
            DropIndex("dbo.ExpenseModels", new[] { "businessid" });
            AddColumn("dbo.ExpenseModels", "business_Id", c => c.Int());
            AlterColumn("dbo.ExpenseModels", "date_created", c => c.String());
            AlterColumn("dbo.ExpenseModels", "businessid", c => c.String());
            CreateIndex("dbo.ExpenseModels", "business_Id");
            AddForeignKey("dbo.ExpenseModels", "business_Id", "dbo.BusinessModels", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ExpenseModels", "business_Id", "dbo.BusinessModels");
            DropIndex("dbo.ExpenseModels", new[] { "business_Id" });
            AlterColumn("dbo.ExpenseModels", "businessid", c => c.Int(nullable: false));
            AlterColumn("dbo.ExpenseModels", "date_created", c => c.DateTime(nullable: false));
            DropColumn("dbo.ExpenseModels", "business_Id");
            CreateIndex("dbo.ExpenseModels", "businessid");
            AddForeignKey("dbo.ExpenseModels", "businessid", "dbo.BusinessModels", "Id", cascadeDelete: true);
        }
    }
}
