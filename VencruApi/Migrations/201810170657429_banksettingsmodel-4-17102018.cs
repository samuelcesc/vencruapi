namespace VencruApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class banksettingsmodel417102018 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.BankSettingsModels", "accountnumber", c => c.String(nullable: false, maxLength: 10));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.BankSettingsModels", "accountnumber", c => c.Int(nullable: false));
        }
    }
}
