namespace VencruApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class businesstablechanged260920185 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.BusinessModels", "userid", c => c.String());
            DropColumn("dbo.BusinessModels", "UsersId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.BusinessModels", "UsersId", c => c.String());
            DropColumn("dbo.BusinessModels", "userid");
        }
    }
}
