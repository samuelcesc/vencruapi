namespace VencruApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class requireddeposite1112018 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.InvoiceModels", "isdeleted", c => c.Boolean(nullable: false));
            AddColumn("dbo.InvoiceModels", "requireddeposit", c => c.Int(nullable: false));
            AddColumn("dbo.InvoicePaymentModels", "userid", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.InvoicePaymentModels", "userid");
            DropColumn("dbo.InvoiceModels", "requireddeposit");
            DropColumn("dbo.InvoiceModels", "isdeleted");
        }
    }
}
