namespace VencruApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addedemailaddresstoinvoice512019 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.InvoiceModels", "email", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.InvoiceModels", "email");
        }
    }
}
