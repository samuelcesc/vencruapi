namespace VencruApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class profilesettings15102018 : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.ProductListModels", new[] { "businessid" });
            AddColumn("dbo.BusinessModels", "domain", c => c.String());
            AddColumn("dbo.BusinessModels", "facebook", c => c.String());
            AddColumn("dbo.BusinessModels", "twitter", c => c.String());
            AddColumn("dbo.BusinessModels", "linkdin", c => c.String());
            AddColumn("dbo.BusinessModels", "imagekey", c => c.String());
            AddColumn("dbo.BusinessModels", "services", c => c.String());
            AddColumn("dbo.BusinessModels", "tax", c => c.Int(nullable: false));
            AddColumn("dbo.BusinessModels", "vatnumber", c => c.String());
            AddColumn("dbo.AspNetUsers", "imagekey", c => c.String());
            AddColumn("dbo.AspNetUsers", "date_created", c => c.String());
            AddColumn("dbo.AspNetUsers", "row_version", c => c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"));
            //AddColumn("dbo.ProductModels", "costofitem", c => c.Int(nullable: false));
            //DropTable("dbo.ProductListModels");
        }
        
        public override void Down()
        {
            //CreateTable(
            //    "dbo.ProductListModels",
            //    c => new
            //        {
            //            id = c.Int(nullable: false, identity: true),
            //            stocknumber = c.String(nullable: false),
            //            productname = c.String(nullable: false),
            //            description = c.String(),
            //            unitprice = c.Int(nullable: false),
            //            costofitem = c.Int(nullable: false),
            //            utime = c.Int(nullable: false),
            //            userid = c.String(nullable: false),
            //            date_created = c.String(),
            //            isdeleted = c.Boolean(nullable: false),
            //            businessid = c.Int(nullable: false),
            //        })
            //    .PrimaryKey(t => t.id);
            
            DropColumn("dbo.ProductModels", "costofitem");
            DropColumn("dbo.AspNetUsers", "row_version");
            DropColumn("dbo.AspNetUsers", "date_created");
            DropColumn("dbo.AspNetUsers", "imagekey");
            DropColumn("dbo.BusinessModels", "vatnumber");
            DropColumn("dbo.BusinessModels", "tax");
            DropColumn("dbo.BusinessModels", "services");
            DropColumn("dbo.BusinessModels", "imagekey");
            DropColumn("dbo.BusinessModels", "linkdin");
            DropColumn("dbo.BusinessModels", "twitter");
            DropColumn("dbo.BusinessModels", "facebook");
            DropColumn("dbo.BusinessModels", "domain");
            //CreateIndex("dbo.ProductListModels", "businessid");
        }
    }
}
