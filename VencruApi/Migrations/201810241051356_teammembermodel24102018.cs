namespace VencruApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class teammembermodel24102018 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TeamMemberModels", "date_created", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.TeamMemberModels", "date_created");
        }
    }
}
