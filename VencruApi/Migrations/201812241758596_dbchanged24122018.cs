namespace VencruApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class dbchanged24122018 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.ClientsModels", "firstname", c => c.String(nullable: false));
            AlterColumn("dbo.ClientsModels", "companyname", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.ClientsModels", "companyname", c => c.String(nullable: false));
            AlterColumn("dbo.ClientsModels", "firstname", c => c.String());
        }
    }
}
