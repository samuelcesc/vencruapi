namespace VencruApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class clientnote16122018 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ClientsModels", "note", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.ClientsModels", "note");
        }
    }
}
