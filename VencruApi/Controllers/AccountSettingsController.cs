﻿using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon.S3.Transfer;
using PayStack.Net;
using SharpRaven;
using SharpRaven.Data;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using VencruApi.Models;
using VencruApi.Models.Classes;
using VencruApi.Models.DalClasses;
using VencruApi.Models.ViewModels;

namespace VencruApi.Controllers
{
    [Authorize]
    //[AllowAnonymous]
    [RoutePrefix("api/AccountSettings")]
    [EnableCors("*", "*", "*")]
    public class AccountSettingsController : ApiController
    {
        //declare aws region end point
        private static readonly RegionEndpoint bucketRegion = RegionEndpoint.USEast1;

        //declare instance of Interface amazons3 client
        private static IAmazonS3 s3Client;

        //private instantiation of useraccountdal
        private UserAccountDal _userAccountDal;

        private PayStackDal _paystackDal;


        //controller contsructor for invoice module
        public AccountSettingsController()
        {

        }

        //pass invoicedal to invoice constructors
        public AccountSettingsController(UserAccountDal userAccountDal, PayStackDal payStackDal)
        {
            userAccountDal = UserAccountDal;
            payStackDal = PayStackDal;
        }


        public UserAccountDal UserAccountDal
        {
            get
            {
                return UserAccountDal = new UserAccountDal();
            }
            private set
            {
                _userAccountDal = value;
            }
        }

        public PayStackDal PayStackDal
        {
            get
            {
                return PayStackDal = new PayStackDal();
            }
            private set
            {
                _paystackDal = value;
            }
        }

        //update profile
        [HttpPost]
        [Route("Updateprofile")]
        public async Task<IHttpActionResult> UpdateprofileAsync(UpdateprofileModel model)
        {
            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            var getuser = new UserInfoViewModel();

            try
            {
                if (!string.IsNullOrEmpty(model.image))
                {
                    string root = HttpContext.Current.Server.MapPath("~/Profilepictures");

                    //var provider = new MultipartFormDataStreamProvider(root);

                    //await Request.Content.ReadAsMultipartAsync(provider);

                    var base64file = Convert.FromBase64String(model.image);

                    string filepath = Path.Combine(root, $"{model.userid}.jpg");

                    if (model.profileimageurl == $"{model.userid}.jpg")
                    {
                        File.Delete(filepath);

                    }
                    else
                    {
                        if (base64file.Length > 0)
                        {
                            using (var stream = new FileStream(filepath, FileMode.Create))
                            {
                                stream.Write(base64file, 0, base64file.Length);
                                stream.Flush();
                            }
                        }
                        s3Client = new AmazonS3Client(bucketRegion);

                        var fileTransferUtility = new TransferUtility(s3Client);

                        //model.image.Headers.ContentDisposition.FileName = model.image.Headers.ContentDisposition.FileName.Replace("\"", "");

                        var fileTransferUtilityRequest = new TransferUtilityUploadRequest
                        {
                            BucketName = $"{UtilitiesClass.profileImageBucketname}",
                            FilePath = filepath,
                            StorageClass = S3StorageClass.StandardInfrequentAccess,
                            //PartSize = 6291456, // 6 MB.
                            Key = $"{model.userid}.jpg",
                            CannedACL = S3CannedACL.PublicRead,
                            ContentType = "/jpg",
                        };

                        model.profileimageurl = $"{model.userid}.jpg";

                        await fileTransferUtility.UploadAsync(fileTransferUtilityRequest);

                        File.Delete(filepath);
                    }
                }
                else
                { }
                var issuucessful = await UserAccountDal.UpdateuserProfile(model);

                if (issuucessful == true)
                {
                    getuser = UserAccountDal.GetUser(model.userid);
                }
                else
                {
                    return BadRequest("invalid user profile");
                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return Ok(getuser);
        }

        [HttpPost]
        [Route("UpdateMobileProfile")]
        public async Task<IHttpActionResult> UpdateMobileProfileAsync(updateprofilemobile model)
        {
            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            var getuser = new UserInfoViewModel();

            IHttpActionResult response = BadRequest("invalid request");

            try
            {
                if (!ModelState.IsValid)
                {
                    response = BadRequest("invalid profile");
                }
                else
                {
                    var isSuccessful = await UserAccountDal.UpdateUserProfileMobile(model);

                    if (isSuccessful == true)
                    {
                        getuser = UserAccountDal.GetUser(model.userid);
                        response = Ok(getuser);

                    }

                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));

            }

            return response;
        }

        [HttpPost]
        [Route("UpdateProfileImage")]
        public async Task<IHttpActionResult> UpdateMobileProfileImageAsync(ProfileImageModel model)
        {
            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            var getuser = new UserInfoViewModel();

            IHttpActionResult response = BadRequest("invalid request");

            try
            {
                if (!ModelState.IsValid)
                {
                    response = BadRequest("invalid profile");
                }
                else
                {
                    if (UtilitiesClass.IsBase64(model.image))
                    {
                        var base64file = Convert.FromBase64String(model.image);

                        s3Client = new AmazonS3Client(bucketRegion);

                        var fileTransferUtility = new PutObjectRequest()
                        {
                            BucketName = $"{UtilitiesClass.profileImageBucketname}",
                            CannedACL = S3CannedACL.PublicRead,
                            Key = $"{model.userid}.jpg",
                        };

                        using (var ms = new MemoryStream(base64file))
                        {
                            fileTransferUtility.InputStream = ms;
                            await s3Client.PutObjectAsync(fileTransferUtility);
                        }

                        model.image = $"{model.userid}.jpg";

                        var prrofileimageupdate = await UserAccountDal.UpdateProfileImage(model);

                        if (!string.IsNullOrEmpty(prrofileimageupdate))
                        {
                            getuser = UserAccountDal.GetUser(model.userid);

                            response = Ok(getuser);
                        }
                        else
                        {
                            response = BadRequest("cannot update user profile image");
                        }
                    }
                    else if (model.image == $"{model.userid}.jpg")
                    {

                        var responseContent = new Dictionary<string, string>()
                        {
                            { "message","profile image already exists" }
                        };

                        response = Ok(responseContent);
                    }
                    else
                    {

                        response = BadRequest("cannot update user profile image");
                    }
                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return response;
        }


        [HttpGet]
        [Route("GetBanks")]
        public IHttpActionResult GetBank()
        {
            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            var responseContent = new BankNameListViewModel();

            try
            {
                responseContent.banks.Add(new BankNameViewModel { bankname = "Access Bank" });
                responseContent.banks.Add(new BankNameViewModel { bankname = "First Bank" });
                responseContent.banks.Add(new BankNameViewModel { bankname = "GT Bank" });
                responseContent.banks.Add(new BankNameViewModel { bankname = "Wema Bank" });

            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return Ok(responseContent);
        }


        //add bank
        [Route("AddBank")]
        [HttpPost]
        public async Task<IHttpActionResult> AddBankAsync(BankSettingsModel model)
        {
            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            var bankSettingsViewModel = new BankSettingsViewModel();

            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest("invalid bank data");
                }
                else
                {
                    var issuccessful = await UserAccountDal.AddBank(model);

                    if (string.IsNullOrEmpty(issuccessful))
                    {
                        return BadRequest("operation failed");
                    }
                    else
                    {
                        bankSettingsViewModel = UserAccountDal.GetBank(model.businessid, Convert.ToInt32(issuccessful), model.userid);
                    }

                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return Ok(bankSettingsViewModel);

        }

        //get bank
        [HttpGet]
        [Route("GetBank")]
        public IHttpActionResult GetBank(int? businessid, int? bankid, string userid)
        {
            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            var bankSettingsViewModel = new BankSettingsViewModel();

            try
            {
                if (!businessid.HasValue || !bankid.HasValue)
                {
                    return BadRequest("invalid bank or business details");
                }
                else
                {
                    bankSettingsViewModel = UserAccountDal.GetBank(businessid, bankid, userid);
                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return Ok(bankSettingsViewModel);

        }

        //get all banks
        [HttpGet]
        [Route("GetallBanks")]
        public IHttpActionResult GetAllBanks(int? businessid, string userid)
        {
            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            var bankListViewModel = new BankListViewModel();

            try
            {
                if (!businessid.HasValue)
                {
                    return BadRequest("invalid bank or business details");
                }
                else
                {
                    bankListViewModel = UserAccountDal.GetAllBanks(businessid, userid);
                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return Ok(bankListViewModel);

        }

        //update bank
        [HttpPost]
        [Route("Updatebank")]
       public async Task<IHttpActionResult> UpdatBankAsync(BankSettingsModel model)
        {
            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            var bankSettingsViewModel = new BankSettingsViewModel();

            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest("invalid bank or business details");
                }
                else
                {
                    var issuccessful = await UserAccountDal.UpdateBank(model);
                    if (issuccessful == true)
                    {
                        bankSettingsViewModel = UserAccountDal.GetBank(model.businessid, model.id, model.userid);
                    }
                    else
                    {
                        return BadRequest("operation failed");
                    }
                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return Ok(bankSettingsViewModel);

        }

        //set default bank
        [HttpPost]
        [Route("SetDefaultBank")]
        public async Task<IHttpActionResult> SetDefaultBankAsync(BankOperationModel model)
        {
            var bankSettingsViewModel = new BankSettingsViewModel();

            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");


            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest("invalid bank or business details");
                }
                else
                {
                    var issuccessful = await UserAccountDal.SetDefaultBank(model);
                    if (issuccessful == true)
                    {
                        bankSettingsViewModel = UserAccountDal.GetBank(model.businessid, model.bankid, model.userid);
                    }
                    else
                    {
                        return BadRequest("operation failed");
                    }
                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return Ok(bankSettingsViewModel);

        }

        //delete bank
        [HttpPost]
        [Route("DeleteBank")]
        public async Task<IHttpActionResult> DeleteBankAsync(BankOperationModel model)
        {

            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            var bankListViewModel = new BankListViewModel();

            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest("invalid bank or business details");
                }
                else
                {
                    var issuccessful = await UserAccountDal.DeleteBank(model);
                    if (issuccessful == true)
                    {
                        bankListViewModel = UserAccountDal.GetAllBanks(model.businessid, model.userid);
                    }
                    else
                    {
                        return BadRequest("operation failed");
                    }
                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return Ok(bankListViewModel);

        }


        [HttpPost]
        [Route("AddTargets")]
        public async Task<IHttpActionResult> AddTargetsAsyn(TargetsModel model)
        {
            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            var business = new BusinessModel();

            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest("invalid details");
                }
                else
                {
                    using (var dbContext = new ApplicationDbContext())
                    {
                        business = await dbContext.BusinessModels.FindAsync(model.businessid);

                        if (business == null)
                        {
                            return BadRequest("business not found");

                        }
                        else
                        {
                            business.monthlytarget = model.monthlytarget;

                            business.estimatedmonthlyrevenue = model.estimatedmonthlytarget;

                            await dbContext.SaveChangesAsync();
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return Ok(business);

        }

        [HttpPost]
        [Route("ConnectPayStack")]
        public IHttpActionResult ConnectPayStackAsync(ConnectPayStackModel model)
        {
            if (ModelState.IsValid)
            {
                var response = PayStackDal.CreateSubAccount(model);

                if (response.Status == true)
                {
                    AuditTrailDal.CreateAuditTrail(AuditActionType.Create, model.businessid, new SubAccountCreateResponse(), response, model.userid, model.businessid);

                    return Ok(response.Data);

                }
                else
                {
                    return BadRequest("unable to connect");
                }
            }
            else
            {
                return BadRequest(ModelState);
            }

        }

    }
}
