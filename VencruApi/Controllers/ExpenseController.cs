﻿using Amazon;
using Amazon.S3;
using Amazon.S3.Transfer;
using SharpRaven;
using SharpRaven.Data;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using VencruApi.Models;
using VencruApi.Models.Classes;
using VencruApi.Models.DalClasses;
using VencruApi.Models.ViewModels;

namespace VencruApi.Controllers
{
    [AllowAnonymous]
   // [Authorize]
    [EnableCors("*", "*", "*")]
    [RoutePrefix("api/Expense")]
    public class ExpenseController : ApiController
    {

        //declare aws region end point
        private static readonly RegionEndpoint bucketRegion = RegionEndpoint.USEast1;

        //declare instance of Interface amazons3 client
        private static IAmazonS3 s3Client;

        private ExpenseDal _expenseDal;

        public ExpenseDal ExpenseDal
        {
            get
            {
                return ExpenseDal = new ExpenseDal();
            }
            private set
            {
                _expenseDal = value;
            }
        }

        public ExpenseController()
        {
        }

        public ExpenseController(ExpenseDal expenseDal)
        {
            ExpenseDal = expenseDal;
        }

        //add expense
        [HttpPost]
        [Route("AddExpense")]
        public async Task<IHttpActionResult> AddExpenseAsync(ExpenseModel expensemodel)
        {

            var expenseViewModel = new ExpenseViewModel();
            
            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            try
            {

                if (!string.IsNullOrEmpty(expensemodel.image))
                {
                    
                    string root = HttpContext.Current.Server.MapPath("~/Receipts");
                                      

                    var base64file = Convert.FromBase64String(expensemodel.image);

                    string filepath = Path.Combine(root, $"{expensemodel.userid}{expensemodel.businessid}{expensemodel.expensenumber}.jpg");

                    if (base64file.Length > 0)
                    {
                        using (var stream = new FileStream(filepath, FileMode.Create))
                        {
                            stream.Write(base64file, 0, base64file.Length);
                            stream.Flush();
                        }
                    }
                    
                    s3Client = new AmazonS3Client(bucketRegion);

                    var fileTransferUtility = new TransferUtility(s3Client);

                    var fileTransferUtilityRequest = new TransferUtilityUploadRequest
                    {
                        BucketName = $"{UtilitiesClass.expenseBucketname}",
                        FilePath = filepath,
                        StorageClass = S3StorageClass.StandardInfrequentAccess,
                        //PartSize = 6291456, // 6 MB.
                        Key = $"{expensemodel.userid}{expensemodel.businessid}{expensemodel.expensenumber}.jpg",
                        CannedACL = S3CannedACL.PublicRead,
                        ContentType = "/jpg",
                    };
                    
                    expensemodel.image = $"{expensemodel.userid}{expensemodel.businessid}{expensemodel.expensenumber}.jpg";
                    
                    await fileTransferUtility.UploadAsync(fileTransferUtilityRequest);
                     
                    File.Delete(filepath);
                }

                var id = await ExpenseDal.Addexpense(expensemodel);

                if (string.IsNullOrEmpty(id))
                {
                   return BadRequest("expense already exits for this business or invalid expense data");
                }
                else
                {
                    expenseViewModel = ExpenseDal.GetExpense(expensemodel.businessid, Convert.ToInt32(id));

                    AuditTrailDal.CreateAuditTrail(AuditActionType.Create, Convert.ToInt32(id), new ExpenseViewModel(), expenseViewModel, expensemodel.userid, expensemodel.businessid);

                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return Ok(expenseViewModel);
        }

        //get expense
        [HttpGet]
        [Route("GetExpense")]
        public IHttpActionResult GetExpense(int? businessid, int? expenseid, string userid)
        {
            var expenseViewModel = new ExpenseViewModel();

            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            try
            {
                if (!expenseid.HasValue || !businessid.HasValue)
                {
                    return BadRequest("user id or expense is null");
                }
                else
                {
                    expenseViewModel = ExpenseDal.GetExpense(businessid, expenseid);
                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return Ok(expenseViewModel);

        }

        //get all expenses
        [HttpGet]
        [Route("GetAllExpenses")]
        public IHttpActionResult GetAllExpenses(int? businessid, int? limit, int? page, string sortby, string userid, string sortorder, string searchquery, string fromdate, string todate)
        {
            var expense = new ExpenseListViewModel();

            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            try
            {
                if (!businessid.HasValue)
                {
                    return BadRequest("Business id is null");
                }
                else
                {
                    var getDataModel = new GetDataModel(businessid)
                    {
                        limit = limit,
                        sortby = sortby,
                        sortorder = sortorder,
                        page = page,
                        listtype = false
                    };

                    expense = ExpenseDal.GetAllExpenses(getDataModel);

                    if (!string.IsNullOrEmpty(searchquery))
                    {
                        searchquery = searchquery.ToLower();
                        var searchqueryData = expense.expenses.Where(m => m.expensenumber.ToLower() == searchquery || m.description.ToLower() == searchquery || m.vendor.ToLower() == searchquery).ToList();
                        expense.expenses = searchqueryData;
                    }
                    else
                    {

                    }

                    if (!string.IsNullOrEmpty(fromdate) && !string.IsNullOrEmpty(todate))
                    {
                        DateTime dtfrom = DateTime.ParseExact(fromdate, "dd/MM/yyyy", null);
                        DateTime dtto = DateTime.ParseExact(todate, "dd/MM/yyyy", null);
                        var searchDateData = expense.expenses.Where(m => m.date_created.Date >= dtfrom.Date && m.date_created.Date <= dtto.Date).ToList();
                        expense.expenses = searchDateData;
                    }

                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return Ok(expense);

        }

        //post update expense end point
        [HttpPost]
        [Route("UpdateExpense")]
        public async Task<IHttpActionResult> UpdateExpenseAsync(ExpenseModel expensemodel)
        {
            var expenseViewModel = new ExpenseViewModel();

            IHttpActionResult result = null;

            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            try
            {
                if (!ModelState.IsValid)
                {
                    result = BadRequest("invalid expense details");
                }

                else
                {

                    var expensedetails = ExpenseDal.GetExpense(expensemodel.businessid, expensemodel.id);

                    if (!string.IsNullOrEmpty(expensemodel.image))
                    {

                        string root = HttpContext.Current.Server.MapPath("~/Receipts");

                        //var provider = new MultipartFormDataStreamProvider(root);

                        //await Request.Content.ReadAsMultipartAsync(provider);
                        byte[] base64file = null;
                        if (!string.IsNullOrEmpty(expensemodel.image))
                            base64file = Convert.FromBase64String(expensemodel.image);

                        string filepath = Path.Combine(root, $"{expensemodel.userid}{expensemodel.businessid}{expensemodel.expensenumber}.jpg");

                        // get file names
                        if (expensedetails.image == $"{expensemodel.userid}{expensemodel.businessid}{expensemodel.expensenumber}.jpg")
                        {
                            File.Delete(filepath);

                        }
                  
                        if (base64file != null && base64file.Length > 0)
                        {
                            using (var stream = new FileStream(filepath, FileMode.Create))
                            {
                                stream.Write(base64file, 0, base64file.Length);
                                stream.Flush();
                            }
                        }

                        s3Client = new AmazonS3Client(bucketRegion);

                        var fileTransferUtility = new TransferUtility(s3Client);

                        //expensemodel.image.Headers.ContentDisposition.FileName = expensemodel.image.Headers.ContentDisposition.FileName.Replace("\"", "");

                        var fileTransferUtilityRequest = new TransferUtilityUploadRequest
                        {
                            BucketName = $"{UtilitiesClass.expenseBucketname}",
                            FilePath = filepath,
                            StorageClass = S3StorageClass.StandardInfrequentAccess,
                            //PartSize = 6291456, // 6 MB.
                            Key = $"{expensemodel.userid}{expensemodel.businessid}{expensemodel.expensenumber}.jpg",
                            CannedACL = S3CannedACL.PublicRead,
                            ContentType = "/jpg",
                        };

                        expensemodel.image = $"{expensemodel.userid}{expensemodel.businessid}{expensemodel.expensenumber}.jpg";

                        await fileTransferUtility.UploadAsync(fileTransferUtilityRequest);

                        File.Delete(filepath);

                    }

                    //expensemodel.totalamount = Convert.ToInt32(expensemodel.amount);

                    var oldexpensedata = ExpenseDal.GetExpense(expensemodel.businessid, expensemodel.id);

                    var isSuccessful = await ExpenseDal.UpdateExpense(expensemodel);

                    if (!isSuccessful)
                    {
                        result = BadRequest("expense data is invalid");
                    }
                    else
                    {
                        var model = ExpenseDal.GetExpense(expensemodel.businessid, expensemodel.id);

                        AuditTrailDal.CreateAuditTrail(AuditActionType.Update, model.businessid, oldexpensedata, model, expensemodel.userid, expensemodel.businessid);

                        result = Ok(model);
                    }

                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }


            return result;
        }

        //get expense summarydata end point
        [HttpGet]
        [Route("GetExpenseSummaryData")]
        public IHttpActionResult GetExpenseSummaryData(int? businessid, string userid)
        {
            var expenseSummary = new ExpenseSummaryViewModel();

            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            try
            {
                if (!businessid.HasValue || string.IsNullOrEmpty(userid))
                {
                    return BadRequest("Business id is null or user id is null");
                }
                else
                {

                     expenseSummary = ExpenseDal.GetExpenseSummaryData(businessid, userid);
                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return Ok(expenseSummary);

        }

        [HttpGet]
        [Route("GetExpenseNumber")]
        public IHttpActionResult GetExpenseNumber(int businessid, string userid)
        {
            var expenseNumber = string.Empty;

            var response = new Dictionary<string, string>();

            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            try
            {
                if (string.IsNullOrEmpty(userid))
                {
                    return Unauthorized();
                }
                else
                {
                    expenseNumber = ExpenseDal.GenerateExpenseNumber(businessid, userid);

                    if (string.IsNullOrEmpty(expenseNumber))
                    {
                        return InternalServerError();

                    }
                    else
                    {
                        response.Add("expensenumber", expenseNumber);
                    }
                  
                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }
            return Ok(response);


        }


        [HttpPost]
        [Route("DeleteExpense")]
        public async Task<IHttpActionResult> DeleteExpense(DeleteExpenseModel model)
        {
            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");
            var Expense = new ExpenseListViewModel();
            var oldExpense = new ExpenseViewModel();

            try
            {

                if (!ModelState.IsValid)
                {
                    return BadRequest("invalid Expense details");
                }
                else
                {
                    oldExpense = ExpenseDal.GetExpense((int)model.businessid, (int)model.expenseid);

                    if (!await ExpenseDal.DeleteExpenseTemp(model.expenseid,model.businessid,model.userid))
                    {
                        return BadRequest("unable to delete Expense");
                    }
                    
                    else
                    {
                        var getdatamodel = new GetDataModel(model.businessid);

                        Expense = ExpenseDal.GetAllExpenses(getdatamodel);

                        var newExpense = Expense.expenses.SingleOrDefault(m => m.id == (int)model.expenseid);

                        AuditTrailDal.CreateAuditTrail(AuditActionType.Delete, (int)model.expenseid, oldExpense, newExpense, model.userid, (int)model.businessid);

                    }
                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return Ok(Expense);

        }
   
    }
}
