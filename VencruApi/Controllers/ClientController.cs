﻿using SharpRaven;
using SharpRaven.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using VencruApi.Models;
using VencruApi.Models.DalClasses;
using VencruApi.Models.ViewModels;

namespace VencruApi.Controllers
{
    [AllowAnonymous]
    //[Authorize]
    [EnableCors("*", "*", "*")]
    [RoutePrefix("api/Client")]
    public class ClientController : ApiController
    {
        private ClientDal _clientDal;

        public ClientController()
        {
        }

        public ClientController(ClientDal dal)
        {
            ClientDal = dal;
        }

        public ClientDal ClientDal
        {
            get
            {
                return ClientDal = new ClientDal();
            }
            private set
            {
                _clientDal = value;
            }
        }

        //add client endpoint
        [HttpPost]
        [AllowAnonymous]
        [Route("AddClient")]
        public async Task<IHttpActionResult> AddClient(ClientsModel client)
        {
            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            var clientViewModel = new ClientViewModel();

            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest("client data is invalid");
                }
                else
                {
                    string id = await ClientDal.AddClient(client);

                    if (string.IsNullOrEmpty(id))
                    {
                        return BadRequest("Client already exists for this business or invalid user details");
                    }
                    else
                    {

                        clientViewModel = ClientDal.GetClient(client.businessid, Convert.ToInt32(id));

                        if (string.IsNullOrEmpty(clientViewModel.companyemail))
                        {
                            return BadRequest("Client not created");
                        }

                        AuditTrailDal.CreateAuditTrail(AuditActionType.Create, Convert.ToInt32(id), new ClientViewModel(), clientViewModel, client.userid, client.businessid);

                    }
                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return Ok(clientViewModel);

        }

        //get client endpoint
        [HttpGet]
        [Route("GetClient")]
        public IHttpActionResult GetClient(int? businessid, int? clientid)
        {
            var clientViewModel = new ClientViewModel();

            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            try
            {
                if (!businessid.HasValue || !clientid.HasValue)
                {
                    return BadRequest("business id or client is invalid");
                }
                else
                {

                    clientViewModel = ClientDal.GetClient(businessid, clientid);
                    if (string.IsNullOrEmpty(clientViewModel.companyname) || string.IsNullOrEmpty(clientViewModel.companyemail))
                    {
                        return BadRequest("client not found");
                    }


                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return Ok(clientViewModel);

        }

        //get all clients end point with search and pagination
        [HttpGet]
        [Route("GetAllClients")]
        [AllowAnonymous]
        public IHttpActionResult GetAllClients(int? businessid, int? limit, int? page, string sortby, string sortorder, string searchquery, string fromdate, string todate, string userid, string filter)
        {
            var clientsModel = new ClientListViewModel();

            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            try
            {
                if (!businessid.HasValue)
                {
                    return BadRequest("Business id is null");
                }
                else
                {
                    var getDataModel = new GetDataModel(businessid)
                    {
                        limit = limit,
                        sortby = sortby,
                        sortorder = sortorder,
                        page = page,
                        listtype = false
                    };

                    clientsModel = ClientDal.GetAllClients(getDataModel);
                    //clientsModel.pagetotal = GetPageTotal(clientsModel.total, (int)limit);

                    if (!string.IsNullOrEmpty(filter))
                    {
                        var filterData = FilterClientData(filter, clientsModel);

                        clientsModel.clients = filterData;
                    }
                    else { }


                    if (!string.IsNullOrEmpty(searchquery))
                    {
                        searchquery = searchquery.ToLower();
                        var searchqueryData = clientsModel.clients.Where(m => m.companyname.ToLower() == searchquery || m.firstname.ToLower() == searchquery || m.lastname.ToLower() == searchquery).ToList();

                        clientsModel.clients = searchqueryData;
                        //clientsModel.pagetotal = GetPageTotal(clientsModel.total, (int)limit);
                    }
                    else
                    {

                    }

                    if (!string.IsNullOrEmpty(fromdate) && !string.IsNullOrEmpty(todate))
                    {
                        DateTime dtfrom = DateTime.ParseExact(fromdate, "dd/MM/yyyy", null);
                        DateTime dtto = DateTime.ParseExact(todate, "dd/MM/yyyy", null);

                        var searchDateData = clientsModel.clients.Where(m => m.date_created >= dtfrom.Date && m.date_created <= dtto).ToList();
                        clientsModel.clients = searchDateData;
                        //clientsModel.pagetotal = GetPageTotal(clientsModel.total, (int)limit);
                    }

                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return Ok(clientsModel);
        }

        private List<ClientViewModel> FilterClientData(string filter, ClientListViewModel clientsModel)
        {

            var filterData = new List<ClientViewModel>();

            if (filter.ToLower() == "today")
            {
                var date = DateTime.Now.ToString("dd/MM/yyyy");

                var dtto = DateTime.ParseExact(date, "dd/MM/yyyy", null);

                filterData = clientsModel.clients.Where(m => m.date_created.Date == dtto.Date).ToList();
            }
            else if (filter.ToLower() == "yesterday")
            {
                var yesterday = DateTime.Now.AddDays(-1).ToString("dd/MM/yyyy");

                var dtto = DateTime.ParseExact(yesterday, "dd/MM/yyyy", null);

                filterData = clientsModel.clients.Where(m => m.date_created.Date == dtto.Date).ToList();

            }
            else if (filter.ToLower() == "thismonth")
            {

                var currentMonth = DateTime.Now.ToString("MM");



                filterData = clientsModel.clients.Where(m => m.date_created.ToString("MM") == currentMonth).ToList();

            }
            else { }

            return filterData;
        }

        //get delete list end point
        [HttpGet]
        [Route("GetDeleteList")]
        public IHttpActionResult GetDeleteList(int? businessid, int? limit, int? page, string sortby, string sortorder, string searchquery, string fromdate, string todate, string userid)
        {
            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            var clientsModel = new ClientListViewModel();

            try
            {

                if (!businessid.HasValue)
                {
                    return BadRequest("Business id is null");
                }
                else
                {
                    var getDataModel = new GetDataModel(businessid)
                    {
                        limit = limit,
                        sortby = sortby,
                        sortorder = sortorder,
                        page = page,
                        listtype = true
                    };

                    clientsModel = ClientDal.GetAllClients(getDataModel);
                    //clientsModel.pagetotal = GetPageTotal(clientsModel.total, (int)limit);
                    if (!string.IsNullOrEmpty(searchquery))
                    {
                        searchquery = searchquery.ToLower();
                        var searchqueryData = clientsModel.clients.Where(m => m.companyname.ToLower() == searchquery || m.firstname.ToLower() == searchquery || m.lastname.ToLower() == searchquery).ToList();
                        clientsModel.clients = searchqueryData;
                        //clientsModel.pagetotal = GetPageTotal(clientsModel.total, (int)limit);
                    }
                    else
                    {

                    }

                    if (!string.IsNullOrEmpty(fromdate) && !string.IsNullOrEmpty(todate))
                    {
                        DateTime dtfrom = DateTime.ParseExact(fromdate, "dd/MM/yyyy", null);
                        DateTime dtto = DateTime.ParseExact(todate, "dd/MM/yyyy", null);

                        var searchDateData = clientsModel.clients.Where(m => m.date_created >= dtfrom.Date && m.date_created <= dtto).ToList();
                        clientsModel.clients = searchDateData;
                        //clientsModel.pagetotal = GetPageTotal(clientsModel.total, (int)limit);
                    }

                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return Ok(clientsModel);


        }

        [HttpGet]
        [Route("GetClientHistory")]
        public IHttpActionResult GetClientHistoryList(int? clientid, int? businessid, int? limit, int? page, string userid)
        {
            if (!clientid.HasValue || !businessid.HasValue || string.IsNullOrEmpty(userid))
            {
                return BadRequest("invalid client or business");
            }
            else
            {
                var historyList = ClientDal.GetClientHistory(clientid, businessid, limit, page, userid);

                return Ok(historyList);
            }
        }

        //get client summarydata end point
        [HttpGet]
        [Route("GetClientSummaryData")]
        public IHttpActionResult GetClientSummaryData(int? businessid)
        {
            var clientSummary = new ClientSummaryViewModel();

            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            try
            {
                if (!businessid.HasValue)
                {
                    return BadRequest("Business id is null");
                }
                else
                {

                    clientSummary = ClientDal.GetClientSummaryData(businessid);

                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return Ok(clientSummary);

        }

        //update client endpoint
        [HttpPost]
        [Route("UpdateClient")]
        public async Task<IHttpActionResult> UpdateClient(ClientsModel client)
        {
            var clientViewModel = new ClientViewModel();

            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest("invalid client data");
                }
                else
                {
                    var oldclient = ClientDal.GetClient(client.businessid, client.id);

                    var updateClient = await ClientDal.UpdateClient(client);

                    if (!updateClient)
                    {
                        return BadRequest("invalid client data");

                    }
                    else
                    {
                        clientViewModel = ClientDal.GetClient(client.businessid, client.id);

                        AuditTrailDal.CreateAuditTrail(AuditActionType.Update, client.id, oldclient, clientViewModel, client.userid, client.businessid);

                    }
                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return Ok(clientViewModel);
        }

        //add client to delete list end point
        [HttpPost]
        [Route("DeleteClient")]
        public async Task<IHttpActionResult> DeleteClient(DeleteClientModel client)
        {
            var clientListViewModel = new ClientListViewModel();

            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            try
            {
                if (client.clientid == 0 || client.businessid == 0)
                {
                    return BadRequest("clientid or business id is null");
                }
                else if (!await ClientDal.DeleteClientTemp(client.clientid, client.businessid))
                {
                    return BadRequest("client id or business id is null");
                }
                else
                {

                    var getclient = ClientDal.GetClient(client.businessid, client.clientid);

                    var getDataModel = new GetDataModel(client.businessid)
                    {
                        limit = 10,
                        sortby = "date_created",
                        sortorder = "DESC",
                        page = 1,
                        listtype = false
                    };

                    
                    clientListViewModel = ClientDal.GetAllClients(getDataModel);

                    AuditTrailDal.CreateAuditTrail(AuditActionType.Delete, client.clientid, getclient, new ClientViewModel(), client.userid, client.businessid);

                }

            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }
            return Ok(clientListViewModel);

        }

        //add multiple clients to delete list end point
        [HttpPost]
        [Route("DeleteMultipleClient")]
        public async Task<IHttpActionResult> DeleteMultipleClients(DeleteMultipleClientModel client)
        {
            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            var clientListViewModel = new ClientListViewModel();

            try
            {
                if (client.items.Count() < 1)
                {
                    return BadRequest("client ids or business ids is null");
                }

                else
                {
                    foreach (var item in client.items)
                    {

                        var getclient = ClientDal.GetClient(item.businessid, item.clientid);

                        await ClientDal.DeleteClientTemp(item.clientid, item.businessid);

                        AuditTrailDal.CreateAuditTrail(AuditActionType.Delete, item.clientid, getclient, new ClientViewModel(), client.userid, item.businessid);

                    }

                    var getDataModel = new GetDataModel(client.items.FirstOrDefault().businessid)
                    {
                        limit = 10,
                        sortby = "date_created",
                        sortorder = "DESC",
                        page = 1,
                        listtype = false
                    };


                    clientListViewModel = ClientDal.GetAllClients(getDataModel);
                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return Ok(clientListViewModel);
        }

        //add restore client end point
        [HttpPost]
        [Route("RestoreClient")]
        public async Task<IHttpActionResult> RestoreClientAsync(DeleteClientModel client)
        {
            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            var clientListViewModel = new ClientListViewModel();

            var oldclientdata = new ClientViewModel();

            try
            {
                oldclientdata = ClientDal.GetClient(client.businessid, client.clientid);

                if (client.businessid == 0 || client.clientid == 0)
                {
                    return BadRequest("clientid or business id is null");
                }
                else if(!await ClientDal.RestoreClient(client.clientid, client.businessid))
                {

                    return BadRequest("client id or business id is null");
                }
                else
                {


                    var getDataModel = new GetDataModel(client.businessid)
                    {
                        limit = 10,
                        sortby = "date_created",
                        sortorder = "DESC",
                        page = 1,
                        listtype = false
                    };

                    getDataModel.businessid = client.businessid;

                    clientListViewModel = ClientDal.GetAllClients(getDataModel);

                    var newclientdata = clientListViewModel.clients.SingleOrDefault(m => m.id == client.clientid);


                    AuditTrailDal.CreateAuditTrail(AuditActionType.Delete, client.clientid, oldclientdata, newclientdata, client.userid, client.businessid);

                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return Ok(clientListViewModel);
        }

        //add restore multiple client end point
        [HttpPost]
        [AllowAnonymous]
        [Route("RestoreMultipleClients")]
        public async Task<IHttpActionResult> RestoreMultipleClients(DeleteMultipleClientModel model)
        {
            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            var clientListViewModel = new ClientListViewModel();

            try
            {

                if (model.items.Count() < 1)
                {
                    return BadRequest("invalid clients data");
                }
                else
                {
                    foreach (var item in model.items)
                    {
                        var oldclientdata = ClientDal.GetClient(item.businessid, item.clientid);

                        await ClientDal.RestoreClient(item.clientid, item.businessid);

                        var newclientdata = ClientDal.GetClient(item.businessid, item.clientid);


                        AuditTrailDal.CreateAuditTrail(AuditActionType.Restore, item.clientid, oldclientdata, newclientdata, model.userid, item.businessid);

                    }
                    var getDataModel = new GetDataModel(model.items.FirstOrDefault().businessid)
                    {
                        limit = 10,
                        sortby = "date_created",
                        sortorder = "DESC",
                        page = 1,
                        listtype = false
                    };

                    clientListViewModel = ClientDal.GetAllClients(getDataModel);


                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return Ok(clientListViewModel);

        }

        //add client notes
        [HttpPost]
        [Route("AddClientNote")]
        public async Task<IHttpActionResult> AddClientNoteAsync(ClientNoteModel model)
        {
            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            var clientViewModel = new ClientViewModel();

            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest("client data is invalid");
                }
                else
                {
                    var oldclientdata = ClientDal.GetClient(model.businessid, Convert.ToInt32(model.clientid));

                    var issuccessful = await ClientDal.AddClientNote(model);

                    if (!issuccessful)
                    {
                        return BadRequest("invalid client details, note not updated for client");
                    }
                    else
                    {

                        clientViewModel = ClientDal.GetClient(model.businessid, Convert.ToInt32(model.clientid));

                        AuditTrailDal.CreateAuditTrail(AuditActionType.Update,clientViewModel.id, oldclientdata, clientViewModel, model.userid, model.businessid);

                    }
                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return Ok(clientViewModel);

        }


    }
}
