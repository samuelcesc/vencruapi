﻿using Amazon;
using Amazon.S3;
using Amazon.S3.Transfer;
using SharpRaven;
using SharpRaven.Data;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using VencruApi.Models;
using VencruApi.Models.Classes;
using VencruApi.Models.DalClasses;
using VencruApi.Models.ViewModels;

namespace VencruApi.Controllers
{
    [EnableCors("*", "*", "*")]
    [RoutePrefix("api/invoice")]
    [AllowAnonymous]
    public class InvoiceController : ApiController
    {
        //declare aws region end point
        private static readonly RegionEndpoint bucketRegion = RegionEndpoint.USEast1;

        //declare instance of Interface amazons3 client
        private static IAmazonS3 s3Client;

        //private instantiation of invoicedals
        private InvoiceDal _invoiceDal;

        //private instatiation of useraccountdal
        private UserAccountDal _userAccountDal;

        private ClientDal _clientDal;
        
        //controller contsructor for invoice module
        public InvoiceController()
        { }

        //pass invoicedal to invoice constructors
        public InvoiceController(InvoiceDal invoiceDal, UserAccountDal userAccountDal)
        {
            InvoiceDal = invoiceDal;
            UserAccountDal = userAccountDal;
        }

        public InvoiceDal InvoiceDal
        {
            get
            {
                return _invoiceDal = new InvoiceDal(UserAccountDal, ClientDal);
            }
            private set
            {
                _invoiceDal = value;
            }
        }

        public UserAccountDal UserAccountDal
        {
            get
            {
                return UserAccountDal = new UserAccountDal();
            }
            private set
            {
                _userAccountDal = value;
            }
        }

        public ClientDal ClientDal
        {
            get
            {
                return ClientDal = new ClientDal();
            }
            private set
            {
                _clientDal = value;
            }
        }
        
        //get invoice number
        [HttpGet]
        [Route("GetInvoiceNumber")]
        public IHttpActionResult GetInvoiceNumber(int businessid, string userid)
        {
            var invoiceNumber = string.Empty;
            var response = new Dictionary<string, string>();
            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            try
            {
                if (string.IsNullOrEmpty(userid))
                {
                    return Unauthorized();
                }
                else
                {
                    
                     invoiceNumber = InvoiceDal.GenerateInvoiceNumber(businessid, userid);

                    response.Add("invoicenumber", invoiceNumber);

                    if (string.IsNullOrEmpty(invoiceNumber))
                    {
                        return InternalServerError();

                    }
                  
                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }
            return Ok(response);

        }

        //create invoice
        [HttpPost]
        [Route("CreateInvoice")]
        public async Task<IHttpActionResult> CreateInvoiceAsync(InvoiceModel model)
        {
            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            var invoice = new InvoiceViewModel();

            try
            {
                //check for invoice status to make fields optional

                var header = Request.Headers;

                string callbackurl = string.Empty;

                var data = await InvoiceDal.CreateInvoice(model);

                if (!string.IsNullOrEmpty(data) && model.sendstyle.ToLower() == "email")
                {

                     invoice = InvoiceDal.GetInvoice(model.businessid, Convert.ToInt32(data), model.userid);

                    if (header.Contains("emailurl"))
                    {

                        callbackurl = header.GetValues("emailurl").First();

                        string url = $"{callbackurl}?token={invoice.invoiceguid}?invoiceid={invoice.id}?businessid={invoice.business.id}";

                        var confirminvoice = new ConfirmInvoiceModel()
                        {
                            code = url,
                            invoice = invoice
                        };

                        var ok = await InvoiceDal.SendToEmail(confirminvoice);

                        string invoicekey = await UploadInvoiceImageAsync(invoice);

                        model.image = invoicekey;

                        return Ok(invoice);

                    }
                    else
                    {
                        return BadRequest("invalid invoice details");
                    }
                }
                else if (string.IsNullOrEmpty(data))
                {
                    return BadRequest("unable to create invoice");
                }
                else
                {
                     invoice = InvoiceDal.GetInvoice(model.businessid, Convert.ToInt32(data), model.userid);

                    AuditTrailDal.CreateAuditTrail(AuditActionType.Create, Convert.ToInt32(data), new InvoiceViewModel(), invoice, model.userid, model.businessid);
                    
                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return Ok(invoice);

        }

        //verify invoice
        [HttpPost]
        [Route("VerifyInvoice")]
        public IHttpActionResult VerifyInvoiceAsync(VerifyInvoiceModel model)
        {
            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest("invalid invoice details");
                }
                else
                {
                    var isvalid = InvoiceDal.ValidateInvoice(model);

                    if (isvalid != 0)
                    {
                        var response = new Dictionary<string, bool>()
                        {
                            {"isvalid", true }
                        };
                        return Ok(response);
                    }
                    else
                    {
                        return BadRequest("invalid invoice");
                    }
                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return Ok("invoice is valid");

        }

        //get invoice
        [HttpGet]
        [Route("GetInvoice")]
        public IHttpActionResult GetInvoice(int? businessid, int? invoiceid, string userid)
        {
            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            var invoice = new InvoiceViewModel();

            try
            {
                if (!businessid.HasValue || !invoiceid.HasValue)
                {
                    return BadRequest("businesss id or invoice is invalid");
                }
                else
                {

                    invoice = InvoiceDal.GetInvoice((int)businessid, (int)invoiceid, userid);

                    if (string.IsNullOrEmpty(invoice.invoicenumber))
                    {
                        return BadRequest("invoice not found");
                    }
                  
                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return Ok(invoice);


        }
        
        //update invoice
        [HttpPost]
        [Route("UpdateInvoice")]
        public async Task<IHttpActionResult> UpdateInvoiceAsync(InvoiceModel model)
        {
            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            var invoice = new InvoiceViewModel();

            try
            {
                var header = Request.Headers;

                string callbackurl = string.Empty;

                var oldinvoicedata = InvoiceDal.GetInvoice(model.businessid, model.id, model.userid);

                var data = await InvoiceDal.UpdateInvoice(model);

                if (data == true && model.sendstyle.ToLower() == "email")
                {
                    invoice = InvoiceDal.GetInvoice(model.businessid, Convert.ToInt32(data), model.userid);

                    if (header.Contains("emailurl"))
                    {

                        callbackurl = header.GetValues("emailurl").First();

                        string url = $"{callbackurl}?token={invoice.invoiceguid}?invoiceid={invoice.id}?businessid={invoice.business.id}";

                        var confirminvoice = new ConfirmInvoiceModel()
                        {
                            code = url,
                            invoice = invoice
                        };

                        var ok = await InvoiceDal.SendToEmail(confirminvoice);

                        string invoicekey = await UploadInvoiceImageAsync(invoice);

                        model.image = invoicekey;

                    }
                    else
                    {
                        return BadRequest("invalid invoice details");
                    }
                }
                else if (!data)
                {
                    return BadRequest("unable to create invoice");
                }
                else
                {
                    invoice = InvoiceDal.GetInvoice(model.businessid, model.id, model.userid);

                    AuditTrailDal.CreateAuditTrail(AuditActionType.Update, Convert.ToInt32(data), oldinvoicedata, invoice, model.userid, model.businessid);

                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }
            return Ok(invoice);

        }

        //get all invoices
        [HttpGet]
        [Route("GetAllInvoices")]
        public IHttpActionResult GetAllInvoices(int? businessid, int? limit, int? page, string sortby, string sortorder, string searchquery, string fromdate, string todate, string status, string invoicetype = "invoice")
        {
            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            var invoice = new InvoiceListViewModel();

            try
            {
                if (!businessid.HasValue)
                {
                    return BadRequest("Business id is null");
                }
                else
                {
                    var getDataModel = new GetDataModel(businessid)
                    {
                        limit = limit,
                        sortby = sortby,
                        sortorder = sortorder,
                        page = page,
                        listtype = true,
                    };

                    invoice = InvoiceDal.GetAllinvoices(getDataModel);

                    //get invoice or receipt
                    if (!string.IsNullOrEmpty(invoicetype?.ToLower()))
                    {
                        invoicetype = invoicetype.ToLower();

                        if (invoicetype == "invoice")
                        {
                            var searchqueryData = invoice.invoices.Where(m => m.invoicetype.ToLower() == "invoice").ToList();
                            invoice.invoices = searchqueryData;
                        }
                        else if(invoicetype == "receipt")
                        {
                            var searchqueryData = invoice.invoices.Where(m => m.invoicetype.ToLower() == "receipt").ToList();

                            invoice.invoices = searchqueryData;
                        }
                        else { }
                      
                    }
                    else
                    {

                    }

                    //filter by invoice status
                    if (!string.IsNullOrEmpty(status))
                    {
                        var invoicestatus = status.ToLower();

                        if (invoicestatus == "draft")
                        {
                            var searchqueryData = invoice.invoices.Where(m => m.invoicestatus.ToLower() == "draft").ToList();
                            invoice.invoices = searchqueryData;
                        }
                        else if (invoicestatus == "not paid")
                        {
                            var searchqueryData = invoice.invoices.Where(m => m.invoicestatus.ToLower() == "not paid").ToList();
                            invoice.invoices = searchqueryData;
                        }
                        else if (invoicestatus == "paid")
                        {
                            var searchqueryData = invoice.invoices.Where(m => m.invoicestatus.ToLower() == "paid").ToList();
                            invoice.invoices = searchqueryData;
                        }
                        else if (invoicestatus == "over due")
                        {
                            var searchqueryData = invoice.invoices.Where(m => m.invoicestatus.ToLower() == "over due").ToList();
                            invoice.invoices = searchqueryData;
                        }
                        else if (invoicestatus == "all")
                        {
                           
                        }
                        else { }
                    }

                    //filter by search query
                    if (!string.IsNullOrEmpty(searchquery?.ToLower()))
                    {
                        searchquery = searchquery.ToLower();
                        var searchqueryData = invoice.invoices.Where(m => m.invoicenumber.ToLower() == searchquery || m.description.ToLower() == searchquery || m.client.companyname.ToLower() == searchquery).ToList();
                        invoice.invoices = searchqueryData;
                    }
                    else
                    {

                    }

                    //filter by date
                    if (!string.IsNullOrEmpty(fromdate) && !string.IsNullOrEmpty(todate))
                    {
                        DateTime dtfrom = DateTime.ParseExact(fromdate, "dd/MM/yyyy", null);
                        DateTime dtto = DateTime.ParseExact(todate, "dd/MM/yyyy", null);
                        var searchDateData = invoice.invoices.Where(m => m.date_created >= dtfrom.Date && m.date_created <= dtto).ToList();
                        invoice.invoices = searchDateData;
                    }

                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return Ok(invoice);

        }

        [HttpGet]
        [Route("GetCancelledInvoice")]
        public IHttpActionResult GetCancelledInvoices(int? businessid, int? limit, int? page, string sortby, string sortorder, string searchquery, string fromdate, string todate, string status)
        {
            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            var invoice = new InvoiceListViewModel();

            try
            {
                if (!businessid.HasValue)
                {
                    return BadRequest("Business id is null");
                }
                else
                {
                    var getDataModel = new GetDataModel(businessid)
                    {
                        limit = limit,
                        sortby = sortby,
                        sortorder = sortorder,
                        page = page,
                        listtype = false,
                    };

                    invoice = InvoiceDal.GetAllinvoices(getDataModel);

                    if (!string.IsNullOrEmpty(searchquery))
                    {
                        searchquery = searchquery.ToLower();
                        var searchqueryData = invoice.invoices.Where(m => m.invoicenumber.ToLower() == searchquery || m.description.ToLower() == searchquery || m.client.companyname.ToLower() == searchquery).ToList();
                        invoice.invoices = searchqueryData;
                    }
                    else
                    {

                    }

                    if (!string.IsNullOrEmpty(fromdate) && !string.IsNullOrEmpty(todate))
                    {
                        DateTime dtfrom = DateTime.ParseExact(fromdate, "dd/MM/yyyy", null);
                        DateTime dtto = DateTime.ParseExact(todate, "dd/MM/yyyy", null);
                        var searchDateData = invoice.invoices.Where(m => m.date_created >= dtfrom.Date && m.date_created <= dtto).ToList();
                        invoice.invoices = searchDateData;
                    }

                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return Ok(invoice);

        }
        
        //delete invoices
        [HttpPost]
        [Route("DeleteInvoice")]
        public async Task<IHttpActionResult> DeleteInvoiceAsync(InvoiceOperationModel model)
        {
            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            var invoice = new InvoiceListViewModel();

            var oldinvoice = new InvoiceViewModel();

            try
            {

                if (!ModelState.IsValid)
                {
                    return BadRequest("invalid invoice details");
                }
                else
                {
                    oldinvoice = InvoiceDal.GetInvoice((int)model.businessid, (int)model.invoiceid, model.userid);

                    if (!await InvoiceDal.DeleteInvoiceTemp(model))
                    {
                        return BadRequest("unable to delete invoice");
                    }
                    else
                    {
                        var getdatamodel = new GetDataModel(model.businessid);

                        invoice = InvoiceDal.GetAllinvoices(getdatamodel);

                        var newinvoice = invoice.invoices.SingleOrDefault(m => m.id == (int)model.invoiceid);

                        AuditTrailDal.CreateAuditTrail(AuditActionType.Delete, (int)model.invoiceid, oldinvoice, newinvoice, model.userid, (int)model.businessid);

                    }
                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return Ok(invoice);

        }

        //delete invoices
        [HttpPost]
        [Route("DeleteItem")]
        public async Task<IHttpActionResult> DeleteItem(InvoiceOperationModel model)
        {
            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            var invoiceItems = new List<InvoiceItemViewModel>();

            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest("invalid invoice details");
                }
                else
                {

                    if (!await InvoiceDal.DeleteInvoiceItem(model))
                    {
                        return BadRequest("unable to delete invoice item");
                    }
                    else
                    {
                        invoiceItems = InvoiceDal.GetItems((int)model.invoiceid);
                    }
                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return Ok(invoiceItems);

        }

        //delete invoices
        [HttpPost]
        [Route("CancelInvoice")]
        public async Task<IHttpActionResult> CancelInvoiceAsync(InvoiceOperationModel model)
        {
            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            var invoice = new InvoiceListViewModel();
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest("invalid invoice details");
                }
                else
                {
                    if (!await InvoiceDal.CancelInvoice(model))
                    {
                        return BadRequest("unable to calcel invoice");
                    }
                    else
                    {
                        var getdatamodel = new GetDataModel(model.businessid);
                        invoice = InvoiceDal.GetAllinvoices(getdatamodel);
                    }
                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return Ok(invoice);

        }

        //update invoices
        [HttpGet]
        [Route("UpdateInvoiceStatus")]
        public async Task<IHttpActionResult> UpdateInvoiceStatusAsync(UpdateinvoiceModel model)
        {
            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            var invoice = new InvoiceViewModel();

            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest("invalid invoice details");
                }
                else
                {
                    if (!await InvoiceDal.UpdateInvoiceStatus(model))
                    {
                        return BadRequest("unable to update invoice");
                    }
                    else
                    {
                        invoice = InvoiceDal.GetInvoice(model.businessid, model.invoiceid, model.userid);
                    }
                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return Ok(invoice);
        }

        //add payment
        [HttpPost]
        [Route("AddPayment")]
        public async Task<IHttpActionResult> AddPaymentAsync(InvoicePaymentModel model)
        {
            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            var payment = new InvoicePaymentViewModel();

            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest("invalid payment details");
                }
                else
                {
                    var issuccessful = await InvoiceDal.AddPayment(model);

                    if (string.IsNullOrEmpty(issuccessful))
                    {
                        return BadRequest("unable to add payment");
                    }
                    else
                    {
                        payment = InvoiceDal.GetPayment(model.invoiceid, Convert.ToInt32(issuccessful), model.userid);
                    }
                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return Ok(payment);

        }

        //add payment
        [HttpPost]
        [Route("UpdatePayment")]
        public async Task<IHttpActionResult> UpdatePaymentAsync(InvoicePaymentModel model)
        {
            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            var payment = new InvoicePaymentViewModel();

            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest("invalid payment details");
                }
                else
                {
                    var issuccessful = await InvoiceDal.UpdatePayment(model);

                    if (!issuccessful)
                    {
                        return BadRequest("unable to update payment");
                    }
                    else
                    {
                        payment = InvoiceDal.GetPayment(model.invoiceid, model.id, model.userid);
                    }
                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return Ok(payment);

        }

        //delete payment
        [HttpPost]
        [Route("DeletePayment")]
        public async Task<IHttpActionResult> DeletePaymentAsync(PaymentOperationModel model)
        {
            var paymentList = new PaymentListViewModel();

            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest("invalid payment details");
                }
                else
                {
                    var issuccessful = await InvoiceDal.DeletePayment(model);
                    if (!issuccessful)
                    {
                        return BadRequest("unable to delete payment");
                    }
                    else
                    {
                        var dataModel = new GetDataModel(model.invoiceid);
                        paymentList = InvoiceDal.GetAllPayments(dataModel);
                    }
                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return Ok(paymentList);
        }

        //get payment
        [HttpGet]
        [Route("GetPayment")]
        public IHttpActionResult GetPayment(int? paymentid, int? invoiceid, string userid)
        {

            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            var paymentList = new InvoicePaymentViewModel();

            try
            {
                if (!paymentid.HasValue || !invoiceid.HasValue)
                {
                    return BadRequest("payment id or invoice is invalid");
                }
                else
                {

                    paymentList = InvoiceDal.GetPayment((int)invoiceid, (int)paymentid, userid);

                    if (string.IsNullOrEmpty(paymentList.paidwith))
                    {
                        return BadRequest("payment not found");
                    }

                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return Ok(paymentList);
        }

        //get all payments
        [HttpGet]
        [Route("GetAllPayment")]
        public IHttpActionResult GetAllPayment(int? invoiceid, int? limit, int? page, string sortby, string sortorder, string searchquery, string fromdate, string todate)
        {
            var payment = new PaymentListViewModel();

            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            try
            {
                if (!invoiceid.HasValue)
                {
                    return BadRequest("invoice id is null");
                }
                else
                {
                    var getDataModel = new GetDataModel(invoiceid)
                    {
                        businessid = invoiceid,
                        limit = limit,
                        sortby = sortby,
                        sortorder = sortorder,
                        page = page,
                    };

                    payment = InvoiceDal.GetAllPayments(getDataModel);

                    if (!string.IsNullOrEmpty(fromdate) && !string.IsNullOrEmpty(todate))
                    {
                        DateTime dtfrom = DateTime.ParseExact(fromdate, "dd/MM/yyyy", null);
                        DateTime dtto = DateTime.ParseExact(todate, "dd/MM/yyyy", null);
                        var searchDateData = payment.payments.Where(m => m.date_created >= dtfrom.Date && m.date_created <= dtto).ToList();
                        payment.payments = searchDateData;
                    }
                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return Ok(payment);
        }

        //get payment
        [HttpGet]
        [Route("GetInvoiceSummary")]
        [Route("GetInvoiceSummaryData")]
        public IHttpActionResult GetInvoiceSummary(int businessid, string userid, string sortby)
        {

            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            var summary = new InvoiceSummaryVieModel();

            try
            {
                summary = InvoiceDal.GetSummary(businessid, userid, sortby);
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return Ok(summary);
        }

        //upload invoice image
        private async Task<string> UploadInvoiceImageAsync(InvoiceViewModel model)
        {
            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            try
            {
                if (!string.IsNullOrEmpty(model.image))
                {
                    string root = HttpContext.Current.Server.MapPath("~/Businesslogos");

                    var base64file = Convert.FromBase64String(model.image);

                    string filepath = Path.Combine(root, $"{model.userid}{model.business.id}.jpg");

                    if (model.image == $"{model.userid}{model.business.id}.jpg")
                    {
                        File.Delete(filepath);
                    }
                    else
                    {
                        if (base64file.Length > 0)
                        {
                            using (var stream = new FileStream(filepath, FileMode.Create))
                            {
                                stream.Write(base64file, 0, base64file.Length);
                                stream.Flush();
                            }
                        }

                        s3Client = new AmazonS3Client(bucketRegion);

                        var fileTransferUtility = new TransferUtility(s3Client);

                        var fileTransferUtilityRequest = new TransferUtilityUploadRequest
                        {
                            BucketName = $"{UtilitiesClass.businesslogoBucketname}",
                            FilePath = filepath,
                            StorageClass = S3StorageClass.StandardInfrequentAccess,
                            Key = $"{model.userid}{model.id}.jpg",
                            CannedACL = S3CannedACL.PublicRead,
                            ContentType = "/jpg",
                        };

                        model.image = $"{model.userid}{model.business.id}.jpg";

                        await fileTransferUtility.UploadAsync(fileTransferUtilityRequest);

                        File.Delete(filepath);
                    }

                    var businessmodel = new BusinessModel()
                    {
                        address = model.business.address,
                        city = model.business.city,
                        onlinepayment = model.business.onlinepayment,
                        service = model.business.service,
                        state = model.business.state,
                        companyname = model.business.companyname,
                        country = model.business.country,
                        currency = model.business.currency,
                        date_created = model.business.date_created,
                        domain = model.business.domain,
                        employeesize = model.business.employeesize,
                        facebookurl = model.business.facebookurl,
                        Id = model.business.id,
                        industry = model.business.industry,
                        instagramurl = model.business.instagramurl,
                        isincorporated = model.business.isincorporated,
                        linkdinurl = model.business.linkdinurl,
                        logourl = model.image,
                        userid = model.business.ownerid,
                        phonenumber = model.business.phonenumber,
                        tax = model.business.tax,
                        twitterurl = model.business.twitterurl,
                        vatnumber = model.business.vatnumber

                    };

                    var issuccessful = await UserAccountDal.UpdateBusiness(businessmodel);


                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }


            return model.image;
        }

    }
}
