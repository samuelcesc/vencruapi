﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Newtonsoft.Json;
using SharpRaven;
using SharpRaven.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using VencruApi.Models;
using VencruApi.Models.Classes;
using VencruApi.Models.DalClasses;
using VencruApi.Models.ViewModels;

namespace VencruApi.Controllers
{
    [Authorize]
    [RoutePrefix("api/Account")]
    [EnableCors("*", "*", "*")]
    //[AllowAnonymous]
    public class AccountController : ApiController
    {
        private const string LocalLoginProvider = "Local";

        private ApplicationUserManager _userManager;

        private UserAccountDal _userAccountDal;

        private TokenModel _myToken;

        private const string ApiUri = "https://api.vencru.com";
        //private const string ApiUri = "http://localhost:50194/";

        public AccountController()
        {
        }

        public AccountController(ApplicationUserManager userManager, ISecureDataFormat<AuthenticationTicket> accessTokenFormat, UserAccountDal userAccountDal)
        {
            UserManager = userManager;
            AccessTokenFormat = accessTokenFormat;
            UserAccountDal = userAccountDal;
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public UserAccountDal UserAccountDal
        {
            get
            {
                return UserAccountDal = new UserAccountDal();
            }
            private set
            {
                _userAccountDal = value;
            }
        }

        public ISecureDataFormat<AuthenticationTicket> AccessTokenFormat { get; private set; }

        [HttpGet]
        [Route("UserInfo")]
        [AllowAnonymous]
        public IHttpActionResult UserInfo(string userid)
        {

            if (string.IsNullOrEmpty(userid))
            {
                return BadRequest("userid is null or invalid");
            }
            else
            {
                var getuserInfo = UserAccountDal.GetUser(userid);


                return Ok(getuserInfo);
            }
        }

        //Add the current business a user selected for continuity
        [HttpPost]
        [Route("AddUserCurrentBusiness")]
        public IHttpActionResult AddUserCurrentBusiness(GetUserCurrentBusinessModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("user id is null or business id is invalid");
            }
            else
            {
                var getcurrentbusiness = UserAccountDal.AddUserCurrentBusiness(model);

                return Ok(getcurrentbusiness);
            }
        }

        //gets the current business a user selected for continuity
        [HttpGet]
        [Route("GetUserCurrentBusiness")]
        public IHttpActionResult GetUserCurrentBusiness(string userid)
        {

            if (!ModelState.IsValid)
            {
                return BadRequest("user id is null or business id is invalid");
            }
            else
            {
                var getcurrentbusiness = UserAccountDal.GetUserCurrentBusiness(userid);
                return Ok(getcurrentbusiness);
            }

        }

        // POST api/Account/Logout
        [Route("Logout")]
        [HttpPost]
        public IHttpActionResult Logout()
        {

            Authentication.SignOut(CookieAuthenticationDefaults.AuthenticationType);

            return Ok();

        }

        // POST api/Account/InitiatePasswordChange
        [HttpPost]
        [Route("InitiatePasswordChange")]
        [AllowAnonymous]
        public async Task<IHttpActionResult> InitiatePasswordChangeAsync(InitiatePasswordChange model)
        {
            string callback = string.Empty;

            var emailConfirmedViewModel = new EmailSentViewModel();

            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            try
            {
                var user = await UserManager.FindByEmailAsync(model.email);

                if (user != null)
                {
                    var header = Request.Headers;

                    if (header.Contains("emailurl"))
                    {
                        callback = header.GetValues("emailurl").First();

                        if (string.IsNullOrEmpty(callback))
                        {
                            return BadRequest();
                        }
                        else
                        {
                            string str = await UserManager.GeneratePasswordResetTokenAsync(user.Id);

                            var uri = $"{callback}?userid={user.Id}&code={str}";

                            PasswordResetModel passwordResetModel = new PasswordResetModel()
                            {
                                CallbackUrl = uri,
                                Email = user.Email,
                                fullname = user.lastname + user.firstname
                            };

                            var emailsent = await AdminDal.SendPasswordResetEmailAsync(passwordResetModel);

                            if (emailsent == true)
                            {
                                emailConfirmedViewModel.issent = true;

                                return Ok(emailConfirmedViewModel);
                            }
                            else
                            {
                                return BadRequest("Invalid email");
                            }
                        }
                    }
                    else
                    {
                        return BadRequest();
                    }
                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            emailConfirmedViewModel.issent = true;

            return Ok(emailConfirmedViewModel);
        }

        // POST api/Account/ResetPassword
        [Route("ResetPassword")]
        [HttpPost]
        [AllowAnonymous]
        public async Task<IHttpActionResult> ResetPasswordAsync(ChangePasswordBindingModel model)
        {
            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            var emailConfirmedViewModel = new EmailConfirmedViewModel();

            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                var result = await UserManager.ResetPasswordAsync(model.userid, model.code, model.newpassword);

                if (!result.Succeeded)
                {
                    return GetErrorResult(result);
                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            emailConfirmedViewModel.isconfirmed = true;

            return Ok(emailConfirmedViewModel);
        }

        //validate password reset token
        [HttpPost]
        [Route("ValidatePasswordResetToken")]
        [AllowAnonymous]
        public async Task<IHttpActionResult> ValidatePasswordResetTokenAsync(VerifyEmailModel model)
        {

            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            var emailConfirmedViewModel = new EmailConfirmedViewModel();

            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                var user = UserManager.FindById(model.userid);

                if (!await UserManager.VerifyUserTokenAsync(model.userid, "ResetPassword", model.code))
                {
                    return BadRequest("invalid token");
                }
                else
                {
                    emailConfirmedViewModel.isconfirmed = true;
                    
                    return Ok(emailConfirmedViewModel);
                }

            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            emailConfirmedViewModel.isconfirmed = true;

            return Ok(emailConfirmedViewModel);
        }

        //register user endpoint
        [AllowAnonymous]
        [Route("Register")]
        public async Task<IHttpActionResult> RegisterAsync(RegisterBindingModel model)
        {

            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                var hashKey = Request.Headers;


                if (hashKey.Contains("emailurl"))
                {
                    string callback = hashKey.GetValues("emailurl").First();

                    var user = new ApplicationUser() { UserName = model.email, Email = model.email };

                    //this line is added as created date is null in db and we need created date to show in user detail setting page 
                    user.date_created = DateTime.Now.ToString("dd/MM/yyyy");
                    IdentityResult result = await UserManager.CreateAsync(user, model.password);



                    if (result.Succeeded)
                    {
                        string str = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);

                        //Uri uri = new Uri(Url.Link(callback, new { userId = user.Id, code = str }));
                        var uri = $"{ callback}?userid={user.Id}&code={str}";

                        ConfirmEmailModel confirmEmailModel = new ConfirmEmailModel()
                        {
                            callbackurl = uri,
                            email = model.email,
                            ismobile = false


                        };

                        var ismobile = UtilitiesClass.CheckIfMobile(callback);

                        await AdminDal.SendWelcomeEmailAsync(confirmEmailModel, ismobile);

                        var loginModel = new LoginModel()
                        {
                            email = model.email,
                            password = model.password
                        };

                        var login = await LoginUserAsync(loginModel);
                    }
                    else
                    {
                        return BadRequest(result.Errors.FirstOrDefault());
                    }
                }
                else
                {
                    return BadRequest("invalid return email url");
                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return Ok(_myToken);
        }

        //Call Api to login and generate accesstoken after registration
        private async Task<HttpResponseMessage> CallApiTaskAsync(string apiEndPoint, Dictionary<string, string> model = null)
        {
            var response = new HttpResponseMessage();

            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(ApiUri);

                    client.DefaultRequestHeaders.Accept.Clear();

                    if (_myToken != null)
                    {
                        client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _myToken.accesstoken);
                    }
                    else
                    {
                        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    }

                    response = await client.PostAsync(apiEndPoint, model != null ? new FormUrlEncodedContent(model) : null);
                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return response;
        }

        //Post api/Account/LoginUser
        [AllowAnonymous]
        [HttpPost]
        [Route("Login")]
        public async Task<IHttpActionResult> LoginUserAsync(LoginModel model)
        {
            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest("invalid username or password");
                }
                else
                {
                    var tokenModel = new Dictionary<string, string>
                    {
                        { "grant_type", "password"},
                        { "username", model.email},
                        { "password", model.password},
                    };

                    var response = await CallApiTaskAsync("/authtoken", tokenModel);

                    if (!response.IsSuccessStatusCode)
                    {
                        var errors = await response.Content.ReadAsStringAsync();
                        var errormodel = JsonConvert.DeserializeObject<ErrorModel>(errors);
                        return BadRequest(errormodel.error_description);
                    }

                    _myToken = response.Content.ReadAsAsync<TokenModel>(new[] { new JsonMediaTypeFormatter() }).Result;
                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return Ok(_myToken);
        }

        // POST api/Account/RegisterExternal
        [HttpPost]
        [AllowAnonymous]
        [OverrideAuthentication]
        [HostAuthentication(DefaultAuthenticationTypes.ExternalBearer)]
        [Route("External")]
        public async Task<IHttpActionResult> AddUserExternalAsync(ExternalUserBindingModel model)
        {
            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                var password = Convert.ToString(model.password);

                var user = await UserManager.FindByEmailAsync(model.email);

                if (user == null)
                {
                    var newUser = new ApplicationUser() { UserName = model.email, Email = model.email, EmailConfirmed = true };
                    IdentityResult result = await UserManager.CreateAsync(newUser, password);

                    if (!result.Succeeded)
                    {
                        return GetErrorResult(result);
                    }

                    var loginModel = new LoginModel()
                    {
                        email = model.email,
                        password = password
                    };

                    var login = await LoginUserAsync(loginModel);

                    var info = new UserLoginInfo("google", password);

                    result = await UserManager.AddLoginAsync(newUser.Id, info);

                    if (!result.Succeeded)
                    {
                        return GetErrorResult(result);
                    }
                }
                else
                {
                    var loginModel = new LoginModel()
                    {
                        email = model.email,
                        password = password
                    };

                    var login = await LoginUserAsync(loginModel);

                }

            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return Ok(_myToken);
        }

        //Get api/Account/ConfirmEmail
        [HttpPost]
        [Route("ConfirmEmail", Name = "ConfirmEmailRoute")]
        [AllowAnonymous]
        public async Task<IHttpActionResult> ConfirmEmailAsync(VerifyEmailModel model)
        {
            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            IHttpActionResult result = null;

            var callback = string.Empty;

            bool flag;

            bool isVerified = false;

            var emailConfirmedViewModel = new EmailConfirmedViewModel();

            try
            {
                flag = (string.IsNullOrWhiteSpace(model.userid) ? true : string.IsNullOrWhiteSpace(model.code));

                if (!flag)
                {
                    var header = Request.Headers;

                    if (header.Contains("emailurl"))
                    {
                        callback = header.GetValues("emailurl").First();

                        var isMobile = UtilitiesClass.CheckIfMobile(callback);

                        if (isMobile == true)
                        {
                           isVerified = await AdminDal.VerifyUser(model);
                        }

                        if (isVerified == true)
                        {
                            emailConfirmedViewModel.isconfirmed = true;
                            result = Ok(emailConfirmedViewModel);
                        }
                        else
                        {
                            result = BadRequest("invalid userid or code");
                        }
                    }
                    else
                    {
                        IdentityResult identityResult = await UserManager.ConfirmEmailAsync(model.userid, model.code);

                        IdentityResult identityResult1 = identityResult;

                        identityResult = null;

                        if (!identityResult1.Succeeded)
                        {
                            result = GetErrorResult(identityResult1);
                        }
                        else
                        {
                            emailConfirmedViewModel.isconfirmed = true;
                            result = Ok(emailConfirmedViewModel);
                        }
                    }
                    
                }
                else
                {
                    ModelState.AddModelError("", "User Id and Code are required");
                    result = BadRequest(ModelState);
                }
             
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return result;
        }

        //Get api/Account/resentActivationemail
        [HttpPost]
        [Route("ResendActivationEmail")]
        [AllowAnonymous]
        public async Task<IHttpActionResult> ResendActivationEmailAsync(ResendActivationEmailModel model)
        {
            IHttpActionResult result = null;

            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            var emailSentViewModel = new EmailSentViewModel();

            try
            {
                string callback = string.Empty;

                var user = await UserManager.FindByEmailAsync(model.email);

                if (user != null)
                {
                    var header = Request.Headers;

                    if (header.Contains("emailurl"))
                    {
                        callback = header.GetValues("emailurl").First();

                        if (string.IsNullOrEmpty(callback))
                        {
                            result = BadRequest();
                        }
                        else
                        {
                            string str = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);

                            //Uri uri = new Uri(Url.Link(callback, new { userId = user.Id, code = str }));
                            var uri = $"{ callback}?userid={user.Id}&code={str}";

                            ConfirmEmailModel confirmEmailModel = new ConfirmEmailModel()
                            {
                                callbackurl = uri,
                                email = user.Email,
                                ismobile = false
                            };

                            var isMobile = UtilitiesClass.CheckIfMobile(callback);

                            var emailsent = await AdminDal.SendWelcomeEmailAsync(confirmEmailModel, isMobile);

                            if (emailsent == true)
                            {
                                emailSentViewModel.issent = true;
                                result = Ok(emailSentViewModel);
                            }
                            else
                            {
                                result = BadRequest("Invalid email");
                            }

                        }
                    }
                    else
                    {
                        result = BadRequest();
                    }

                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return result;
        }

        [AllowAnonymous]
        [HttpGet]
        [Route("GetTransactionReferece")]
        public IHttpActionResult GetTransactionReferece(string userid, int planid)
        {
            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            var reference = string.Empty;

            try
            {
                if (string.IsNullOrEmpty(userid))
                {
                    return BadRequest("invalid userid");
                }
                else
                {
                    reference = UserAccountDal.GenerateTransactionreference(userid, planid);

                    if (string.IsNullOrEmpty(reference))
                    {
                        return BadRequest("invalid user or invalid plan");
                    }
                    else
                    {
                        return Ok(reference);
                    }
                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return Ok(reference);
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("AddSubscription")]
        public async Task<IHttpActionResult> AddSubscriptionAsync(UserSubscriptionModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("invalid subscription details");
            }
            else
            {
                var result = await AdminDal.AddUserSubscription(model);
                if (string.IsNullOrEmpty(result))
                {
                    return BadRequest("user already susbscribed to plan");
                }
            }
            return Ok("user subscription added");
        }

        //Dispose resources
        protected override void Dispose(bool disposing)
        {
            if (disposing && _userManager != null)
            {
                _userManager.Dispose();
                _userManager = null;
            }

            base.Dispose(disposing);
        }

        #region Helpers

        private IAuthenticationManager Authentication
        {
            get { return Request.GetOwinContext().Authentication; }
        }

        private IHttpActionResult GetErrorResult(IdentityResult result)
        {
            if (result == null)
            {
                return InternalServerError();
            }

            if (!result.Succeeded)
            {
                if (result.Errors != null)
                {
                    foreach (string error in result.Errors)
                    {
                        ModelState.AddModelError("", error);
                    }
                }

                if (ModelState.IsValid)
                {
                    // No ModelState errors are available to send, so just return an empty BadRequest.
                    return BadRequest();
                }

                return BadRequest(ModelState);
            }

            return null;
        }

        private class ExternalLoginData
        {
            public string LoginProvider { get; set; }
            public string ProviderKey { get; set; }
            public string UserName { get; set; }

            public IList<Claim> GetClaims()
            {
                IList<Claim> claims = new List<Claim>();
                claims.Add(new Claim(ClaimTypes.NameIdentifier, ProviderKey, null, LoginProvider));

                if (UserName != null)
                {
                    claims.Add(new Claim(ClaimTypes.Name, UserName, null, LoginProvider));
                }

                return claims;
            }

            public static ExternalLoginData FromIdentity(ClaimsIdentity identity)
            {
                if (identity == null)
                {
                    return null;
                }

                Claim providerKeyClaim = identity.FindFirst(ClaimTypes.NameIdentifier);

                if (providerKeyClaim == null || String.IsNullOrEmpty(providerKeyClaim.Issuer)
                    || String.IsNullOrEmpty(providerKeyClaim.Value))
                {
                    return null;
                }

                if (providerKeyClaim.Issuer == ClaimsIdentity.DefaultIssuer)
                {
                    return null;
                }

                return new ExternalLoginData
                {
                    LoginProvider = providerKeyClaim.Issuer,
                    ProviderKey = providerKeyClaim.Value,
                    UserName = identity.FindFirstValue(ClaimTypes.Name)
                };
            }
        }

        private static class RandomOAuthStateGenerator
        {
            private static RandomNumberGenerator _random = new RNGCryptoServiceProvider();

            public static string Generate(int strengthInBits)
            {

                const int bitsPerByte = 8;

                if (strengthInBits % bitsPerByte != 0)
                {
                    throw new ArgumentException("strengthInBits must be evenly divisible by 8.", "strengthInBits");
                }

                int strengthInBytes = strengthInBits / bitsPerByte;

                byte[] data = new byte[strengthInBytes];

                _random.GetBytes(data);

                return HttpServerUtility.UrlTokenEncode(data);
            }
        }

        #endregion
    }
}
