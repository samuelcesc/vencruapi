﻿using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon.S3.Transfer;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Sentry;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using VencruApi.Models;
using VencruApi.Models.Classes;
using VencruApi.Models.DalClasses;
using VencruApi.Models.ViewModels;

namespace VencruApi.Controllers
{
    //[AllowAnonymous]
    [Authorize]
    [RoutePrefix("api/onboarding")]
    [EnableCors("*", "*", "*")]
    public class OnboardingController : ApiController
    {
        //declare aws region end point
        private static readonly RegionEndpoint bucketRegion = RegionEndpoint.USEast1;

        //declare instance of Interface amazons3 client
        private static IAmazonS3 s3Client;

        private ApplicationRoleManager _roleManager;

        private ApplicationUserManager _userManager;

        //private instantiation of useraccountdal
        private UserAccountDal _userAccountDal;

        private List<RoleViewModel> businessroles = new List<RoleViewModel>();

        public OnboardingController()
        {
        }

        public OnboardingController(ApplicationRoleManager roleManager, ApplicationUserManager userManager, UserAccountDal userAccountDal)
        {
            RoleManager = roleManager;
            UserManager = userManager;
            UserAccountDal = userAccountDal;
        }

        public ApplicationRoleManager RoleManager
        {
            get
            {
                return _roleManager ?? Request.GetOwinContext().Get<ApplicationRoleManager>();
            }
            private set
            {
                _roleManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? Request.GetOwinContext().Get<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public UserAccountDal UserAccountDal
        {
            get
            {
                return UserAccountDal = new UserAccountDal();
            }
            private set
            {
                _userAccountDal = value;
            }
        }



        //user onboarding after registration
        [HttpPost]
        [Route("Addonboarding")]
        public async Task<IHttpActionResult> AddonboardingAsync(BusinessModel model)
        {
            using (SentrySdk.Init("https://bcee3fe7e5aa41c5bf64b0eadde0e8d9@sentry.io/1291369"))
            {

                string isSuccessful = await UserAccountDal.AddOnBoardingDetails(model);

                if (string.IsNullOrEmpty(isSuccessful))
                {
                    return BadRequest("Invalid user details");
                }
                else
                {

                    var userCurrentBusinessModel = new GetUserCurrentBusinessModel();

                    userCurrentBusinessModel.businessid = Convert.ToInt32(isSuccessful);

                    userCurrentBusinessModel.userid = model.userid;

                    UserAccountDal.AddUserCurrentBusiness(userCurrentBusinessModel);

                    var isroleaddedtobusiness = await AddBusinessRolesAsync(Convert.ToInt32(isSuccessful), businessroles);

                    bool isuseraddedtobusinessrole;

                    if (!isroleaddedtobusiness)
                    { }
                    else
                    {
                        string[] userrole = { "admin" };
                        isuseraddedtobusinessrole = await AddUserToBusinessRoleAsync(Convert.ToInt32(isSuccessful), userrole, model.userid);
                    }

                    //await UserAccountDal.AddUserToBusiness(model.userid, isSuccessful);
                    BusinessViewModel user = UserAccountDal.GetUserBusiness(model.userid, Convert.ToInt32(isSuccessful));

                    return Ok(user);
                }
            }
        }

        //update business
        [Route("UpdateBusiness")]
        [HttpPost]
        public async Task<IHttpActionResult> UpdateBusinessAsync(BusinessModel businessmodel)
        {
            IHttpActionResult response = BadRequest("invalid request");

            if (UtilitiesClass.IsBase64(businessmodel.image))
            {
                var base64file = Convert.FromBase64String(businessmodel.image);

                s3Client = new AmazonS3Client(bucketRegion);

                var fileTransferUtility = new PutObjectRequest()
                {
                    BucketName = $"{UtilitiesClass.businesslogoBucketname}",
                    CannedACL = S3CannedACL.PublicRead,
                    Key = $"{businessmodel.userid}{businessmodel.Id}.jpg",
                };

                using (var ms = new MemoryStream(base64file))
                {
                    fileTransferUtility.InputStream = ms;
                    await s3Client.PutObjectAsync(fileTransferUtility);
                }

                businessmodel.logourl = $"{businessmodel.userid}{businessmodel.Id}.jpg";
            }
            else
            {
                businessmodel.logourl = businessmodel.image;
            }

            var issuccessful = await UserAccountDal.UpdateBusiness(businessmodel);

            if (issuccessful == true)
            {
                var business = UserAccountDal.GetUserBusiness(businessmodel.userid, businessmodel.Id);
                response = Ok(business);
            }
            else
            {
                response = BadRequest("update operation failed");
            }

            return response;
        }

        private async Task<bool> AddBusinessRolesAsync(int businessid, List<RoleViewModel> businessroles)
        {
            businessroles.Add(new RoleViewModel { name = "admin", businessid = businessid });
            businessroles.Add(new RoleViewModel { name = "accountant", businessid = businessid });
            businessroles.Add(new RoleViewModel { name = "staff", businessid = businessid });

            foreach (var item in businessroles)
            {
                var result = await RoleManager.CreateAsync(new ApplicationRole(item.name, item.businessid));
            }

            return true;
        }

        private async Task<bool> AddUserToBusinessRoleAsync(int businessid, string[] rolenames, string userid)
        {
            bool issuccessful = false;

            var appuser = await UserManager.FindByIdAsync(userid);

            if (appuser == null)
            {
                return issuccessful;
            }

            ////get user roles
            //var currentroles = await UserManager.GetRolesAsync(appuser.Id);

            //get user roles in business
            var getbusinessroles = RoleManager.Roles.Where(r => r.businessid == businessid);

            //check if role exists in rolenames parameter
            var rolesnotexist = rolenames.Except(getbusinessroles.Select(x => x.Name)).ToArray();

            if (rolesnotexist.Count() < 0)
            {
                return issuccessful;
            }

            ////clear user roles from businesss
            //IdentityResult removeResult = await UserManager.RemoveFromRolesAsync(appuser.Id, currentroles.ToArray());

            //if (!removeResult.Succeeded)
            //{
            //    return issuccessful;
            //}

            //add user to role
            IdentityResult addResult = await UserManager.AddToRolesAsync(appuser.Id, rolenames);

            if (!addResult.Succeeded)
            {
                return issuccessful;
            }
            else
            {
                issuccessful = true;
            }

            return issuccessful;
        }

        //invite user
        [HttpPost]
        [Route("InviteTeamMember")]
        public async Task<IHttpActionResult> AddTeamMemberAsync(TeamMemberModel model)
        {

            IHttpActionResult result;

            if (!ModelState.IsValid)
            {
                result = BadRequest("invalid team member details");
            }
            else
            {

                var hashKey = Request.Headers;

                string callback = hashKey.GetValues("emailurl").First();

                if (hashKey.Contains("emailurl") && !string.IsNullOrEmpty(callback))
                {
                    var inviteid = await UserAccountDal.AddTeamMember(model);

                    if (inviteid != null)
                    {
                        string str = await UserManager.GenerateUserTokenAsync("inviteteammember", model.userid);

                        var uri = $"{callback}?userid={model.userid}&code={str}&business={model.businessid}&memberemail={model.memberemail}&inviteid={inviteid}";

                        var getbusiness = UserAccountDal.GetUserBusiness(model.userid, model.businessid);

                        var teammemberinviteviewmodel = new TeamMemberInviteViewModel()
                        {
                            callbackurl = uri,
                            firstname = model.firstname,
                            lastname = model.lastname,
                            memberemail = model.memberemail,
                            business = getbusiness,
                            userid = model.userid,

                        };

                        var issuccessful = await AdminDal.SendTeamMemberInvite(teammemberinviteviewmodel);

                        result = Ok("invitation sent");
                    }
                    else
                    {
                        result = BadRequest("invalid invite details");
                    }
                }
                else
                {
                    result = BadRequest("invalid team member details");
                }


            }

            return result;
        }

        //validate team member invite
        [HttpPost]
        [Route("ValidateTeamMemberInvite")]
        public async Task<IHttpActionResult> ValidateTeamMemberInviteAsync(VerifyTeamMemberModel model)
        {
            using (SentrySdk.Init("https://bcee3fe7e5aa41c5bf64b0eadde0e8d9@sentry.io/1291369"))
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                var user = UserManager.FindById(model.userid);

                //code = HttpUtility.UrlEncode(code);

                //var ok = await UserManager.VerifyUserTokenAsync(model.UserId, "ResetPassword", model.code);

                if (!await UserManager.VerifyUserTokenAsync(model.userid, "inviteteammember", model.code))
                {
                    return BadRequest("invalid token");
                }
                else
                {

                    var checkinvite = UserAccountDal.CheckInvite(model);

                    if (checkinvite == true)
                    {

                        await UserAccountDal.VerifyTeamMemberInvite(model);

                        var getinvite = UserAccountDal.GetInvite(model.businessid, model.inviteid, model.userid);

                        var member = UserManager.FindById(model.memberuserid);

                        string[] roles = { getinvite.roles };

                        await AddUserToBusinessRoleAsync(model.businessid, roles, member.Id);

                        await UserAccountDal.AddUserToBusiness(member.Id, model.businessid);

                        return Ok("token is valid");
                    }
                    else
                    {
                        return BadRequest("invalid");
                    }

                }
            }
        }

        //get invite
        [HttpGet]
        [Route("GetTeamMemberInvite")]
        public IHttpActionResult GetTeamMemberInviteAsync(int? businessid, int? inviteid, string userid)
        {
            if (!inviteid.HasValue || !businessid.HasValue || string.IsNullOrEmpty(userid))
            {
                return BadRequest("invalid invite details");
            }
            else
            {
                var invite = UserAccountDal.GetInvite(businessid, inviteid, userid);
                return Ok(invite);
            }
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("GetOtherIndustry")]
        public IHttpActionResult GetOtherIndustry()
        {
            var industries = new List<string>();
            industries.Add("Fashon/Accessories");
            industries.Add("Photography");
            industries.Add("Bakery/Desert");
            industries.Add("Graphic Design");
            industries.Add("Business Consulting");
            industries.Add("Planning");
            industries.Add("Videography");
            industries.Add("Marketing/PR");
            industries.Add("Venue");
            industries.Add("Rentals/Photo Booths");
            industries.Add("Hair/Makeup");
            industries.Add("Catering");
            industries.Add("Fashon/Accessories");
            industries.Add("Interior Design");
            industries.Add("Not Listed");

            return Ok(industries);
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("GetCurrency")]
        public IHttpActionResult GetCurrency()
        {
            var currencies = new List<string>();
            currencies.Add("₦ - Nigerian Naira");
            currencies.Add("$ - US Dollar");
            currencies.Add("KSh - Kenyan Shilling");
            currencies.Add("GHC - Ghanaian Cedi");
            currencies.Add("$ - Canadian Dollar");
            currencies.Add("£ - UK Pound Sterling");

            return Ok(currencies);
        }


        //get invite
        [HttpGet]
        [Route("GetAllTeamMemberInvite")]
        public IHttpActionResult GetAllTeamMemberInviteAsync(int? businessid, string userid)
        {
            if (!businessid.HasValue || string.IsNullOrEmpty(userid))
            {
                return BadRequest("invalid invite details");
            }
            else
            {
                var invite = UserAccountDal.GetAllInvite(businessid, userid);
                return Ok(invite);
            }
        }

        //update invite
        [HttpPost]
        [Route("UpdateTeamMemberInvite")]
        public async Task<IHttpActionResult> UpdateInviteAsync(TeamMemberModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("invalid invite details");
            }
            else
            {
                var issuccessful = await UserAccountDal.UpdateInvite(model);

                if (issuccessful == true)
                {

                    var invite = UserAccountDal.GetInvite(model.businessid, model.id, model.userid);

                    return Ok(invite);

                }
                else
                {
                    return BadRequest("unable to update invite or invalid data");
                }
            }
        }

        //resend invite
        [HttpPost]
        [Route("ResendTeamMemberInvite")]
        public async Task<IHttpActionResult> ResendInviteAsync(TeamMemberModel model)
        {
            IHttpActionResult result;

            var hashKey = Request.Headers;

            string callback = hashKey.GetValues("emailurl").First();

            if (!ModelState.IsValid)
            {
                result = BadRequest("invalid invite details");
            }
            else if (hashKey.Contains("emailurl") && !string.IsNullOrEmpty(callback))
            {
                result = BadRequest("invalid invite details");
            }
            else
            {
                var invite = UserAccountDal.GetInvite(model.businessid, model.id, model.userid);

                if (invite != null)
                {
                    string str = await UserManager.GenerateUserTokenAsync("inviteteammember", invite.userid);

                    var uri = $"{callback}?userid={invite.userid}&code={str}&business={invite.business.id}&memberemail={invite.memberemail}&inviteid={invite.id}";

                    var getbusiness = UserAccountDal.GetUserBusiness(invite.userid, invite.business.id);

                    var teammemberinviteviewmodel = new TeamMemberInviteViewModel()
                    {
                        callbackurl = uri,
                        firstname = invite.firstname,
                        lastname = invite.lastname,
                        memberemail = invite.memberemail,
                        business = getbusiness,
                        userid = invite.userid,

                    };

                    var issuccessful = AdminDal.SendTeamMemberInvite(teammemberinviteviewmodel);

                    result = Ok("invitation sent");
                }
                else
                {
                    result = BadRequest("invalid invite details");
                }

            }

            return result;
        }
        //delete invite
        [HttpPost]
        [Route("DeleteTeamMemberInvite")]
        public async Task<IHttpActionResult> DeleteInviteAsync(TeamMemberModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("invalid invite details");
            }
            else
            {
                var issuccessful = await UserAccountDal.DeleteInvite(model.id, model.businessid, model.userid);
                if (issuccessful == true)
                {
                    var invite = UserAccountDal.GetAllInvite(model.businessid, model.userid);
                    return Ok(invite);

                }
                else
                {
                    return BadRequest("invalid invite details or operation could not be completed");
                }
            }
        }

        [HttpPost]
        [Route("RemoveMemberRole")]
        public async Task<IHttpActionResult> RemoveMemberRoleAsync(TeamRoleModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("invalid member details");
            }
            else
            {
                var user = UserManager.FindByEmail(model.memberemail);

                var issuccessful = await ClearUserRolesAsync(model);

                if (issuccessful == true)
                {
                    return Ok("user role removed");
                }
                else
                {
                    return BadRequest("invalid member data");
                }

            };
        }

        [HttpPost]
        [Route("AddMemberToRole")]
        public async Task<IHttpActionResult> AddMemberRoleAsync(TeamRoleModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("invalid member details");
            }
            else
            {
                var user = UserManager.FindByEmail(model.memberemail);

                string[] roles = { model.rolename };

                var issuccessful = await AddUserToBusinessRoleAsync(model.businessid, roles, user.Id);

                if (issuccessful == true)
                {
                    return Ok("user role removed");
                }
                else
                {
                    return BadRequest("invalid member data");
                }

            };
        }

        public async Task<bool> ClearUserRolesAsync(TeamRoleModel model)
        {

            var user = UserManager.FindByEmail(model.memberemail);

            if (user == null)
            {
                return false;
            }

            var roleinbusiness = RoleManager.Roles.Where(m => m.businessid == model.businessid).SingleOrDefault(m => m.Name == model.rolename);


            //remove user roles from businesss
            var removeResult = await UserManager.RemoveFromRoleAsync(user.Id, roleinbusiness.Id);

            if (!removeResult.Succeeded)
            {
                return false;

            }
            else
            {
                return true;
            }
        }

        //Dispose resources
        protected override void Dispose(bool disposing)
        {
            if (disposing && _userManager != null)
            {
                _userManager.Dispose();
                _userManager = null;
                _roleManager.Dispose();
                _roleManager = null;
            }

            base.Dispose(disposing);
        }
    }
}
