﻿using SharpRaven;
using SharpRaven.Data;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using VencruApi.Models;
using VencruApi.Models.DalClasses;

namespace VencruApi.Controllers
{
    // [AllowAnonymous]
    [Authorize]
    [EnableCors("*", "*", "*")]
    [RoutePrefix("api/ProductList")]
    public class ProductListController : ApiController
    {
        private ProductListDal _productlistDal;

        public ProductListDal ProductListDal
        {
            get
            {
                return ProductListDal = new ProductListDal();
            }
            private set
            {
                _productlistDal = value;
            }
        }

        public ProductListController()
        {
        }

        public ProductListController(ProductListDal productListDal)
        {
            ProductListDal = productListDal;
        }


        //add product end point
        [HttpPost]
        [Route("AddProduct")]
        public async Task<IHttpActionResult> AddProduct(ProductModel model)
        {
            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            var productModel = new ProductViewModel();

            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest("product data is invalid");
                }
                else
                {
                    string id = await ProductListDal.AddProductList(model);

                    if (string.IsNullOrEmpty(id))
                    {
                        return BadRequest("product already exits for this business or user is invalid");
                    }
                    else
                    {
                        productModel = ProductListDal.GetProduct(Convert.ToInt32(model.businessid), Convert.ToInt32(id));

                        AuditTrailDal.CreateAuditTrail(AuditActionType.Create, model.businessid, new ProductModel(), model, model.userid, model.businessid);
                    }
                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return Ok(productModel);
        }

        //get product end point
        [HttpGet]
        [Route("GetProduct")]
        public IHttpActionResult GetProduct(int? BusinessId, int? ProductId)
        {
            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            var product = new ProductViewModel();

            try
            {

                if (!BusinessId.HasValue || !ProductId.HasValue)
                {
                    return BadRequest("business id or product is invalid");
                }
                else
                {

                    product = ProductListDal.GetProduct(BusinessId, ProductId);

                    if (string.IsNullOrEmpty(product.productname))
                    {
                        return BadRequest("product not found");
                    }
                    else
                    {

                    }
                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return Ok(product);
        }

        //get product list end point
        [HttpGet]
        [Route("GetAllProducts")]
        public IHttpActionResult GetAllProducts(int? businessid, int? limit, int? page, string sortby, string sortorder, string searchquery, string fromdate, string todate)
        {

            var product = new ProductListViewModel();

            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            try
            {

                if (!businessid.HasValue)
                {
                    return BadRequest("Business id is null");
                }
                else
                {
                    var getDataModel = new GetDataModel(businessid)
                    {
                        limit = limit,
                        sortby = sortby,
                        sortorder = sortorder,
                        page = page,
                    };

                    product = ProductListDal.GetAllProductLists(getDataModel);

                    if (!string.IsNullOrEmpty(searchquery))
                    {
                        searchquery = searchquery.ToLower();
                        var searchqueryData = product.products.Where(m => m.stocknumber.ToLower() == searchquery || m.description.ToLower() == searchquery || m.productname.ToLower() == searchquery).ToList();
                        product.products = searchqueryData;
                    }
                    else
                    {

                    }

                    if (!string.IsNullOrEmpty(fromdate) && !string.IsNullOrEmpty(todate))
                    {
                        DateTime dtfrom = DateTime.ParseExact(fromdate, "dd/MM/yyyy", null);
                        DateTime dtto = DateTime.ParseExact(todate, "dd/MM/yyyy", null);
                        var searchDateData = product.products.Where(m => m.date_created.Date >= dtfrom.Date && m.date_created.Date <= dtto.Date).ToList();
                        product.products = searchDateData;
                    }

                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return Ok(product);

        }

        //get delete list end point
        [HttpGet]
        [Route("GetDeleteList")]
        public IHttpActionResult GetDeleteList(int? businessid, int? limit, int? page, string sortby, string sortorder, string searchquery, string fromdate, string todate)
        {
            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            var product = new ProductListViewModel();

            try
            {
                if (!businessid.HasValue)
                {
                    return BadRequest("Business id is null");
                }
                else
                {
                    var getDataModel = new GetDataModel(businessid)
                    {
                        limit = limit,
                        sortby = sortby,
                        sortorder = sortorder,
                        page = page,
                    };

                    product = ProductListDal.GetDeleteList(getDataModel);

                    if (!string.IsNullOrEmpty(searchquery))
                    {
                        searchquery = searchquery.ToLower();
                        var searchqueryData = product.products.Where(m => m.stocknumber.ToLower() == searchquery || m.description.ToLower() == searchquery || m.productname.ToLower() == searchquery).ToList();
                        product.products = searchqueryData;
                    }
                    else
                    {

                    }

                    if (!string.IsNullOrEmpty(fromdate) && !string.IsNullOrEmpty(todate))
                    {
                        DateTime dtfrom = DateTime.ParseExact(fromdate, "dd/MM/yyyy", null);
                        DateTime dtto = DateTime.ParseExact(todate, "dd/MM/yyyy", null);
                        var searchDateData = product.products.Where(m => m.date_created >= dtfrom.Date && m.date_created <= dtto).ToList();
                        product.products = searchDateData;
                    }
                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return Ok(product);

        }

        //post update product end point
        [HttpPost]
        [Route("UpdateProduct")]
        public async Task<IHttpActionResult> UpdateProduct(ProductModel model)
        {

            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            var product = ProductListDal.GetProduct(model.businessid, model.id);

            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest("invalid product data");
                }
                else
                {
                    var oldproductdata = ProductListDal.GetProduct(model.businessid, model.id);

                    var updateClient = await ProductListDal.UpdateProductList(model);

                    if (!updateClient)
                    {
                        return BadRequest("invalid client data");
                    }
                    else
                    {
                        product = ProductListDal.GetProduct(model.businessid, model.id);

                        AuditTrailDal.CreateAuditTrail(AuditActionType.Update, model.businessid, oldproductdata, product, model.userid, model.businessid);
                    }
                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return Ok(product);
        }

        //post delete product end point
        [HttpPost]
        [Route("DeleteProduct")]
        public async Task<IHttpActionResult> DeleteProduct(DeleteProuductListModel model)
        {

            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            var productdata = new ProductViewModel();

            var productlist = new ProductListViewModel();

            try
            {

                productdata = ProductListDal.GetProduct(model.businessid, model.productid);

                if (!(model.productid.HasValue) || !model.businessid.HasValue)
                {
                    return BadRequest("product id or business id is null");
                }
                else if (!await ProductListDal.DeleteproductTemp(model.productid, model.businessid))
                {
                    return BadRequest("unable to delete product");
                }
                else
                {
                    var getDataModel = new GetDataModel(model.businessid);

                    productlist = ProductListDal.GetAllProductLists(getDataModel);

                    AuditTrailDal.CreateAuditTrail(AuditActionType.Delete, Convert.ToInt32(model.productid), productdata, new ProductViewModel(), model.userid, (int)model.businessid);
                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return Ok(productdata);
        }

        //post delete multiple products end point
        [HttpPost]
        [Route("DeleteMultipleProducts")]
        public async Task<IHttpActionResult> DeleteMultipleProducts(DeleteMultipleProductModel product)
        {
            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            var productlist = new ProductListViewModel();

            try
            {
                if (product.items.Count() < 1)
                {
                    return BadRequest("product ids or business ids is null");
                }

                else
                {
                    foreach (var item in product.items)
                    {
                        var oldproductproduct = ProductListDal.GetProduct(item.businessid, item.productid);

                        await ProductListDal.DeleteproductTemp(item.productid, item.businessid);

                        AuditTrailDal.CreateAuditTrail(AuditActionType.Delete, (int)item.productid, oldproductproduct, new ClientViewModel(), product.userid, (int)item.businessid);

                    }

                    var businessId = product.items.FirstOrDefault().businessid;

                    var getDataModel = new GetDataModel(businessId);

                    productlist = ProductListDal.GetAllProductLists(getDataModel);
                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return Ok(productlist);

        }

        //post restore product end point
        [HttpPost]
        [Route("Restoreproduct")]
        public async Task<IHttpActionResult> Restoreproduct(DeleteProuductListModel product)
        {
            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            var productlist = new ProductListViewModel();

            var oldproductdata = new ProductViewModel();

            try
            {
                oldproductdata = ProductListDal.GetProduct(product.businessid, product.productid);

                if (product.businessid == 0 || product.productid == 0)
                {
                    return BadRequest("product id or business id is null");
                }
                else if (!await ProductListDal.RestoreProduct(product.productid, product.businessid))
                {
                    return BadRequest("unable to restore product");
                }
                else
                {
                    var getDataModel = new GetDataModel(product.businessid);

                    productlist = ProductListDal.GetAllProductLists(getDataModel);

                    var newproductdatadata = productlist.products.SingleOrDefault(m => m.id == product.productid);


                    AuditTrailDal.CreateAuditTrail(AuditActionType.Restore, (int)product.productid, oldproductdata, newproductdatadata, product.userid,(int)product.businessid);

                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return Ok(productlist);

        }

        //post restore multiple product end point
        [HttpPost]
        [Route("RestoreMultipleproduct")]
        public async Task<IHttpActionResult> RestoreMultipleproduct(DeleteMultipleProductModel product)
        {
            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            var productlist = new ProductListViewModel();

            try
            {
                if (product.items.Count() <= 0)
                {
                    return BadRequest("product id or business id is null");
                }
                else
                {
                    foreach (var item in product.items)
                    {
                        var oldproductdata = ProductListDal.GetProduct(item.businessid, item.businessid);

                        await ProductListDal.RestoreProduct(item.productid, item.businessid);

                        var newproductdata = ProductListDal.GetProduct(item.businessid, item.businessid);

                        AuditTrailDal.CreateAuditTrail(AuditActionType.Restore, Convert.ToInt32(item.productid), oldproductdata, newproductdata, product.userid, (int)item.businessid);
                    }

                    var getDataModel = new GetDataModel(product.items.FirstOrDefault().businessid);

                    productlist = ProductListDal.GetAllProductLists(getDataModel);

                }

            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return Ok(productlist);

        }

        //get product summarydata end point
        [HttpGet]
        [Route("GetProductSummaryData")]
        public IHttpActionResult GetProductSummaryData(int? businessid, string userid)
        {

            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            var productsummary = new ProductSummaryViewModel();

            try
            {
                if (!businessid.HasValue)
                {
                    return BadRequest("Business id is null");
                }
                else
                {

                    productsummary = ProductListDal.GetProductSummaryData(businessid, userid);


                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return Ok(productsummary);

        }
    }
}
