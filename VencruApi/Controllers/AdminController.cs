﻿using SharpRaven;
using SharpRaven.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using VencruApi.Models;
using VencruApi.Models.DalClasses;
using VencruApi.Models.ViewModels;

namespace VencruApi.Controllers
{
   // [AllowAnonymous]
    [Authorize]
    [EnableCors("*", "*", "*")]
    [RoutePrefix("api/Admin")]
    public class AdminController : ApiController
    {

        //private instantiation of invoicedals
        private AdminDal _adminDal;

        public AdminController()
        {

        }

        public AdminController(AdminDal adminDal)
        {
            AdminDal = adminDal;
        }
        
        public AdminDal AdminDal
        {
            get
            {
                return _adminDal = new AdminDal();
            }
            private set
            {
                _adminDal = value;
            }
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("Waitlist")]
        public async Task<IHttpActionResult> WaitlistAsync(WaitListModel waitList)
        {
            bool waitlistResult = false;

            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            try
            {
                if (ModelState.IsValid)
                {
                    var checkWaitList = AdminDal.CheckWaitList(waitList);
                    if (checkWaitList.Count() > 0)
                    {
                        return Ok("Email added to waitlist");
                    }
                    else
                    {
                        waitlistResult = AdminDal.AddWaitList(waitList);
                        if (waitlistResult == true)
                        {
                            await AdminDal.SendWaitingListWelcomeEmailAsync(waitList);

                        }
                        else
                        {
                            return BadRequest("Invalid email");
                        }
                    }
                }
                else
                {
                    return BadRequest("Invalid email");
                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return Ok("Email added to waitlist");
        }

        [HttpPost]
        [Route("AddPlan")]
        public async Task<IHttpActionResult> AddPlanAsync(PlanModel model)
        {
            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            var planViewModel = new PlanViewModel();

            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest("invalid plan details");
                }
                else
                {
                    var id = await AdminDal.AddPlan(model);
                    if (string.IsNullOrEmpty(id))
                    {
                        return BadRequest("unable to create plan or plan already exists");
                    }
                    else
                    {
                        planViewModel = AdminDal.GetPlan(Convert.ToInt32(id));
                    }
                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return Ok(planViewModel);

        }

        [HttpGet]
        [Route("GetPlan")]
        public IHttpActionResult GetPlan(string planid)
        {
            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            var planViewModel = new PlanViewModel();

            try
            {
                if (string.IsNullOrEmpty(planid))
                {
                    return NotFound();
                }
                else
                {
                    planViewModel = AdminDal.GetPlan(Convert.ToInt32(planid));
                    if (string.IsNullOrEmpty(planViewModel.name))
                    {
                        return NotFound();

                    }
                   
                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return Ok(planViewModel);
        }

        [HttpGet]
        [Route("GetAllPlan")]
        public IHttpActionResult GetAllPlan()
        {
            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            var planViewModel = new List<PlanViewModel>();

            try
            {
                 planViewModel = AdminDal.GetAllPlans();
                if (planViewModel.Count() <= 0)
                {
                    return NotFound();
                }
               
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return Ok(planViewModel);
        }

        [Route("DeletePlan")]
        [HttpPost]
        public async Task<IHttpActionResult> DeletePlanAsync(int? planid)
        {

            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            var planViewModel = new List<PlanViewModel>();

            try
            {
                if (!planid.HasValue)
                {
                    return BadRequest("invalid plan");
                }
                else
                {
                    var issuucessful = await AdminDal.DeletePlan((int)planid);

                    if (issuucessful == true)
                    {
                        planViewModel = AdminDal.GetAllPlans();

                    }
                    else
                    {
                        return BadRequest("unable to delete plan");
                    }

                }

            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return Ok(planViewModel);
        }

        [HttpPost]
        [Route("AddFeedback")]
        public async Task<IHttpActionResult> AddFeedback(FeedbackModel model)
        {

            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest("invalid feedback data");

                }
                else
                {
                    var id = await AdminDal.AddFeedback(model);
                    if (string.IsNullOrEmpty(id))
                    {
                        return BadRequest("unable to add feedback");
                    }
                    
                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return Ok("feedback sent");

        }

        [HttpGet]
        [Route("GetFeedback")]
        public IHttpActionResult GetFeedback(string id)
        {
            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            var feedbackModel = new FeedbackModel();

            try
            {
                if (string.IsNullOrEmpty(id))
                {
                    return NotFound();
                }
                else
                {
                     feedbackModel = AdminDal.GetFeedback(id);
                    if (string.IsNullOrEmpty(feedbackModel.emailaddress))
                    {
                        return NotFound();

                    }
                   
                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return Ok(feedbackModel);

        }

        [HttpGet]
        [Route("GetAllFeedback")]
        public IHttpActionResult GetAllFeedback()
        {
            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            var feedbackModel = new List<FeedbackModel>();

            try
            {
                feedbackModel = AdminDal.GetAllFeedback();

                if (feedbackModel.Count() <= 0)
                {
                    return NotFound();
                }
               
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return Ok(feedbackModel);
        }

        [HttpPost]
        [Route("AddReview")]
        public async Task<IHttpActionResult> AddReview(ReviewsModel model)
        {
            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest("invalid reviews data");

                }
                else
                {
                    var id = await AdminDal.AddReview(model);
                    if (string.IsNullOrEmpty(id))
                    {
                        return BadRequest("unable to add review");
                    }
                    
                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return Ok("review added");
        }

        [HttpPost]
        [Route("ContactUs")]
        [AllowAnonymous]
        public async Task<IHttpActionResult> ContactUsAsync(ContactUsModel model)
        {
            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest("invalid contact us data");

                }
                else
                {
                    var id = await AdminDal.AddContactUs(model);
                    if (string.IsNullOrEmpty(id))
                    {
                        return BadRequest("unable to send contact us data");
                    }

                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return Ok("Contact us sent");

        }

        [HttpPost]
        [Route("ApproveReview")]
        public async Task<IHttpActionResult> ApproveReviewAsync(int? reviewid)
        {
            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            try
            {
                if (!reviewid.HasValue)
                {
                    return BadRequest("invalid review details");
                }
                else
                {
                    var issuccessful = await AdminDal.ApproveReview((int)reviewid);

                    if (!issuccessful)
                    {
                        return BadRequest("review not approved");

                    }
                    else
                    {
                    }
                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return Ok("review approved");

        }

        [HttpGet]
        [Route("GetReview")]
        public IHttpActionResult GetReview(string id)
        {
            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            var reviewModel = new ReviewsModel();

            try
            {
                if (string.IsNullOrEmpty(id))
                {
                    return NotFound();
                }
                else
                {
                    reviewModel = AdminDal.GetReview(id);

                    if (string.IsNullOrEmpty(reviewModel.reviewcomment))
                    {
                        return NotFound();

                    }
                   
                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }
            return Ok(reviewModel);

        }

        [HttpGet]
        [Route("GetAllReviews")]
        public IHttpActionResult GetAllReview()
        {
            var reviewModel = new List<ReviewsModel>();

            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            try
            {
                 reviewModel = AdminDal.GetAllReviews();

               
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return Ok(reviewModel);
        }

        [HttpGet]
        [Route("GetApprovedReviews")]
        public IHttpActionResult GetApprovedReviews()
        {
            var reviewModel = new List<ReviewsModel>();

            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            try
            {
                 reviewModel = AdminDal.GetApprovedReviews();

              
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return Ok(reviewModel);

        }

        [HttpPost]
        [Route("AddpromoCode")]
        public async Task<IHttpActionResult> AddPromoCodeAsync(PromoCodeModel model)
        {

            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            var promoCodeDetails = new PromoCodeModel();

            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest("invalid promocode details");
                }
                else
                {
                    var result = await AdminDal.AddPromoCode(model);
                    if (!string.IsNullOrEmpty(result))
                    {
                        promoCodeDetails = AdminDal.GetPromoCode(model.created_by, model.code, Convert.ToInt32(result));
                    }
                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return Ok(promoCodeDetails);
        }

        [HttpPost]
        [Route("ApplypromoCode")]
        public async Task<IHttpActionResult> ApplyPromoCodeAsync(PromoCodeUserModel model)
        {

            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest("invalid promocode or user details");
                }
                else
                {
                    var result = await AdminDal.ApplyPromoCode(model);

                    if (result == false)
                    {
                        return BadRequest("promocode limit reached or invalid promocode");
                    }
                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return Ok("promocode details saved");

        }

        [HttpGet]
        [Route("GetPromoCode")]
        public IHttpActionResult GetPromoCode(string userid, string code, int codeid)
        {
            var PromoCodeModel = new PromoCodeModel();

            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            try
            {
                PromoCodeModel = AdminDal.GetPromoCode(userid, code, codeid);
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return Ok(PromoCodeModel);

        }

        [HttpGet]
        [Route("GetAllUsers")]
        public IHttpActionResult GetAllUsersForAdmin(string code)
        {
            var users = new List<ApplicationUser>();
            if (code != "ebukam")
            {
                return BadRequest();
            }
            else
            {
                using (var dbContext = new ApplicationDbContext())
                {
                    users = dbContext.Users.Select(m => m).ToList();
                }

                



                return Ok(users);
            }
        }

    }
}
