﻿using SharpRaven;
using SharpRaven.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Mvc;
using VencruApi.Models;
using VencruApi.Models.DalClasses;

namespace VencruApi.Controllers
{
    [EnableCors("*", "*", "*")]
    [System.Web.Http.RoutePrefix("api/AuditTrail")]
    [System.Web.Http.AllowAnonymous]
    public class AuditTrailController : ApiController
    {


        private AuditTrailDal _auditTrailDal;

        public AuditTrailController()
        {
        }

        public AuditTrailController(AuditTrailDal dal)
        {
            AuditTrailDal = dal;
        }

        public AuditTrailDal AuditTrailDal
        {
            get
            {
                return AuditTrailDal = new AuditTrailDal();
            }
            private set
            {
                _auditTrailDal = value;
            }
        }
        // GET: AuditTrail
     
        [System.Web.Http.HttpGet]
        [System.Web.Mvc.Route("GetAllAudit")]
        public IHttpActionResult GetAllAudit(int? businessid, int? limit, int? page, string sortby)
        {
            List<AuditTrailModel> audittrail = new List<AuditTrailModel>();
            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");
            try
            {
                if (!businessid.HasValue)
                {
                    return BadRequest("Business id is null");
                }
                else
                {
                    audittrail=AuditTrailDal.GetAllAudit(businessid.Value, sortby, page, limit);
                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return Ok(audittrail);

        }





    }
}