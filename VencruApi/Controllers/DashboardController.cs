﻿using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using VencruApi.Models;
using VencruApi.Models.DalClasses;
using VencruApi.Models.ViewModels;

namespace VencruApi.Controllers
{
    
    //[Authorize]
    [EnableCors("*", "*", "*")]
    [RoutePrefix("api/Dashboard")]
    public class DashboardController : ApiController
    {
        private DashboardDal _dashboardDal;

        public DashboardController()
        {
        }

        public DashboardController(DashboardDal dal)
        {
            DashboardDal = dal;
        }

        public DashboardDal DashboardDal
        {
            get
            {
                return DashboardDal = new DashboardDal();
            }
            private set
            {
                _dashboardDal = value;
            }
        }

        [HttpGet]
        [Route("GetBusinessSummarySalesBreakdown")]
        public IHttpActionResult GetBusinessSummary(string userid, int? businessid, string sortby)
        {
            if (!businessid.HasValue || string.IsNullOrEmpty(userid))
            {
                return BadRequest("invalid data");
            }
            else
            {
                if(string.IsNullOrEmpty(sortby) || sortby == "date_created")
                {
                    sortby = "thismonth";
                }
                var businesssummary = DashboardDal.GetBusinessSummary(userid, (int)businessid, sortby);

                return Ok(businesssummary);
            }
        }

        [HttpGet]
        [Route("GetExpenseBreakDown")]
        public IHttpActionResult GetExpenseBreakDown(string userid, int businessid, string sortby)
        {
            var expenseummary = DashboardDal.GetExpenseBreakDown(userid, businessid, sortby);

            return Ok(expenseummary);
        }

        [HttpGet]
        [Route("GetIncomeSources")]
        public IHttpActionResult GetIncomeSources(string userid, int businessid, string sortby)
        {
            var incomesources = DashboardDal.GetIncomeSources(userid, businessid, sortby);

            return Ok(incomesources);
        }

        [HttpGet]
        [Route("GetProfit")]
        public IHttpActionResult GetProfit(string userid, int businessid, string sortby)
        {
            var profitsummary = DashboardDal.ToTalProfit(userid, businessid, sortby);

            return Ok(profitsummary);
        }



        [HttpGet]
        [Route("GetGoal")]
        public IHttpActionResult GetGoal(string userid, int businessid)
        {
            var BusinessGoal = DashboardDal.GetGoal(userid, businessid);
            return Ok(BusinessGoal);
        }

        [HttpPost]
        [Route("AddGoal")]
        public async Task<IHttpActionResult> AddGoal(AddGoalViewModel model)
        {
            GoalViewModel returnValue = new GoalViewModel();
            if (!ModelState.IsValid)
            {
                return BadRequest("invalid Goal details");
            }

            if (!await DashboardDal.AddGoal(model))
            {
                return BadRequest("unable to update invoice");
            }
            else
            {
                returnValue = DashboardDal.GetGoal(model.userid, model.businessid);
            }
            return Ok(returnValue);
        }


        [HttpPost]
        [Route("UpdateGoal")]
        public async Task<IHttpActionResult> updateGoal(UpdateGoalViewModel model)
        {

           
            if (!ModelState.IsValid)
            {
                return BadRequest("invalid Goal details");
            }
            
            GoalViewModel returnValue = new GoalViewModel();
            if (!ModelState.IsValid)
            {
                return BadRequest("invalid Goal details");
            }

            var oldGoal = DashboardDal.GetGoal(model.userid, model.businessid);

            if (!await DashboardDal.updateGoal(model))
            {
                return BadRequest("unable to update invoice");
            }
            else
            {

                returnValue = DashboardDal.GetGoal(model.userid, model.businessid);
                AuditTrailDal.CreateAuditTrail(AuditActionType.Update, model.businessid, oldGoal, returnValue, model.userid, model.businessid);

            }
            return Ok(returnValue);

        }

    }
}
