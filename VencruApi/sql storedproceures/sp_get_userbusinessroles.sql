IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[sp_get_userbusinessroles]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[sp_get_userbusinessroles]
GO
CREATE PROCEDURE [dbo].[sp_get_userbusinessroles]
@Userid nvarchar (240),
@BusinessId int
AS
--26092018 SE created
if exists(select * from AspNetUsers where id = @Userid)
BEGIN
SET NOCOUNT ON;
SELECT * from AspNetRoles as r inner join AspNetUserRoles as u on u.RoleId = r.Id where r.businessid = @BusinessId and u.UserId = @Userid
	END
ELSE
BEGIN
SET NOCOUNT ON;
END