use [vencrudb]
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[sp_get_clienthistory]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[sp_get_clienthistory]
GO
CREATE PROCEDURE [dbo].[sp_get_clienthistory]
@Sortorder VARCHAR(4)    = 'desc',
@Businessid int,
@Clientid int,
@Limit int = 10,
@Page int = 1
AS
--05102018 SE created
BEGIN
  SET NOCOUNT ON;
 
  WITH x AS
  (
    SELECT i.Id as invoiceid, i.clientid, i.invoicenumber,i.amountdue, i.invoicestatus,
	p.date_created as date_added, p.paidwith, p.amount, @Page as pagenumber, @Limit as limit from InvoiceModels i 
	inner join InvoicePaymentModels p on i.Id = p.invoiceid where i.clientid = @Clientid and i.businessid = @Businessid
  ORDER BY i.invoicenumber
   DESC OFFSET @Limit * (@Page - 1) ROWS
      FETCH NEXT @Limit ROWS ONLY
  ),
  TotalPayments AS 
	(
	 select count(x.invoiceid) as totalpayments from x 
	),
	TotalOutstanding AS 
	(
	 select sum(x.amount) - sum(x.amountdue) as totaloutstanding from x
	),
	Overdue AS 
	(
	 select sum(x.amountdue) - sum(x.amount) as overdue from x where x.invoicestatus = 'overdue' 
	),
	Due AS 
	(
	 select sum(x.amountdue)- sum(x.amount) as due from x where x.invoicestatus = 'due' 
	),
	Draft AS 
	(
	 select sum(x.amountdue)- sum(x.amount) as draft from x where x.invoicestatus = 'draft' 
	)
SELECT *
from x as t, TotalPayments, TotalOutstanding, Due, Overdue, Draft where exists (select *, CONVERT(varchar(10), CONVERT(date, date_created, 106), 103) from InvoiceModels as c where c.Id = t.invoiceid) 
OPTION (RECOMPILE)
END