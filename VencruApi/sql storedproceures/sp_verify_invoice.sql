IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[sp_verify_invoice]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[sp_verify_invoice]
GO
CREATE PROCEDURE [dbo].[sp_verify_invoice]
@code nvarchar(240),
@businessid int,
@invoiceid int
AS
--26092018 SE created

BEGIN
SELECT count(*) as count FROM InvoiceModels WHERE id = @invoiceid and invoiceguid = @code and businessid = @businessid;
 END
GO