IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[sp_get_promocodecount]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[sp_get_promocodecount]
GO
CREATE PROCEDURE [dbo].[sp_get_promocodecount]
@promocodeid int
AS
--26092018 SE created
Begin
select count(ApplicationUser_Id) as count from PromoCodeModelApplicationUsers where PromoCodeModel_id = @promocodeid
End
GO