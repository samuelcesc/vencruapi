IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[sp_update_invite]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[sp_update_invite]
GO
CREATE PROCEDURE [dbo].[sp_update_invite]
@businessid int,
@userId nvarchar(200),
@firstname nvarchar(100),
@lastname nvarchar(200),
@memberemail nvarchar(100),
@roles nvarchar(100), 
@status bit,
@Id int
AS
--26092018 SE created

if exists(select * from TeamMemberModels where businessid = @businessid and id = @Id)
BEGIN
update TeamMemberModels set firstname = @firstname,
lastname = @lastname, memberemail = @memberemail, roles = @roles where id = @Id and businessid = @businessid
END
ELSE
BEGIN
SET NOCOUNT ON;

END
GO