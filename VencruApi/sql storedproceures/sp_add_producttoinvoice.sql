IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[sp_add_producttoinvoice]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[sp_add_producttoinvoice]
GO
CREATE PROCEDURE [dbo].[sp_add_producttoinvoice]
@productid int,
@invoiceid int
AS
--26092018 SE created

BEGIN
SET NOCOUNT ON;
INSERT INTO [dbo].[productmodelinvoicemodels]
	VALUES (@productid, @invoiceid)
END
GO