IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[sp_update_item]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[sp_update_item]
GO
CREATE PROCEDURE [dbo].[sp_update_item]
@description nvarchar(100),
@price int,
@quantity int,
@amount int,
@invoiceid int,
@Id int
AS
--26092018 SE created
if exists(select * from ItemsModels where invoiceid = @invoiceid and Id = @Id)
BEGIN
update ItemsModels set description = @description, price = @price, quantity = @quantity, amount = @amount where invoiceid = @invoiceid and id = @Id
	END
ELSE
BEGIN
SET NOCOUNT ON;

END
GO