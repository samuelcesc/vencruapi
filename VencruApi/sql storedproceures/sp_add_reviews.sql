IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[sp_add_review]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[sp_add_review]
GO
CREATE PROCEDURE [dbo].[sp_add_review]
@comment int,
@fullname nvarchar(100),
@isapproved bit,
@Id int output
AS
--26092018 SE created

BEGIN
SET NOCOUNT ON;
INSERT INTO [dbo].[ReviewModels]
	VALUES (@fullname, @comment, @isapproved, GETDATE())
    SELECT @Id = SCOPE_IDENTITY()
END
GO