IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[sp_update_payment]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[sp_update_payment]
GO
CREATE PROCEDURE [dbo].[sp_update_payment]
@amount int,
@date_paid nvarchar(100),
@invoiceid int,
@paidwith nvarchar(240) = null,
@Id int
AS
if exists(select * from InvoicePaymentModels where id = @Id and @invoiceid = @invoiceid)
BEGIN
SET NOCOUNT ON;
update InvoicePaymentModels set amount = @amount, date_paid = @date_paid, paidwith = @paidwith where invoiceid = @invoiceid and id = @Id
END
GO