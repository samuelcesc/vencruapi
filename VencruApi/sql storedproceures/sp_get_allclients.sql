USE [aspnet-VencruApi-20180903084237]
GO
/****** Object:  StoredProcedure [dbo].[sp_get_allclients]    Script Date: 2018-10-01 12:36:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[sp_get_allclients]
@Businessid int,
@Page int = 0,
@Limit int = 5,
@Searchquery nvarchar(200) = null,
@Fromdate nvarchar(200) = null,
@Todate nvarchar(200) = null
AS
--27092018 SE created
BEGIN
select * from ClientsModels where BusinessId = @Businessid 
	order by date_created ASC offset @Page row fetch next @Limit Rows Only
	END
GO

