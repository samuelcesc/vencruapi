IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[sp_add_subscription]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[sp_add_subscription]
GO
CREATE PROCEDURE [dbo].[sp_add_subscription]
@date_expire datetime,
@istrial bit,
@planid int, 
@referencenumber nvarchar(250),
@status bit,
@trialdays int,
@userid nvarchar(250),
@promocode nvarchar(100),
@id int out
AS
if exists(select * from SubscriptionModels where userid = @userid and status = 'true' and planid = @planid)
BEGIN
select @id = SCOPE_IDENTITY()
END
else
Begin
insert into SubscriptionModels values(@userid, GETDATE(), @status, @planid, @referencenumber, @date_expire, @istrial, @trialdays, @promocode)
select @id = SCOPE_IDENTITY()
end
GO