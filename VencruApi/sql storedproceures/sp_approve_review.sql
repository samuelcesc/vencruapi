IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[sp_approve_review]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[sp_approve_review]
GO
CREATE PROCEDURE [dbo].[sp_approve_review]
@id int
AS
--26092018 SE created

BEGIN
SET NOCOUNT ON;
update ReviewsModels set isapproved = 'true' where id = @id
End
GO