IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[sp_delete_payment]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[sp_delete_payment]
GO
CREATE PROCEDURE [dbo].[sp_delete_payment]
@paymentid int,
@invoiceid int

AS
if exists(select * from InvoicePaymentModels where id = @paymentid and @invoiceid = @invoiceid)
BEGIN
SET NOCOUNT ON;
delete InvoicePaymentModels where id = @paymentid and invoiceid = @invoiceid
END
GO