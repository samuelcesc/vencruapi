IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[sp_apply_promocode]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[sp_apply_promocode]
GO
CREATE PROCEDURE [dbo].[sp_apply_promocode]
@promocodeid int,
@userid nvarchar(250)
AS
--26092018 SE created
Begin
insert into PromoCodeModelApplicationUsers values(@promocodeid, @userid)
End
GO