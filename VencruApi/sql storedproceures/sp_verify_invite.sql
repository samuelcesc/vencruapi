IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[sp_verify_invite]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[sp_verify_invite]
GO
CREATE PROCEDURE [dbo].[sp_verify_invite]
@Id int, 
@userid nvarchar(100), 
@memberemail nvarchar(100)
AS
--26092018 SE created
if exists(select * from TeamMemberModels where id = @Id and userid = @userid and memberemail = @memberemail)
Begin
update TeamMemberModels set status = 'true' where id = @Id and userid = @userid and memberemail = @memberemail
End
Else
begin
Set NoCount on
End
GO