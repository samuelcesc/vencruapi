IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[sp_update_business]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[sp_update_business]
GO
CREATE PROCEDURE [dbo].[sp_update_business]
@id int,
@Userid nvarchar (240),
@Address nvarchar (240) = null,
@Country nvarchar (240) = null,
@City nvarchar (240) = null,
@State nvarchar (240) = null,
@Phonenumber nvarchar (240),
@Isincorporated tinyint,
@Industry nvarchar (240) = null,
@Service nvarchar (240) = null,
@Employeesize nvarchar (240) = null,
@ImgUrl nvarchar (240) = null,
@Currency nvarchar (240) = null,
@Onlinepayment tinyint,
@domain nvarchar(200) = null,
@Facebookurl nvarchar (240) = null,
@TwitterUrl nvarchar (240) = null,
@LinkdinUrl nvarchar(200) = null,
@InstagramUrl nvarchar(200) = null,
@companyname nvarchar(200) = null,
@Vatnumber nvarchar(200) = null,
@Tax int
AS
--15102018 SE created

if exists(select * from BusinessModels where Id = @id and userid = @Userid)
BEGIN
update BusinessModels set address = @Address, country = @Country, city = @City, state = @State, phonenumber = @Phonenumber,
isincorporated = @Isincorporated, industry = @Industry, service = @Service, employeesize = @Employeesize, logourl = @ImgUrl,
currency = @Currency, companyname = @companyname, onlinepayment = @Onlinepayment, domain = @domain, facebookurl = @Facebookurl, twitterurl = @TwitterUrl,
linkdinurl = @LinkdinUrl, instagramurl = @InstagramUrl where id = @id and userid = @Userid
END
ELSE
BEGIN
SET NOCOUNT ON;
return null
END
GO