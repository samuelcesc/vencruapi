IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[sp_add_clientnote]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[sp_add_clientnote]
GO
CREATE PROCEDURE [dbo].[sp_add_clientnote]
@clientid int,
@businessid int,
@note nvarchar(max)
AS
--26092018 SE created
BEGIN
SET NOCOUNT ON;
update  [dbo].[ClientsModels]
	set note = @note where businessid = @businessid and id = @clientid
END
GO