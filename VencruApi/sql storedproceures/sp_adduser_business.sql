IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[sp_adduser_busines]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[sp_adduser_busines]
GO
CREATE PROCEDURE [dbo].[sp_adduser_busines]
@userId nvarchar (240),
@businessId int
AS
--26092018 SE created

BEGIN
	INSERT INTO [ApplicationUserBusinessModels] (ApplicationUser_Id, BusinessModel_Id) 
	VALUES (@userId, @businessId)

END