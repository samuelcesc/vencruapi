IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[sp_get_client]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[sp_get_client]
GO
CREATE PROCEDURE [dbo].[sp_get_client]
@Userid nvarchar(250),
@Clientid int,
@Businessid int

AS
--27092018 SE created
BEGIN
	select * from ClientsModels where Id = @Clientid and (BusinessId = @Businessid and userid = @Userid)

END
GO