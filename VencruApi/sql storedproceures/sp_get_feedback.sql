IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[sp_get_feedback]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[sp_get_feedback]
GO
CREATE PROCEDURE [dbo].[sp_get_feedback]
@Id int
AS
--26092018 SE created

BEGIN
SET NOCOUNT ON;
select * from feedbackmodels where id = @Id
End
GO