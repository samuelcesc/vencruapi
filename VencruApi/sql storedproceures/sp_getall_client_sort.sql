USE [aspnet-VencruApi-20180903084237]
GO
/****** Object:  StoredProcedure [dbo].[sp_get_allclients]    Script Date: 2018-10-03 1:41:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[sp_get_allclients]
 @Sortby    NVARCHAR(128) = 'date_created',
  @Sortorder VARCHAR(4)    = 'DESC',
  @Businessid int,
  --@Searchquery nvarchar(200) = null,
  --@Fromdate nvarchar(200) = null,
  --@Todate nvarchar(200) = null,
  @Limit int = 10,
  @Page int = 1
AS
--27092018 SE created
BEGIN
;WITH x AS
  (
    SELECT *,
      rn = ROW_NUMBER() OVER (
        ORDER BY CASE @Sortby 
          WHEN 'companyname'     THEN CompanyName
          WHEN 'date_created'   THEN  date_created
          WHEN 'firstname'        THEN Firstname
          WHEN 'lastname'   THEN Lastname 
      END
      ) * CASE @Sortorder WHEN 'DESC' THEN 1 ELSE -1 END
    FROM dbo.ClientsModels where BusinessId = @Businessid and isdeleted = 'false' order by rn   OFFSET @Limit * (@Page - 1) ROWS
      FETCH NEXT @Limit ROWS ONLY
  ),
  TotalClients AS 
	(
	 select count(Id) as clienttotal from ClientsModels WHERE (BusinessId = @Businessid and isdeleted = 'false')
	)
SELECT *
from ClientsModels as t, TotalClients where exists (select * from x where t.Id = x.Id) 
OPTION (RECOMPILE)
	END
