IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[sp_restore_client]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[sp_restore_client]
GO
CREATE PROCEDURE [dbo].[sp_restore_client]
@ClientId nvarchar (240),
@BusinessId int

AS
--30092018 SE created

if exists(select * from ClientsModels where id = @ClientId and BusinessId = @BusinessId and isdeleted = 'true')
Begin
update ClientsModels set isdeleted = 'false' where id = @ClientId and BusinessId = @BusinessId
END
else
Begin
return null
End
GO