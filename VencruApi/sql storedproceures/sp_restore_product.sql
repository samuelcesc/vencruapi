IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[sp_restore_product]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[sp_restore_product]
GO
CREATE PROCEDURE [dbo].[sp_restore_product]
@ProductId nvarchar (240),
@BusinessId int

AS
--30092018 SE created
if exists(select * from ProductModels where id = @ProductId and BusinessId = @BusinessId and isdeleted = 'true')
Begin
update ProductModels set isdeleted = 'false' where id = @ProductId and BusinessId = @BusinessId
END
else
Begin
return null
End