IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[sp_get_approvedreviews]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[sp_get_approvedreviews]
GO
CREATE PROCEDURE [dbo].[sp_get_approvedreviews]
@Id int
AS
--26092018 SE created

BEGIN
SET NOCOUNT ON;
select * from ReviewsModels where isapproved = 'true'
End
GO