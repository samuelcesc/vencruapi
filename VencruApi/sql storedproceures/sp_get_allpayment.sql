IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[sp_get_allpayment]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[sp_get_allpayment]
GO
CREATE PROCEDURE [dbo].[sp_get_allpayment]
@Sortby    NVARCHAR(120) = 'date_created',
@Sortorder VARCHAR(4)    = 'desc',
@paymentid int,
@Limit int = 10,
@Page int = 1,
@Listtype int = 0
AS
--05102018 SE created
BEGIN
  SET NOCOUNT ON;
 
  ;WITH x AS
  (
    SELECT *, @Page as pagenumber,@Limit as limit
  FROM dbo.InvoicePaymentModels where invoiceid = @paymentid 
  ORDER BY 
    CASE WHEN @Sortorder = 'asc' THEN
      CASE @Sortby 
        WHEN 'paidthwith'     THEN date_paid
         WHEN 'date_created'   THEN CONVERT(CHAR(19), date_created, 120) 
      END
    END ASC,
    CASE WHEN @Sortorder = 'desc' THEN
     CASE @Sortby 
        WHEN 'paidwith'     THEN paidwith
       
        WHEN 'date_created'   THEN CONVERT(CHAR(19), date_created, 120) 
      END
  END DESC OFFSET @Limit * (@Page - 1) ROWS
      FETCH NEXT @Limit ROWS ONLY
  ),
  totalpayments AS 
	(
	 select count(Id) as paymenttotal from InvoicePaymentModels WHERE id = @paymentid
	)
SELECT *
from x as t, totalpayments where exists (select *, CONVERT(varchar(10), CONVERT(date, date_created, 106), 103) from InvoicePaymentModels as c where c.Id = t.Id) 
OPTION (RECOMPILE)
END