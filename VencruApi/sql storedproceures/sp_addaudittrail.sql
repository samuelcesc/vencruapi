IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[sp_add_audittrail]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[sp_add_audittrail]
GO
CREATE PROCEDURE [dbo].[sp_add_audittrail]
@auditactiontypeenum int,
@changes nvarchar(max),
@datamodel nvarchar(100),
@keyfieldid int,
@valueafter nvarchar(max),
@valuebefore nvarchar(max),
@userid nvarchar(200),
@businessid int
AS
--26092018 SE created
BEGIN
SET NOCOUNT ON;
INSERT INTO [dbo].[AuditTrailModels]
	VALUES (@keyfieldid, @auditactiontypeenum, GETDATE(), @changes, @valueafter, @userid, @datamodel, @valuebefore, @businessid)
END
GO