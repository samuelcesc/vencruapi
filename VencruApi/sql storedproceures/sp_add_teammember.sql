IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[sp_add_teammember]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[sp_add_teammember]
GO
CREATE PROCEDURE [dbo].[sp_add_teammember]
@businessid int,
@userId nvarchar(200),
@firstname nvarchar(100),
@lastname nvarchar(200),
@memberemail nvarchar(100),
@roles nvarchar(100), 
@status bit,
@Id int output
AS
--26092018 SE created

if exists(select * from TeamMemberModels where businessid = @businessid and memberemail = @memberemail)
BEGIN
select @Id = SCOPE_IDENTITY()
	END
ELSE
BEGIN
SET NOCOUNT ON;
INSERT INTO [dbo].[TeamMemberModels]
	VALUES (@memberemail, @firstname, @lastname, @roles, @status, @businessid, @userId, GETDATE())
    SELECT @Id = SCOPE_IDENTITY()
END
GO