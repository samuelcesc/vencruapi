USE [aspnet-VencruApi-20180903084237]
GO
/****** Object:  StoredProcedure [dbo].[sp_add_onboarding]    Script Date: 2018-10-01 2:53:50 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[sp_add_onboarding]
@userId nvarchar(100),
@firstname nvarchar(100) = null,
@lastname nvarchar(500) = null,
@country nvarchar(100) = null,
@city nvarchar(100) = null,
@companyname nvarchar (500),
@isincorporated tinyint,
@industry nvarchar(100) = null,
@state nvarchar(100) = null,
@phonenumber nvarchar(100) = null,
@address nvarchar(100) = null,
@employeesize nvarchar (500) = null,
@currency nvarchar(100) = null,
@onlinepayment tinyint,
@Id int output
AS
--20180915 SE created
if exists(select * from BusinessModels where userid = @userId and companyname = @companyname)
Begin
Select @Id = SCOPE_IDENTITY()
End
Else
BEGIN
insert into [dbo].[BusinessModels] (country, city, companyname, isincorporated, industry, state, address, employeesize, currency, onlinepayment, phonenumber, date_created, userid) 
values (@country, @city, @companyname, @isincorporated, @industry, @state, @address, @employeesize, @currency, @onlinepayment, @phonenumber, GETDATE(), @userId)
select @Id = SCOPE_IDENTITY()
END
Begin
insert into [dbo].[ApplicationUserBusinessModels] (ApplicationUser_Id, BusinessModel_Id) values (@userId, @Id)
End
Begin
update [dbo].[AspNetUsers] set firstname = @firstname, lastname = @lastname where Id = @userId
End
