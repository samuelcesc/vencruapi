IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[sp_get_userplan]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[sp_get_userplan]
GO
CREATE PROCEDURE [dbo].[sp_get_userplan]
@userid nvarchar(240)
AS
--26092018 SE created
BEGIN
select p.id as planid, p.name, p.plantype, p.price, s.date_expire, s.status from PlanModels p inner join SubscriptionModels s on p.id = s.planid where s.userid = @userid and s.status = 1
END
GO