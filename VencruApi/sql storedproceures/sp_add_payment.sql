IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[sp_add_payment]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[sp_add_payment]
GO
CREATE PROCEDURE [dbo].[sp_add_payment]
@amount int,
@date_paid nvarchar(100),
@invoiceid int,
@paidwith nvarchar(240) = null,
@userid nvarchar(100),
@Id int output
AS

BEGIN
SET NOCOUNT ON;
INSERT INTO [dbo].[InvoicePaymentModels]
	VALUES (@amount, @paidwith, @date_paid, GETDATE(),@invoiceid, @userid)
    SELECT @Id = SCOPE_IDENTITY()
END
GO