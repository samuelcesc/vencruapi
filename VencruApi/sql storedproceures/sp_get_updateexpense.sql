IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[sp_get_update_expense]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[sp_get_update_expense]
GO
Create Procedure [dbo].[sp_get_update_expense]
@ExpenseNumber nvarchar(200),
@Description nvarchar(200) = null,
@Totalamount int,
@Category nvarchar(200) = null,
@Date_created nvarchar(200) = null,
@Vendor nvarchar(200) = null,
@Paidwith nvarchar(200) = null,
@ImgUrl nvarchar(200) = null,
@Businessid int,
@Id int
AS
--05102018 SE created
BEGIN
update ExpenseModels set Description = @Description, TotalAmount = @Totalamount, 
Category = @Category,
Date_Created = @Date_created, 
Vendor = @Vendor, PaidWith = @Paidwith, 
receiptimageurl = @ImgUrl where BusinessId = @BusinessId and Id = @id
END