IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[sp_add_promocode]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[sp_add_promocode]
GO
CREATE PROCEDURE [dbo].[sp_add_promocode]
@code nvarchar(240),
@discount int,
@limit int, 
@userid nvarchar(250),
@id int out
AS
--26092018 SE created
if exists(select * from PromoCodeModels where code = @code)
BEGIN
select @id = SCOPE_IDENTITY()
 END
 else
 begin
 insert into PromoCodeModels values(@code, @discount, @limit, GetDate(), @userid)
 select @id = SCOPE_IDENTITY()
 End
GO