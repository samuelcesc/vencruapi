IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[sp_get_payment]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[sp_get_payment]
GO
CREATE PROCEDURE [dbo].[sp_get_payment]
@invoiceid int,
@paymentid int
AS
BEGIN
SET NOCOUNT ON;
	select * from InvoicePaymentModels where id = @paymentid and @invoiceid = @invoiceid
END
GO