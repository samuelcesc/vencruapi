IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[sp_add_expense]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[sp_add_expense]
GO
CREATE PROCEDURE [dbo].[sp_add_expense]
@Expensenumber nvarchar (240),
@Description nvarchar (240) = null,
@Category nvarchar (240) = null,
@Date_Created nvarchar (240) = null,
@UserId nvarchar (240),
@BusinessId nvarchar (240) = null,
@Bid int,
@Utime int,
@Vendor nvarchar (240) = null,
@PaidWith nvarchar (240) = null,
@ImgUrl nvarchar (240) = null,
@Totalamount int,
@Id int output
AS
--26092018 SE created

if exists(select * from ExpenseModels where BusinessId = @BusinessId and ExpenseNumber = @Expensenumber)
BEGIN
select @Id = SCOPE_IDENTITY()
	END
ELSE
BEGIN
SET NOCOUNT ON;
INSERT INTO [dbo].[ExpenseModels]
	VALUES (@Expensenumber, @Description, @Category, @Date_Created, @Vendor, @PaidWith, @Totalamount, 
	@BusinessId, @Utime, @ImgUrl, @UserId, @Bid)
    SELECT @Id = SCOPE_IDENTITY()
END
GO