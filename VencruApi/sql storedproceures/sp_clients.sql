IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[sp_delete_client]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[sp_delete_client]
GO
CREATE PROCEDURE [dbo].[sp_delete_client]
@Clientid int,
@Businessid int

AS
--27092018 SE created
BEGIN
	delete from ClientsModels where Id = @Clientid and BusinessId = BusinessId

END
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[sp_update_client]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[sp_update_client]
GO
CREATE PROCEDURE [dbo].[sp_update_client]
@Firstname nvarchar(200) = null,
@Lastname nvarchar(200) = null,
@Company nvarchar(200),
@Companyemail nvarchar(200),
@Phonenumber nvarchar(200) = null,
@Street nvarchar(200) = null,
@City nvarchar(200) = null,
@Country nvarchar(200) = null,
@BusinessId int,
@ClientId int

AS
--27092018 SE created
BEGIN
update ClientsModels set Firstname = @Firstname, Lastname = @Lastname, CompanyName = @Company, Companyemail = @Companyemail,
Phonenumber = @Phonenumber, Street = @Street, City = @City, Country = @Country where BusinessId = @BusinessId and Id = @ClientId
END
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[sp_get_allclients]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[sp_get_allclients]
GO
CREATE PROCEDURE [dbo].[sp_get_allclients]
@BusinessId int
AS
--27092018 SE created
BEGIN
select * from ClientsModels where BusinessId = @BusinessId
END
GO
