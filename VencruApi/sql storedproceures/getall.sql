IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[Sort_DynamicSQL]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Sort_DynamicSQL]
GO
CREATE PROCEDURE dbo.Sort_DynamicSQL
  @SortColumn    NVARCHAR(128) = N'Companyname',
  @SortDirection VARCHAR(4)    = 'ASC',
  @Businessid int,
  @Searchquery nvarchar(200) = null,
  @Fromdate nvarchar(200) = null,
  @Todate nvarchar(200) = null
AS
BEGIN
  SET NOCOUNT ON;
 
  ;WITH x AS
  (
    SELECT *,
      rn = ROW_NUMBER() OVER (
        ORDER BY CASE @SortColumn 
          WHEN 'companyname'     THEN CompanyName
          WHEN 'date_created'   THEN  date_created
          WHEN 'firstname'        THEN Firstname
          WHEN 'lastname'   THEN Lastname 
      END
      ) * CASE @SortDirection WHEN 'ASC' THEN 1 ELSE -1 END
    FROM dbo.ClientsModels
  ),
  CTE_TotalRows AS 
(
	 select count(Id) as totalclients from ClientsModels WHERE (BusinessId = @Businessid and isdeleted = 'false')
), 
SearchWithQuery as 
(
select * from x where case RTRIM(LTRIM(@SortColumn))
when 'companyname' then 'CompanyName'
when 'firstname' then 'Firstname'
when 'lastname' then 'Lastname'
when '' then 'CompanyName'
end like '%'+@Searchquery+'%' or ISNULL(@Searchquery, '') = ''
),
SearchWithDate as 
(
 select * from x where date_created >= @Fromdate and date_created <= @Todate or (ISNULL(@Fromdate, '')= '' and ISNULL(@Todate, '') = '')
)

SELECT CompanyName, Companyemail, Phonenumber, Id as businessid, date_created, Firstname, Lastname, Street, Phonenumber,Country, BusinessId, UserId as ownerid 
from x,CTE_TotalRows, SearchWithDate, SearchWithQuery where BusinessId = @Businessid and isdeleted = 'false'
ORDER BY rn;
   
END
GO