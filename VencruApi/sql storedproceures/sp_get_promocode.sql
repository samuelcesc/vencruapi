IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[sp_get_promocode]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[sp_get_promocode]
GO
CREATE PROCEDURE [dbo].[sp_get_promocode]
@promocode nvarchar(240) = null,
@id int
AS
--26092018 SE created
if @promocode is null
BEGIN
select * from PromoCodeModels where id = @id
 END
 else if @id = 0
 begin
select * from PromoCodeModels where code = @promocode
 End
 else
 begin
 set nocount on
 end
GO