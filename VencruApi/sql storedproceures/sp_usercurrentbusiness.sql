IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[sp_add_usercurrent_business]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[sp_add_usercurrent_business]
GO
CREATE PROCEDURE [dbo].[sp_add_usercurrent_business]
@UserId nvarchar (240),
@BusinessId int

AS
--28092018 SE created
if exists(select * from UserCurrentBusinessModels where UserId = @UserId)
BEGIN
update UserCurrentBusinessModels set BusinessId = @BusinessId where UserId = @UserId
END
else
BEGIN
	INSERT INTO [UserCurrentBusinessModels] (BusinessId, UserId) 
	VALUES (@BusinessId, @UserId)
END
GO

