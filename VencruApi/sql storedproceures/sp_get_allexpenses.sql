IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[sp_get_allexpenses]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[sp_get_allexpenses]
GO
CREATE PROCEDURE [dbo].[sp_get_allexpenses]
@Sortby    NVARCHAR(120) = 'date_created',
@Sortorder VARCHAR(4)    = 'desc',
@Businessid int,
@Limit int = 10,
@Page int = 1,
@Listtype int = 0
AS

--05102018 SE created
BEGIN
  SET NOCOUNT ON;
 
  ;WITH x AS
  (
    SELECT *, @Page as pagenumber,@Limit as limit
  FROM dbo.ExpenseModels where businessid = @Businessid
  ORDER BY 
    CASE WHEN @Sortorder = 'asc' THEN
      CASE @Sortby 
        WHEN 'expensenumber'     THEN ExpenseNumber
        WHEN 'amount'   THEN 
	  RIGHT(COALESCE(NULLIF(LEFT(RTRIM([TotalAmount]),1),'-'),'0') 
	   + REPLICATE('0', 23) + RTRIM([TotalAmount]), 24)
        WHEN 'vendor'        THEN Vendor
	    WHEN 'paidwith'        THEN PaidWith
        WHEN 'date_created'   THEN CONVERT(CHAR(19), date_created, 120) 
      END
    END ASC,
    CASE WHEN @Sortorder = 'desc' THEN
      CASE @Sortby 
      WHEN 'expensenumber'     THEN ExpenseNumber
        WHEN 'amount'   THEN 
	  RIGHT(COALESCE(NULLIF(LEFT(RTRIM([TotalAmount]),1),'-'),'0') 
	   + REPLICATE('0', 23) + RTRIM([TotalAmount]), 24)
        WHEN 'vendor'        THEN Vendor
	    WHEN 'paidwith'        THEN PaidWith
        WHEN 'date_created'   THEN CONVERT(CHAR(19), date_created, 120) 
    END
  END DESC OFFSET @Limit * (@Page - 1) ROWS
      FETCH NEXT @Limit ROWS ONLY
  ),
  TotalExpenses AS 
	(
	 select count(Id) as expensetotal from ExpenseModels WHERE BusinessId = @Businessid
	)

SELECT *
from x as t, TotalExpenses where exists (select *, CONVERT(varchar(10), CONVERT(date, date_created, 106), 103) from ExpenseModels as c where c.Id = t.Id) 
OPTION (RECOMPILE)
END