IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[sp_get_invoice]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[sp_get_invoice]
GO
CREATE PROCEDURE [dbo].[sp_get_invoice]
@businessid int, 
@invoiceid int
AS
--26092018 SE created
BEGIN
SET NOCOUNT ON;
select * from InvoiceModels as t inner join ItemsModels as i on t.Id = i.invoiceid where t.businessid = @businessid and t.Id = @invoiceid
END
GO