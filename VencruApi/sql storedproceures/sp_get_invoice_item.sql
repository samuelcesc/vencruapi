IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[sp_get_invoice_item]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[sp_get_invoice_item]
GO
CREATE PROCEDURE [dbo].[sp_get_invoice_item]
@invoiceid int
AS
--26092018 SE created
BEGIN
SET NOCOUNT ON;
select * from ItemsModels where invoiceid = @invoiceid
END
GO