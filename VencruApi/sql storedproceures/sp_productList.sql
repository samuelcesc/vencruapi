--Add Product List
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[sp_add_productlist]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[sp_add_productlist]
GO
CREATE PROCEDURE [dbo].[sp_add_productlist]
@StockNumber nvarchar(200),
@ProductName nvarchar(200),
@Description nvarchar(200) = null,
@UnitPrice int = 0,
@UserId nvarchar(200),
@BusinessId int,
@Id int = 0 output
AS
--27092018 SE created

if exists(select * from ProductListModels where ProductName = @ProductName and BusinessId = @BusinessId)
begin
Select @Id = SCOPE_IDENTITY()
return @Id
end
else
begin
insert into ProductListModels (StockNumber, ProductName, Description, UnitPrice, UserId, BusinessId) values(@StockNumber, @ProductName, @Description, @UnitPrice, @UserId, @BusinessId)
Select @Id = SCOPE_IDENTITY()
return @Id
end
GO

--Delete Product List
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[sp_delete_productlist]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[sp_delete_productlist]
GO
CREATE PROCEDURE [dbo].[sp_delete_productlist]
@Id int,
@Businessid int

AS
--27092018 SE created
BEGIN
	delete from ProductListModels where Id = @Id and BusinessId = BusinessId

END
GO

--Update product List
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[sp_update_productlist]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[sp_update_productlist]
GO
CREATE PROCEDURE [dbo].[sp_update_productlist]
@StockNumber nvarchar(200),
@ProductName nvarchar(200),
@Description nvarchar(200) = null,
@UnitPrice nvarchar(200),
@BusinessId int,
@Id int

AS
--27092018 SE created
BEGIN
update ProductListModels set StockNumber = @StockNumber, ProductName = @ProductName, Description = @Description, UnitPrice = @UnitPrice where BusinessId = @BusinessId and Id = @Id
END
GO

--Get All Product List
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[sp_get_allproductlist]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[sp_get_allproductlist]
GO
CREATE PROCEDURE [dbo].[sp_get_allproductlist]
@BusinessId int
AS
--27092018 SE created
BEGIN
select * from ProductListModels where BusinessId = @BusinessId
END
GO

--Get Product List
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[sp_get_productlist]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[sp_get_productlist]
GO
CREATE PROCEDURE [dbo].[sp_get_productlist]
@BusinessId int,
@Id int
AS
--27092018 SE created
BEGIN
select * from ProductListModels where BusinessId = @BusinessId and Id = @Id
END
GO
