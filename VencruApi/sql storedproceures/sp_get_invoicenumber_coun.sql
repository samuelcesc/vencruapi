IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[sp_get_invoicenumber_count]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[sp_get_invoicenumber_count]
GO
CREATE PROCEDURE [dbo].[sp_get_invoicenumber_count]
@Businessid int
AS
--26092018 SE created
BEGIN
SET NOCOUNT ON;
select COUNT(Id) as invoicecount from InvoiceModels where businessid = @Businessid
END
GO