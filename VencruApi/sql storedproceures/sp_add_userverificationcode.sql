IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[sp_add_userverificationcode]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[sp_add_userverificationcode]
GO
CREATE PROCEDURE [dbo].[sp_add_userverificationcode]
@email nvarchar(200),
@code nvarchar(100)
AS
--26092018 SE created

BEGIN
SET NOCOUNT ON;
update AspNetUsers set verificationcode = @code where Email =  @email
End
GO