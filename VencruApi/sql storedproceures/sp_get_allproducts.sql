USE [aspnet-VencruApi-20180903084237]
GO
/****** Object:  StoredProcedure [dbo].[sp_get_allproductlist]    Script Date: 2018-10-04 12:35:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[sp_get_allproductlist]
 @Sortby    NVARCHAR(120) = 'utime',
  @Sortorder VARCHAR(4)    = 'DESC',
  @Businessid int,
  @Listtype int,
  --@Searchquery nvarchar(200) = null,
  --@Fromdate nvarchar(200) = null,
  --@Todate nvarchar(200) = null,
  @Limit int = 10,
  @Page int = 1
AS
BEGIN
  SET NOCOUNT ON;
 
  ;WITH x AS
  (
    SELECT *, @Page as pagenumber,@Limit as limit
  FROM dbo.ProductModels where businessid = @Businessid and isdeleted = case @Listtype when 1 then 'true' when 0 then 'false'  end
  ORDER BY 
    CASE WHEN @Sortorder = 'ASC' THEN
      CASE @Sortby 
        WHEN 'productname'     THEN productname
        WHEN 'unitprice'   THEN 
	  RIGHT(COALESCE(NULLIF(LEFT(RTRIM([unitprice]),1),'-'),'0') 
	   + REPLICATE('0', 23) + RTRIM([unitprice]), 24)
        WHEN 'stocknumber'        THEN stocknumber
        WHEN 'date_created'   THEN CONVERT(CHAR(19), date_created, 120) 
      END
    END,
    CASE WHEN @Sortorder = 'DESC' THEN
      CASE @Sortby 
        WHEN 'productname'     THEN productname
        WHEN 'unitprice'   THEN 
	  RIGHT(COALESCE(NULLIF(LEFT(RTRIM([unitprice]),1),'-'),'0') 
	   + REPLICATE('0', 23) + RTRIM([unitprice]), 24)
        WHEN 'stocknumber'        THEN stocknumber
        WHEN 'date_created'   THEN CONVERT(CHAR(19), date_created, 120) 
    END
  END DESC OFFSET @Limit * (@Page - 1) ROWS
      FETCH NEXT @Limit ROWS ONLY
  ),
  TotalProducts AS 
	(
	 select count(Id) as producttotal from ProductModels WHERE (BusinessId = @Businessid and isdeleted = 'false')
	)

SELECT *
from x as t, TotalProducts where exists (select *, CONVERT(varchar(10), CONVERT(date, date_created, 106), 103) from ProductModels as c where c.Id = t.Id) 
OPTION (RECOMPILE)
END

