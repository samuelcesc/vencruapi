IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[sp_getusercurrent_business]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[sp_getusercurrent_business]
GO
CREATE PROCEDURE [dbo].[sp_getusercurrent_business]
@UserId nvarchar (240)

AS
--28092018 SE created
Begin
select * from UserCurrentBusinessModels where UserId = @UserId
END
GO