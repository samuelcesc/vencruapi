IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[sp_getall_clientdelete_list]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[sp_getall_clientdelete_list]
GO
CREATE PROCEDURE [dbo].[sp_getall_clientdelete_list]
@BusinessId int
AS
--30092018 SE created

Begin
select * from ClientsModels where isdeleted = 'true' and Businessid = @BusinessId
END

GO