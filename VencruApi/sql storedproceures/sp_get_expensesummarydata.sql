USE [vencrudb]
GO
/****** Object:  StoredProcedure [dbo].[sp_get_expensesummary]    Script Date: 2018-12-11 10:46:45 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[sp_get_expensesummary]
@Businessid int
AS
--05102018 SE created

Begin
with TotalExpense as
(
select sum(TotalAmount) as totalexpenses from ExpenseModels where BusinessId = @BusinessId 
),
MonthlyTotal as (
select COALESCE(sum(TotalAmount), 0) as monthlytotal  from ExpenseModels where datepart(MM,GETDATE()) = datepart(MM,date_created) and BusinessId = @BusinessId
),
HighestExpense as 
(
SELECT 
  Max(TotalAmount) as highestamount  FROM ExpenseModels where businessid = @Businessid 
)

select top 1 * from ExpenseModels as e, TotalExpense, MonthlyTotal, HighestExpense where e.BusinessId = @BusinessId

END
