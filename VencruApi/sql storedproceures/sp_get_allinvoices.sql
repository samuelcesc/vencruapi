IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[sp_get_allinvoices]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[sp_get_allinvoices]
GO
CREATE PROCEDURE [dbo].[sp_get_allinvoices]
@Sortby    NVARCHAR(120) = 'date_created',
@Sortorder VARCHAR(4)    = 'desc',
@Businessid int,
@Limit int = 10,
@Page int = 1,
@Listtype int = 0
AS
--05102018 SE created
BEGIN
  SET NOCOUNT ON;
 
  ;WITH x AS
  (
    SELECT *, @Page as pagenumber,@Limit as limit
  FROM dbo.InvoiceModels where businessid = @Businessid 
  ORDER BY 
    CASE WHEN @Sortorder = 'asc' THEN
      CASE @Sortby 
        WHEN 'invoicenumber'     THEN invoicenumber
        WHEN 'status'        THEN invoicestatus
	    WHEN 'paymenttype'        THEN paymenttype
        WHEN 'date_created'   THEN CONVERT(CHAR(19), date_created, 120) 
      END
    END ASC,
    CASE WHEN @Sortorder = 'desc' THEN
     CASE @Sortby 
        WHEN 'invoicenumber'     THEN invoicenumber
        WHEN 'status'        THEN invoicestatus
	    WHEN 'paymenttype'        THEN paymenttype
        WHEN 'date_created'   THEN CONVERT(CHAR(19), date_created, 120) 
      END
  END DESC OFFSET @Limit * (@Page - 1) ROWS
      FETCH NEXT @Limit ROWS ONLY
  ),
  TotalInvoices AS 
	(
	 select count(Id) as invoicetotal from InvoiceModels WHERE BusinessId = @Businessid
	)
SELECT *
from x as t, TotalInvoices where exists (select *, CONVERT(varchar(10), CONVERT(date, date_created, 106), 103) from InvoiceModels as c where c.Id = t.Id) 
OPTION (RECOMPILE)
END