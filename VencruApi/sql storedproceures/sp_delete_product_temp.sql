IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[sp_delete_product_temp]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[sp_delete_product_temp]
GO
CREATE PROCEDURE [dbo].[sp_delete_product_temp]
@Productid nvarchar (240),
@BusinessId int
AS
--30092018 SE created
Begin
update ProductModels set isdeleted = 'true' where id = @Productid and BusinessId = @BusinessId
END