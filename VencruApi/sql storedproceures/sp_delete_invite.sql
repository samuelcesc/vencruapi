IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[sp_delete_invite]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[sp_delete_invite]
GO
CREATE PROCEDURE [dbo].[sp_delete_invite]
@inviteid int, 
@userid nvarchar(100), 
@businessid int
AS
--26092018 SE created
if exists(select * from TeamMemberModels where id = @inviteid and userid = @userid)
Begin
delete from TeamMemberModels where id = @inviteid and businessid = @businessid and userid = @userid
End
Else
begin
Set NoCount on
End
GO