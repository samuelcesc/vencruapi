IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[sp_delete_invoicetemp]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[sp_delete_invoicetemp]
GO
CREATE PROCEDURE [dbo].[sp_delete_invoicetemp]
@invoiceid int,
@businessid int
AS
--30092018 SE created
Begin
update InvoiceModels set isdeleted = 'true' where businessid = @businessid and id = @invoiceid
END