IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[sp_add_item]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[sp_add_item]
GO
CREATE PROCEDURE [dbo].[sp_add_item]
@description nvarchar(100),
@price int,
@quantity int,
@amount int,
@invoiceid int,
@Id int output
AS
--26092018 SE created
BEGIN
SET NOCOUNT ON;
INSERT INTO [dbo].[ItemsModels]
	VALUES (@description, @price, @quantity, @amount, @invoiceid, GETDATE())
    SELECT @Id = SCOPE_IDENTITY()
END
GO