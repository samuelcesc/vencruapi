IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[sp_get_clientsummarydata]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[sp_get_clientsummarydata]
GO
CREATE PROCEDURE [dbo].[sp_get_clientsummarydata]
@BusinessId int
AS
--30092018 SE created

Begin
select(select count(Id) from ClientsModels where BusinessId = @BusinessId) as totalclients
select(select count(Id) from ClientsModels where datepart(MM,GETDATE()) = datepart(MM,date_created) and BusinessId = @BusinessId) as thismonthclients 
select(select COUNT(Id) from InvoiceModels  where BusinessModel_Id = @BusinessId)as repeatclients
END

GO