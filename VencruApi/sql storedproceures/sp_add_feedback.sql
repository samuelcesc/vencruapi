IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[sp_add_feedback]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[sp_add_feedback]
GO
CREATE PROCEDURE [dbo].[sp_add_feedback]
@netpromoterscore int,
@details nvarchar(100),
@emailaddress nvarchar(50),
@Id int output
AS
--26092018 SE created

BEGIN
SET NOCOUNT ON;
INSERT INTO [dbo].[FeedbackModels]
	VALUES (@netpromoterscore, @details, GETDATE(), @emailaddress)
    SELECT @Id = SCOPE_IDENTITY()
END
GO