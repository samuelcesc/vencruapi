IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[sp_get_invite]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[sp_get_invite]
GO
CREATE PROCEDURE [dbo].[sp_get_invite]
@inviteid int, 
@businessid int
AS
--26092018 SE created
if exists(select * from TeamMemberModels where businessid = @businessid)
Begin
select * from TeamMemberModels where businessid = @businessid and id = @inviteid
End
Else
begin
Set NoCount on
End
GO