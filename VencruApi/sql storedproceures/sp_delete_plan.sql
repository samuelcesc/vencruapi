IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[sp_delete_plan]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[sp_delete_plan]
GO
CREATE PROCEDURE [dbo].[sp_delete_plan]
@id int
AS
--26092018 SE created
BEGIN
delete from planmodels where id = @id
End
GO