USE [vencrudb]
GO
/****** Object:  StoredProcedure [dbo].[sp_getuser]    Script Date: 2018-09-27 4:12:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[sp_getuser]
@Userid nvarchar(100)
AS
--20180713 SE created
BEGIN
    SET NOCOUNT ON;

	select [AspNetUsers] .[Id] as userid, [AspNetUsers].[Email], [AspNetUsers].[EmailConfirmed], [BusinessModels].*, AspNetUserLogins.LoginProvider from AspNetUsers inner join BusinessModels on BusinessModels.userid = aspnetusers.Id left join AspNetUserLogins on AspNetUsers.id = AspNetUserLogins.UserId  where aspnetusers.Id = @userid

END
