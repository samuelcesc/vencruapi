IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[sp_delete_client_temp]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[sp_delete_client_temp]
GO
CREATE PROCEDURE [dbo].[sp_delete_client_temp]
@ClientId nvarchar (240),
@BusinessId int

AS
--30092018 SE created
Begin
update ClientsModels set isdeleted = 'true' where id = @ClientId and BusinessId = @BusinessId
END
GO