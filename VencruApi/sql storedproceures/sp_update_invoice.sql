IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[sp_update_invoice]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[sp_update_invoice]
GO
CREATE PROCEDURE [dbo].[sp_update_invoice]
@subtotal int,
@description nvarchar(100),
@paymenttype nvarchar(50),
@discount int = 0,
@notes nvarchar(240) = null,
@amountdue int,
@userid nvarchar(100),
@color nvarchar(50) = null,
@deposit int,
@font nvarchar(100) = null,
@sendstyle nvarchar(50),
@invoicestyle int,
@personalmessage nvarchar(100) = null,
@invoicestatus nvarchar(50),
@due_date nvarchar(50),
@businessid int,
@clientid int,
@image nvarchar(250) = null,
@invoicenumber nvarchar(250),
@paymentlink nvarchar(250),
@Id int
AS
--26092018 SE created
if exists(select * from InvoiceModels where BusinessId = @BusinessId and Id = @Id)
BEGIN
update InvoiceModels set subtotal = @subtotal, Description = @description, paymenttype = @paymenttype, discount = @discount, notes = @notes,
amountdue = @amountdue, color = @color, deposit = @deposit, font = @font, sendstyle = @sendstyle, invoicestyle  = @invoicestyle,
personalmessage = @personalmessage, invoicestatus = @invoicestatus, due_date = @due_date, clientid = @clientid, invoicenumber = @invoicenumber, paymentlink = @paymentlink
where id = @Id and businessid = @businessid
	END
ELSE
BEGIN
SET NOCOUNT ON;

END
GO