USE [aspnet-VencruApi-20180903084237]
GO
/****** Object:  StoredProcedure [dbo].[sp_add_client]    Script Date: 2018-10-01 12:50:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[sp_add_client]
@Firstname nvarchar(200) = null,
@Lastname nvarchar(200) = null,
@Company nvarchar(200),
@Companyemail nvarchar(200),
@Phonenumber nvarchar(200) = null,
@Street nvarchar(200) = null,
@City nvarchar(200) = null,
@Country nvarchar(200) = null,
@Userid nvarchar(200),
@Isdeleted bit,
@BusinessId int,
@Id int output
AS
--20180719 SE created
if exists(select * from ClientsModels where CompanyName = @Company)
begin 
select @Id = SCOPE_IDENTITY()
end
else
BEGIN
    SET NOCOUNT ON;

	INSERT INTO [dbo].[Clientsmodels] (City, Companyemail, date_created, Firstname, Lastname, Phonenumber, Street,CompanyName,Country,BusinessId, UserId, isdeleted) 
	VALUES (@City, @Companyemail, GETDATE(), @Firstname, @Lastname,@Phonenumber,@Street,@Company,@Country,@BusinessId, @Userid, @Isdeleted)
    SELECT @Id = SCOPE_IDENTITY()
END
