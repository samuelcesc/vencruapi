IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[sp_add_plan]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[sp_add_plan]
GO
CREATE PROCEDURE [dbo].[sp_add_plan]
@price int,
@name nvarchar(100),
@plantype nvarchar(50),
@Id int output
AS
--26092018 SE created

if exists(select * from PlanModels where name = @name and plantype = @plantype)
BEGIN
select @Id = SCOPE_IDENTITY()
	END
ELSE
BEGIN
SET NOCOUNT ON;
INSERT INTO [dbo].[PlanModels]
	VALUES (@price, @name, @plantype, GETDATE())
    SELECT @Id = SCOPE_IDENTITY()
END
GO