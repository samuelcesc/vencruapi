IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[sp_add_invoice]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[sp_add_invoice]
GO
CREATE PROCEDURE [dbo].[sp_add_invoice]
@subtotal int,
@description nvarchar(100),
@paymenttype nvarchar(50),
@discount int = 0,
@notes nvarchar(240) = null,
@amountdue int,
@userid nvarchar(100),
@color nvarchar(50) = null,
@deposit int,
@font nvarchar(100) = null,
@sendstyle nvarchar(50),
@invoicestyle int,
@personalmessage nvarchar(100) = null,
@invoicestatus nvarchar(50) = "Not Paid",
@utime int,
@due_date nvarchar(50),
@businessid int,
@clientid int,
@image nvarchar(250) = null,
@invoicenumber nvarchar(250),
@invoiceguid nvarchar(250),
@paymentlink nvarchar(250),
@requireddeposit int,
@isdeleted bit,

@Id int output
AS
--26092018 SE created
if exists(select * from InvoiceModels where BusinessId = @BusinessId and invoicenumber = @invoicenumber)
BEGIN
select @Id = SCOPE_IDENTITY()
	END
ELSE
BEGIN
SET NOCOUNT ON;
INSERT INTO [dbo].[InvoiceModels]
	VALUES (@description, @businessid, GETDATE(), @utime, @subtotal, @discount, @amountdue, @deposit, 
	@paymenttype, @notes, @invoicestyle, @color, @font, @sendstyle, @personalmessage, @invoicestatus, @clientid, @image, @userid, @due_date, @invoicenumber, @invoiceguid, @paymentlink, @requireddeposit, @isdeleted)
    SELECT @Id = SCOPE_IDENTITY()
END
GO