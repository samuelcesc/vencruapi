IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[sp_getuser_business]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[sp_getuser_business]
GO
CREATE PROCEDURE [dbo].[sp_getuser_business]
@UserId nvarchar(250),
@BusinessId int

AS
--28092018 SE created
BEGIN
select * from BusinessModels where Id = @BusinessId and userid = @UserId
END
GO