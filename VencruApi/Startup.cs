﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(VencruApi.Startup))]

namespace VencruApi
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {

            ConfigureAuth(app);

        }
    }
}
