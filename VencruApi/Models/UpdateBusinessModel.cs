﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace VencruApi.Models
{
    public class UpdateBusinessModel
    {
        [Required]
        public int businessid { get; set; }
        [Required]
        public string userid { get; set; }
        [Required]
        public string company { get; set; }
        public string phonenumber { get; set; }
        public string services { get; set; }
        public string industry { get; set; }
        public string address { get; set; }
        public string city { get; set; }
    }
}