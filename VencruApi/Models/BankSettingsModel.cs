﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace VencruApi.Models
{
    public class BankSettingsModel
    {
        [Key]
        public int id { get; set; }
        [Required]
        public string bankname { get; set; }
        [Required, StringLength(10)]
        public string accountnumber { get; set; }
        [Required]
        public string accountname { get; set; }
        public string accounttype { get; set; }
        public string date_created { get; set; }
        public bool isdefault { get; set; }
        public string userid { get; set; }
        public int businessid { get; set; }
        public BusinessModel business { get; set; }

    }
}