﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace VencruApi.Models
{
    public class TeamMemberModel
    {
        [Key]
        public int id { get; set; }
        [EmailAddress]
        public string memberemail { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string roles { get; set; }
        public bool status { get; set; }
        [Required]
        public int businessid { get; set; }
        public BusinessModel business { get; set; }
        public string userid { get; set; }
        public DateTime date_created { get; set; }

    }
    
}