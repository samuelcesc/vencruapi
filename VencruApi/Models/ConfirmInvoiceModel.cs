﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VencruApi.Models.ViewModels;

namespace VencruApi.Models
{
    public class ConfirmInvoiceModel
    {
        public string code { get; set; }
        public InvoiceViewModel invoice { get; set; }
    }
}