﻿using System;
using System.ComponentModel.DataAnnotations;

namespace VencruApi.Models
{
    public class ConfirmEmailModel
    {
        public string callbackurl { get; set; }
        public string email { get; set; }
        public bool ismobile { get; set; }
    }
    public class VerifyEmailModel
    {
        public string userid { get; set; }
        public string code { get; set; }
    }
    public class VerifyTeamMemberModel
    {
        public string userid { get; set; }
        public string code { get; set; }
        public int inviteid { get; set; }
        public int businessid { get; set; }
        public string memberuserid { get; set; }
    }
    public class ResendActivationEmailModel
    {
        [Required]
        [EmailAddress]
        public string email { get; set; }
    }
}