﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace VencruApi.Models
{
    public class AuditTrailModel
    {
        [Key]
        public int id { get; set; }
        public string userid { get; set; }
        public int keyfieldid { get; set; }
        public int auditactiontypeenum { get; set; }
        public string action_date { get; set; }
        public string datamodel { get; set; }
        public string changes { get; set; }
        public string valuesbefore { get; set; }
        public string valueafter { get; set; }
        public int businessid { get; set; }
    }
}