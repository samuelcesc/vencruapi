﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VencruApi.Models
{
    public class TargetsModel
    {
        public int estimatedmonthlytarget { get; set; }
        public int monthlytarget { get; set; }
        public int businessid { get; set; }
        public string userid { get; set; }
    }
}