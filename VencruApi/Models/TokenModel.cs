﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VencruApi.Models
{
    public class TokenModel
    {
        [JsonProperty("access_token")]
        public string accesstoken
        {
            get;
            set;
        }

        [JsonProperty(".expires")]
        public DateTime expires
        {
            get;
            set;
        }

        [JsonProperty("expires_in")]
        public int expiresin
        {
            get;
            set;
        }

        [JsonProperty(".issued")]
        public DateTime issued
        {
            get;
            set;
        }

        [JsonProperty("token_type")]
        public string tokentype
        {
            get;
            set;
        }

        [JsonProperty("userId")]
        public string userid
        {
            get;
            set;
        }

        [JsonProperty("userName")]
        public string username
        {
            get;
            set;
        }

        [JsonProperty("emailconfirmed")]
        public bool emailconfirmed
        {
            get;
            set;
        }
    }
}