﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace VencruApi.Models
{
    public class SubscriptionModel
    {
        [Key]
        public int Id { get; set; }
        public int planid { get; set; }
        public PlanModel plan { get; set; }
        public string userid { get; set; }
        public ApplicationUser user { get; set; }
        public DateTime date_created { get; set; }
        public bool status { get; set; }
        public string referencenumber { get; set; }
        public DateTime date_expire { get; set; }
        public bool istrial { get; set; }
        public int trialdays { get; set; }
        public string promocode { get; set; }
    }
}