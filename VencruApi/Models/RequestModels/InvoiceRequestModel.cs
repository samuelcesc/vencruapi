﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace VencruApi.Models.RequestModels
{
    public class InvoiceRequestModel
    {
        [Required]
        public string invoicenumber { get; set; }
    }
}