﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VencruApi.Models
{
    public class DeleteExpenseModel
    {
        public int expenseid { get; set; }
        public int businessid { get; set; }
        public string userid { get; set; }
    }
}