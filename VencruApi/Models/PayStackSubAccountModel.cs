﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace VencruApi.Models
{
    public class PayStackSubAccountModel
    {
        [ForeignKey("Business")]
        public int Id { get; set; }
        [Required]
        public string userid { get; set; }
        [Required]
        public int businessid { get; set; }
        [Required]
        public string subaccountcode { get; set; }

        public virtual BusinessModel Business { get; set; }
    }
}