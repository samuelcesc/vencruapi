﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VencruApi.Models.ViewModels
{
    public class ExpenseViewModel
    {
        public int id { get; set; }

        public string expensenumber { get; set; }

        public string description { get; set; }

        public string category { get; set; }

        public DateTime date_created { get; set; }

        public DateTime expensedate { get; set; }

        public string vendor { get; set; }

        public string paidwith { get; set; }
                
        public int totalamount { get; set; }

        public int businessid { get; set; }

        public string image { get; set; }

    }
}