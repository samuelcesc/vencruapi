﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VencruApi.Models.ViewModels
{
    public class ClientHistoryDataViewModel
    {
        public int invoiceid { get; set; }
        public DateTime date { get; set; }
        public string invoicenumber { get; set; }
        public string paidwith { get; set; }
        public int amount { get; set; }
    }
}