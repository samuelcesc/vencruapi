﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VencruApi.Models.ViewModels
{
    public class ClientListViewModel
    {
        public List<ClientViewModel> clients { get; set; }
        public int currentpage { get; set; }
        public int total { get; set; }
        public double pagetotal { get; set; }

        public ClientListViewModel()
        {
            clients = new List<ClientViewModel>();
        }
    }


}