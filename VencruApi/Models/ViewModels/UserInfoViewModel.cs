﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VencruApi.Models.ViewModels;

namespace VencruApi.Models
{
    public class UserInfoViewModel
    {
        public string userid { get; set; }

        public string email { get; set; }

        public bool emailconfirmed { get; set; }

        public string loginprovider { get; set; }

        public string firstname { get; set; }

        public string lastname { get; set; }

        public int currentbusinessid { get; set; }

        public string profileimageurl { get; set; }

        public string profileimagekey { get; set; }
        public string date_created { get; set; }

        

        public UserPlanViewModel userplan { get; set; }

        public List<BusinessViewModel> business { get; set; }
    }
}