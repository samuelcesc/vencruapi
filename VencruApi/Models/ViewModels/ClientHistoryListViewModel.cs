﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VencruApi.Models.ViewModels
{
    public class ClientHistoryListViewModel
    {
        public List<ClientHistoryDataViewModel> clienthistorydata { get; set; }

        public ClientHistorySummaryData summarydata { get; set; }

        public int page { get; set; }

        public int limit { get; set; }

        public int currentpage { get; set; }

        public int total { get; set; }

        public double pagetotal { get; set; }

        public ClientHistoryListViewModel()
        {
            clienthistorydata = new List<ClientHistoryDataViewModel>();
            summarydata = new ClientHistorySummaryData();
        }
    }
    
   
}