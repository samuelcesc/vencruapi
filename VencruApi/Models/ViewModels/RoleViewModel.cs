﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace VencruApi.Models.ViewModels
{
    public class RoleViewModel
    {
        public RoleViewModel()
        {

        }

        public RoleViewModel(ApplicationRole role)
        {
            id = role.Id;
            name = role.Name;
            businessid = role.businessid;
        }
        public string id { get; set; }
        [Required]
        public string name { get; set; }
        [Required]
        public int businessid { get; set; }
    }
}