﻿namespace VencruApi.Models.ViewModels
{
    public class BusinessSummaryViewModel
    {
        public int totalincomereceived { get; set; }
        public int totaloutstanding { get; set; }
        public int totalexpenses { get; set; }
        public SalesBreakdownViewModel salessummary { get; set; }
        public OutStandingRevenueViewModel revenuesummary { get; set; }
        public BusinessSummaryViewModel(SalesBreakdownViewModel model, OutStandingRevenueViewModel outStandingRevenueViewModel)
        {
            salessummary = new SalesBreakdownViewModel();
            revenuesummary = new OutStandingRevenueViewModel();
        }
    }
}