﻿using System.Globalization;

namespace VencruApi.Models.ViewModels
{
    public class TotalProfitDashboardViewModel
    {
        public int month { get; set; }

        public string monthname
        {
            get
            {
                return CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(month);
            }
        }
        public int total { get; set; }
    }
}