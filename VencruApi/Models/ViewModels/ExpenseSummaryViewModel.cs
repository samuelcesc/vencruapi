﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VencruApi.Models.ViewModels
{
    public class ExpenseSummaryViewModel
    {
        public int totalexpenses { get; set; }

        public int monthlytotal { get; set; }

        public string highestexpense { get; set; }
    }
}