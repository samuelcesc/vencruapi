﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VencruApi.Models.ViewModels
{
    public class IncomeSourcesViewModel
    {
        public int itemid { get; set; }

        public string item
        {
            get
            {
                return GetItemName(itemid);
            }
            set
            {
            }
        }
        
        public int total { get; set; }

        private string GetItemName(int id)
        {
            string productName;

            using (var dbcontext = new ApplicationDbContext())
            {
                productName = dbcontext.ProductModels.Find(id).productname;

            }

            return productName;
        }
    }
}