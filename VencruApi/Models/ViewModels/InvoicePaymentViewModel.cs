﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VencruApi.Models.ViewModels
{
    public class InvoicePaymentViewModel
    {
        public int id { get; set; }
        public int amount { get; set; }
        public string paidwith { get; set; }
        public DateTime date_paid { get; set; }
        public DateTime date_created { get; set; }
    }
}