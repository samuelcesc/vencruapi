﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VencruApi.Models.ViewModels
{
    public class UserPlanViewModel
    {
        public int planid { get; set; }
        public string planname { get; set; }
        public string plantype { get; set; }
        public int price { get; set; }
        public DateTime date_expire { get; set; }
        public bool isactive { get; set; }
        public string planinfo { get; set; }
    }
}