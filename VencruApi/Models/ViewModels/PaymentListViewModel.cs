﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VencruApi.Models.ViewModels
{
    public class PaymentListViewModel
    {
        public List<InvoicePaymentViewModel> payments { get; set; }
        public int currentpage { get; set; }
        public int total { get; set; }
        public double pagetotal { get; set; }

        public PaymentListViewModel()
        {
            payments = new List<InvoicePaymentViewModel>();
        }
    }
}