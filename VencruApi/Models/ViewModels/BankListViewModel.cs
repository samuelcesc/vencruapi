﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VencruApi.Models.ViewModels
{
    public class BankListViewModel
    {
        public List<BankSettingsViewModel> banks { get; set; }

        public BankListViewModel()
        {
            banks = new List<BankSettingsViewModel>();

        }
    }

}