﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VencruApi.Models
{
    public class ProductViewModel
    {
        public int id { get; set; }

        public string stocknumber { get; set; }

        public string productname { get; set; }

        public string description { get; set; }

        public int unitprice { get; set; }

        public string userid { get; set; }

        public int businessid { get; set; }

        public int costofitem { get; set; }

        public bool isdeleted { get; set; }

        public DateTime date_created { get; set; }
    }
}