﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VencruApi.Models
{
    public class ClientViewModel
    {
        public int id { get; set; }

        public string firstname { get; set; }

        public string lastname { get; set; }

        public string companyname { get; set; }

        public string phonenumber { get; set; }

        public string street { get; set; }

        public string country { get; set; }

        public string city { get; set; }
        
        public string companyemail { get; set; }

        public string userid { get; set; }

        public DateTime date_created { get; set; }

        public int businessid { get; set; }

        public bool isdeleted { get; set; }

        public string note { get; set; }
    }
}