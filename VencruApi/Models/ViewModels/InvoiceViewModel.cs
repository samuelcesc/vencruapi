﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace VencruApi.Models.ViewModels
{
    public class InvoiceViewModel
    {
        public int id { get; set; }

        public string description { get; set; }

        public int subtotal { get; set; }

        public string image { get; set; }

        public int discount { get; set; }

        public int amountdue { get; set; }

        public int deposit { get; set; }

        public string email { get; set; }

        public string paymenttype { get; set; }

        public string notes { get; set; }

        public int invoicestyle { get; set; }

        public string userid { get; set; }

        public string color { get; set; }

        public string font { get; set; }

        public string sendstyle { get; set; }

        public string personalmessage { get; set; }

        public string invoicestatus { get; set; }

        public string callbackurl { get; set; }

        public string invoicenumber { get; set; }

        public string invoicetype { get; set; }

        public DateTime issue_date { get; set; }

        [NotMapped]
        public string invoiceguid { get; set; }

        public DateTime date_created { get; set; }

        public DateTime due_date { get; set; }


        public BusinessViewModel business { get; set; }

        public ClientViewModel client { get; set; }

        public List<InvoiceItemViewModel> items { get; set; }

        public List<InvoicePaymentModel> payment { get; set; }
    }
}