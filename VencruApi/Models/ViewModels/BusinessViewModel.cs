﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using VencruApi.Models.ViewModels;

namespace VencruApi.Models
{
    public class BusinessViewModel
    {
        public int id { get; set; }

        public string city { get; set; }

        public string companyname { get; set; }

        public string country { get; set; }

        public string industry { get; set; }

        public byte isincorporated { get; set; }

        public string state { get; set; }
        
        public string address { get; set; }

        public string employeesize { get; set; }

        public string currency { get; set; }

        public byte onlinepayment { get; set; }

        public string phonenumber { get; set; }

        public string date_created { get; set; }
        
        public string ownerid { get; set; }

        public string domain { get; set; }

        public string facebookurl { get; set; }

        public string twitterurl { get; set; }

        public string linkdinurl { get; set; }

        public string instagramurl { get; set; }

        public string logourl { get; set; }

        public string logokey { get; set; }

        public string service { get; set; }

        public int tax { get; set; }

        public string vatnumber { get; set; }

        public int monthlytarget { get; set; }

        public int estimatedmonthlyrevenue { get; set; }

        public ArrayList userroles { get; set; }

        public List<OnlinePaymentBusinessViewModel> connectedpayments { get; set; }

        public BusinessViewModel()
        {
            connectedpayments = new List<OnlinePaymentBusinessViewModel>();
        }

        public bool getCurrentBusiness(int businessid)
        {
            if (businessid == id)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

    }
}