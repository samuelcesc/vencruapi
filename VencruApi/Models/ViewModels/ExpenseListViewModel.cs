﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VencruApi.Models.ViewModels
{
    public class ExpenseListViewModel
    {
        public List<ExpenseViewModel> expenses { get; set; }
        public int currentpage { get; set; }
        public int total { get; set; }
        public double pagetotal { get; set; }

        public ExpenseListViewModel()
        {
            expenses = new List<ExpenseViewModel>();
        }
    }
}