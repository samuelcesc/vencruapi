﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VencruApi.Models.ViewModels
{
    public class ClientHistorySummaryData
    {
        public int totaloutstanding { get; set; }
        public int due { get; set; }
        public int overdue { get; set; }
        public int draft { get; set; }
        public int totalearning { get; set; }
    }
}