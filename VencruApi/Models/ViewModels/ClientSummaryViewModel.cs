﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VencruApi.Models
{
    public class ClientSummaryViewModel
    {
        public int totalclient { get; set; }
        public int thismonthclients { get; set; }
        public int repeatclients { get; set; }
    }
}