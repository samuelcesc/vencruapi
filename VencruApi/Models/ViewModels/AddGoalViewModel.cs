﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace VencruApi.Models.ViewModels
{
    public class AddGoalViewModel
    {
        [Required]
        public int monthlytarget { get; set; }
        [Required]
        public int estimatedmonthlyrevenue { get; set; }


        public int businessid { get; set; }

        public string userid { get; set; }
    }
}