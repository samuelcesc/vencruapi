﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VencruApi.Models.ViewModels
{
    public class BankNameViewModel
    {
        public string bankname { get; set; }
    }

    public class BankNameListViewModel
    {
        public List<BankNameViewModel> banks { get; set; }

        public BankNameListViewModel()
        {
            banks = new List<BankNameViewModel>();
        }
    }
}