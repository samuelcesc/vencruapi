﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VencruApi.Models.ViewModels
{
    public class SalesBreakdownViewModel
    {
        public int totaloutstanding { get; set; }
        public int totalreceived { get; set; }
        public int totalsales { get { return GetTotalSales(totaloutstanding, totalreceived); } set { } }
        public int percentage { get; set; }
        public string message { get; set; }
        public bool isgood { get; set; }

        private int GetTotalSales(int outstanding, int received)
        {
            return outstanding + received;
        }
    }
}