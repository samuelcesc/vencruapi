﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VencruApi.Models
{
    public class ProductSummaryViewModel
    {
        public int totalproducts { get; set; }
        public string mostsolditem { get; set; }
        public int monthlytotal { get; set; }
    }
}