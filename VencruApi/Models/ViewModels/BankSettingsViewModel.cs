﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VencruApi.Models.ViewModels
{
    public class BankSettingsViewModel
    {
        public int id { get; set; }
        public string bankname { get; set; }
        public string accountnumber { get; set; }
        public string accounttype { get; set; }
        public DateTime date_created { get; set; }
        public bool isdefault { get; set; }
        public string userid { get; set; }
        public int businessid { get; set; }
        public string accountname { get; set; }
    }
}