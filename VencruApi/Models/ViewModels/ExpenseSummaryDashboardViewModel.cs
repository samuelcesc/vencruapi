﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VencruApi.Models.ViewModels
{
    public class ExpenseSummaryDashboardViewModel
    {
        public string category { get; set; }
        public int total { get; set; }
    }
}