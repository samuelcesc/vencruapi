﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace VencruApi.Models.ViewModels
{
    public class UpdateGoalViewModel
    {
        [Required]
        public int monthlytarget { get; set; }
       
        public int businessid { get; set; }

        public string userid { get; set; }
    }
}