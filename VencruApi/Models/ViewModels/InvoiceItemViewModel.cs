﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VencruApi.Models.ViewModels
{
    public class InvoiceItemViewModel
    {
        public int id { get; set; }
        public string description { get; set; }
        public int price { get; set; }
        public int quantity { get; set; }
        public int amount { get; set; }

        public DateTime date_created { get; set; }
    }
}