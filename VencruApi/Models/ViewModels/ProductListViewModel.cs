﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VencruApi.Models.DalClasses
{
    public class ProductListViewModel
    {
        public List<ProductViewModel> products { get; set; }
        public int currentpage { get; set; }
        public int total { get; set; }
        public double pagetotal { get; set; }

        public ProductListViewModel()
        {
            products = new List<ProductViewModel>();
        }
    }
}