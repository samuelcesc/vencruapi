﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VencruApi.Models.ViewModels
{
    public class PlanViewModel
    {
        public int id { get; set; }
        public int price { get; set; }
        public string name { get; set; }
        public string plantype { get; set; }
        public DateTime date_created { get; set; }
    }
}