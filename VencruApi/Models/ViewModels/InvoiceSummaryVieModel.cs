﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VencruApi.Models.ViewModels
{
    public class InvoiceSummaryVieModel
    {
        public int totalpaid { get; set; }
        public int totaloutstanding { get; set; }
        public int totaldraft { get; set; }
        public List<InvoiceViewModel> recentoverdue { get; set; }
        public List<InvoiceViewModel> recentunpaid { get; set; }

        public InvoiceSummaryVieModel()
        {
            recentoverdue = new List<InvoiceViewModel>();
            recentunpaid = new List<InvoiceViewModel>();
        }
    }
}