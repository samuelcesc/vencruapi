﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VencruApi.Models.ViewModels
{
    public class OnlinePaymentBusinessViewModel
    {
        public string onlinepayment { get; set; }
        public bool isconnected { get; set; }

        public OnlinePaymentBusinessViewModel(string paymentgateway, bool connectedstatus)
        {
            onlinepayment = paymentgateway;
            isconnected = connectedstatus;
        }
    }
}