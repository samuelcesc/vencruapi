﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VencruApi.Models
{
    public class UserSubscriptionModel
    {
        public string userid { get; set; }
        public int planid { get; set; }
        public string referencenumber { get; set; }
        public bool istrial { get; set; }
        public int trialdays { get; set; } = 30;
        public string promocode { get; set; }
    }
}