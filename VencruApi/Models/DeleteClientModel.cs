﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VencruApi.Models
{
    public class DeleteClientModel
    {
        public int businessid { get; set; }
        public int clientid { get; set; }
        public string  userid { get; set; }
    }
}