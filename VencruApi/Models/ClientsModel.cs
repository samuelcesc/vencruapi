﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace VencruApi.Models
{
    public class ClientsModel
    {
        [Key]
        public int id { get; set; }

        [Required]
        public string firstname { get; set; }

        public string lastname { get; set; }

        public string companyname { get; set; }

        public string phonenumber { get; set; }

        public string street { get; set; }

        public string country { get; set; }

        public string city { get; set; }

        [Required]
        [EmailAddress]
        public string companyemail { get; set; }

        [Required]
        public string userid { get; set; }

        public bool isdeleted { get; set; }

        public string date_created { get; set; }

        public int utime
        {
            get { return getUtime(); }

            set { }
        }

        public int businessid { get; set; }

        public string note { get; set; }

        public BusinessModel business { get; set; }

        public ICollection<InvoiceModel> invoice { get; set; }

        private int getUtime()
        {
            return utime = (Int32)DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1)).TotalSeconds;
        }
    }
   

}