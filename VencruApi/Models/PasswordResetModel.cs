﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VencruApi.Models
{
    public class PasswordResetModel
    {
        public string CallbackUrl { get; set; }
        public string Email { get; set; }
        public string fullname { get; set; }
    }
}