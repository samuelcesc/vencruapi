﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VencruApi.Models
{
    public class PaymentOperationModel
    {
        public int id { get; set; }
        public int invoiceid { get; set; }
        public string userid { get; set; }
    }
}