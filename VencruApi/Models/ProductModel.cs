﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace VencruApi.Models
{
    public class ProductModel
    {
        //public ProductModel()
        //{
        //    Invoices = new HashSet<InvoiceModel>();
        //}

        [Key]
        public int id { get; set; }

        [Required]
        public string stocknumber { get; set; }

        [Required]
        public string productname { get; set; }

        public string description { get; set; }

        public int unitprice { get; set; }

        public int utime
        {
            get { return getUtime(); }

            set { }
        }

        [Required]
        public string userid { get; set; }

        public string date_created { get; set; }

        public bool isdeleted { get; set; }

        public int costofitem { get; set; }

        [Required]
        public int businessid { get; set; }

        public BusinessModel business { get; set; }

        public ICollection<ItemsModel> items { get; set; }

        //public virtual ICollection<InvoiceModel> Invoices { get; set; }

        private int getUtime()
        {
            return utime = (Int32)DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1)).TotalSeconds;

        }
    }
}