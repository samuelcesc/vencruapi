﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace VencruApi.Models
{
    public class VerifyInvoiceModel
    {
        [Required]
        public string code { get; set; }
        [Required]
        public int invoiceid { get; set; }
        [Required]
        public int businessid { get; set; }
    }
}