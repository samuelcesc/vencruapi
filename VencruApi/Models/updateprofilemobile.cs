﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace VencruApi.Models
{
    public class updateprofilemobile
    {
        [Required]
        public string firstname { get; set; }
        [Required]
        public string lastname { get; set; }
        [Required]
        public int businessid { get; set; }
        public string domain { get; set; }
        public string facebookurl { get; set; }
        public string instagramurl { get; set; }
        public string twitterurl { get; set; }
        public string linkdinurl { get; set; }
        [Required]
        public string userid { get; set; }
    }
}