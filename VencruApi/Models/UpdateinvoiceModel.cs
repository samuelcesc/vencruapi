﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace VencruApi.Models
{
    public class UpdateinvoiceModel
    {
        [Required]
        public int businessid { get; set; }
        [Required]
        public int invoiceid { get; set; }
        [Required]
        public string userid { get; set; }
        [Required]
        public string status { get; set; }
    }
}