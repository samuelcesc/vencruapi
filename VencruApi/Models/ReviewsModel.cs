﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace VencruApi.Models
{
    public class ReviewsModel
    {
        [Key]
        public int id { get; set; }
        [Required]
        public string fullname { get; set; }
        [Required]
        public string reviewcomment { get; set; }
        public bool isapproved { get; set; }
        public DateTime date_created { get; set; }
    }
}