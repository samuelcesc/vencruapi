﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace VencruApi.Models
{
    public class PromoCodeModel
    {
        public PromoCodeModel()
        {
            Users = new HashSet<ApplicationUser>();
        }

        public virtual ICollection<ApplicationUser> Users { get; set; }

        [Key]
        public int id { get; set; }
        [Required]
        public string code { get; set; }
        [Required]
        public int discount { get; set; }
        [Required]
        public int limit { get; set; }
        public string date_created { get; set; }
        public string created_by { get; set; }
    }
}