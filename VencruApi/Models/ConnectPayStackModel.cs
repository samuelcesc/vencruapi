﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace VencruApi.Models
{
    public class ConnectPayStackModel
    {
        [Required]
        public string userid { get; set; }
        [Required]
        public int businessid { get; set; }
        [Required]
        public string bankname { get; set; }
        [Required]
        public string accountnumber { get; set; }
        [Required]
        public string businessname { get; set; }
    }
}