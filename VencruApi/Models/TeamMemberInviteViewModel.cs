﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VencruApi.Models
{
    public class TeamMemberInviteViewModel
    {
        public int id { get; set; }
        public string memberemail { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string roles { get; set; }
        public bool status { get; set; }
        public string userid { get; set; }
        public string date_created { get; set; }
        public string callbackurl { get; set; }
        public BusinessViewModel business { get; set; }
    }
}