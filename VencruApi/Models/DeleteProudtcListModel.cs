﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VencruApi.Models
{
    public class DeleteProuductListModel
    {
        public int? businessid { get; set; }
        public int? productid { get; set; }
        public string userid { get; set; }
    }
}