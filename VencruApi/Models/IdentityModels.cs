﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using VencruApi.Models.DalClasses;

namespace VencruApi.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit https://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager, string authenticationType)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, authenticationType);
            // Add custom user claims here
            return userIdentity;
        }

        public ApplicationUser()
        {
            Business = new HashSet<BusinessModel>();
            PromoCode = new HashSet<PromoCodeModel>();
        }

        public virtual ICollection<BusinessModel> Business { get; set; }

        public ICollection<SubscriptionModel> subscriptions { get; set; }

        public virtual ICollection<PromoCodeModel> PromoCode { get; set; }

        public string firstname { get; set; }

        public string imagekey { get; set; }

        public string lastname { get; set; }

        public string date_created { get; set; }

        [Timestamp]
        public byte[] row_version { get; set; }

        public string verificationcode { get; set; }

    }
    public class ApplicationRole : IdentityRole
    {
        public ApplicationRole() : base() { }

        public ApplicationRole(string rolename, int businessid) : base(rolename)
        {
            this.businessid = businessid;
            
        }

        public virtual int businessid { get; set; }
    }
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {

        public ApplicationDbContext()
            : base($"{ConnectionStringDal.ConnectionString}", throwIfV1Schema: false)
        { }

        public virtual DbSet<ClientsModel> ClientsModels { get; set; }
        public virtual DbSet<ExpenseModel> ExpenseModels { get; set; }
        public virtual DbSet<ProductModel> ProductModels { get; set; }
        public virtual DbSet<BusinessModel> BusinessModels { get; set; }
        public virtual DbSet<InvoiceModel> InvoiceModels { get; set; }
        public virtual DbSet<BankSettingsModel> PaymentSettingsModels { get; set; }
        public virtual DbSet<UserCurrentBusinessModel> UserCurrentBusinessModels { get; set; }
        public virtual DbSet<WaitListModel> WaitListModels { get; set; }
        public virtual DbSet<FeedbackModel> FeedbackModels { get; set; }
        public virtual DbSet<PlanModel> PlanModels { get; set; }
        public virtual DbSet<ReviewsModel> ReviewsModels { get; set; }
        public virtual DbSet<SubscriptionModel> SubscriptionModels { get; set; }
        public virtual DbSet<TeamMemberModel> TeamMembers { get; set; }
        public virtual DbSet<InvoicePaymentModel> InvoicePaymentModels { get; set; }
        public virtual DbSet<ItemsModel> ItemsModels { get; set; }
        public virtual DbSet<PromoCodeModel> PromoCodeModels { get; set; }
        public virtual DbSet<ContactUsModel> ContactUsModels { get; set; }
        public virtual DbSet<AuditTrailModel> AuditTrailModels { get; set; }
        public virtual DbSet<PayStackSubAccountModel> PayStackSubAccountModels { get; set; }
        
        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

    }
}