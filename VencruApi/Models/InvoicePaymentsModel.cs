﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace VencruApi.Models
{
    public class InvoicePaymentModel
    {
        [Key]
        public int id { get; set; }
        [Required]
        public int amount { get; set; }
        public string paidwith { get; set; }
        [Required]
        public string date_paid { get; set; }
        public string date_created { get; set; }
        public string userid { get; set; }
        [Required]
        public int invoiceid { get; set; }
        public InvoiceModel invoice { get; set; }
    }
}