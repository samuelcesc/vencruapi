﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VencruApi.Models
{
    public class OnBoardingModel
    {
        public string city
        {
            get;
            set;
        }

        public string companyName
        {
            get;
            set;
        }

        public string country
        {
            get;
            set;
        }

        public string firstName
        {
            get;
            set;
        }

        public string industry
        {
            get;
            set;
        }

        public byte isIncorporated
        {
            get;
            set;
        }

        public string lastName
        {
            get;
            set;
        }

        public string phoneNumber
        {
            get;
            set;
        }

        public string state
        {
            get;
            set;
        }

        //public string Street
        //{
        //    get;
        //    set;
        //}

        public string userId
        {
            get;
            set;
        }

        public byte onlinePayment { get; set; }

        public string currency { get; set; }

        public string employeeSize { get; set; }

        public string address { get; set; }

    }
}