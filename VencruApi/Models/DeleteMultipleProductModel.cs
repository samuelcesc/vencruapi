﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VencruApi.Models
{
    public class DeleteMultipleProductModel
    {
        public string userid { get; set; }
        public List<DeleteProuductListModel> items { get; set; }
    }
}