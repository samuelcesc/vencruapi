﻿using Amazon;
using Amazon.S3;
using Amazon.S3.Transfer;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using VencruApi.Models.Classes;
namespace VencruApi.Models
{
    public class ExpenseModel
    {
        [Key]
        public int id { get; set; }

        [Required]
        public string expensenumber { get; set; }

        public string description { get; set; }

        public string category { get; set; }
       
        public string date_created { get; set; }

        public string expensedate { get; set; }

        public string vendor { get; set; }

        public string paidwith { get; set; }

        //public string receiptimageurl { get; set; }
       
        public string image { get; set; }

        [Required]
        public int totalamount { get; set; }
       
        public int businessid { get; set; }

        [Required]
        public string userid { get; set; }

        public int utime
        {
            get { return getUtime(); }

            set { }
        }

        public BusinessModel business { get; set; }
        
        private int getUtime()
        {
            return utime = (Int32)DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1)).TotalSeconds;
        }
    }

    
}

