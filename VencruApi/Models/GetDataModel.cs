﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VencruApi.Models
{
    public class GetDataModel
    {
        public int? businessid { get; set; }
        public int? page { get; set; }
        public int? limit { get; set; } 
        public string sortby { get; set; }
        public string sortorder { get; set; }
        public bool listtype { get; set; }
        public string filter { get; set; }
        public GetDataModel(int? bid)
        {
            businessid = bid;
            page = 1;
            limit = 10;
            sortby = "date_created";
            sortorder = "desc";
            filter = string.Empty;
        }
    }
}