﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace VencruApi.Models
{
    public class FeedbackModel
    {
        [Key]
        public int id { get; set; }
        [Required]
        public string emailaddress { get; set; }
        public int netpromoterscore { get; set; }
        [Required]
        public string details { get; set; }
        public DateTime date_created { get; set; }
    }
}