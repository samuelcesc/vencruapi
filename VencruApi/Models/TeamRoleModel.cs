﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace VencruApi.Models
{
    public class TeamRoleModel
    {
        [Required]
        public int inviteid { get; set; }
        [Required]
        public int businessid { get; set; }
        [Required]
        public string userid { get; set; }
        [Required]
        public string memberemail { get; set; }
        [Required]
        public string rolename { get; set; }
    }
}