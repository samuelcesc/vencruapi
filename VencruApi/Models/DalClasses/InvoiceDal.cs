﻿using SharpRaven;
using SharpRaven.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using VencruApi.Models.Classes;
using VencruApi.Models.ViewModels;

namespace VencruApi.Models.DalClasses
{
    public class InvoiceDal
    {

        public InvoiceDal(UserAccountDal userAccountDal, ClientDal clientDal)
        {
            userAccountDal = UserAccountDal;

            clientDal = ClientDal;
        }

        private UserAccountDal UserAccountDal
        {
            get
            {
                return UserAccountDal = new UserAccountDal();
            }
            set
            {

            }
        }

        private ClientDal ClientDal
        {
            get
            {
                return ClientDal = new ClientDal();
            }
            set
            {


            }
        }

        //generate expense number
        private static string GetInvoiceNumber(string count)
        {
            string expensenumber = string.Format("INV-{0:0}", count);

            return expensenumber;
        }

        //get expense number
        public static string GenerateInvoiceNumber(int businessid, string userid)
        {
            int invoicecount = 0;

            string invoicenumber = string.Empty;

            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");


            try
            {
                DataSet userDataSet = new DataSet();

                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter();

                using (SqlConnection connection = new SqlConnection(ConnectionStringDal.ConnectionString))
                {
                    SqlDataAdapter sqlAdapter = new SqlDataAdapter();
                    SqlCommand sqlCommand = new SqlCommand(StoredProcedureDal.GetInvoiceNumberCount, connection);
                    connection.Open();
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.AddWithValue("@Businessid", businessid);

                    sqlDataAdapter.SelectCommand = sqlCommand;
                    sqlDataAdapter.Fill(userDataSet);
                    if (userDataSet != null & userDataSet.Tables.Count > 0)
                    {
                        for (int index = 0; index <= userDataSet.Tables[0].Rows.Count - 1; index++)
                        {
                            invoicecount = Convert.ToInt32(userDataSet.Tables[0].Rows[index]["invoicecount"]);
                        }
                    }
                    else { }
                }
                var expensecountpadded = invoicecount + 1;
                invoicenumber = GetInvoiceNumber(expensecountpadded.ToString("D7"));

            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return invoicenumber;
        }

        //add invoice
        public async Task<string> CreateInvoice(InvoiceModel model)
        {
            string issuccessful = string.Empty;

            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            try
            {
                if (!UserAccountDal.CheckUser(model.userid))
                {
                    return null;
                }
                else
                {
                    if (string.IsNullOrEmpty(model.sendstyle))
                    {
                        model.invoicestatus = "draft";
                    }
                    else
                    {
                        model.invoicestatus = "not paid";
                    }
                    using (var connection = new SqlConnection(ConnectionStringDal.ConnectionString))
                    {
                        var sqlAdapter = new SqlDataAdapter();
                        var sqlCommand = new SqlCommand(StoredProcedureDal.AddInvoice, connection);
                        connection.Open();
                        sqlCommand.CommandType = CommandType.StoredProcedure;
                        sqlCommand.Parameters.Add("@Id", SqlDbType.Int).Direction = ParameterDirection.Output;


                        sqlCommand.Parameters.AddWithValue("@description", model.description = model.description ?? CreateParameterValue(model.description));
                        sqlCommand.Parameters.AddWithValue("@subtotal", model.subtotal = (int?)model.subtotal ?? 0);
                        sqlCommand.Parameters.AddWithValue("@discount", model.discount = (int?)model.discount ?? 0);
                        sqlCommand.Parameters.AddWithValue("@amountdue", model.amountdue = (int?)model.amountdue ?? 0);
                        sqlCommand.Parameters.AddWithValue("@deposit", model.deposit = (int?)model.deposit ?? 0);
                        sqlCommand.Parameters.AddWithValue("@paymenttype", model.paymenttype = model.paymenttype ?? CreateParameterValue(model.paymenttype));
                        sqlCommand.Parameters.AddWithValue("@notes", model.notes = model.notes ?? CreateParameterValue(model.notes));
                        sqlCommand.Parameters.AddWithValue("@invoicestyle", model.invoicestyle = (int?)model.invoicestyle ?? 0);
                        sqlCommand.Parameters.AddWithValue("@userid", model.userid);
                        sqlCommand.Parameters.AddWithValue("@color", model.color = model.color ?? CreateParameterValue(model.color));
                        sqlCommand.Parameters.AddWithValue("@font", model.font = model.font ?? CreateParameterValue(model.font));
                        sqlCommand.Parameters.AddWithValue("@personalmessage", model.personalmessage = model.personalmessage ?? CreateParameterValue(model.personalmessage));
                        sqlCommand.Parameters.AddWithValue("@sendstyle", model.sendstyle = model.sendstyle ?? "draft");
                        sqlCommand.Parameters.AddWithValue("@invoicestatus", model.invoicestatus);
                        sqlCommand.Parameters.AddWithValue("@utime", model.utime);
                        sqlCommand.Parameters.AddWithValue("@due_date", model.due_date = model.due_date ?? CreateParameterValue(model.due_date));
                        sqlCommand.Parameters.AddWithValue("@businessid", model.businessid);
                        sqlCommand.Parameters.AddWithValue("@clientid", model.clientid);
                        sqlCommand.Parameters.AddWithValue("@invoicenumber", model.invoicenumber);
                        sqlCommand.Parameters.AddWithValue("@invoiceguid", model.invoiceguid);
                        sqlCommand.Parameters.AddWithValue("@paymentlink", model.paymentlink = model.paymentlink ?? CreateParameterValue(model.paymentlink));
                        sqlCommand.Parameters.AddWithValue("@type", model.invoicetype);
                        sqlCommand.Parameters.AddWithValue("@issuedate", model.issue_date);
                        sqlCommand.Parameters.AddWithValue("@requireddeposit", model.requireddeposit = (int?)model.requireddeposit ?? 0);
                        sqlCommand.Parameters.AddWithValue("@email", model.email = model.email ?? CreateParameterValue(model.email));


                        //sqlCommand.Parameters.AddWithValue("@description", model.description);
                        //sqlCommand.Parameters.AddWithValue("@subtotal", model.subtotal);
                        //sqlCommand.Parameters.AddWithValue("@discount", model.discount);
                        //sqlCommand.Parameters.AddWithValue("@amountdue", model.amountdue);
                        //sqlCommand.Parameters.AddWithValue("@deposit", model.deposit);
                        //sqlCommand.Parameters.AddWithValue("@paymenttype", model.paymenttype);
                        //sqlCommand.Parameters.AddWithValue("@notes", model.notes);
                        //sqlCommand.Parameters.AddWithValue("@invoicestyle", model.invoicestyle);
                        //sqlCommand.Parameters.AddWithValue("@userid", model.userid);
                        //sqlCommand.Parameters.AddWithValue("@color", model.color);
                        //sqlCommand.Parameters.AddWithValue("@font", model.font);
                        //sqlCommand.Parameters.AddWithValue("@sendstyle", model.sendstyle);
                        //sqlCommand.Parameters.AddWithValue("@personalmessage", model.personalmessage);
                        //sqlCommand.Parameters.AddWithValue("@invoicestatus", model.invoicestatus);
                        //sqlCommand.Parameters.AddWithValue("@utime", model.utime);
                        //sqlCommand.Parameters.AddWithValue("@due_date", model.due_date);
                        //sqlCommand.Parameters.AddWithValue("@businessid", model.businessid);
                        //sqlCommand.Parameters.AddWithValue("@clientid", model.clientid);
                        //sqlCommand.Parameters.AddWithValue("@invoicenumber", model.invoicenumber);
                        //sqlCommand.Parameters.AddWithValue("@invoiceguid", model.invoiceguid);
                        //sqlCommand.Parameters.AddWithValue("@paymentlink", model.paymentlink);
                        //sqlCommand.Parameters.AddWithValue("@type", model.invoicetype);
                        //sqlCommand.Parameters.AddWithValue("@requireddeposit", model.requireddeposit);
                        sqlCommand.Parameters.AddWithValue("@isdeleted", model.isdeleted = false);
                        await sqlCommand.ExecuteNonQueryAsync();
                        issuccessful = sqlCommand.Parameters["@Id"].Value.ToString();
                    }
                }

                foreach (var item in model.items)
                {
                    item.invoiceid = Convert.ToInt32(issuccessful);
                    await Update(item);
                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }
            return issuccessful;
        }

        //get invoice
        public InvoiceViewModel GetInvoice(int businessid, int invoiceid, string userid)
        {
            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            var invoiceModel = new InvoiceViewModel();

            var invoiceinfo = new InvoiceViewModel();

            int clientid = 0;

            try
            {
                DataSet userDataSet = new DataSet();
                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter();
                using (SqlConnection connection = new SqlConnection(ConnectionStringDal.ConnectionString))
                {
                    SqlDataAdapter sqlAdapter = new SqlDataAdapter();
                    SqlCommand sqlCommand = new SqlCommand(StoredProcedureDal.GetInvoice, connection);
                    connection.Open();
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.AddWithValue("@invoiceid", invoiceid);
                    sqlCommand.Parameters.AddWithValue("@businessid", businessid);
                    sqlDataAdapter.SelectCommand = sqlCommand;
                    sqlDataAdapter.Fill(userDataSet);
                    if (userDataSet != null & userDataSet.Tables.Count > 0)
                    {
                        for (int index = 0; index <= userDataSet.Tables[0].Rows.Count - 1; index++)
                        {
                            invoiceinfo.description = Convert.ToString(userDataSet.Tables[0].Rows[index]["description"]);
                            invoiceinfo.subtotal = Convert.ToInt32(userDataSet.Tables[0].Rows[index]["subtotal"]);
                            invoiceinfo.email = Convert.ToString(userDataSet.Tables[0].Rows[index]["email"]);
                            invoiceinfo.discount = Convert.ToInt32(userDataSet.Tables[0].Rows[index]["discount"]);
                            invoiceinfo.paymenttype = Convert.ToString(userDataSet.Tables[0].Rows[index]["paymenttype"]);
                            invoiceinfo.notes = Convert.ToString(userDataSet.Tables[0].Rows[index]["notes"]);
                            invoiceinfo.invoicestyle = Convert.ToInt32(userDataSet.Tables[0].Rows[index]["invoicestyle"]);
                            invoiceinfo.date_created = Convert.ToDateTime(userDataSet.Tables[0].Rows[index]["date_created"]);
                            invoiceinfo.issue_date = DateTime.ParseExact(Convert.ToString(userDataSet.Tables[0].Rows[index]["issue_date"]), "dd/MM/yyyy", null);
                            invoiceinfo.color = Convert.ToString(userDataSet.Tables[0].Rows[index]["color"]);
                            invoiceinfo.id = Convert.ToInt32(userDataSet.Tables[0].Rows[index]["id"]);
                            invoiceinfo.font = Convert.ToString(userDataSet.Tables[0].Rows[index]["font"]);
                            invoiceinfo.sendstyle = Convert.ToString(userDataSet.Tables[0].Rows[index]["sendstyle"]);
                            invoiceinfo.personalmessage = Convert.ToString(userDataSet.Tables[0].Rows[index]["personalmessage"]);
                            invoiceinfo.invoicestatus = Convert.ToString(userDataSet.Tables[0].Rows[index]["invoicestatus"]);
                            invoiceinfo.userid = Convert.ToString(userDataSet.Tables[0].Rows[index]["userid"]);
                            invoiceinfo.due_date = DateTime.ParseExact(Convert.ToString(userDataSet.Tables[0].Rows[index]["due_date"]), "dd/MM/yyyy", null);
                            clientid = Convert.ToInt32(userDataSet.Tables[0].Rows[index]["clientid"]);
                            invoiceinfo.invoiceguid = Convert.ToString(userDataSet.Tables[0].Rows[index]["invoiceguid"]);
                            invoiceinfo.invoicenumber = Convert.ToString(userDataSet.Tables[0].Rows[index]["invoicenumber"]);
                            invoiceinfo.invoicetype = Convert.ToString(userDataSet.Tables[0].Rows[index]["invoicetype"]);
                            invoiceinfo.amountdue = Convert.ToInt32(userDataSet.Tables[0].Rows[index]["amountdue"]);

                        }

                        invoiceinfo.image = UtilitiesClass.GetImgUrl(invoiceinfo.image, UtilitiesClass.businesslogoBucketname);

                    }


                }

                invoiceinfo.business = UserAccountDal.GetUserBusiness(userid, businessid);

                invoiceinfo.client = ClientDal.GetClient(businessid, clientid);

                invoiceinfo.items = GetItems(invoiceinfo.id);

                invoiceModel = invoiceinfo;

                var totalpayments = GetAmountDue(businessid, invoiceModel.id, userid);

                invoiceModel.amountdue = invoiceModel.amountdue - totalpayments;

            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return invoiceModel;
        }

        //get all invoices
        public InvoiceListViewModel GetAllinvoices(GetDataModel model)
        {
            var invoicelist = new InvoiceListViewModel();

            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            try
            {
                var userDataSet = new DataSet();
                int clientid = 0;

                var sqlDataAdapter = new SqlDataAdapter();

                using (var connection = new SqlConnection(ConnectionStringDal.ConnectionString))
                {
                    SqlDataAdapter sqlAdapter = new SqlDataAdapter();
                    SqlCommand sqlCommand = new SqlCommand(StoredProcedureDal.GetAllInvoices, connection);
                    connection.Open();
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.AddWithValue("@BusinessId", model.businessid);
                    sqlCommand.Parameters.AddWithValue("@Page", model.page);
                    sqlCommand.Parameters.AddWithValue("@Limit", model.limit);
                    sqlCommand.Parameters.AddWithValue("@Sortorder", model.sortorder);
                    sqlCommand.Parameters.AddWithValue("@Sortby", model.sortby);
                    sqlCommand.Parameters.AddWithValue("@Listtype", model.listtype);

                    sqlDataAdapter.SelectCommand = sqlCommand;
                    sqlDataAdapter.Fill(userDataSet);
                    if (userDataSet != null & userDataSet.Tables.Count > 0)
                    {
                        for (int index = 0; index <= userDataSet.Tables[0].Rows.Count - 1; index++)
                        {
                           

                            InvoiceViewModel invoiceinfo = new InvoiceViewModel()
                            {
                                amountdue = Convert.ToInt32(userDataSet.Tables[0].Rows[index]["amountdue"]),
                                color = Convert.ToString(userDataSet.Tables[0].Rows[index]["color"]),
                                deposit = Convert.ToInt32(userDataSet.Tables[0].Rows[index]["deposit"]),
                                discount = Convert.ToInt32(userDataSet.Tables[0].Rows[index]["discount"]),
                                due_date = DateTime.ParseExact(Convert.ToString(userDataSet.Tables[0].Rows[index]["due_date"]), "dd/MM/yyyy", null),
                                font = Convert.ToString(userDataSet.Tables[0].Rows[index]["font"]),
                                notes = Convert.ToString(userDataSet.Tables[0].Rows[index]["notes"]),
                                subtotal = Convert.ToInt32(userDataSet.Tables[0].Rows[index]["subtotal"]),
                                sendstyle = Convert.ToString(userDataSet.Tables[0].Rows[index]["sendstyle"]),
                                personalmessage = Convert.ToString(userDataSet.Tables[0].Rows[index]["personalmessage"]),
                                paymenttype = Convert.ToString(userDataSet.Tables[0].Rows[index]["paymenttype"]),
                                invoicestyle = Convert.ToInt32(userDataSet.Tables[0].Rows[index]["invoicestyle"]),
                                invoicestatus = Convert.ToString(userDataSet.Tables[0].Rows[index]["invoicestatus"]),
                                description = Convert.ToString(userDataSet.Tables[0].Rows[index]["Description"]),
                                id = Convert.ToInt32(userDataSet.Tables[0].Rows[index]["Id"]),
                                userid = Convert.ToString(userDataSet.Tables[0].Rows[index]["UserId"]),
                                date_created = DateTime.ParseExact(Convert.ToString(userDataSet.Tables[0].Rows[index]["due_date"]), "dd/MM/yyyy", null),
                                invoicenumber = Convert.ToString(userDataSet.Tables[0].Rows[index]["invoicenumber"]),
                                invoicetype = Convert.ToString(userDataSet.Tables[0].Rows[index]["invoicetype"]),
                                email = Convert.ToString(userDataSet.Tables[0].Rows[index]["email"]),
                                issue_date = DateTime.ParseExact(Convert.ToString(userDataSet.Tables[0].Rows[index]["issue_date"]), "dd/MM/yyyy", null),
                            };

                            clientid = Convert.ToInt32(userDataSet.Tables[0].Rows[index]["clientid"]);

                            invoiceinfo.image = UtilitiesClass.GetImgUrl(invoiceinfo.image, UtilitiesClass.businesslogoBucketname);

                            invoiceinfo.business = UserAccountDal.GetUserBusiness(invoiceinfo.userid, (int)model.businessid);

                            invoiceinfo.client = ClientDal.GetClient(model.businessid, clientid);

                            invoiceinfo.items = GetItems(invoiceinfo.id);

                            var totalpayments = GetAmountDue(invoiceinfo.business.id, invoiceinfo.id, invoiceinfo.userid);

                            invoiceinfo.amountdue = invoiceinfo.amountdue - totalpayments;

                            invoicelist.invoices.Add(invoiceinfo);

                            invoicelist.currentpage = Convert.ToInt32(userDataSet.Tables[0].Rows[0]["pagenumber"]);

                            invoicelist.total = Convert.ToInt32(userDataSet.Tables[0].Rows[0]["invoicetotal"]);

                            invoicelist.pagetotal = GetPageTotal(invoicelist.total, Convert.ToInt32(userDataSet.Tables[0].Rows[0]["limit"]));

                        }

                    }

                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }
            return invoicelist;
        }

        //validate invoice
        public int ValidateInvoice(VerifyInvoiceModel model)
        {
            int count = 0;

            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            try
            {
                DataSet userDataSet = new DataSet();
                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter();
                using (SqlConnection connection = new SqlConnection(ConnectionStringDal.ConnectionString))
                {
                    SqlDataAdapter sqlAdapter = new SqlDataAdapter();
                    SqlCommand sqlCommand = new SqlCommand(StoredProcedureDal.VerifyInvoice, connection);
                    connection.Open();
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.AddWithValue("@invoiceid", model.invoiceid);
                    sqlCommand.Parameters.AddWithValue("@businessid", model.businessid);
                    sqlCommand.Parameters.AddWithValue("@code", model.code);
                    sqlDataAdapter.SelectCommand = sqlCommand;
                    sqlDataAdapter.Fill(userDataSet);
                    if (userDataSet != null & userDataSet.Tables.Count > 0)
                    {
                        for (int index = 0; index <= userDataSet.Tables[0].Rows.Count - 1; index++)
                        {
                            count = Convert.ToInt32(userDataSet.Tables[0].Rows[index]["count"]);

                        }

                    }

                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return count;
        }

        //update invoice status
        public async Task<bool> UpdateInvoiceStatus(UpdateinvoiceModel model)
        {
            bool issuccessful = false;

            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            try
            {
                if (!UserAccountDal.CheckUser(model.userid))
                {
                    return issuccessful;
                }
                else
                {
                    using (var connection = new SqlConnection(ConnectionStringDal.ConnectionString))
                    {
                        var sqlAdapter = new SqlDataAdapter();
                        var sqlCommand = new SqlCommand(StoredProcedureDal.AddClient, connection);
                        connection.Open();
                        sqlCommand.CommandType = CommandType.StoredProcedure;
                        sqlCommand.Parameters.Add("@Id", SqlDbType.Int).Direction = ParameterDirection.Output;
                        sqlCommand.Parameters.AddWithValue("@businessid", model.businessid);
                        sqlCommand.Parameters.AddWithValue("@invoiceid", model.invoiceid);
                        sqlCommand.Parameters.AddWithValue("@status", model.status);
                        sqlCommand.Parameters.AddWithValue("@userid", model.userid);

                        await sqlCommand.ExecuteNonQueryAsync();
                    }

                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }
            return issuccessful;
        }

        //update invoice
        public async Task<bool> UpdateInvoice(InvoiceModel model)
        {
            bool issuccessful = false;

            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            try
            {
                if (!UserAccountDal.CheckUser(model.userid))
                {
                    return issuccessful;
                }
                else
                {
                    using (var connection = new SqlConnection(ConnectionStringDal.ConnectionString))
                    {
                        var sqlAdapter = new SqlDataAdapter();
                        var sqlCommand = new SqlCommand(StoredProcedureDal.UpdateInvoice, connection);
                        connection.Open();
                        sqlCommand.CommandType = CommandType.StoredProcedure;
                        sqlCommand.Parameters.AddWithValue("@id", model.id);
                        sqlCommand.Parameters.AddWithValue("@description", model.description);
                        sqlCommand.Parameters.AddWithValue("@subtotal", model.subtotal);
                        sqlCommand.Parameters.AddWithValue("@discount", model.discount);
                        sqlCommand.Parameters.AddWithValue("@amountdue", model.amountdue);
                        sqlCommand.Parameters.AddWithValue("@deposit", model.deposit);
                        sqlCommand.Parameters.AddWithValue("@paymenttype", model.paymenttype);
                        sqlCommand.Parameters.AddWithValue("@notes", model.notes);
                        sqlCommand.Parameters.AddWithValue("@invoicestyle", model.invoicestyle);
                        sqlCommand.Parameters.AddWithValue("@userid", model.userid);
                        sqlCommand.Parameters.AddWithValue("@color", model.color);
                        sqlCommand.Parameters.AddWithValue("@font", model.font);
                        sqlCommand.Parameters.AddWithValue("@sendstyle", model.sendstyle);
                        sqlCommand.Parameters.AddWithValue("@personalmessage", model.personalmessage);
                        sqlCommand.Parameters.AddWithValue("@invoicestatus", model.invoicestatus);
                        sqlCommand.Parameters.AddWithValue("@due_date", model.due_date);
                        sqlCommand.Parameters.AddWithValue("@businessid", model.businessid);
                        sqlCommand.Parameters.AddWithValue("@clientid", model.clientid);
                        sqlCommand.Parameters.AddWithValue("@invoicenumber", model.invoicenumber);
                        sqlCommand.Parameters.AddWithValue("@paymentlink", model.paymentlink);
                        
                        await sqlCommand.ExecuteNonQueryAsync();
                    }
                }

                foreach (var item in model.items)
                {
                    await Update(item);
                }

                issuccessful = true;
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return issuccessful;
        }

        //add product to invoice //not used
        private async Task<bool> AddProductToInvoice(int productid, int invoiceid)
        {
            bool issuccessful = false;

            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            try
            {

                using (var connection = new SqlConnection(ConnectionStringDal.ConnectionString))
                {
                    var sqlAdapter = new SqlDataAdapter();
                    var sqlCommand = new SqlCommand(StoredProcedureDal.AddProductToInvoice, connection);
                    connection.Open();
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.AddWithValue("@productid", productid);
                    sqlCommand.Parameters.AddWithValue("@invoiceid", invoiceid);
                    await sqlCommand.ExecuteNonQueryAsync();
                    issuccessful = true;
                }

            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return issuccessful;
        }

        //add invoice item
        private async Task<bool> AddItem(ItemsModel model)
        {
            bool issuccessful = false;

            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            try
            {

                using (var connection = new SqlConnection(ConnectionStringDal.ConnectionString))
                {
                    var sqlAdapter = new SqlDataAdapter();
                    var sqlCommand = new SqlCommand(StoredProcedureDal.AddInvoiceItem, connection);
                    connection.Open();
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.AddWithValue("@id", model.id);
                    sqlCommand.Parameters.AddWithValue("@description", model.description);
                    sqlCommand.Parameters.AddWithValue("@amount", model.amount);
                    sqlCommand.Parameters.AddWithValue("@invoiceid", model.invoiceid);
                    sqlCommand.Parameters.AddWithValue("@productid", model.productid);
                    sqlCommand.Parameters.AddWithValue("@price", model.price);
                    sqlCommand.Parameters.AddWithValue("@quantity", model.quantity);
                    await sqlCommand.ExecuteNonQueryAsync();
                    issuccessful = true;
                }

            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return issuccessful;
        }

        //delete invoice temp
        public async Task<bool> DeleteInvoiceTemp(InvoiceOperationModel model)
        {
            bool isSuccessful = false;

            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            try
            {
                using (var connection = new SqlConnection(ConnectionStringDal.ConnectionString))
                {
                    var sqlAdapter = new SqlDataAdapter();
                    var sqlCommand = new SqlCommand(StoredProcedureDal.DeleteInvoiceTemp, connection);
                    connection.Open();
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.AddWithValue("@businessid", model.businessid);
                    sqlCommand.Parameters.AddWithValue("@invoiceid", model.invoiceid);
                    await sqlCommand.ExecuteNonQueryAsync();
                    isSuccessful = true;
                }

            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }
            return isSuccessful;
        }

        //cancel invoice
        public async Task<bool> CancelInvoice(InvoiceOperationModel model)
        {
            bool isSuccessful = false;

            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            try
            {
                using (var connection = new SqlConnection(ConnectionStringDal.ConnectionString))
                {
                    var sqlAdapter = new SqlDataAdapter();
                    var sqlCommand = new SqlCommand(StoredProcedureDal.CancelInvoice, connection);
                    connection.Open();
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.AddWithValue("@businessid", model.businessid);
                    sqlCommand.Parameters.AddWithValue("@invoiceid", model.invoiceid);
                    await sqlCommand.ExecuteNonQueryAsync();
                    isSuccessful = true;
                }

            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return isSuccessful;
        }

        //get items
        public static List<InvoiceItemViewModel> GetItems(int invoiceid)
        {
            var invoiceitemlist = new List<InvoiceItemViewModel>();

            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            try
            {
                DataSet userDataSet = new DataSet();
                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter();
                using (SqlConnection connection = new SqlConnection(ConnectionStringDal.ConnectionString))
                {
                    SqlDataAdapter sqlAdapter = new SqlDataAdapter();

                    SqlCommand sqlCommand = new SqlCommand(StoredProcedureDal.GetInvoiceItem, connection);
                    connection.Open();
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.AddWithValue("@invoiceid", invoiceid);
                    sqlDataAdapter.SelectCommand = sqlCommand;
                    sqlDataAdapter.Fill(userDataSet);
                    if (userDataSet != null & userDataSet.Tables.Count > 0)
                    {
                        for (int index = 0; index <= userDataSet.Tables[0].Rows.Count - 1; index++)
                        {
                            var invoiceinfo = new InvoiceItemViewModel()
                            {
                                description = Convert.ToString(userDataSet.Tables[0].Rows[index]["description"]),
                                amount = Convert.ToInt32(userDataSet.Tables[0].Rows[index]["amount"]),
                                id = Convert.ToInt32(userDataSet.Tables[0].Rows[index]["id"]),
                                price = Convert.ToInt32(userDataSet.Tables[0].Rows[index]["price"]),
                                quantity = Convert.ToInt32(userDataSet.Tables[0].Rows[index]["quantity"]),
                                date_created = Convert.ToDateTime(userDataSet.Tables[0].Rows[index]["date_created"]),

                            };

                            invoiceitemlist.Add(invoiceinfo);

                        }

                    }


                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }
            return invoiceitemlist;
        }

        //update items
        private async Task<bool> UpdateItem(ItemsModel model)

        {
            bool issuccessful = false;

            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            try
            {

                using (var connection = new SqlConnection(ConnectionStringDal.ConnectionString))
                {
                    var sqlAdapter = new SqlDataAdapter();
                    var sqlCommand = new SqlCommand(StoredProcedureDal.UpdateInvoiceItem, connection);
                    connection.Open();
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.AddWithValue("@id", model.id);
                    sqlCommand.Parameters.AddWithValue("@description", model.description);
                    sqlCommand.Parameters.AddWithValue("@amount", model.amount);
                    sqlCommand.Parameters.AddWithValue("@invoiceid", model.invoiceid);
                    sqlCommand.Parameters.AddWithValue("@productid", model.productid);
                    sqlCommand.Parameters.AddWithValue("@price", model.price);
                    sqlCommand.Parameters.AddWithValue("@quantity", model.quantity);
                    await sqlCommand.ExecuteNonQueryAsync();
                    issuccessful = true;
                }

            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }
            return issuccessful;
        }

        //delete invoice item
        public async Task<bool> DeleteInvoiceItem(InvoiceOperationModel model)
        {
            bool isSuccessful = false;

            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            try
            {
                using (var connection = new SqlConnection(ConnectionStringDal.ConnectionString))
                {
                    var sqlAdapter = new SqlDataAdapter();
                    var sqlCommand = new SqlCommand(StoredProcedureDal.DeleteInvoiceItem, connection);
                    connection.Open();
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.AddWithValue("@businessid", model.businessid);
                    sqlCommand.Parameters.AddWithValue("@invoiceid", model.invoiceid);
                    sqlCommand.Parameters.AddWithValue("@itemid", model.itemid);
                    await sqlCommand.ExecuteNonQueryAsync();
                    isSuccessful = true;
                }

            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }
            return isSuccessful;
        }


        //add payment to invoice
        public async Task<string> AddPayment(InvoicePaymentModel model)
        {
            string issuccessful = string.Empty;

            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            try
            {
                if (!UserAccountDal.CheckUser(model.userid))
                {
                    return null;
                }
                else
                {
                    using (var connection = new SqlConnection(ConnectionStringDal.ConnectionString))
                    {
                        var sqlAdapter = new SqlDataAdapter();
                        var sqlCommand = new SqlCommand(StoredProcedureDal.AddPayment, connection);
                        connection.Open();
                        sqlCommand.CommandType = CommandType.StoredProcedure;
                        sqlCommand.Parameters.Add("@Id", SqlDbType.Int).Direction = ParameterDirection.Output;
                        sqlCommand.Parameters.AddWithValue("@amount", model.amount);
                        sqlCommand.Parameters.AddWithValue("@date_paid", model.date_paid);
                        sqlCommand.Parameters.AddWithValue("@invoiceid", model.invoiceid);
                        sqlCommand.Parameters.AddWithValue("@paidwith", model.paidwith);
                        sqlCommand.Parameters.AddWithValue("@userid", model.userid);
                        await sqlCommand.ExecuteNonQueryAsync();
                        issuccessful = sqlCommand.Parameters["@Id"].Value.ToString();
                    }

                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return issuccessful;
        }

        //get payment
        public static InvoicePaymentViewModel GetPayment(int invoiceid, int paymentid, string userid)
        {

            InvoicePaymentViewModel invoicepaymentmodel = new InvoicePaymentViewModel();

            var paymentinfo = new InvoicePaymentViewModel();

            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            try
            {
                DataSet userDataSet = new DataSet();
                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter();
                using (SqlConnection connection = new SqlConnection(ConnectionStringDal.ConnectionString))
                {
                    SqlDataAdapter sqlAdapter = new SqlDataAdapter();
                    SqlCommand sqlCommand = new SqlCommand(StoredProcedureDal.GetPayment, connection);
                    connection.Open();
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.AddWithValue("@invoiceid", invoiceid);
                    sqlCommand.Parameters.AddWithValue("@paymentid", paymentid);
                    sqlDataAdapter.SelectCommand = sqlCommand;
                    sqlDataAdapter.Fill(userDataSet);
                    if (userDataSet != null & userDataSet.Tables.Count > 0)
                    {
                        for (int index = 0; index <= userDataSet.Tables[0].Rows.Count - 1; index++)
                        {
                            paymentinfo.paidwith = Convert.ToString(userDataSet.Tables[0].Rows[index]["paidwith"]);
                            paymentinfo.amount = Convert.ToInt32(userDataSet.Tables[0].Rows[index]["amount"]);
                            paymentinfo.paidwith = Convert.ToString(userDataSet.Tables[0].Rows[index]["paidwith"]);
                            paymentinfo.date_paid = Convert.ToDateTime(userDataSet.Tables[0].Rows[index]["date_paid"]);
                            paymentinfo.id = Convert.ToInt32(userDataSet.Tables[0].Rows[index]["id"]);
                            paymentinfo.date_created = Convert.ToDateTime(userDataSet.Tables[0].Rows[index]["date_created"]);
                        }

                    }

                }

                invoicepaymentmodel = paymentinfo;
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }
            return invoicepaymentmodel;
        }

        //get all payments
        public PaymentListViewModel GetAllPayments(GetDataModel model)
        {
            var paymentlist = new PaymentListViewModel();

            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            try
            {
                var userDataSet = new DataSet();

                var sqlDataAdapter = new SqlDataAdapter();

                using (var connection = new SqlConnection(ConnectionStringDal.ConnectionString))
                {
                    SqlDataAdapter sqlAdapter = new SqlDataAdapter();
                    SqlCommand sqlCommand = new SqlCommand(StoredProcedureDal.GetAllPayment, connection);
                    connection.Open();
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.AddWithValue("@paymentid", model.businessid);
                    sqlCommand.Parameters.AddWithValue("@Page", model.page);
                    sqlCommand.Parameters.AddWithValue("@Limit", model.limit);
                    sqlCommand.Parameters.AddWithValue("@Sortorder", model.sortorder);
                    sqlCommand.Parameters.AddWithValue("@Sortby", model.sortby);
                    sqlCommand.Parameters.AddWithValue("@Listtype", model.listtype);
                    sqlDataAdapter.SelectCommand = sqlCommand;
                    sqlDataAdapter.Fill(userDataSet);
                    if (userDataSet != null & userDataSet.Tables.Count > 0)
                    {
                        for (int index = 0; index <= userDataSet.Tables[0].Rows.Count - 1; index++)
                        {
                            InvoicePaymentViewModel invoiceinfo = new InvoicePaymentViewModel()
                            {
                                amount = Convert.ToInt32(userDataSet.Tables[0].Rows[index]["amount"]),
                                date_paid = Convert.ToDateTime(userDataSet.Tables[0].Rows[index]["date_paid"]),
                                paidwith = Convert.ToString(userDataSet.Tables[0].Rows[index]["paidwith"]),
                                id = Convert.ToInt32(userDataSet.Tables[0].Rows[index]["id"]),
                                date_created = Convert.ToDateTime(userDataSet.Tables[0].Rows[index]["date_created"]),
                            };

                            paymentlist.payments.Add(invoiceinfo);

                            paymentlist.currentpage = Convert.ToInt32(userDataSet.Tables[0].Rows[0]["pagenumber"]);

                            paymentlist.total = Convert.ToInt32(userDataSet.Tables[0].Rows[0]["paymenttotal"]);

                            paymentlist.pagetotal = GetPageTotal(paymentlist.total, Convert.ToInt32(userDataSet.Tables[0].Rows[0]["limit"]));

                        }

                    }

                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }
            return paymentlist;
        }

        //delete payment
        public async Task<bool> DeletePayment(PaymentOperationModel model)
        {
            bool isSuccessful = false;

            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            try
            {
                using (var connection = new SqlConnection(ConnectionStringDal.ConnectionString))
                {
                    var sqlAdapter = new SqlDataAdapter();
                    var sqlCommand = new SqlCommand(StoredProcedureDal.DeletePayment, connection);
                    connection.Open();
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.AddWithValue("@paymentid", model.id);
                    sqlCommand.Parameters.AddWithValue("@invoiceid", model.invoiceid);
                    await sqlCommand.ExecuteNonQueryAsync();
                    isSuccessful = true;
                }

            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }
            return isSuccessful;
        }

        public InvoiceSummaryVieModel GetSummary(int businessid, string userid, string sortby)
        {


            var invoicesummary = new InvoiceSummaryVieModel();

            var payments = new List<InvoicePaymentModel>();

            var invoices = new List<InvoiceModel>();

            var ok = new InvoiceViewModel();
            const string Format = "dd/MM/yyyy";
            var date = DateTime.Now.ToString(Format);

            var dtto = DateTime.ParseExact(date, Format, null);

            if (UserAccountDal.CheckUser(userid, businessid))
            {
                using (var dbConext = new ApplicationDbContext())
                {
                    // payments = dbConext.InvoicePaymentModels.Select(m => m).ToList();
                    invoices = dbConext.InvoiceModels.Include("payment").Where(m => m.businessid == businessid && m.invoicetype.ToLower() == "invoice").ToList();
                }


                if (sortby.ToLower() == "today")
                {
                    
                    invoices = invoices.Where(x => Convertdate(x.date_created).Value.Date == dtto.Date).ToList();
                }
                else if (sortby.ToLower() == "thismonth" && invoices.Count() > 0)
                {
                    var currentMonth = DateTime.Now.ToString("MM");
                    invoices = invoices.Where(x => Convertdate(x.date_created).Value.Date.ToString("MM") == currentMonth).ToList();
                }
                else if (sortby == "yesterday" && invoices.Count() > 0)
                {
                    var yesterday = DateTime.Now.Date.AddDays(-1);

                    invoices = invoices.Where(m => Convertdate(m.date_created).Value.Date == yesterday.Date).ToList();
                }
                else if (sortby == "thisweek" && invoices.Count() > 0)
                {
                    var cultureInfo = new CultureInfo("en-US");

                    var myCalender = cultureInfo.Calendar;

                    var myCalenderRule = cultureInfo.DateTimeFormat.CalendarWeekRule;

                    var myFirstDayOfWeek = cultureInfo.DateTimeFormat.FirstDayOfWeek;

                    var thisWeekNumber = myCalender.GetWeekOfYear(DateTime.Now, myCalenderRule, myFirstDayOfWeek);

                    var invoicesModel = new List<InvoiceModel>();
                    InvoiceModel invoicesingle = new InvoiceModel();

                    foreach (var item in invoices)
                    {
                        var weekNumber = myCalender.GetWeekOfYear(Convertdate(item.date_created).Value.Date, myCalenderRule, myFirstDayOfWeek);

                        if (thisWeekNumber == weekNumber)
                        {
                            invoicesingle = item;
                            invoicesModel.Add(invoicesingle);
                        }
                    }

                    invoices = invoicesModel;

                    }
                else if (sortby == "thisyear" && invoices.Count() > 0)
                    {
                    var year = DateTime.Now.Year;
                    invoices = invoices.Where(m => Convertdate(m.date_created).Value.Date.Year == year).ToList();
                    }

                invoicesummary.totalpaid = invoices.Sum(m => m.payment.Sum(x => x.amount));

                int amountdue = invoices.Sum(m => m.amountdue);

                invoicesummary.totaloutstanding = amountdue - invoicesummary.totalpaid;

                invoicesummary.totaldraft = invoices.Where(m => m.invoicestatus.ToLower() == "draft").Sum(x => x.payment.Sum(a => a.amount));

                var recentunpaid = invoices.FindAll(m => m.payment == null).OrderBy(x => x.date_created).Take(3).ToList();

                //add recent unpaid to invoice summary view model
                foreach (var item in recentunpaid)
                {
                    invoicesummary.recentunpaid.Add(new InvoiceViewModel
                    {
                        id = item.id,
                        amountdue = item.amountdue,
                        color = item.color,
                        date_created = Convert.ToDateTime(item.date_created),
                        deposit = item.deposit,
                        description = item.description,
                        discount = item.discount,
                        due_date = Convert.ToDateTime(item.due_date),
                        font = item.font,
                        invoicenumber = item.invoicenumber,
                        invoicestatus = item.invoicestatus,
                        invoiceguid = item.invoiceguid.ToString(),
                        invoicestyle = item.invoicestyle,
                        invoicetype = item.invoicetype,
                        notes = item.notes,
                        paymenttype = item.paymenttype,
                        sendstyle = item.sendstyle,
                        subtotal = item.subtotal,
                        personalmessage = item.personalmessage,
                        userid = item.userid,
                    });
                }

                //get all over due invoices and add to invoice summary view model
                foreach (var item in invoices)
                {
                    DateTime getDate = DateTime.Now;

                    string timeZoneId = "W. Central Africa Standard Time";

                    TimeZoneInfo getTimeZone = TimeZoneInfo.FindSystemTimeZoneById(timeZoneId);

                    var timeNow = TimeZoneInfo.ConvertTime(getDate, getTimeZone);

                    var getDateOfTimeNow = timeNow.ToShortDateString();

                    var compareDateResult = 0;

                    DateTime dt;
                    if (DateTime.TryParse(item.due_date, out dt))
                    {
                        compareDateResult = DateTime.Compare(dt, Convert.ToDateTime(getDateOfTimeNow));
                    }
                    if (compareDateResult < 0)
                    {
                        invoicesummary.recentoverdue.Add(new InvoiceViewModel
                        {
                            id = item.id,
                            amountdue = item.amountdue,
                            color = item.color,
                            date_created = Convert.ToDateTime(item.date_created),
                            deposit = item.deposit,
                            description = item.description,
                            discount = item.discount,
                            due_date = Convert.ToDateTime(item.due_date),
                            font = item.font,
                            invoicenumber = item.invoicenumber,
                            invoicestatus = item.invoicestatus,
                            invoiceguid = item.invoiceguid.ToString(),
                            invoicestyle = item.invoicestyle,
                            invoicetype = item.invoicetype,
                            notes = item?.notes,
                            paymenttype = item.paymenttype,
                            sendstyle = item.sendstyle,
                            subtotal = item.subtotal,
                            personalmessage = item.personalmessage,
                            userid = item.userid,
                        });
                    }


                }

                invoicesummary.recentoverdue = invoicesummary.recentoverdue.OrderBy(m => m.date_created).Take(3).ToList();

            }

            return invoicesummary;
        }

        public  DateTime? Convertdate( string date)
        {
            if(date == "Jan 21 2019 10:21PM")
            {

            }
           // const string Format = "dd/MM/yyyy";
            DateTime dt = DateTime.Now;
            var dtto = DateTime.TryParse(date,out dt);
            if(!dtto)
            {
                return null;
            }
          
            return dt;
        }



        //update payment
        public async Task<bool> UpdatePayment(InvoicePaymentModel model)
        {
            bool issuccessful = false;

            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            try
            {
                using (var connection = new SqlConnection(ConnectionStringDal.ConnectionString))
                {
                    var sqlAdapter = new SqlDataAdapter();
                    var sqlCommand = new SqlCommand(StoredProcedureDal.UpdatePayment, connection);
                    connection.Open();
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.AddWithValue("@id", model.id);
                    sqlCommand.Parameters.AddWithValue("@date_paid", model.date_paid);
                    sqlCommand.Parameters.AddWithValue("@amount", model.amount);
                    sqlCommand.Parameters.AddWithValue("@paidwith", model.paidwith);
                    sqlCommand.Parameters.AddWithValue("@invoiceid", model.invoiceid);

                    await sqlCommand.ExecuteNonQueryAsync();
                    issuccessful = true;
                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }
            return issuccessful;
        }

        //update method to select if invoice item should be added to an invoice
        //or updated on the invoice
        public async Task<bool> Update(ItemsModel model)
        {
            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            bool isSuccessful = false;
            try
            {
                if (model.id <= 0)
                {
                    isSuccessful = await AddItem(model);

                }
                else
                {
                    isSuccessful = await UpdateItem(model);
                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return isSuccessful;

        }

        //send email link to client
        public async Task<bool> SendToEmail(ConfirmInvoiceModel model)
        {
            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            try
            {
                await AdminDal.SendInvoiceEmail(model);

            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }
            return true;
        }

        //get page total for get invoice list with paging details
        private double GetPageTotal(int totalproducts, int limit)
        {
            var result = totalproducts / (float)limit;
            return (int)Math.Ceiling(result);
        }


        private int GetAmountDue(int businessid, int invoiceid, string userid)
        {
            var getdataModel = new GetDataModel(businessid)
            {
                businessid = invoiceid,
                limit = 100,
                listtype = false,
            };

            var getpayments = GetAllPayments(getdataModel);

            var totalPayment = getpayments.payments.Sum(m => m.amount);

            return totalPayment;

        }

        private string CreateParameterValue(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return DBNull.Value.ToString();
            }
            else
            {
                return value;
            }
        }

    }
}