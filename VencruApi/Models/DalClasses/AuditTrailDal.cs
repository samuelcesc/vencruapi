﻿using KellermanSoftware.CompareNetObjects;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using VencruApi.Models.ViewModels;




namespace VencruApi.Models.DalClasses
{
    public class AuditTrailDal
    {
        public static void CreateAuditTrail(AuditActionType action, int keyfieldid, object oldobject, object newobject, string userid, int businessid)
        {
            var compObjects = new CompareLogic();

            compObjects.Config.MaxDifferences = 99;

            var compResult = compObjects.Compare(oldobject, newobject);

            var dataList = new List<AuditData>();

            foreach (var change in compResult.Differences)
            {
                var data = new AuditData();
                if (change.PropertyName.Substring(0, 1) == ".")
                {
                    data.fieldname = change.PropertyName.Substring(1, change.PropertyName.Length - 1);
                    data.valuebefore = change.Object1Value;
                    data.valueafter = change.Object2Value;
                    dataList.Add(data);
                }

            }

            var audit = new AuditTrailModel();
            audit.auditactiontypeenum = (int)action;
            audit.datamodel = newobject.GetType().Name;
            audit.keyfieldid = keyfieldid;
            audit.valuesbefore = JsonConvert.SerializeObject(oldobject); // if use xml instead of json, can use xml annotation to describe field names etc better
            audit.valueafter = JsonConvert.SerializeObject(newobject);
            audit.changes = JsonConvert.SerializeObject(dataList);
            audit.userid = userid;
            audit.businessid = businessid;

            SaveAuditTrailData(audit);

        }

        private static void SaveAuditTrailData(AuditTrailModel model)
        {
            using (var connection = new SqlConnection(ConnectionStringDal.ConnectionString))
            {
                var sqlAdapter = new SqlDataAdapter();
                var sqlCommand = new SqlCommand(StoredProcedureDal.AddAuditTrail, connection);
                connection.Open();
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@auditactiontypeenum", model.auditactiontypeenum);
                sqlCommand.Parameters.AddWithValue("@changes", model.changes);
                sqlCommand.Parameters.AddWithValue("@datamodel", model.datamodel);
                sqlCommand.Parameters.AddWithValue("@keyfieldid", model.keyfieldid);
                sqlCommand.Parameters.AddWithValue("@valueafter", model.valueafter);
                sqlCommand.Parameters.AddWithValue("@valuebefore", model.valuesbefore);
                sqlCommand.Parameters.AddWithValue("@userid", model.userid);
                sqlCommand.Parameters.AddWithValue("@businessid", model.businessid);

                sqlCommand.ExecuteNonQuery();
            }
        }

        public List<AuditTrailModel> GetAllAudit(int businessid, string sortyby,int? pagenumber, int? limit)
        {
            if (limit == null)
                limit = 10;

            if (pagenumber == null)
                pagenumber = 1;

         
            List<AuditTrailModel> Fulllist = new List<AuditTrailModel>();

            var date = DateTime.Now.ToString("dd/MM/yyyy");
            var dtto = DateTime.ParseExact(date, "dd/MM/yyyy", null);


            using (var dbContext = new ApplicationDbContext())
            {
                Fulllist = dbContext.AuditTrailModels.Where(m => m.businessid == businessid).ToList();
            }

            if (sortyby.ToLower() == "today")
            {
                Fulllist = Fulllist.Where(m => Convertdate( m.action_date).Value.Date == dtto).Skip((pagenumber.Value * limit.Value)-1).Take(limit.Value).ToList();
            } 
            else if (sortyby.ToLower() == "thismonth" && Fulllist.Count() > 0)
            {
                var currentMonth = DateTime.Now.ToString("MM");
                Fulllist = Fulllist.Where(x => Convertdate(x.action_date).Value.Date.ToString("MM") == currentMonth).Skip((pagenumber.Value * limit.Value) - 1).Take(limit.Value).ToList();
            }
            else if (sortyby == "yesterday" && Fulllist.Count() > 0)
            {
                var yesterday = DateTime.Now.Date.AddDays(-1);

                Fulllist = Fulllist.Where(m => Convertdate( m.action_date).Value.Date == yesterday).Skip((pagenumber.Value * limit.Value) - 1).Take(limit.Value).ToList();
            }
            else if (sortyby == "thisweek" && Fulllist.Count() > 0)
            {
                var cultureInfo = new CultureInfo("en-US");

                var myCalender = cultureInfo.Calendar;

                var myCalenderRule = cultureInfo.DateTimeFormat.CalendarWeekRule;

                var myFirstDayOfWeek = cultureInfo.DateTimeFormat.FirstDayOfWeek;

                var thisWeekNumber = myCalender.GetWeekOfYear(DateTime.Now, myCalenderRule, myFirstDayOfWeek);

                var auditTrailList = new List<AuditTrailModel>();
                AuditTrailModel auditTrailModel = new AuditTrailModel();
                Fulllist = Fulllist.Skip((pagenumber.Value * limit.Value) - 1).Take(limit.Value).ToList();
                foreach (var item in Fulllist)
                {
                    var weekNumber = myCalender.GetWeekOfYear(Convertdate(item.action_date).Value.Date, myCalenderRule, myFirstDayOfWeek);

                    if (thisWeekNumber == weekNumber)
                    {
                        auditTrailModel = item;
                        auditTrailList.Add(auditTrailModel);
                    }
                }
                Fulllist = auditTrailList;
            }
            else if (sortyby == "thisyear" && Fulllist.Count() > 0)
            {
                var year = DateTime.Now.Year;
                Fulllist = Fulllist.Where(m => Convertdate(m.action_date).Value.Date.Year == year).Skip((pagenumber.Value * limit.Value) - 1).Take(limit.Value).ToList();
            }

            else { }

            return Fulllist;
        }



        public DateTime? Convertdate(string date)
        {
            if (date == "Jan 21 2019 10:21PM")
            {

            }
            // const string Format = "dd/MM/yyyy";
            DateTime dt = DateTime.Now;
            var dtto = DateTime.TryParse(date, out dt);
            if (!dtto)
            {
                return null;
            }

            return dt;
        }
    }
}