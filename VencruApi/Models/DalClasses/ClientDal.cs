﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;
using VencruApi.Models.ViewModels;

namespace VencruApi.Models.DalClasses
{

    public class ClientDal
    {
        public UserAccountDal UserAccountDal
        {
            get
            {
                return UserAccountDal = new UserAccountDal();
            }
            set
            {

            }
        }

        //add client
        public async Task<string> AddClient(ClientsModel model)
        {
            string isSuccessful;

            try
            {
                if (!UserAccountDal.CheckUser(model.userid))
                {
                    return null;
                }
                else
                {
                    using (var connection = new SqlConnection(ConnectionStringDal.ConnectionString))
                    {
                        var sqlAdapter = new SqlDataAdapter();
                        var sqlCommand = new SqlCommand(StoredProcedureDal.AddClient, connection);
                        connection.Open();
                        sqlCommand.CommandType = CommandType.StoredProcedure;
                        sqlCommand.Parameters.Add("@Id", SqlDbType.Int).Direction = ParameterDirection.Output;
                        sqlCommand.Parameters.AddWithValue("@Firstname", model.firstname);
                        sqlCommand.Parameters.AddWithValue("@Lastname", model.lastname);
                        sqlCommand.Parameters.AddWithValue("@Company", model.companyname);
                        sqlCommand.Parameters.AddWithValue("@Companyemail", model.companyemail);
                        sqlCommand.Parameters.AddWithValue("@Phonenumber", model.phonenumber);
                        sqlCommand.Parameters.AddWithValue("@Street", model.street);
                        sqlCommand.Parameters.AddWithValue("@City", model.city);
                        sqlCommand.Parameters.AddWithValue("@Country", model.country);
                        sqlCommand.Parameters.AddWithValue("@Note", model.note);
                        sqlCommand.Parameters.AddWithValue("@Userid", model.userid);
                        sqlCommand.Parameters.AddWithValue("@BusinessId", model.businessid);
                        sqlCommand.Parameters.AddWithValue("@Isdeleted", model.isdeleted = false);
                        sqlCommand.Parameters.AddWithValue("@utime", model.utime);
                        await sqlCommand.ExecuteNonQueryAsync();
                        isSuccessful = sqlCommand.Parameters["@Id"].Value.ToString();
                    }

                }


            }
            catch (Exception ex)
            {
                throw ex;
            }

            return isSuccessful;
        }

        //add client to delete list
        public async Task<bool> DeleteClientTemp(int? clientId, int? businessid)
        {
            bool isSuccessful = false;

            try
            {
                using (var connection = new SqlConnection(ConnectionStringDal.ConnectionString))
                {
                    var sqlAdapter = new SqlDataAdapter();
                    var sqlCommand = new SqlCommand(StoredProcedureDal.DeleteClientTemp, connection);
                    connection.Open();
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.AddWithValue("@Businessid", businessid);
                    sqlCommand.Parameters.AddWithValue("@Clientid", clientId);
                    await sqlCommand.ExecuteNonQueryAsync();
                    isSuccessful = true;

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return isSuccessful;
        }

        //restore client from delete list
        public async Task<bool> RestoreClient(int? clientId, int? businessid)
        {
            bool isSuccessful = false;

            try
            {
                using (var connection = new SqlConnection(ConnectionStringDal.ConnectionString))
                {
                    var sqlAdapter = new SqlDataAdapter();
                    var sqlCommand = new SqlCommand(StoredProcedureDal.RestoreClient, connection);
                    connection.Open();
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.AddWithValue("@Businessid", businessid);
                    sqlCommand.Parameters.AddWithValue("@Clientid", clientId);
                    await sqlCommand.ExecuteNonQueryAsync();
                    isSuccessful = true;

                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return isSuccessful;
        }

        //method cron jobs use to delete clients that has been on delete list for days > 30
        public async Task<bool> DeleteClient(int? clientId, int? businessid)
        {
            bool isSuccessful = false;
            try
            {
                using (var connection = new SqlConnection(ConnectionStringDal.ConnectionString))
                {
                    var sqlAdapter = new SqlDataAdapter();
                    var sqlCommand = new SqlCommand(StoredProcedureDal.DeleteClient, connection);
                    connection.Open();
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.AddWithValue("@Businessid", businessid);
                    sqlCommand.Parameters.AddWithValue("@Clientid", clientId);
                    await sqlCommand.ExecuteNonQueryAsync();
                    isSuccessful = true;

                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return isSuccessful;
        }

        //update client
        public async Task<bool> UpdateClient(ClientsModel model)
        {
            bool isSuccessful = false;

            try
            {
                if (!UserAccountDal.CheckUser(model.userid))
                {
                    return false;
                }
                else
                {
                    using (var connection = new SqlConnection(ConnectionStringDal.ConnectionString))
                    {
                        var sqlAdapter = new SqlDataAdapter();
                        var sqlCommand = new SqlCommand(StoredProcedureDal.UpdateClient, connection);
                        connection.Open();
                        sqlCommand.CommandType = CommandType.StoredProcedure;
                        sqlCommand.Parameters.AddWithValue("@ClientId", model.id);
                        sqlCommand.Parameters.AddWithValue("@BusinessId", model.businessid);
                        sqlCommand.Parameters.AddWithValue("@Firstname", model.firstname);
                        sqlCommand.Parameters.AddWithValue("@Lastname", model.lastname);
                        sqlCommand.Parameters.AddWithValue("@Company", model.companyname);
                        sqlCommand.Parameters.AddWithValue("@Companyemail", model.companyemail);
                        sqlCommand.Parameters.AddWithValue("@Phonenumber", model.phonenumber);
                        sqlCommand.Parameters.AddWithValue("@Street", model.street);
                        sqlCommand.Parameters.AddWithValue("@City", model.city);
                        sqlCommand.Parameters.AddWithValue("@Note", model.note);
                        sqlCommand.Parameters.AddWithValue("@Country", model.country);
                        await sqlCommand.ExecuteNonQueryAsync();
                        isSuccessful = true;
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return isSuccessful;
        }

        //get all clients not in delete list
        //listtype parameter is used to know if method should fetch deleted list or non deleted list. 0 for non deleted list, 1 for deleted list
        //default for list type is 0
        public ClientListViewModel GetAllClients(GetDataModel model)
        {

            var clientListViewModel = new ClientListViewModel();

            try
            {
                var userDataSet = new DataSet();

                var sqlDataAdapter = new SqlDataAdapter();

                using (var connection = new SqlConnection(ConnectionStringDal.ConnectionString))
                {
                    SqlDataAdapter sqlAdapter = new SqlDataAdapter();
                    SqlCommand sqlCommand = new SqlCommand(StoredProcedureDal.GetAllClients, connection);
                    connection.Open();
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.AddWithValue("@BusinessId", model.businessid);
                    sqlCommand.Parameters.AddWithValue("@Page", model.page);
                    sqlCommand.Parameters.AddWithValue("@Limit", model.limit);
                    sqlCommand.Parameters.AddWithValue("@Sortorder", model.sortorder);
                    sqlCommand.Parameters.AddWithValue("@Sortby", model.sortby);
                    sqlCommand.Parameters.AddWithValue("@Listtype", model.listtype);
                    sqlDataAdapter.SelectCommand = sqlCommand;
                    sqlDataAdapter.Fill(userDataSet);
                    if (userDataSet != null & userDataSet.Tables.Count > 0)
                    {
                        for (int index = 0; index <= userDataSet.Tables[0].Rows.Count - 1; index++)
                        {
                            ClientViewModel userInfo = new ClientViewModel()
                            {
                                firstname = Convert.ToString(userDataSet.Tables[0].Rows[index]["FirstName"]),
                                lastname = Convert.ToString(userDataSet.Tables[0].Rows[index]["LastName"]),
                                city = Convert.ToString(userDataSet.Tables[0].Rows[index]["City"]),
                                companyname = Convert.ToString(userDataSet.Tables[0].Rows[index]["CompanyName"]),
                                companyemail = Convert.ToString(userDataSet.Tables[0].Rows[index]["CompanyEmail"]),
                                country = Convert.ToString(userDataSet.Tables[0].Rows[index]["Country"]),
                                date_created = Convert.ToDateTime(userDataSet.Tables[0].Rows[index]["date_created"]),
                                phonenumber = Convert.ToString(userDataSet.Tables[0].Rows[index]["PhoneNumber"]),
                                street = Convert.ToString(userDataSet.Tables[0].Rows[index]["Street"]),
                                id = Convert.ToInt32(userDataSet.Tables[0].Rows[index]["Id"]),
                                businessid = Convert.ToInt32(userDataSet.Tables[0].Rows[index]["BusinessId"]),
                                userid = Convert.ToString(userDataSet.Tables[0].Rows[index]["UserId"]),
                                isdeleted = Convert.ToBoolean(userDataSet.Tables[0].Rows[index]["isdeleted"]),
                                note = Convert.ToString(userDataSet.Tables[0].Rows[index]["note"]),

                        };

                            if (string.IsNullOrEmpty(model.filter))
                            {
                                clientListViewModel.currentpage = Convert.ToInt32(userDataSet.Tables[0].Rows[0]["pagenumber"]);
                                clientListViewModel.total = Convert.ToInt32(userDataSet.Tables[0].Rows[0]["clienttotal"]);
                                int pagelimit = Convert.ToInt32(userDataSet.Tables[0].Rows[0]["limit"]);
                                clientListViewModel.pagetotal = GetPageTotal(clientListViewModel.total, pagelimit);
                                clientListViewModel.clients.Add(userInfo);
                            }
                            else
                            {
                                clientListViewModel.clients.Add(userInfo);

                            }


                        }



                    }

                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            return clientListViewModel;
        }

        //get client history list
        public ClientHistoryListViewModel GetClientHistory(int? clientid, int? businessid, int? limit, int? page, string userid)
        {
            var clientListViewModel = new ClientHistoryListViewModel();

            try
            {
                var userDataSet = new DataSet();

                var sqlDataAdapter = new SqlDataAdapter();

                using (var connection = new SqlConnection(ConnectionStringDal.ConnectionString))
                {
                    SqlDataAdapter sqlAdapter = new SqlDataAdapter();
                    SqlCommand sqlCommand = new SqlCommand(StoredProcedureDal.GetClientHistoryList, connection);
                    connection.Open();
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.AddWithValue("@Clientid", clientid);
                    sqlCommand.Parameters.AddWithValue("@BusinessId", businessid);
                    sqlCommand.Parameters.AddWithValue("@Page", page);
                    sqlCommand.Parameters.AddWithValue("@Limit", limit);
                    sqlDataAdapter.SelectCommand = sqlCommand;
                    sqlDataAdapter.Fill(userDataSet);
                    if (userDataSet != null & userDataSet.Tables.Count > 0)
                    {

                        for (int index = 0; index <= userDataSet.Tables[0].Rows.Count - 1; index++)
                        {
                            ClientHistoryDataViewModel clientInfo = new ClientHistoryDataViewModel()
                            {
                                amount = Convert.ToInt32(userDataSet.Tables[0].Rows[index]["amount"]),
                                date = Convert.ToDateTime(userDataSet.Tables[0].Rows[index]["date_added"]),
                                invoicenumber = Convert.ToString(userDataSet.Tables[0].Rows[index]["invoicenumber"]),
                                paidwith = Convert.ToString(userDataSet.Tables[0].Rows[index]["paidwith"]),
                                invoiceid = Convert.ToInt32(userDataSet.Tables[0].Rows[index]["invoiceid"]),

                            };

                            clientListViewModel.summarydata.totaloutstanding = DBNull.Value.Equals(userDataSet.Tables[0].Rows[0]["totaloutstanding"]) ? 0 : Convert.ToInt32(userDataSet.Tables[0].Rows[0]["totaloutstanding"]);

                            clientListViewModel.summarydata.draft = DBNull.Value.Equals(userDataSet.Tables[0].Rows[0]["draft"]) ? 0 : Convert.ToInt32(userDataSet.Tables[0].Rows[0]["draft"]);

                            clientListViewModel.summarydata.due = DBNull.Value.Equals(userDataSet.Tables[0].Rows[0]["due"]) ? 0 : Convert.ToInt32(userDataSet.Tables[0].Rows[0]["due"]);

                            clientListViewModel.summarydata.overdue = DBNull.Value.Equals(userDataSet.Tables[0].Rows[0]["overdue"]) ? 0 : Convert.ToInt32(userDataSet.Tables[0].Rows[0]["overdue"]);

                            clientListViewModel.summarydata.totalearning = DBNull.Value.Equals(userDataSet.Tables[0].Rows[0]["totalearning"]) ? 0 : Convert.ToInt32(userDataSet.Tables[0].Rows[0]["totalearning"]);



                            clientListViewModel.currentpage = Convert.ToInt32(userDataSet.Tables[0].Rows[0]["pagenumber"]);

                            clientListViewModel.total = Convert.ToInt32(userDataSet.Tables[0].Rows[0]["totalpayments"]);

                            int pagelimit = Convert.ToInt32(userDataSet.Tables[0].Rows[0]["limit"]);

                            clientListViewModel.pagetotal = GetPageTotal(clientListViewModel.total, pagelimit);

                            clientListViewModel.clienthistorydata.Add(clientInfo);
                        }
                    }

                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            return clientListViewModel;
        }
        //get client summary data
        public ClientSummaryViewModel GetClientSummaryData(int? businessid)
        {
            var clientsummary = new ClientSummaryViewModel();
            try
            {
                DataSet userDataSet = new DataSet();
                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter();
                using (SqlConnection connection = new SqlConnection(ConnectionStringDal.ConnectionString))
                {
                    SqlDataAdapter sqlAdapter = new SqlDataAdapter();
                    SqlCommand sqlCommand = new SqlCommand(StoredProcedureDal.GetClientSummaryData, connection);
                    connection.Open();
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.AddWithValue("@Businessid", businessid);
                    sqlDataAdapter.SelectCommand = sqlCommand;
                    sqlDataAdapter.Fill(userDataSet);
                    if (userDataSet != null & userDataSet.Tables.Count > 0)
                    {
                        for (int index = 0; index <= userDataSet.Tables[0].Rows.Count - 1; index++)
                        {
                            var summaryinfo = new ClientSummaryViewModel()
                            {
                                totalclient = Convert.ToInt32(userDataSet.Tables[0].Rows[index]["totalclients"]),
                                thismonthclients = Convert.ToInt32(userDataSet.Tables[0].Rows[index]["thismonthclients"]),
                                repeatclients = Convert.ToInt32(userDataSet.Tables[0].Rows[index]["repeatclients"]),
                            };

                            clientsummary = summaryinfo;

                        }
                    }


                }
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return clientsummary;
        }

        //get client
        public ClientViewModel GetClient(int? businessid, int? clientid)
        {
            ClientViewModel clientsModel;
            var userInfo = new ClientViewModel();
            try
            {
                DataSet userDataSet = new DataSet();
                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter();
                using (SqlConnection connection = new SqlConnection(ConnectionStringDal.ConnectionString))
                {
                    SqlDataAdapter sqlAdapter = new SqlDataAdapter();
                    SqlCommand sqlCommand = new SqlCommand(StoredProcedureDal.GetClient, connection);
                    connection.Open();
                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    sqlCommand.Parameters.AddWithValue("@Clientid", clientid);
                    sqlCommand.Parameters.AddWithValue("@Businessid", businessid);
                    sqlDataAdapter.SelectCommand = sqlCommand;
                    sqlDataAdapter.Fill(userDataSet);
                    if (userDataSet != null & userDataSet.Tables.Count > 0)
                    {
                        for (int index = 0; index <= userDataSet.Tables[0].Rows.Count - 1; index++)
                        {
                            userInfo.firstname = Convert.ToString(userDataSet.Tables[0].Rows[index]["FirstName"]);
                            userInfo.lastname = Convert.ToString(userDataSet.Tables[0].Rows[index]["LastName"]);
                            userInfo.city = Convert.ToString(userDataSet.Tables[0].Rows[index]["City"]);
                            userInfo.companyname = Convert.ToString(userDataSet.Tables[0].Rows[index]["CompanyName"]);
                            userInfo.companyemail = Convert.ToString(userDataSet.Tables[0].Rows[index]["CompanyEmail"]);
                            userInfo.country = Convert.ToString(userDataSet.Tables[0].Rows[index]["Country"]);
                            userInfo.date_created = Convert.ToDateTime(userDataSet.Tables[0].Rows[index]["Date_Created"]);
                            userInfo.phonenumber = Convert.ToString(userDataSet.Tables[0].Rows[index]["PhoneNumber"]);
                            userInfo.street = Convert.ToString(userDataSet.Tables[0].Rows[index]["Street"]);
                            //userInfo.Zipcode = Convert.ToString(userDataSet.Tables[0].Rows[index]["Zipcode"]);
                            userInfo.id = Convert.ToInt32(userDataSet.Tables[0].Rows[index]["Id"]);
                            userInfo.businessid = Convert.ToInt32(userDataSet.Tables[0].Rows[index]["BusinessId"]);
                            userInfo.userid = Convert.ToString(userDataSet.Tables[0].Rows[index]["UserId"]);
                            userInfo.isdeleted = Convert.ToBoolean(userDataSet.Tables[0].Rows[index]["isdeleted"]);
                            userInfo.note = Convert.ToString(userDataSet.Tables[0].Rows[index]["note"]);

                        }
                    }
                    clientsModel = userInfo;
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            return clientsModel;
        }

        //add client note
        public async Task<bool> AddClientNote(ClientNoteModel model)
        {
            bool isSuccessful = false;

            try
            {
                if (!UserAccountDal.CheckUser(model.userid))
                {
                    return isSuccessful;
                }
                else
                {
                    using (var connection = new SqlConnection(ConnectionStringDal.ConnectionString))
                    {
                        var sqlAdapter = new SqlDataAdapter();
                        var sqlCommand = new SqlCommand(StoredProcedureDal.AddClientNote, connection);
                        connection.Open();
                        sqlCommand.CommandType = CommandType.StoredProcedure;
                        sqlCommand.Parameters.AddWithValue("@businessid", model.businessid);
                        sqlCommand.Parameters.AddWithValue("@clientid", model.clientid);
                        sqlCommand.Parameters.AddWithValue("@note", model.note);
                        await sqlCommand.ExecuteNonQueryAsync();
                        isSuccessful = true;
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return isSuccessful;
        }

        //get delete list data
        public ClientListViewModel GetDeleteList(GetDataModel model)
        {
            var clientListViewModel = new ClientListViewModel();
            try
            {
                var userDataSet = new DataSet();

                var sqlDataAdapter = new SqlDataAdapter();

                using (var connection = new SqlConnection(ConnectionStringDal.ConnectionString))
                {
                    SqlDataAdapter sqlAdapter = new SqlDataAdapter();
                    SqlCommand sqlCommand = new SqlCommand(StoredProcedureDal.GetAllClients, connection);
                    connection.Open();
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.AddWithValue("@BusinessId", model.businessid);
                    sqlCommand.Parameters.AddWithValue("@Page", model.page);
                    sqlCommand.Parameters.AddWithValue("@Limit", model.limit);
                    sqlCommand.Parameters.AddWithValue("@Sortorder", model.sortorder);
                    sqlCommand.Parameters.AddWithValue("@Sortby", model.sortby);
                    sqlCommand.Parameters.AddWithValue("@Listtype", model.listtype);

                    sqlDataAdapter.SelectCommand = sqlCommand;
                    sqlDataAdapter.Fill(userDataSet);
                    if (userDataSet != null & userDataSet.Tables.Count > 0)
                    {
                        for (int index = 0; index <= userDataSet.Tables[0].Rows.Count - 1; index++)
                        {
                            ClientViewModel userInfo = new ClientViewModel()
                            {
                                firstname = Convert.ToString(userDataSet.Tables[0].Rows[index]["FirstName"]),
                                lastname = Convert.ToString(userDataSet.Tables[0].Rows[index]["LastName"]),
                                city = Convert.ToString(userDataSet.Tables[0].Rows[index]["City"]),
                                companyname = Convert.ToString(userDataSet.Tables[0].Rows[index]["CompanyName"]),
                                companyemail = Convert.ToString(userDataSet.Tables[0].Rows[index]["CompanyEmail"]),
                                country = Convert.ToString(userDataSet.Tables[0].Rows[index]["Country"]),
                                date_created = Convert.ToDateTime(userDataSet.Tables[0].Rows[index]["date_created"]),
                                phonenumber = Convert.ToString(userDataSet.Tables[0].Rows[index]["PhoneNumber"]),
                                street = Convert.ToString(userDataSet.Tables[0].Rows[index]["Street"]),
                                id = Convert.ToInt32(userDataSet.Tables[0].Rows[index]["Id"]),
                                businessid = Convert.ToInt32(userDataSet.Tables[0].Rows[index]["BusinessId"]),
                                userid = Convert.ToString(userDataSet.Tables[0].Rows[index]["UserId"]),
                                isdeleted = Convert.ToBoolean(userDataSet.Tables[0].Rows[index]["isdeleted"]),

                            };

                            clientListViewModel.currentpage = Convert.ToInt32(userDataSet.Tables[0].Rows[0]["pagenumber"]);
                            clientListViewModel.total = Convert.ToInt32(userDataSet.Tables[0].Rows[0]["clienttotal"]);
                            int pagelimit = Convert.ToInt32(userDataSet.Tables[0].Rows[0]["limit"]);
                            clientListViewModel.pagetotal = GetPageTotal(clientListViewModel.total, pagelimit);

                            clientListViewModel.clients.Add(userInfo);
                        }



                    }

                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            return clientListViewModel;
        }
        //get page total for get client list with paging details
        private double GetPageTotal(int totalclients, int limit)
        {
            var result = totalclients / (float)limit;
            return (int)Math.Ceiling(result);
        }

    }
}