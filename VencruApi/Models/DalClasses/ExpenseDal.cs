﻿using SharpRaven;
using SharpRaven.Data;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;
using VencruApi.Models.Classes;
using VencruApi.Models.ViewModels;

namespace VencruApi.Models.DalClasses
{
    public class ExpenseDal
    {
        public UserAccountDal UserAccountDal
        {
            get
            {
                return UserAccountDal = new UserAccountDal();
            }
            set
            {

            }
        }

        //add expense
        public async Task<string> Addexpense(ExpenseModel model)
        {
            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            var isSuccessful = string.Empty;

            try
            {
                if (!UserAccountDal.CheckUser(model.userid, model.businessid))
                {
                    return null;
                }
                else
                {
                    using (var connection = new SqlConnection(ConnectionStringDal.ConnectionString))
                    {
                        var sqlAdapter = new SqlDataAdapter();
                        var sqlCommand = new SqlCommand(StoredProcedureDal.AddExpense, connection);
                        connection.Open();
                        sqlCommand.CommandType = CommandType.StoredProcedure;
                        sqlCommand.Parameters.Add("@Id", SqlDbType.Int).Direction = ParameterDirection.Output;
                        sqlCommand.Parameters.AddWithValue("@Expensenumber", model.expensenumber);
                        sqlCommand.Parameters.AddWithValue("@Description", model.description);
                        sqlCommand.Parameters.AddWithValue("@Category", model.category);
                        sqlCommand.Parameters.AddWithValue("@UserId", model.userid);
                        sqlCommand.Parameters.AddWithValue("@BusinessId", model.businessid);
                        sqlCommand.Parameters.AddWithValue("@Utime", model.utime);
                        sqlCommand.Parameters.AddWithValue("@vendor", model.vendor);
                        sqlCommand.Parameters.AddWithValue("@Paidwith", model.paidwith);
                        sqlCommand.Parameters.AddWithValue("@Imgurl", model.image);
                        sqlCommand.Parameters.AddWithValue("@Totalamount", model.totalamount);
                        sqlCommand.Parameters.AddWithValue("@Expensedate", model.expensedate);
                        await sqlCommand.ExecuteNonQueryAsync();
                        isSuccessful = sqlCommand.Parameters["@Id"].Value.ToString();
                    }
                }

            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return isSuccessful;
        }

        //get expense
        public ExpenseViewModel GetExpense(int? businessid, int? expesenseid)
        {
            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            var expense = new ExpenseViewModel();

            try
            {
                DataSet userDataSet = new DataSet();
                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter();
                using (SqlConnection connection = new SqlConnection(ConnectionStringDal.ConnectionString))
                {
                    SqlDataAdapter sqlAdapter = new SqlDataAdapter();
                    SqlCommand sqlCommand = new SqlCommand(StoredProcedureDal.GetExpense, connection);
                    connection.Open();
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.AddWithValue("@Expenseid", expesenseid);
                    sqlCommand.Parameters.AddWithValue("@Businessid", businessid);

                    sqlDataAdapter.SelectCommand = sqlCommand;
                    sqlDataAdapter.Fill(userDataSet);
                    if (userDataSet != null & userDataSet.Tables.Count > 0)
                    {
                        for (int index = 0; index <= userDataSet.Tables[0].Rows.Count - 1; index++)
                        {
                            var expenseinfo = new ExpenseViewModel()
                            {
                                expensenumber = Convert.ToString(userDataSet.Tables[0].Rows[index]["expensenumber"]),
                                description = Convert.ToString(userDataSet.Tables[0].Rows[index]["description"]),
                                category = Convert.ToString(userDataSet.Tables[0].Rows[index]["category"]),
                                date_created = Convert.ToDateTime(userDataSet.Tables[0].Rows[index]["date_created"]),
                                vendor = Convert.ToString(userDataSet.Tables[0].Rows[index]["vendor"]),
                                id = Convert.ToInt32(userDataSet.Tables[0].Rows[index]["id"]),
                                businessid = Convert.ToInt32(userDataSet.Tables[0].Rows[index]["businessid"]),
                                paidwith = Convert.ToString(userDataSet.Tables[0].Rows[index]["paidwith"]),
                                image = Convert.ToString(userDataSet.Tables[0].Rows[index]["image"]),
                                totalamount = Convert.ToInt32(userDataSet.Tables[0].Rows[index]["totalamount"]),
                                expensedate = Convert.ToDateTime(userDataSet.Tables[0].Rows[index]["expensedate"])
                                //imagename = Convert.ToString(userDataSet.Tables[0].Rows[index]["receiptimageurl"])
                            };
                            expense = expenseinfo;
                        }
                    }
                    else { }

                    expense.image = UtilitiesClass.GetImgUrl(expense.image, UtilitiesClass.expenseBucketname);

                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return expense;
        }

        // get expense list the list type property in the model determines the list type to get ie delete list or non delete list
        public ExpenseListViewModel GetAllExpenses(GetDataModel model)
        {
            var expenselist = new ExpenseListViewModel();

            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            try
            {
                var userDataSet = new DataSet();

                var sqlDataAdapter = new SqlDataAdapter();

                using (var connection = new SqlConnection(ConnectionStringDal.ConnectionString))
                {
                    SqlDataAdapter sqlAdapter = new SqlDataAdapter();
                    SqlCommand sqlCommand = new SqlCommand(StoredProcedureDal.GetAllExpenses, connection);
                    connection.Open();
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.AddWithValue("@BusinessId", model.businessid);
                    sqlCommand.Parameters.AddWithValue("@Page", model.page);
                    sqlCommand.Parameters.AddWithValue("@Limit", model.limit);
                    sqlCommand.Parameters.AddWithValue("@Sortorder", model.sortorder);
                    sqlCommand.Parameters.AddWithValue("@Sortby", model.sortby);
                    sqlCommand.Parameters.AddWithValue("@Listtype", model.listtype);

                    sqlDataAdapter.SelectCommand = sqlCommand;
                    sqlDataAdapter.Fill(userDataSet);
                    if (userDataSet != null & userDataSet.Tables.Count > 0)
                    {
                        for (int index = 0; index <= userDataSet.Tables[0].Rows.Count - 1; index++)
                        {
                            var expenseinfo = new ExpenseViewModel()
                            {
                                expensenumber = Convert.ToString(userDataSet.Tables[0].Rows[index]["expensenumber"]),
                                description = Convert.ToString(userDataSet.Tables[0].Rows[index]["description"]),
                                category = Convert.ToString(userDataSet.Tables[0].Rows[index]["category"]),
                                date_created = Convert.ToDateTime(userDataSet.Tables[0].Rows[index]["date_created"]),
                                vendor = Convert.ToString(userDataSet.Tables[0].Rows[index]["vendor"]),
                                id = Convert.ToInt32(userDataSet.Tables[0].Rows[index]["id"]),
                                businessid = Convert.ToInt32(userDataSet.Tables[0].Rows[index]["businessid"]),
                                paidwith = Convert.ToString(userDataSet.Tables[0].Rows[index]["paidwith"]),
                                image = Convert.ToString(userDataSet.Tables[0].Rows[index]["image"]),
                                totalamount = Convert.ToInt32(userDataSet.Tables[0].Rows[index]["totalamount"]),
                                expensedate = Convert.ToDateTime(userDataSet.Tables[0].Rows[index]["expensedate"])
                            };

                            expenselist.expenses.Add(expenseinfo);
                            expenselist.currentpage = Convert.ToInt32(userDataSet.Tables[0].Rows[0]["pagenumber"]);
                            expenselist.total = Convert.ToInt32(userDataSet.Tables[0].Rows[0]["expensetotal"]);
                            expenselist.pagetotal = GetPageTotal(expenselist.total, Convert.ToInt32(userDataSet.Tables[0].Rows[0]["limit"]));
                        }

                    }
                    else { }

                    foreach (var item in expenselist.expenses)
                    {
                        item.image = UtilitiesClass.GetImgUrl(item.image, UtilitiesClass.expenseBucketname);
                    }
                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }
            return expenselist;
        }

        public async Task<bool> UpdateExpense(ExpenseModel model)
        {
            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            bool isSuccessful = false;

            try
            {
                using (var connection = new SqlConnection(ConnectionStringDal.ConnectionString))
                {
                    var sqlAdapter = new SqlDataAdapter();
                    var sqlCommand = new SqlCommand(StoredProcedureDal.UpdateExpense, connection);
                    connection.Open();
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.AddWithValue("@Id", model.id);
                    sqlCommand.Parameters.AddWithValue("@BusinessId", model.businessid);
                    sqlCommand.Parameters.AddWithValue("@ExpenseNumber", model.expensenumber);
                    sqlCommand.Parameters.AddWithValue("@Description", model.description);
                    sqlCommand.Parameters.AddWithValue("@Expensedate", model.expensedate);
                    sqlCommand.Parameters.AddWithValue("@Totalamount", model.totalamount);
                    sqlCommand.Parameters.AddWithValue("@Paidwith", model.paidwith);
                    sqlCommand.Parameters.AddWithValue("@ImgUrl", model.image);
                    sqlCommand.Parameters.AddWithValue("@Vendor", model.vendor);
                    sqlCommand.Parameters.AddWithValue("@Category", model.category);
                    await sqlCommand.ExecuteNonQueryAsync();
                    isSuccessful = true;
                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }
            return isSuccessful;
        }

        //get page total for get client list with paging details
        private double GetPageTotal(int totalexpenses, int limit)
        {
            var result = totalexpenses / (float)limit;
            return (int)Math.Ceiling(result);
        }

        //generate expense number
        private string GetExpenseNumber(string count)
        {
            string expensenumber = string.Format("EX-{0:0}", count);

            return expensenumber;
        }

        //get expense number
        public string GenerateExpenseNumber(int businessid, string userid)
        {
            int expensecount = 0;

            string expensenumber = string.Empty;

            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            try
            {
                DataSet userDataSet = new DataSet();

                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter();

                using (SqlConnection connection = new SqlConnection(ConnectionStringDal.ConnectionString))
                {
                    SqlDataAdapter sqlAdapter = new SqlDataAdapter();
                    SqlCommand sqlCommand = new SqlCommand(StoredProcedureDal.GetExpenseNumberCount, connection);
                    connection.Open();
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.AddWithValue("@Businessid", businessid);
                    sqlDataAdapter.SelectCommand = sqlCommand;
                    sqlDataAdapter.Fill(userDataSet);
                    if (userDataSet != null & userDataSet.Tables.Count > 0)
                    {
                        for (int index = 0; index <= userDataSet.Tables[0].Rows.Count - 1; index++)
                        {
                            expensecount = Convert.ToInt32(userDataSet.Tables[0].Rows[index]["expensecount"]);
                        }
                    }
                    else { }
                }

                var expensecountpadded =expensecount + 1;

                expensenumber = GetExpenseNumber(expensecountpadded.ToString("D7"));

            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return expensenumber;
        }


        //delete expense temp
        public async Task<bool> DeleteExpenseTemp(int ExpenseId, int businessid, string UserId)
        {
            bool isSuccessful = false;

            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            try
            {
                using (var connection = new SqlConnection(ConnectionStringDal.ConnectionString))
                {
                    var sqlAdapter = new SqlDataAdapter();
                    var sqlCommand = new SqlCommand(StoredProcedureDal.DeleteExpenseTemp, connection);
                    connection.Open();
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.AddWithValue("@expenseId", ExpenseId);
                    sqlCommand.Parameters.AddWithValue("@businessid", businessid);
                    sqlCommand.Parameters.AddWithValue("@userId", UserId);
                    await sqlCommand.ExecuteNonQueryAsync();
                    isSuccessful = true;
                }

            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }
            return isSuccessful;
        }


        //get client summary data
        public ExpenseSummaryViewModel GetExpenseSummaryData(int? businessid, string userid)
        {
            var expensesummary = new ExpenseSummaryViewModel();

            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            try
            {
                int highestexpenseid;
                DataSet userDataSet = new DataSet();
                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter();
                using (SqlConnection connection = new SqlConnection(ConnectionStringDal.ConnectionString))
                {
                    SqlDataAdapter sqlAdapter = new SqlDataAdapter();
                    SqlCommand sqlCommand = new SqlCommand(StoredProcedureDal.GetExpenseSummaryData, connection);
                    connection.Open();
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.AddWithValue("@Businessid", businessid);
                    sqlDataAdapter.SelectCommand = sqlCommand;
                    sqlDataAdapter.Fill(userDataSet);
                    if (userDataSet != null & userDataSet.Tables.Count > 0)
                    {
                        for (int index = 0; index <= userDataSet.Tables[0].Rows.Count - 1; index++)
                        {
                            var summaryinfo = new ExpenseSummaryViewModel()
                            {
                                totalexpenses = Convert.ToInt32(userDataSet.Tables[0].Rows[index]["totalexpenses"]),
                                monthlytotal = Convert.ToInt32(userDataSet.Tables[0].Rows[index]["monthlytotal"]),
                                highestexpense = Convert.ToString(userDataSet.Tables[0].Rows[index]["highestamount"]),
                            };

                            //return expense with hisghest value
                            /*
                            highestexpenseid = Convert.ToInt32(userDataSet.Tables[0].Rows[index]["highestexpense"]);

                            var expense = GetExpense(businessid, highestexpenseid);


                            summaryinfo.highestexpense = expense;
                            */

                            expensesummary = summaryinfo;

                        }

                    }

                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return expensesummary;
        }

    }
}