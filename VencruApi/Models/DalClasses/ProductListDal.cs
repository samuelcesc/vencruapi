﻿using SharpRaven;
using SharpRaven.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace VencruApi.Models.DalClasses
{
    public class ProductListDal
    {
        public UserAccountDal UserAccountDal
        {
            get
            {
                return UserAccountDal = new UserAccountDal();
            }
            set
            {

            }
        }

        //add product list
        public async Task<string> AddProductList(ProductModel model)
        {
            string isSuccessful = string.Empty;

            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");


            try
            {
                if (!UserAccountDal.CheckUser(model.userid, model.businessid))
                {
                    return null;
                }
                else
                {
                    using (var connection = new SqlConnection(ConnectionStringDal.ConnectionString))
                    {
                        var sqlAdapter = new SqlDataAdapter();
                        var sqlCommand = new SqlCommand(StoredProcedureDal.AddProductList, connection);
                        connection.Open();
                        sqlCommand.CommandType = CommandType.StoredProcedure;
                        sqlCommand.Parameters.Add("@Id", SqlDbType.Int).Direction = ParameterDirection.Output;
                        sqlCommand.Parameters.AddWithValue("@StockNumber", model.stocknumber);
                        sqlCommand.Parameters.AddWithValue("@ProductName", model.productname);
                        sqlCommand.Parameters.AddWithValue("@Description", model.description);
                        sqlCommand.Parameters.AddWithValue("@UnitPrice", model.unitprice);
                        sqlCommand.Parameters.AddWithValue("@UserId", model.userid);
                        sqlCommand.Parameters.AddWithValue("@BusinessId", model.businessid);
                        sqlCommand.Parameters.AddWithValue("@Utime", model.utime);
                        sqlCommand.Parameters.AddWithValue("@Costofitem", model.costofitem);
                        //sqlCommand.Parameters.AddWithValue("@Costofitem", model.costofitem);
                        sqlCommand.Parameters.AddWithValue("@Isdeleted", model.isdeleted = false);
                        await sqlCommand.ExecuteNonQueryAsync();
                        isSuccessful = sqlCommand.Parameters["@Id"].Value.ToString();
                    }
                }

            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }
            return isSuccessful;
        }

        //delete product list for cron job
        public async Task<bool> DeleteProductList(int? ProductListId, int? businessid)
        {
            bool isSuccessful = false;

            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            try
            {
                using (var connection = new SqlConnection(ConnectionStringDal.ConnectionString))
                {
                    var sqlAdapter = new SqlDataAdapter();
                    var sqlCommand = new SqlCommand(StoredProcedureDal.DeleteProductList, connection);
                    connection.Open();
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.AddWithValue("@Businessid", businessid);
                    sqlCommand.Parameters.AddWithValue("@Id", ProductListId);
                    await sqlCommand.ExecuteNonQueryAsync();
                    isSuccessful = true;

                }

            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return isSuccessful;
        }

        //delete product temp
        public async Task<bool> DeleteproductTemp(int? id, int? businessid)
        {
            bool isSuccessful = false;

            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            try
            {
                using (var connection = new SqlConnection(ConnectionStringDal.ConnectionString))
                {
                    var sqlAdapter = new SqlDataAdapter();
                    var sqlCommand = new SqlCommand(StoredProcedureDal.DeleteProductTemp, connection);
                    connection.Open();
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.AddWithValue("@Businessid", businessid);
                    sqlCommand.Parameters.AddWithValue("@Productid", id);
                    await sqlCommand.ExecuteNonQueryAsync();
                    isSuccessful = true;
                }

            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }
            return isSuccessful;
        }

        //update product list
        public async Task<bool> UpdateProductList(ProductModel model)
        {
            bool isSuccessful = false;

            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            try
            {
                using (var connection = new SqlConnection(ConnectionStringDal.ConnectionString))
                {
                    var sqlAdapter = new SqlDataAdapter();
                    var sqlCommand = new SqlCommand(StoredProcedureDal.UpdateproductList, connection);
                    connection.Open();
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.AddWithValue("@Id", model.id);
                    sqlCommand.Parameters.AddWithValue("@BusinessId", model.businessid);
                    sqlCommand.Parameters.AddWithValue("@StockNumber", model.stocknumber);
                    sqlCommand.Parameters.AddWithValue("@ProductName", model.productname);
                    sqlCommand.Parameters.AddWithValue("@Description", model.description);
                    sqlCommand.Parameters.AddWithValue("@UnitPrice", model.unitprice);
                    sqlCommand.Parameters.AddWithValue("@Costofitem", model.costofitem);
                    await sqlCommand.ExecuteNonQueryAsync();
                    isSuccessful = true;
                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }
            

            return isSuccessful;
        }

        //get all products
        public ProductListViewModel GetAllProductLists(GetDataModel model)
        {
            var productlist = new ProductListViewModel();

            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            try
            {
                var userDataSet = new DataSet();

                var sqlDataAdapter = new SqlDataAdapter();

                using (var connection = new SqlConnection(ConnectionStringDal.ConnectionString))
                {
                    SqlDataAdapter sqlAdapter = new SqlDataAdapter();
                    SqlCommand sqlCommand = new SqlCommand(StoredProcedureDal.GetAllProductLists, connection);
                    connection.Open();
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.AddWithValue("@BusinessId", model.businessid);
                    sqlCommand.Parameters.AddWithValue("@Page", model.page);
                    sqlCommand.Parameters.AddWithValue("@Limit", model.limit);
                    sqlCommand.Parameters.AddWithValue("@Sortorder", model.sortorder);
                    sqlCommand.Parameters.AddWithValue("@Sortby", model.sortby);
                    sqlCommand.Parameters.AddWithValue("@Listtype", 0);

                    sqlDataAdapter.SelectCommand = sqlCommand;
                    sqlDataAdapter.Fill(userDataSet);
                    if (userDataSet != null & userDataSet.Tables.Count > 0)
                    {
                        for (int index = 0; index <= userDataSet.Tables[0].Rows.Count - 1; index++)
                        {
                            ProductViewModel productinfo = new ProductViewModel()
                            {
                                stocknumber = Convert.ToString(userDataSet.Tables[0].Rows[index]["StockNumber"]),
                                description = Convert.ToString(userDataSet.Tables[0].Rows[index]["Description"]),
                                productname = Convert.ToString(userDataSet.Tables[0].Rows[index]["ProductName"]),
                                unitprice = Convert.ToInt32(userDataSet.Tables[0].Rows[index]["UnitPrice"]),
                                id = Convert.ToInt32(userDataSet.Tables[0].Rows[index]["Id"]),
                                businessid = Convert.ToInt32(userDataSet.Tables[0].Rows[index]["BusinessId"]),
                                userid = Convert.ToString(userDataSet.Tables[0].Rows[index]["UserId"]),
                                date_created = Convert.ToDateTime(userDataSet.Tables[0].Rows[index]["date_created"]),
                                costofitem = Convert.ToInt32(userDataSet.Tables[0].Rows[index]["costofitem"]),
                                isdeleted = Convert.ToBoolean(userDataSet.Tables[0].Rows[index]["isdeleted"])
                            };
                            productlist.products.Add(productinfo);
                            productlist.currentpage = Convert.ToInt32(userDataSet.Tables[0].Rows[0]["pagenumber"]);
                            productlist.total = Convert.ToInt32(userDataSet.Tables[0].Rows[0]["producttotal"]);
                            productlist.pagetotal = GetPageTotal(productlist.total, Convert.ToInt32(userDataSet.Tables[0].Rows[0]["limit"]));
                        }

                    }

                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }
            return productlist;
        }

        //get product
        public ProductViewModel GetProduct(int? businessid, int? ProductListid)
        {

            var product = new ProductViewModel();

            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            try
            {
                DataSet userDataSet = new DataSet();
                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter();
                using (SqlConnection connection = new SqlConnection(ConnectionStringDal.ConnectionString))
                {
                    SqlDataAdapter sqlAdapter = new SqlDataAdapter();
                    SqlCommand sqlCommand = new SqlCommand(StoredProcedureDal.GetProductList, connection);
                    connection.Open();
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    //sqlCommand.Parameters.AddWithValue("@Userid", userId);
                    sqlCommand.Parameters.AddWithValue("@Id", ProductListid);
                    sqlCommand.Parameters.AddWithValue("@Businessid", businessid);

                    sqlDataAdapter.SelectCommand = sqlCommand;
                    sqlDataAdapter.Fill(userDataSet);
                    if (userDataSet != null & userDataSet.Tables.Count > 0)
                    {
                        for (int index = 0; index <= userDataSet.Tables[0].Rows.Count - 1; index++)
                        {
                            ProductViewModel productinfo = new ProductViewModel()
                            {
                                stocknumber = Convert.ToString(userDataSet.Tables[0].Rows[index]["StockNumber"]),
                                description = Convert.ToString(userDataSet.Tables[0].Rows[index]["Description"]),
                                productname = Convert.ToString(userDataSet.Tables[0].Rows[index]["ProductName"]),
                                date_created = Convert.ToDateTime(userDataSet.Tables[0].Rows[index]["date_created"]),
                                unitprice = Convert.ToInt32(userDataSet.Tables[0].Rows[index]["UnitPrice"]),
                                id = Convert.ToInt32(userDataSet.Tables[0].Rows[index]["Id"]),
                                businessid = Convert.ToInt32(userDataSet.Tables[0].Rows[index]["BusinessId"]),
                                userid = Convert.ToString(userDataSet.Tables[0].Rows[index]["UserId"]),
                                costofitem = Convert.ToInt32(userDataSet.Tables[0].Rows[index]["costofitem"]),
                                isdeleted = Convert.ToBoolean(userDataSet.Tables[0].Rows[index]["isdeleted"])
                            };

                            product = productinfo;
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }
            return product;
        }

        //get delete list data
        public ProductListViewModel GetDeleteList(GetDataModel model)
        {
            var productinfo = new ProductListViewModel();

            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");


            try
            {
                DataSet userDataSet = new DataSet();
                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter();
                using (SqlConnection connection = new SqlConnection(ConnectionStringDal.ConnectionString))
                {
                    SqlDataAdapter sqlAdapter = new SqlDataAdapter();
                    SqlCommand sqlCommand = new SqlCommand(StoredProcedureDal.GetAllProductLists, connection);
                    connection.Open();
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.AddWithValue("@Businessid", model.businessid);
                    sqlCommand.Parameters.AddWithValue("@Page", model.page);
                    sqlCommand.Parameters.AddWithValue("@Limit", model.limit);
                    sqlCommand.Parameters.AddWithValue("@Sortby", model.sortby);
                    sqlCommand.Parameters.AddWithValue("@Sortorder", model.sortorder);
                    sqlCommand.Parameters.AddWithValue("@Listtype", 1);

                    sqlDataAdapter.SelectCommand = sqlCommand;
                    sqlDataAdapter.Fill(userDataSet);
                    if (userDataSet != null & userDataSet.Tables.Count > 0)
                    {
                        for (int index = 0; index <= userDataSet.Tables[0].Rows.Count - 1; index++)
                        {
                            var product = new ProductViewModel()
                            {
                                stocknumber = Convert.ToString(userDataSet.Tables[0].Rows[index]["stocknumber"]),
                                description = Convert.ToString(userDataSet.Tables[0].Rows[index]["description"]),
                                productname = Convert.ToString(userDataSet.Tables[0].Rows[index]["productname"]),
                                unitprice = Convert.ToInt32(userDataSet.Tables[0].Rows[index]["unitprice"]),
                                id = Convert.ToInt32(userDataSet.Tables[0].Rows[index]["id"]),
                                businessid = Convert.ToInt32(userDataSet.Tables[0].Rows[index]["businessid"]),
                                userid = Convert.ToString(userDataSet.Tables[0].Rows[index]["userid"]),
                                date_created = Convert.ToDateTime(userDataSet.Tables[0].Rows[index]["date_created"]),
                                costofitem = Convert.ToInt32(userDataSet.Tables[0].Rows[index]["costofitem"])

                            };

                            productinfo.products.Add(product);

                            productinfo.currentpage = Convert.ToInt32(userDataSet.Tables[0].Rows[0]["pagenumber"]);
                            productinfo.total = Convert.ToInt32(userDataSet.Tables[0].Rows[0]["producttotal"]);
                            productinfo.pagetotal = GetPageTotal(productinfo.total, Convert.ToInt32(userDataSet.Tables[0].Rows[0]["limit"]));
                        }
                       

                    }
                                      

                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }
            return productinfo;
        }

        //get page total for get product list with paging details
        private double GetPageTotal(int totalproducts, int limit)
        {
            var result = totalproducts / (float)limit;
            return (int)Math.Ceiling(result);
        }

        //restore client from delete list
        public async Task<bool> RestoreProduct(int? clientId, int? businessid)
        {
            bool isSuccessful = false;

            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            try
            {
                using (var connection = new SqlConnection(ConnectionStringDal.ConnectionString))
                {
                    var sqlAdapter = new SqlDataAdapter();
                    var sqlCommand = new SqlCommand(StoredProcedureDal.RestoreProduct, connection);
                    connection.Open();
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.AddWithValue("@Businessid", businessid);
                    sqlCommand.Parameters.AddWithValue("@Productid", clientId);
                    await sqlCommand.ExecuteNonQueryAsync();
                    isSuccessful = true;
                }

            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }
            return isSuccessful;
        }

        //get client summary data
        public ProductSummaryViewModel GetProductSummaryData(int? businessid, string userid)
        {
            var productsummary = new ProductSummaryViewModel();

            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            try
            {
                DataSet userDataSet = new DataSet();
                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter();
                using (SqlConnection connection = new SqlConnection(ConnectionStringDal.ConnectionString))
                {
                    SqlDataAdapter sqlAdapter = new SqlDataAdapter();
                    SqlCommand sqlCommand = new SqlCommand(StoredProcedureDal.GetProductSummaryData, connection);
                    connection.Open();
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.AddWithValue("@Businessid", businessid);
                    sqlDataAdapter.SelectCommand = sqlCommand;
                    sqlDataAdapter.Fill(userDataSet);
                    if (userDataSet != null & userDataSet.Tables.Count > 0)
                    {
                        for (int index = 0; index <= userDataSet.Tables[0].Rows.Count - 1; index++)
                        {
                            var summaryinfo = new ProductSummaryViewModel()
                            {
                                monthlytotal = Convert.ToInt32(userDataSet.Tables[0].Rows[index]["monthlytotal"]),
                                mostsolditem = Convert.ToString(userDataSet.Tables[0].Rows[index]["mostsold"]),
                                totalproducts = Convert.ToInt32(userDataSet.Tables[0].Rows[index]["totalproducts"]),
                            };

                            productsummary = summaryinfo;

                        }
                    }

                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }
            return productsummary;
        }
    }
}