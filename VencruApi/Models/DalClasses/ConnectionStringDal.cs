﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace VencruApi.Models.DalClasses
{
    public class ConnectionStringDal
    {
        //set dev or live environment
        private static bool isAppTest { get; set; } = false;

        public static string ConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings[$"{ returnconstring(isAppTest)}"].ConnectionString;
            }
        }

        //return connection string based on environment
        private static string returnconstring(bool isAppTest)
        {
            if (isAppTest == true)
            {
                return "Dev";
            }
            else
            {
                return "Live";
            }
        }

    }
}