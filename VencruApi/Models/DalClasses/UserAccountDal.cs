﻿using SharpRaven;
using SharpRaven.Data;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;
using VencruApi.Models.Classes;
using VencruApi.Models.ViewModels;

namespace VencruApi.Models.DalClasses
{
    public class UserAccountDal
    {
        //add user onboarding details
        public async Task<string> AddOnBoardingDetails(BusinessModel model)
        {
            string isSuccessful;
            if (!CheckUser(model.userid))
            {
                return null;
            }
            else
            {
                using (var connection = new SqlConnection(ConnectionStringDal.ConnectionString))
                {
                    var sqlAdapter = new SqlDataAdapter();
                    var sqlCommand = new SqlCommand(StoredProcedureDal.AddOnBoarding, connection);
                    connection.Open();
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.Add("@Id", SqlDbType.Int).Direction = ParameterDirection.Output;
                    sqlCommand.Parameters.AddWithValue("@userId", model.userid);
                    sqlCommand.Parameters.AddWithValue("@address", model.address);
                    sqlCommand.Parameters.AddWithValue("@country", model.country);
                    sqlCommand.Parameters.AddWithValue("@firstname", model.firstname);
                    sqlCommand.Parameters.AddWithValue("@lastname", model.lastname);
                    sqlCommand.Parameters.AddWithValue("@city", model.city);
                    sqlCommand.Parameters.AddWithValue("@state", model.state);
                    sqlCommand.Parameters.AddWithValue("@phonenumber", model.phonenumber);
                    sqlCommand.Parameters.AddWithValue("@estimatedmonthlyrevenue", model.estimatedmonthlyrevenue);
                    sqlCommand.Parameters.AddWithValue("@monthlytarget", model.monthlytarget);
                    sqlCommand.Parameters.AddWithValue("@companyname", model.companyname);
                    sqlCommand.Parameters.AddWithValue("@isincorporated", model.isincorporated);
                    sqlCommand.Parameters.AddWithValue("@industry", model.industry);
                    sqlCommand.Parameters.AddWithValue("@employeesize", model.employeesize);
                    sqlCommand.Parameters.AddWithValue("@currency", model.currency);
                    sqlCommand.Parameters.AddWithValue("@onlinepayment", model.onlinepayment);
                    sqlCommand.Parameters.AddWithValue("@utime", model.utime);
                    await sqlCommand.ExecuteNonQueryAsync();
                    isSuccessful = sqlCommand.Parameters["@Id"].Value.ToString();
                    //isSuccessful = (int)sqlCommand.ExecuteScalar();
                }
            }

            return isSuccessful;

        }

        //add user to business method
        public async Task<bool> AddUserToBusiness(string userid, int businessId)
        {
            bool isSuccessful;
            if (!CheckUser(userid))
            {
                return false;
            }
            else
            {
                using (var connection = new SqlConnection(ConnectionStringDal.ConnectionString))
                {
                    var sqlAdapter = new SqlDataAdapter();
                    var sqlCommand = new SqlCommand("sp_adduser_business", connection);
                    connection.Open();
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.AddWithValue("@userId", userid);
                    sqlCommand.Parameters.AddWithValue("@businessId", businessId);

                    await sqlCommand.ExecuteNonQueryAsync();
                    isSuccessful = true;
                }
            }


            return isSuccessful;
        }

        //get user method, gets user details, current business id of user and all businesses associated to the user
        public UserInfoViewModel GetUser(string userid)
        {
            UserInfoViewModel userInfoViewModel;

            var userInfo = new UserInfoViewModel();

            var userBusinessModel = new BusinessViewModel();

            var userBusinessModelList = new List<BusinessViewModel>();

            var userCurrentBusiness = GetUserCurrentBusiness(userid);

            DataSet userDataSet = new DataSet();

            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter();

            using (var connection = new SqlConnection(ConnectionStringDal.ConnectionString))
            {
                SqlDataAdapter sqlAdapter = new SqlDataAdapter();
                SqlCommand sqlCommand = new SqlCommand(StoredProcedureDal.GetUser, connection);
                connection.Open();
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@Userid", userid);
                sqlDataAdapter.SelectCommand = sqlCommand;
                sqlDataAdapter.Fill(userDataSet);
                if (userDataSet != null & userDataSet.Tables.Count > 0)
                {
                    for (int index = 0; index <= userDataSet.Tables[0].Rows.Count - 1; index++)
                    {
                        userInfo.email = Convert.ToString(userDataSet.Tables[0].Rows[index]["Email"]);
                        userInfo.emailconfirmed = Convert.ToBoolean(userDataSet.Tables[0].Rows[index]["EmailConfirmed"]);
                        userInfo.userid = Convert.ToString(userDataSet.Tables[0].Rows[index]["UserId"]);
                        userInfo.loginprovider = Convert.ToString(userDataSet.Tables[0].Rows[index]["LoginProvider"]);
                        userInfo.firstname = Convert.ToString(userDataSet.Tables[0].Rows[index]["firstname"]);
                        userInfo.lastname = Convert.ToString(userDataSet.Tables[0].Rows[index]["lastname"]);
                        userInfo.profileimagekey = Convert.ToString(userDataSet.Tables[0].Rows[index]["imagekey"]);
                        userInfo.currentbusinessid = userCurrentBusiness.businessid;
                        userInfo.date_created = Convert.ToString(userDataSet.Tables[0].Rows[index]["date_created"]);
                        
                        userInfo.profileimageurl = UtilitiesClass.GetImgUrl(userInfo.profileimagekey, UtilitiesClass.profileImageBucketname);

                        if (userDataSet.Tables[0].Rows[index]["companyname"] == DBNull.Value)
                        {

                        }
                        else
                        {

                            userBusinessModel = new BusinessViewModel
                            {
                                id = Convert.ToInt32(userDataSet.Tables[0].Rows[index]["Id"]),
                                ownerid = Convert.ToString(userDataSet.Tables[0].Rows[index]["userid"]),
                                companyname = Convert.ToString(userDataSet.Tables[0].Rows[index]["companyname"]),
                                country = Convert.ToString(userDataSet.Tables[0].Rows[index]["country"]),
                                address = Convert.ToString(userDataSet.Tables[0].Rows[index]["address"]),
                                city = Convert.ToString(userDataSet.Tables[0].Rows[index]["city"]),
                                currency = Convert.ToString(userDataSet.Tables[0].Rows[index]["currency"]),
                                date_created = Convert.ToString(userDataSet.Tables[0].Rows[index]["date_created"]),
                                employeesize = Convert.ToString(userDataSet.Tables[0].Rows[index]["employeesize"]),
                                industry = Convert.ToString(userDataSet.Tables[0].Rows[index]["industry"]),
                                isincorporated = Convert.ToByte(userDataSet.Tables[0].Rows[index]["isincorporated"]),
                                onlinepayment = Convert.ToByte(userDataSet.Tables[0].Rows[index]["onlinepayment"]),
                                phonenumber = Convert.ToString(userDataSet.Tables[0].Rows[index]["phonenumber"]),
                                state = Convert.ToString(userDataSet.Tables[0].Rows[index]["state"]),
                                facebookurl = Convert.ToString(userDataSet.Tables[0].Rows[index]["facebookurl"]),
                                instagramurl = Convert.ToString(userDataSet.Tables[0].Rows[index]["instagramurl"]),
                                linkdinurl = Convert.ToString(userDataSet.Tables[0].Rows[index]["linkdinurl"]),
                                twitterurl = Convert.ToString(userDataSet.Tables[0].Rows[index]["twitterurl"]),
                                logourl = Convert.ToString(userDataSet.Tables[0].Rows[index]["logourl"]),
                                tax = Convert.ToInt32(userDataSet.Tables[0].Rows[index]["tax"]),
                                vatnumber = Convert.ToString(userDataSet.Tables[0].Rows[index]["vatnumber"]),
                                service = Convert.ToString(userDataSet.Tables[0].Rows[index]["service"]),
                                domain = Convert.ToString(userDataSet.Tables[0].Rows[index]["domain"]),
                                monthlytarget = Convert.ToInt32(userDataSet.Tables[0].Rows[index]["monthlytarget"]),
                                estimatedmonthlyrevenue = Convert.ToInt32(userDataSet.Tables[0].Rows[index]["estimatedmonthlyrevenue"])
                            };

                            userBusinessModel.logourl = UtilitiesClass.GetImgUrl(userBusinessModel.logourl, UtilitiesClass.businesslogoBucketname);

                            userBusinessModelList.Add(userBusinessModel);

                            //get user roles
                            userBusinessModel.userroles = GetUserBusinessRoles(userBusinessModel.id, userid);
                        }

                    }

                    //get user plan
                    userInfo.userplan = GetUserPlan(userid);


                    //userBusinessModelList.Add(userBusinessModel);
                    userInfo.business = userBusinessModelList;
                }

                userInfoViewModel = userInfo;
            }

            return userInfoViewModel;
        }

        //get userplan
        public UserPlanViewModel GetUserPlan(string userid)
        {
            UserPlanViewModel planModel = new UserPlanViewModel();

            var planInfo = new UserPlanViewModel();

            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            try
            {
                DataSet userDataSet = new DataSet();
                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter();
                using (SqlConnection connection = new SqlConnection(ConnectionStringDal.ConnectionString))
                {
                    SqlDataAdapter sqlAdapter = new SqlDataAdapter();
                    SqlCommand sqlCommand = new SqlCommand(StoredProcedureDal.GetUserPlan, connection);
                    connection.Open();
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.AddWithValue("@userid", userid);
                    sqlDataAdapter.SelectCommand = sqlCommand;
                    sqlDataAdapter.Fill(userDataSet);
                    if (userDataSet != null & userDataSet.Tables.Count > 0)
                    {
                        for (int index = 0; index <= userDataSet.Tables[0].Rows.Count - 1; index++)
                        {
                            planInfo.planid = Convert.ToInt32(userDataSet.Tables[0].Rows[index]["planid"]);
                            planInfo.planname = Convert.ToString(userDataSet.Tables[0].Rows[index]["name"]);
                            planInfo.plantype = Convert.ToString(userDataSet.Tables[0].Rows[index]["plantype"]);
                            planInfo.price = Convert.ToInt32(userDataSet.Tables[0].Rows[index]["price"]);
                            planInfo.isactive = Convert.ToBoolean(userDataSet.Tables[0].Rows[index]["status"]);
                            planInfo.date_expire = Convert.ToDateTime(userDataSet.Tables[0].Rows[index]["date_expire"]);


                        }
                    }
                }

                planModel = planInfo;

            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return planModel;
        }

        //get user roles for business
        public ArrayList GetUserBusinessRoles(int businessid, string userid)
        {
            DataSet userDataSet = new DataSet();

            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter();

            var roleListViewModel = new List<RoleViewModel>();

            ArrayList roles = new ArrayList();

            using (var connection = new SqlConnection(ConnectionStringDal.ConnectionString))
            {
                SqlDataAdapter sqlAdapter = new SqlDataAdapter();
                SqlCommand sqlCommand = new SqlCommand(StoredProcedureDal.GetUserBusinessRoles, connection);
                connection.Open();
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@Userid", userid);
                sqlCommand.Parameters.AddWithValue("@Businessid", businessid);
                //string[] roles;
                sqlDataAdapter.SelectCommand = sqlCommand;
                sqlDataAdapter.Fill(userDataSet);
                if (userDataSet != null & userDataSet.Tables.Count > 0)
                {
                    for (int index = 0; index <= userDataSet.Tables[0].Rows.Count - 1; index++)
                    {

                        roles.Add(Convert.ToString(userDataSet.Tables[0].Rows[index]["Name"]));
                        //var roles = new RoleViewModel()
                        //{
                        //    name = Convert.ToString(userDataSet.Tables[0].Rows[index]["Name"]),
                        //    id = Convert.ToString(userDataSet.Tables[0].Rows[index]["Id"]),
                        //    businessid = Convert.ToInt32(userDataSet.Tables[0].Rows[index]["businessid"]),
                        //};

                        //roleListViewModel.Add(roles);
                    }
                }
            }

            return roles;
        }

        //get user business
        public BusinessViewModel GetUserBusiness(string userid, int businessId)
        {

            BusinessViewModel userbusinessViewModel;

            var userbusinessInfo = new BusinessViewModel();

            if (!CheckUser(userid, businessId))
            {
                return null;
            }
            else
            {

                DataSet userDataSet = new DataSet();

                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter();

                using (var connection = new SqlConnection(ConnectionStringDal.ConnectionString))
                {

                    SqlDataAdapter sqlAdapter = new SqlDataAdapter();

                    SqlCommand sqlCommand = new SqlCommand(StoredProcedureDal.GetUserBusiness, connection);

                    connection.Open();

                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    sqlCommand.Parameters.AddWithValue("@Userid", userid);

                    sqlCommand.Parameters.AddWithValue("@BusinessId", businessId);

                    sqlDataAdapter.SelectCommand = sqlCommand;

                    sqlDataAdapter.Fill(userDataSet);

                    if (userDataSet != null & userDataSet.Tables.Count > 0)
                    {
                        for (int index = 0; index <= userDataSet.Tables[0].Rows.Count - 1; index++)
                        {

                            userbusinessInfo = new BusinessViewModel
                            {
                                id = Convert.ToInt32(userDataSet.Tables[0].Rows[index]["Id"]),
                                ownerid = Convert.ToString(userDataSet.Tables[0].Rows[index]["userid"]),
                                //currentbusinessid = userCurrentBusiness.businessid,
                                companyname = Convert.ToString(userDataSet.Tables[0].Rows[index]["companyname"]),
                                country = Convert.ToString(userDataSet.Tables[0].Rows[index]["country"]),
                                address = Convert.ToString(userDataSet.Tables[0].Rows[index]["address"]),
                                city = Convert.ToString(userDataSet.Tables[0].Rows[index]["city"]),
                                currency = Convert.ToString(userDataSet.Tables[0].Rows[index]["currency"]),
                                date_created = Convert.ToString(userDataSet.Tables[0].Rows[index]["date_created"]),
                                employeesize = Convert.ToString(userDataSet.Tables[0].Rows[index]["employeesize"]),
                                industry = Convert.ToString(userDataSet.Tables[0].Rows[index]["industry"]),
                                isincorporated = Convert.ToByte(userDataSet.Tables[0].Rows[index]["isincorporated"]),
                                onlinepayment = Convert.ToByte(userDataSet.Tables[0].Rows[index]["onlinepayment"]),
                                phonenumber = Convert.ToString(userDataSet.Tables[0].Rows[index]["phonenumber"]),
                                state = Convert.ToString(userDataSet.Tables[0].Rows[index]["state"]),
                                facebookurl = Convert.ToString(userDataSet.Tables[0].Rows[index]["facebookurl"]),
                                instagramurl = Convert.ToString(userDataSet.Tables[0].Rows[index]["instagramurl"]),
                                linkdinurl = Convert.ToString(userDataSet.Tables[0].Rows[index]["linkdinurl"]),
                                twitterurl = Convert.ToString(userDataSet.Tables[0].Rows[index]["twitterurl"]),
                                logokey = Convert.ToString(userDataSet.Tables[0].Rows[index]["logourl"]),
                                tax = Convert.ToInt32(userDataSet.Tables[0].Rows[index]["tax"]),
                                vatnumber = Convert.ToString(userDataSet.Tables[0].Rows[index]["vatnumber"]),
                                service = Convert.ToString(userDataSet.Tables[0].Rows[index]["service"]),
                                domain = Convert.ToString(userDataSet.Tables[0].Rows[index]["domain"]),
                                
                            };


                            userbusinessInfo.logourl = UtilitiesClass.GetImgUrl(userbusinessInfo.logokey, UtilitiesClass.businesslogoBucketname);

                        }

                    }

                    userbusinessViewModel = userbusinessInfo;
                }

                userbusinessViewModel.userroles = GetUserBusinessRoles(userbusinessViewModel.id, userid);

                var getPayStack = PayStackDal.IsConnected(userbusinessViewModel.id);

                userbusinessViewModel.connectedpayments.Add(getPayStack);

            }

            return userbusinessViewModel;
        }

        //add user current business for continuity
        public GetUserCurrentBusinessModel AddUserCurrentBusiness(GetUserCurrentBusinessModel model)
        {
            GetUserCurrentBusinessModel result;
            if (!CheckUser(model.userid, model.businessid))
            {
                return null;
            }
            else
            {
                using (var connection = new SqlConnection(ConnectionStringDal.ConnectionString))
                {

                    var sqlCommand = new SqlCommand(StoredProcedureDal.AddUserCurrentBusiness, connection);
                    connection.Open();
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.AddWithValue("@UserId", model.userid);
                    sqlCommand.Parameters.AddWithValue("@BusinessId", model.businessid);
                    sqlCommand.ExecuteNonQuery();
                    result = GetUserCurrentBusiness(model.userid);
                }

            }

            return result;
        }

        //gets the current business a user selected
        public GetUserCurrentBusinessModel GetUserCurrentBusiness(string UserId)
        {
            GetUserCurrentBusinessModel userInfoViewModel;

            var userInfo = new GetUserCurrentBusinessModel();

            DataSet userDataSet = new DataSet();
            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter();
            using (var connection = new SqlConnection(ConnectionStringDal.ConnectionString))
            {
                SqlDataAdapter sqlAdapter = new SqlDataAdapter();
                SqlCommand sqlCommand = new SqlCommand(StoredProcedureDal.GetuserCurrentBusiness, connection);
                connection.Open();
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@UserId", UserId);

                sqlDataAdapter.SelectCommand = sqlCommand;
                sqlDataAdapter.Fill(userDataSet);
                if (userDataSet != null & userDataSet.Tables.Count > 0)
                {
                    for (int index = 0; index <= userDataSet.Tables[0].Rows.Count - 1; index++)
                    {
                        userInfo.businessid = Convert.ToInt32(userDataSet.Tables[0].Rows[index]["BusinessId"]);
                        userInfo.userid = Convert.ToString(userDataSet.Tables[0].Rows[index]["UserId"]);

                    }
                }

                userInfoViewModel = userInfo;
            }

            return userInfoViewModel;
        }

        //check user method
        public bool CheckUser(string userid, int businessid)
        {
            var checkuser = GetUser(userid);
            bool isinBusiness = false;
            if (string.IsNullOrEmpty(checkuser.email) || string.IsNullOrEmpty(checkuser.userid) || (!checkuser.emailconfirmed))
            {
                return isinBusiness;
            }
            else
            {
                foreach (var item in checkuser.business)
                {
                    if (item.id == businessid)
                    {
                        isinBusiness = true;
                    }
                }
            }

            return isinBusiness;

        }

        //update user profile
        public async Task<bool> UpdateuserProfile(UpdateprofileModel model)
        {
            bool isSuucessful = false;
            if (!CheckUser(model.userid))
            {
                return isSuucessful;
            }
            else
            {
                using (var connection = new SqlConnection(ConnectionStringDal.ConnectionString))
                {
                    var sqlAdapter = new SqlDataAdapter();
                    var sqlCommand = new SqlCommand(StoredProcedureDal.Updateprofile, connection);
                    connection.Open();
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.AddWithValue("@userId", model.userid);
                    sqlCommand.Parameters.AddWithValue("@firstname", model.firstname);
                    sqlCommand.Parameters.AddWithValue("@lastname", model.lastname);
                    sqlCommand.Parameters.AddWithValue("@imgurl", model.profileimageurl);
                    sqlCommand.Parameters.AddWithValue("@emailaddress", model.emailaddress);
                    await sqlCommand.ExecuteNonQueryAsync();
                    isSuucessful = true;

                }
            }

            return isSuucessful;
        }

        //update business
        public async Task<bool> UpdateBusiness(BusinessModel model)
        {
            bool isSuccessful = false;
            if (!CheckUser(model.userid, model.Id))
            {
                return isSuccessful;
            }
            else
            {
                using (var connection = new SqlConnection(ConnectionStringDal.ConnectionString))
                {
                    var sqlAdapter = new SqlDataAdapter();
                    var sqlCommand = new SqlCommand(StoredProcedureDal.UpdateBusiness, connection);
                    connection.Open();
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.AddWithValue("@id", model.Id);
                    sqlCommand.Parameters.AddWithValue("@userId", model.userid);
                    sqlCommand.Parameters.AddWithValue("@address", model.address);
                    sqlCommand.Parameters.AddWithValue("@country", model.country);
                    sqlCommand.Parameters.AddWithValue("@city", model.city);
                    sqlCommand.Parameters.AddWithValue("@state", model.state);
                    sqlCommand.Parameters.AddWithValue("@phonenumber", model.phonenumber);
                    sqlCommand.Parameters.AddWithValue("@companyname", model.companyname);
                    sqlCommand.Parameters.AddWithValue("@tax", model.tax);
                    sqlCommand.Parameters.AddWithValue("@vatnumber", model.vatnumber);
                    sqlCommand.Parameters.AddWithValue("@service", model.service);
                    sqlCommand.Parameters.AddWithValue("@ImgUrl", model.logourl);
                    sqlCommand.Parameters.AddWithValue("@isincorporated", model.isincorporated);
                    sqlCommand.Parameters.AddWithValue("@industry", model.industry);
                    sqlCommand.Parameters.AddWithValue("@employeesize", model.employeesize);
                    sqlCommand.Parameters.AddWithValue("@currency", model.currency);
                    sqlCommand.Parameters.AddWithValue("@onlinepayment", model.onlinepayment);
                    //sqlCommand.Parameters.AddWithValue("@domain", model.domain);
                    //sqlCommand.Parameters.AddWithValue("@facebookurl", model.facebookurl);
                    //sqlCommand.Parameters.AddWithValue("@twitterurl", model.twitterurl);
                    //sqlCommand.Parameters.AddWithValue("@linkdinurl", model.linkdinurl);
                    //sqlCommand.Parameters.AddWithValue("@instagramurl", model.instagramurl);
                    await sqlCommand.ExecuteNonQueryAsync();
                    isSuccessful = true;
                }
            }

            return isSuccessful;
        }

        //add team member
        public async Task<string> AddTeamMember(TeamMemberModel model)
        {
            string isSuccessful;

            if (!CheckUser(model.userid, model.businessid))
            {
                return null;
            }
            else
            {
                using (var connection = new SqlConnection(ConnectionStringDal.ConnectionString))
                {
                    var sqlAdapter = new SqlDataAdapter();
                    var sqlCommand = new SqlCommand(StoredProcedureDal.AddTeamMember, connection);
                    connection.Open();
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.Add("@Id", SqlDbType.Int).Direction = ParameterDirection.Output;
                    sqlCommand.Parameters.AddWithValue("@userId", model.userid);
                    sqlCommand.Parameters.AddWithValue("@memberemail", model.memberemail);
                    sqlCommand.Parameters.AddWithValue("@firstname", model.firstname);
                    sqlCommand.Parameters.AddWithValue("@lastname", model.lastname);
                    sqlCommand.Parameters.AddWithValue("@businessid", model.businessid);
                    sqlCommand.Parameters.AddWithValue("@roles", model.roles);
                    sqlCommand.Parameters.AddWithValue("@status", model.status = false);
                    await sqlCommand.ExecuteNonQueryAsync();
                    isSuccessful = sqlCommand.Parameters["@Id"].Value.ToString();
                }
            }

            return isSuccessful;
        }

        //method cron jobs use to delete clients that has been on delete list for days > 30
        public async Task<bool> UpdateInvite(TeamMemberModel model)
        {
            bool isSuccessful = false;
            try
            {
                using (var connection = new SqlConnection(ConnectionStringDal.ConnectionString))
                {
                    var sqlAdapter = new SqlDataAdapter();
                    var sqlCommand = new SqlCommand(StoredProcedureDal.UpdateInvite, connection);
                    connection.Open();
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.AddWithValue("@id", model.id);
                    sqlCommand.Parameters.AddWithValue("@userId", model.userid);
                    sqlCommand.Parameters.AddWithValue("@memberemail", model.memberemail);
                    sqlCommand.Parameters.AddWithValue("@firstname", model.firstname);
                    sqlCommand.Parameters.AddWithValue("@lastname", model.lastname);
                    sqlCommand.Parameters.AddWithValue("@businessid", model.businessid);
                    sqlCommand.Parameters.AddWithValue("@roles", model.roles);
                    sqlCommand.Parameters.AddWithValue("@status", model.status = false);
                    await sqlCommand.ExecuteNonQueryAsync();
                    isSuccessful = true;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return isSuccessful;
        }

        public async Task<bool> VerifyTeamMemberInvite(VerifyTeamMemberModel model)
        {
            bool isSuccessful = false;
            try
            {
                using (var connection = new SqlConnection(ConnectionStringDal.ConnectionString))
                {
                    var sqlAdapter = new SqlDataAdapter();
                    var sqlCommand = new SqlCommand(StoredProcedureDal.VerifyInvite, connection);
                    connection.Open();
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.AddWithValue("@id", model.inviteid);
                    sqlCommand.Parameters.AddWithValue("@userId", model.userid);
                    sqlCommand.Parameters.AddWithValue("@memberemail", model.businessid);
                    await sqlCommand.ExecuteNonQueryAsync();
                    isSuccessful = true;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return isSuccessful;
        }

        //update client
        public async Task<bool> DeleteInvite(int? inviteid, int? businessid, string userid)
        {
            bool isSuccessful = false;

            try
            {
                if (!CheckUser(userid))
                {
                    return false;
                }
                else
                {
                    using (var connection = new SqlConnection(ConnectionStringDal.ConnectionString))
                    {
                        var sqlAdapter = new SqlDataAdapter();
                        var sqlCommand = new SqlCommand(StoredProcedureDal.Deleteinvite, connection);
                        connection.Open();
                        sqlCommand.CommandType = CommandType.StoredProcedure;
                        sqlCommand.Parameters.AddWithValue("@inviteid", inviteid);
                        sqlCommand.Parameters.AddWithValue("@businessid", businessid);
                        sqlCommand.Parameters.AddWithValue("@userid", userid);
                        await sqlCommand.ExecuteNonQueryAsync();
                        isSuccessful = true;
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return isSuccessful;
        }

        //get all invite
        public List<TeamMemberInviteViewModel> GetAllInvite(int? businessid, string userid)
        {

            var inviteListViewModel = new List<TeamMemberInviteViewModel>();
            var userInfo = new TeamMemberInviteViewModel();
            try
            {
                var userDataSet = new DataSet();

                var sqlDataAdapter = new SqlDataAdapter();

                using (var connection = new SqlConnection(ConnectionStringDal.ConnectionString))
                {
                    SqlDataAdapter sqlAdapter = new SqlDataAdapter();
                    SqlCommand sqlCommand = new SqlCommand(StoredProcedureDal.GetAllInvite, connection);
                    connection.Open();
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.AddWithValue("@businessid", businessid);
                    sqlCommand.Parameters.AddWithValue("@userid", userid);
                    sqlDataAdapter.SelectCommand = sqlCommand;
                    sqlDataAdapter.Fill(userDataSet);
                    if (userDataSet != null & userDataSet.Tables.Count > 0)
                    {
                        for (int index = 0; index <= userDataSet.Tables[0].Rows.Count - 1; index++)
                        {
                            userInfo.firstname = Convert.ToString(userDataSet.Tables[0].Rows[index]["firstname"]);
                            userInfo.lastname = Convert.ToString(userDataSet.Tables[0].Rows[index]["lastname"]);
                            userInfo.id = Convert.ToInt32(userDataSet.Tables[0].Rows[index]["id"]);
                            userInfo.userid = Convert.ToString(userDataSet.Tables[0].Rows[index]["userid"]);
                            userInfo.status = Convert.ToBoolean(userDataSet.Tables[0].Rows[index]["status"]);
                            userInfo.memberemail = Convert.ToString(userDataSet.Tables[0].Rows[index]["memberemail"]);
                            userInfo.roles = Convert.ToString(userDataSet.Tables[0].Rows[index]["roles"]);
                            userInfo.date_created = Convert.ToString(userDataSet.Tables[0].Rows[index]["roles"]);
                        }

                        userInfo.business = GetUserBusiness(userid, (int)businessid);

                        inviteListViewModel.Add(userInfo);
                    }

                }
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return inviteListViewModel;
        }

        //get invite
        public TeamMemberInviteViewModel GetInvite(int? businessid, int? inviteid, string userid)
        {
            TeamMemberInviteViewModel inviteViewModel;
            var userInfo = new TeamMemberInviteViewModel();
            try
            {
                DataSet userDataSet = new DataSet();
                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter();
                using (SqlConnection connection = new SqlConnection(ConnectionStringDal.ConnectionString))
                {
                    SqlDataAdapter sqlAdapter = new SqlDataAdapter();
                    SqlCommand sqlCommand = new SqlCommand(StoredProcedureDal.GetInvite, connection);
                    connection.Open();
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.AddWithValue("@inviteid", inviteid);
                    sqlCommand.Parameters.AddWithValue("@businessid", businessid);
                    sqlDataAdapter.SelectCommand = sqlCommand;
                    sqlDataAdapter.Fill(userDataSet);
                    if (userDataSet != null & userDataSet.Tables.Count > 0)
                    {
                        for (int index = 0; index <= userDataSet.Tables[0].Rows.Count - 1; index++)
                        {
                            userInfo.firstname = Convert.ToString(userDataSet.Tables[0].Rows[index]["firstname"]);
                            userInfo.lastname = Convert.ToString(userDataSet.Tables[0].Rows[index]["lastname"]);
                            userInfo.id = Convert.ToInt32(userDataSet.Tables[0].Rows[index]["id"]);
                            userInfo.userid = Convert.ToString(userDataSet.Tables[0].Rows[index]["userid"]);
                            userInfo.status = Convert.ToBoolean(userDataSet.Tables[0].Rows[index]["status"]);
                            userInfo.memberemail = Convert.ToString(userDataSet.Tables[0].Rows[index]["memberemail"]);
                            userInfo.roles = Convert.ToString(userDataSet.Tables[0].Rows[index]["roles"]);
                            userInfo.date_created = Convert.ToString(userDataSet.Tables[0].Rows[index]["roles"]);

                        }

                        userInfo.business = GetUserBusiness(userid, (int)businessid);
                    }

                    inviteViewModel = userInfo;
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return inviteViewModel;

        }

        //add bank
        public async Task<string> AddBank(BankSettingsModel model)
        {
            string isSuccessful;
            if (!CheckUser(model.userid, model.businessid))
            {
                return null;
            }
            else
            {
                using (var connection = new SqlConnection(ConnectionStringDal.ConnectionString))
                {
                    var sqlAdapter = new SqlDataAdapter();
                    var sqlCommand = new SqlCommand(StoredProcedureDal.AddBank, connection);
                    connection.Open();
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.Add("@Id", SqlDbType.Int).Direction = ParameterDirection.Output;
                    sqlCommand.Parameters.AddWithValue("@userId", model.userid);
                    sqlCommand.Parameters.AddWithValue("@bankname", model.bankname);
                    sqlCommand.Parameters.AddWithValue("@accountnumber", model.accountnumber);
                    sqlCommand.Parameters.AddWithValue("@accounttype", model.accounttype);
                    sqlCommand.Parameters.AddWithValue("@businessid", model.businessid);
                    sqlCommand.Parameters.AddWithValue("@isdefault", model.isdefault);
                    sqlCommand.Parameters.AddWithValue("@accountname", model.accountname);

                    await sqlCommand.ExecuteNonQueryAsync();
                    isSuccessful = sqlCommand.Parameters["@Id"].Value.ToString();
                }
            }

            return isSuccessful;
        }

        //get bank
        public BankSettingsViewModel GetBank(int? businessid, int? bankid, string userid)
        {

            var bank = new BankSettingsViewModel();
            try
            {
                DataSet userDataSet = new DataSet();
                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter();
                using (SqlConnection connection = new SqlConnection(ConnectionStringDal.ConnectionString))
                {
                    SqlDataAdapter sqlAdapter = new SqlDataAdapter();
                    SqlCommand sqlCommand = new SqlCommand(StoredProcedureDal.GetBank, connection);
                    connection.Open();
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.AddWithValue("@Bankid", bankid);
                    sqlCommand.Parameters.AddWithValue("@Businessid", businessid);

                    sqlDataAdapter.SelectCommand = sqlCommand;
                    sqlDataAdapter.Fill(userDataSet);
                    if (userDataSet != null & userDataSet.Tables.Count > 0)
                    {
                        for (int index = 0; index <= userDataSet.Tables[0].Rows.Count - 1; index++)
                        {
                            var bankinfo = new BankSettingsViewModel()
                            {
                                bankname = Convert.ToString(userDataSet.Tables[0].Rows[index]["bankname"]),
                                accounttype = Convert.ToString(userDataSet.Tables[0].Rows[index]["accounttype"]),
                                accountnumber = Convert.ToString(userDataSet.Tables[0].Rows[index]["accountnumber"]),
                                date_created = Convert.ToDateTime(userDataSet.Tables[0].Rows[index]["date_created"]),
                                isdefault = Convert.ToBoolean(userDataSet.Tables[0].Rows[index]["isdefault"]),
                                id = Convert.ToInt32(userDataSet.Tables[0].Rows[index]["id"]),
                                businessid = Convert.ToInt32(userDataSet.Tables[0].Rows[index]["businessid"]),
                                userid = Convert.ToString(userDataSet.Tables[0].Rows[index]["userid"]),
                                accountname = Convert.ToString(userDataSet.Tables[0].Rows[index]["accountname"]),
                            };

                            bank = bankinfo;
                        }
                    }
                    else { }

                }
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return bank;
        }

        //get all banks
        public BankListViewModel GetAllBanks(int? businessid, string userid)
        {
            var banklist = new BankListViewModel();
            try
            {
                var userDataSet = new DataSet();

                var sqlDataAdapter = new SqlDataAdapter();

                using (var connection = new SqlConnection(ConnectionStringDal.ConnectionString))
                {
                    SqlDataAdapter sqlAdapter = new SqlDataAdapter();
                    SqlCommand sqlCommand = new SqlCommand(StoredProcedureDal.GetAllBanks, connection);
                    connection.Open();
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.AddWithValue("@BusinessId", businessid);
                    sqlCommand.Parameters.AddWithValue("@Userid", userid);
                    sqlDataAdapter.SelectCommand = sqlCommand;
                    sqlDataAdapter.Fill(userDataSet);
                    if (userDataSet != null & userDataSet.Tables.Count > 0)
                    {
                        for (int index = 0; index <= userDataSet.Tables[0].Rows.Count - 1; index++)
                        {
                            var bankinfo = new BankSettingsViewModel()
                            {
                                bankname = Convert.ToString(userDataSet.Tables[0].Rows[index]["bankname"]),
                                accounttype = Convert.ToString(userDataSet.Tables[0].Rows[index]["accounttype"]),
                                accountnumber = Convert.ToString(userDataSet.Tables[0].Rows[index]["accountnumber"]),
                                date_created = Convert.ToDateTime(userDataSet.Tables[0].Rows[index]["date_created"]),
                                isdefault = Convert.ToBoolean(userDataSet.Tables[0].Rows[index]["isdefault"]),
                                id = Convert.ToInt32(userDataSet.Tables[0].Rows[index]["id"]),
                                businessid = Convert.ToInt32(userDataSet.Tables[0].Rows[index]["businessid"]),
                                userid = Convert.ToString(userDataSet.Tables[0].Rows[index]["userid"]),
                                accountname = Convert.ToString(userDataSet.Tables[0].Rows[index]["accountname"]),

                            };

                            banklist.banks.Add(bankinfo);

                        }

                    }
                    else { }

                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            return banklist;
        }

        //update bank
        public async Task<bool> UpdateBank(BankSettingsModel model)
        {
            bool isSuccessful = false;
            if (!CheckUser(model.userid, model.businessid))
            {
                return isSuccessful;
            }
            else
            {
                using (var connection = new SqlConnection(ConnectionStringDal.ConnectionString))
                {
                    var sqlAdapter = new SqlDataAdapter();
                    var sqlCommand = new SqlCommand(StoredProcedureDal.UpdateBank, connection);
                    connection.Open();
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.AddWithValue("@BusinessId", model.businessid);
                    sqlCommand.Parameters.AddWithValue("@UserId", model.userid);
                    sqlCommand.Parameters.AddWithValue("@Bankname", model.bankname);
                    sqlCommand.Parameters.AddWithValue("@Accountnumber", model.accountnumber);
                    sqlCommand.Parameters.AddWithValue("@Accounttype", model.accounttype);
                    sqlCommand.Parameters.AddWithValue("@Isdefault", model.isdefault);
                    sqlCommand.Parameters.AddWithValue("@Id", model.id);
                    sqlCommand.Parameters.AddWithValue("@Accountname", model.accountname);

                    await sqlCommand.ExecuteNonQueryAsync();
                    isSuccessful = true;
                }
            }

            return isSuccessful;
        }

        //delete bank
        public async Task<bool> DeleteBank(BankOperationModel model)
        {
            bool isSuccessful = false;
            if (!CheckUser(model.userid, model.businessid))
            {
                return isSuccessful;
            }
            else
            {
                using (var connection = new SqlConnection(ConnectionStringDal.ConnectionString))
                {
                    var sqlAdapter = new SqlDataAdapter();
                    var sqlCommand = new SqlCommand(StoredProcedureDal.DeleteBank, connection);
                    connection.Open();
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.AddWithValue("@userId", model.userid);
                    sqlCommand.Parameters.AddWithValue("@bankid", model.bankid);
                    sqlCommand.Parameters.AddWithValue("@businessid", model.businessid);
                    await sqlCommand.ExecuteNonQueryAsync();
                    isSuccessful = true;
                }
            }

            return isSuccessful;
        }

        //setdefault bank
        public async Task<bool> SetDefaultBank(BankOperationModel model)
        {
            bool isSuccessful = false;
            if (!CheckUser(model.userid, model.businessid))
            {
                return isSuccessful;
            }
            else
            {
                using (var connection = new SqlConnection(ConnectionStringDal.ConnectionString))
                {
                    var sqlAdapter = new SqlDataAdapter();
                    var sqlCommand = new SqlCommand(StoredProcedureDal.SetDefaultBank, connection);
                    connection.Open();
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.AddWithValue("@userId", model.userid);
                    sqlCommand.Parameters.AddWithValue("@bankid", model.bankid);
                    sqlCommand.Parameters.AddWithValue("@businessid", model.businessid);
                    await sqlCommand.ExecuteNonQueryAsync();
                    isSuccessful = true;
                }
            }

            return isSuccessful;
        }

        //overloaded method for CheckUser
        public bool CheckUser(string userid)
            {
            var checkuser = GetUser(userid);

            bool isinBusiness = false;

            if (string.IsNullOrEmpty(checkuser.email) || string.IsNullOrEmpty(checkuser.userid) || (!checkuser.emailconfirmed))
            {
                return isinBusiness;
            }
            else
            {
                isinBusiness = true;
            }

            return isinBusiness;
        }

        //overloaded method for CheckUser
        public bool CheckInvite(VerifyTeamMemberModel model)
        {
            var getinvite = GetInvite(model.businessid, model.inviteid, model.userid);

            bool isinBusiness = false;

            if (string.IsNullOrEmpty(getinvite.memberemail) || string.IsNullOrEmpty(getinvite.userid))
            {
                return isinBusiness;
            }
            else
            {
                isinBusiness = true;
            }

            return isinBusiness;
        }

        //generate transaction reference
        public string GenerateTransactionreference(string userid, int planid)
        {
            var plan = AdminDal.GetPlan(planid);

            if (string.IsNullOrEmpty(plan.name))
            {
                return null;
            }
            else if (!CheckUser(userid))
            {
                return null;
            }
            else
            {
                var transactionreferece = UtilitiesClass.PayStackPublic;
                return transactionreferece.ToString();
            }
        }

        public async Task<string> UpdateProfileImage(ProfileImageModel model)
        {
            var imageKey = string.Empty;

            var newData = model;

            var olddata = new ProfileImageModel();

            if (CheckUser(model.userid, model.businessid))
            {
                using (var dbContext = new ApplicationDbContext())
                {
                   var user = dbContext.Users.Find(model.userid);
                    //get old data for audit tail

                    olddata.image = user.imagekey;
                    olddata.businessid = model.businessid;
                    olddata.userid = model.userid;
                    user.imagekey = model.image;
                    await dbContext.SaveChangesAsync();
                    imageKey = user.imagekey;
                }

                AuditTrailDal.CreateAuditTrail(AuditActionType.Update, model.businessid, olddata, newData, model.userid, model.businessid);
            }

            imageKey = UtilitiesClass.GetImgUrl(imageKey, UtilitiesClass.profileImageBucketname) ?? string.Empty;

            return imageKey;
        }

        public async Task<bool> UpdateUserProfileMobile(updateprofilemobile model)
        {
            bool isSucessful = false;

            var newData = model;

            var olddata = new updateprofilemobile();

            if (CheckUser(model.userid, model.businessid))
            {
                using (var dbContext = new ApplicationDbContext())
                {
                    var user = dbContext.Users.Find(model.userid);

                    var business = dbContext.BusinessModels.Find(model.businessid);

                    //get old data for audit tail
                    olddata.firstname = user.firstname;
                    olddata.lastname = user.lastname;
                    olddata.domain = business.domain;
                    olddata.facebookurl = business.facebookurl;
                    olddata.twitterurl = business.twitterurl;
                    olddata.instagramurl = business.instagramurl;
                    olddata.linkdinurl = business.linkdinurl;

                    user.firstname = model.firstname;
                    user.lastname = model.lastname;
                    business.domain = model.domain;
                    business.facebookurl = model.facebookurl;
                    business.twitterurl = model.twitterurl;
                    business.instagramurl = model.instagramurl;
                    business.linkdinurl = model.linkdinurl;

                    await dbContext.SaveChangesAsync();

                    isSucessful = true;
                }

                AuditTrailDal.CreateAuditTrail(AuditActionType.Update, model.businessid, olddata, newData, model.userid, model.businessid);

            }

            return isSucessful;

        }

    }
}