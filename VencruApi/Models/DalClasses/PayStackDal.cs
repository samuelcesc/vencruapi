﻿using PayStack.Net;
using VencruApi.Models.Classes;
using VencruApi.Models.ViewModels;

namespace VencruApi.Models.DalClasses
{
    public class PayStackDal
    {
        public UserAccountDal UserAccountDal
        {
            get
            {
                return UserAccountDal = new UserAccountDal();
            }
            set
            {

            }
        }

        private static string test { get { return UtilitiesClass.PayStackSecret; } set { } }

        public SubAccountCreateResponse CreateSubAccount(ConnectPayStackModel model)
        {
            var createSubaccountResponse = new SubAccountCreateResponse();

            if (UserAccountDal.CheckUser(model.userid, model.businessid))
            {
                var api = new PayStackApi(test);

                createSubaccountResponse = api.SubAccounts.Create(model.businessname, model.bankname, model.accountnumber, "0");

                if (createSubaccountResponse.Status == true)
                {
                    AddSubAccountToDb(createSubaccountResponse, model);
                }
            }

            return createSubaccountResponse;

        }

        private bool AddSubAccountToDb(SubAccountCreateResponse subaccountresponse, ConnectPayStackModel connectmodel)
        {
            using (var dbContext = new ApplicationDbContext())
            {
                var paystackSubAccountModel = new PayStackSubAccountModel();
                paystackSubAccountModel.businessid = connectmodel.businessid;
                paystackSubAccountModel.subaccountcode = subaccountresponse.Data.SubaccountCode;
                paystackSubAccountModel.userid = connectmodel.userid;
                paystackSubAccountModel.Id = connectmodel.businessid;
                dbContext.PayStackSubAccountModels.Add(paystackSubAccountModel);
                dbContext.SaveChanges();
                return true;
            }
        }

        public static OnlinePaymentBusinessViewModel IsConnected(int businessid)
        {
            var accountdetails = new PayStackSubAccountModel();

            var paymentlistbusinessviewmodel = new OnlinePaymentBusinessViewModel("paystack",false);

            using (var dbContext = new ApplicationDbContext())
            {
                accountdetails = dbContext.PayStackSubAccountModels.Find(businessid);
            }
            if (accountdetails != null)
            {
                var api = new PayStackApi(test);

                var response = api.SubAccounts.Fetch(accountdetails.subaccountcode);

                if (response.Status == true)
                {
                    paymentlistbusinessviewmodel.isconnected = true;
                }

            }

            return paymentlistbusinessviewmodel;
        }
    }
}