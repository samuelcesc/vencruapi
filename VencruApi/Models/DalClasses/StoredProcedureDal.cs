﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VencruApi.Models.DalClasses
{
    public class StoredProcedureDal
    {
        //admin stored procedures
        public const string AddWaitlist = "sp_add_waitlist";

        public const string GetWaitlist = "sp_get_waitlist";

        public const string GetallWaitlist = "sp_get_allwaitlist";

        public const string AddPlan = "sp_add_plan";

        public const string GetPlan = "sp_get_plan";

        public const string DeletePlan = "sp_delete_plan";

        public const string GetAllPlan = "sp_get_allplan";

        public const string GetApprovedReviews = "sp_get_approvedreviews";

        public const string GetAllReviews = "sp_get_allreviews";

        public const string ApproveReview = "sp_approve_review";

        public const string AddReview = "sp_add_review";

        public const string GetReview = "sp_get_review";
        
        public const string AddFeedback = "sp_add_feedback";

        public const string GetFeedback = "sp_get_feedback";

        public const string GetAllFeedback = "sp_get_allfeedback";

        public const string VerifyInvite = "sp_verify_invite";

        public const string Deleteinvite = "sp_delete_invite";

        public const string GetAllInvite = "sp_get_allinvite";

        public const string GetInvite = "sp_get_invite";

        public const string AddPromoCode = "sp_add_promocode";

        public const string GetPromoCode = "sp_get_promocode";

        public const string ApplyPromoCode = "sp_apply_promocode";

        public const string GetPromoCodeCount = "sp_get_promocodecount";

        public static string AddContactUs = "sp_add_contactus";

        //Account and onboarding stored procedures

        public const string AddOnBoarding = "sp_add_onboarding";

        public const string GetUser = "sp_getuser";

        public const string GetUserBusiness = "sp_getuser_business";

        public const string AddUserCurrentBusiness = "sp_add_usercurrent_business";

        public const string GetuserCurrentBusiness = "sp_getusercurrent_business";

        public const string Updateprofile = "sp_update_profile";

        public const string UpdateBusiness = "sp_update_business";

        public const string AddBank = "sp_add_bank";

        public const string GetBank = "sp_get_bank";

        public const string GetAllBanks = "sp_get_allbanks";

        public const string UpdateBank = "sp_update_bank";

        public const string DeleteBank = "sp_delete_bank";

        public const string SetDefaultBank = "sp_set_defaultbank";

        public const string GetUserBusinessRoles = "sp_get_userbusinessroles";

        public const string AddTeamMember = "sp_add_teammember";

        public const string UpdateInvite = "sp_update_invite";

        public const string AddSubscription = "sp_add_subscription";

        public const string AddUserVerificationCode = "sp_add_userverificationcode";

        public const string GetUserPlan = "sp_get_userplan";


        //Client module stored procedures
        public const string AddClient = "sp_add_client";

        public const string AddClientNote = "sp_add_clientnote";

        public const string GetClient = "sp_get_client";

        public const string GetAllClientDeleteList = "sp_getall_clientdelete_list";

        public const string GetAllClients = "sp_get_allclients";

        public const string UpdateClient = "sp_update_client";

        public const string DeleteClient = "sp_delete_client";

        public const string DeleteClientTemp = "sp_delete_client_temp";

        public const string RestoreClient = "sp_restore_client";

        public const string GetClientSummaryData = "sp_get_clientsummarydata";

        public const string GetClientHistoryList = "sp_get_clienthistory";


        //Product and services list stored procedure
        public const string AddProductList = "sp_add_productlist";

        public const string GetProductList = "sp_get_productlist";

        public const string GetAllProductLists = "sp_get_allproductlist";

        public const string UpdateproductList = "sp_update_productlist";

        public const string DeleteProductList = "sp_delete_productlist";

        public const string DeleteProductTemp = "sp_delete_product_temp";

        public const string GetAllProductDeleteList = "sp_getall_productdelete_list";

        public const string RestoreProduct = "sp_restore_product";

        public const string GetProductSummaryData = "sp_get_productsummary";

        // Expenses stored procedure
        public const string AddExpense = "sp_add_expense";

        public const string GetExpense = "sp_get_expense";

        public const string GetAllExpenses = "sp_get_allexpenses";

        public const string GetExpenseSummaryData = "sp_get_expensesummary";

        public const string UpdateExpense = "sp_get_update_expense";
        public const string DeleteExpenseTemp = "sp_delete_Expensetemp";

        public const string GetExpenseNumberCount = "sp_get_expensenumber_count";

        //Invoice stored procedure
        public const string AddInvoice = "sp_add_Invoice";

        public const string DeleteInvoiceItem = "sp_delete_invoiceitem";

        public const string CancelInvoice = "sp_cancel_invoice";
        
        public const string AddInvoiceItem = "sp_add_item";

        public const string AddProductToInvoice = "sp_add_producttoinvoice";

        public const string GetInvoice = "sp_get_Invoice";

        public const string GetAllInvoices = "sp_get_allInvoices";

        public const string DeleteInvoiceTemp = "sp_delete_invoicetemp";

        public const string UpdateInvoice = "sp_update_Invoice";

        public const string GetInvoiceItem = "sp_get_invoice_item";

        public const string GetInvoiceNumberCount = "sp_get_invoicenumber_count";

        public const string VerifyInvoice = "sp_verify_invoice";

        public static string UpdateInvoiceItem = "sp_update_item";
        
        public const string AddPayment = "sp_add_payment";

        public const string GetPayment = "sp_get_payment";

        public const string GetAllPayment = "sp_get_allpayment";

        public static string DeletePayment = "sp_delete_payment";

        public static string UpdatePayment = "sp_update_payment";


        //DashBoard stored procedure
        public const string AddGoal = "sp_add_goal";
        public const string updateGoal = "sp_update_goal";



        //audit trail
        public static string AddAuditTrail = "sp_add_audittrail";
        public static string GetAuditTrail = "sp_Get_audittrail";



    }
}