﻿using SharpRaven;
using SharpRaven.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using VencruApi.Models.ViewModels;

namespace VencruApi.Models.DalClasses
{
    public class DashboardDal
    {
        private List<ExpenseModel> MyExpenses { get; set; }

        private List<InvoicePaymentModel> MyPayments { get; set; }

        private List<InvoiceModel> MyInvoices { get; set; }

        //Get business summary data
        public BusinessSummaryViewModel GetBusinessSummary(string userid, int businessid, string sortby)
        {
            var salessummary = new SalesBreakdownViewModel();
            var outstandingviewmodel = new OutStandingRevenueViewModel();

            var businessSummary = new BusinessSummaryViewModel(salessummary, outstandingviewmodel);

            using (var dbContext = new ApplicationDbContext())
            {
                MyExpenses = dbContext.ExpenseModels.  Where(m => m.businessid == businessid).ToList();
                MyPayments = dbContext.InvoicePaymentModels.Where(m => m.invoice.businessid == businessid).ToList();
                MyInvoices = dbContext.InvoiceModels.Where(m => m.businessid == businessid).ToList();
            }

            var expensesummary = GetExpenseSummaryData(MyExpenses, sortby, businessSummary);

            var incomesummary = GetIncomeSummaryData(MyPayments, sortby, expensesummary);

            var totaloutstanding = GetTotalOutStandingRevenue(MyInvoices, sortby, incomesummary);

            totaloutstanding.salessummary.totalreceived = totaloutstanding.totalincomereceived;

            totaloutstanding.salessummary.totaloutstanding = totaloutstanding.totaloutstanding;

            var summary = AnazlyseBusinessData(totaloutstanding, businessid);

            return summary;
        }

        private BusinessSummaryViewModel GetExpenseSummaryData(List<ExpenseModel> expenses, string sortyby, BusinessSummaryViewModel businessSummary)
        {
            var ExpenseData = new List<ExpenseViewModel>();

            var date = DateTime.Now.ToString("dd/MM/yyyy");

            var dtto = DateTime.ParseExact(date, "dd/MM/yyyy", null);

            foreach (var item in expenses)
            {
                ExpenseData.Add(new ExpenseViewModel
                {
                    businessid = item.businessid,
                    category = item.category,
                    date_created = Convert.ToDateTime(item.date_created).Date,
                    description = item.description,
                    totalamount = item.totalamount,
                    expensedate = Convert.ToDateTime(item.expensedate),
                    expensenumber = item.expensenumber,
                    image = item.image,
                    paidwith = item.paidwith,
                    //receiptimageurl = item.image,
                    vendor = item.vendor,
                    id = item.id
                });
            }

            if (sortyby.ToLower() == "today")
            {
                businessSummary.totalexpenses = ExpenseData.Where(m => m.date_created == dtto).Sum(m => m.totalamount);
            }
            else if (sortyby.ToLower() == "thismonth" && expenses.Count() > 0)
            {
                var currentMonth = DateTime.Now.ToString("MM");
                businessSummary.totalexpenses = ExpenseData.Where(m => m.date_created.ToString("MM") == currentMonth).Sum(m => m.totalamount);
            }
            else if (sortyby == "yesterday" && expenses.Count() > 0)
            {
                var yesterday = DateTime.Now.Date.AddDays(-1);

                businessSummary.totalexpenses = ExpenseData.Where(m => m.date_created == yesterday).Sum(m => m.totalamount);
            }
            else if (sortyby == "thisweek" && expenses.Count() > 0)
            {
                var cultureInfo = new CultureInfo("en-US");

                var myCalender = cultureInfo.Calendar;

                var myCalenderRule = cultureInfo.DateTimeFormat.CalendarWeekRule;

                var myFirstDayOfWeek = cultureInfo.DateTimeFormat.FirstDayOfWeek;

                var thisWeekNumber = myCalender.GetWeekOfYear(DateTime.Now, myCalenderRule, myFirstDayOfWeek);

                var newExpenseData = new List<ExpenseViewModel>();

                foreach (var item in ExpenseData)
                {
                    var weekNumber = myCalender.GetWeekOfYear(item.date_created, myCalenderRule, myFirstDayOfWeek);

                    if (thisWeekNumber == weekNumber)
                    {
                        newExpenseData.Add(new ExpenseViewModel
                        {
                            businessid = item.businessid,
                            category = item.category,
                            date_created = Convert.ToDateTime(item.date_created).Date,
                            description = item.description,
                            totalamount = item.totalamount,
                            expensedate = Convert.ToDateTime(item.expensedate),
                            expensenumber = item.expensenumber,
                            paidwith = item.paidwith,
                            image = item.image,
                            vendor = item.vendor,
                            id = item.id
                        });
                    }
                }

                businessSummary.totalexpenses = newExpenseData.Sum(m => m.totalamount);

            }
            else if (sortyby == "thisyear" && expenses.Count() > 0)
            {
                var year = DateTime.Now.Year;

                businessSummary.totalexpenses = ExpenseData.Where(m => m.date_created.Year == year).Sum(m => m.totalamount);
            }

            else { }

            return businessSummary;
        }

        private BusinessSummaryViewModel GetTotalOutStandingRevenue(List<InvoiceModel> invoices, string sortby, BusinessSummaryViewModel businessSummary)
        {

            var InvoiceData = new List<InvoiceViewModel>();

            var date = DateTime.Now.ToString("dd/MM/yyyy");

            var dtto = DateTime.ParseExact(date, "dd/MM/yyyy", null);

            foreach (var item in invoices)
            {
                InvoiceData.Add(new InvoiceViewModel
                {
                    id = item.id,
                    date_created = Convert.ToDateTime(item.date_created).Date,
                    amountdue = item.amountdue,
                    subtotal = item.subtotal,
                    invoicenumber = item.invoicenumber,
                    deposit = item.deposit,
                    discount = item.discount,

                });
            }

            if (sortby.ToLower() == "today")
            {
                var invoiceTotal = InvoiceData.Where(m => m.date_created == dtto).Sum(m => m.amountdue);

                businessSummary.totaloutstanding = invoiceTotal - businessSummary.totalincomereceived;
            }
            else if (sortby.ToLower() == "thismonth")
            {
                var currentMonth = DateTime.Now.ToString("MM");

                var invoiceTotal = InvoiceData.Where(m => m.date_created.ToString("MM") == currentMonth).Sum(m => m.amountdue);

                businessSummary.totaloutstanding = invoiceTotal - businessSummary.totalincomereceived;

            }
            else if (sortby == "yesterday")
            {
                var yesterday = DateTime.Now.Date.AddDays(-1);

                var invoiceTotal = InvoiceData.Where(m => m.date_created == yesterday).Sum(m => m.amountdue);

                businessSummary.totaloutstanding = invoiceTotal - businessSummary.totalincomereceived;

            }
            else if (sortby == "thisweek")
            {
                var cultureInfo = new CultureInfo("en-US");

                var myCalender = cultureInfo.Calendar;

                var myCalenderRule = cultureInfo.DateTimeFormat.CalendarWeekRule;

                var myFirstDayOfWeek = cultureInfo.DateTimeFormat.FirstDayOfWeek;

                var thisWeekNumber = myCalender.GetWeekOfYear(DateTime.Now, myCalenderRule, myFirstDayOfWeek);

                var newInvoicetData = new List<InvoiceViewModel>();

                foreach (var item in InvoiceData)
                {
                    var weekNumber = myCalender.GetWeekOfYear(item.date_created, myCalenderRule, myFirstDayOfWeek);

                    if (thisWeekNumber == weekNumber)
                    {
                        newInvoicetData.Add(new InvoiceViewModel
                        {
                            id = item.id,
                            date_created = Convert.ToDateTime(item.date_created).Date,
                            amountdue = item.amountdue,
                            subtotal = item.subtotal,
                            invoicenumber = item.invoicenumber,
                            deposit = item.deposit,
                            discount = item.discount,
                        });
                    }
                }

                var invoiceTotal = newInvoicetData.Sum(m => m.amountdue);

                businessSummary.totaloutstanding = invoiceTotal - businessSummary.totalincomereceived;

            }
            else if (sortby == "thisyear")
            {
                var year = DateTime.Now.Year;

                var invoiceTotal = InvoiceData.Where(m => m.date_created.Year == year).Sum(m => m.amountdue);

                businessSummary.totaloutstanding = invoiceTotal - businessSummary.totalincomereceived;

            }

            else { }

            return businessSummary;
        }

        public List<ExpenseSummaryDashboardViewModel> GetExpenseBreakDown(string userid, int businessid, string sortby)
        {
            var expenseSummaryViewModel = new List<ExpenseSummaryDashboardViewModel>();

            using (var dbContext = new ApplicationDbContext())
            {
                var newExpenseSummary = dbContext.ExpenseModels.Where(m => m.businessid == businessid).GroupBy(x => x.category, (key, values) => new { category = key, total = values.Sum(x => x.totalamount) }).ToList();

                foreach (var item in newExpenseSummary)
                {
                    expenseSummaryViewModel.Add(new ExpenseSummaryDashboardViewModel { total = item.total, category = item.category });
                }
            }
            return expenseSummaryViewModel.ToList();
        }

        private BusinessSummaryViewModel GetIncomeSummaryData(List<InvoicePaymentModel> payments, string sortyby, BusinessSummaryViewModel businessSummary)
        {
            var PaymentData = new List<InvoicePaymentViewModel>();

            var date = DateTime.Now.ToString("dd/MM/yyyy");

            var dtto = DateTime.ParseExact(date, "dd/MM/yyyy", null);

            foreach (var item in payments)
            {
                PaymentData.Add(new InvoicePaymentViewModel
                {
                    id = item.id,
                    amount = item.amount,
                    date_created = Convert.ToDateTime(item.date_created).Date,
                    paidwith = item.paidwith,
                    date_paid = Convert.ToDateTime(item.date_paid),
                });
            }

            if (sortyby.ToLower() == "today")
            {
                businessSummary.totalincomereceived = PaymentData.Where(m => m.date_created == dtto).Sum(m => m.amount);
            }
            else if (sortyby.ToLower() == "thismonth")
            {
                var currentMonth = DateTime.Now.ToString("MM");

                businessSummary.totalincomereceived = PaymentData.Where(m => m.date_created.ToString("MM") == currentMonth).Sum(m => m.amount);
            }
            else if (sortyby == "yesterday")
            {
                var yesterday = DateTime.Now.Date.AddDays(-1);

                businessSummary.totalincomereceived = PaymentData.Where(m => m.date_created == yesterday).Sum(m => m.amount);
            }
            else if (sortyby == "thisweek")
            {
                var cultureInfo = new CultureInfo("en-US");

                var myCalender = cultureInfo.Calendar;

                var myCalenderRule = cultureInfo.DateTimeFormat.CalendarWeekRule;

                var myFirstDayOfWeek = cultureInfo.DateTimeFormat.FirstDayOfWeek;

                var thisWeekNumber = myCalender.GetWeekOfYear(DateTime.Now, myCalenderRule, myFirstDayOfWeek);

                var newPaymentData = new List<InvoicePaymentViewModel>();

                foreach (var item in PaymentData)
                {
                    var weekNumber = myCalender.GetWeekOfYear(item.date_created, myCalenderRule, myFirstDayOfWeek);

                    if (thisWeekNumber == weekNumber)
                    {
                        newPaymentData.Add(new InvoicePaymentViewModel
                        {
                            id = item.id,
                            amount = item.amount,
                            date_created = Convert.ToDateTime(item.date_created).Date,
                            paidwith = item.paidwith,
                            date_paid = Convert.ToDateTime(item.date_paid),
                        });
                    }
                }

                businessSummary.totalexpenses = newPaymentData.Sum(m => m.amount);

            }
            else if (sortyby == "thisyear")
            {
                var year = DateTime.Now.Year;

                businessSummary.totalincomereceived = PaymentData.Where(m => m.date_created.Year == year).Sum(m => m.amount);
            }

            else { }

            return businessSummary;
        }

        private BusinessSummaryViewModel AnazlyseBusinessData(BusinessSummaryViewModel model, int businessid)
        {
            int expectedincome;
            int targetincome;
            int totalsales = 0;
            using (var dbContext = new ApplicationDbContext())
            {
                expectedincome = dbContext.BusinessModels.SingleOrDefault(m => m.Id == businessid).estimatedmonthlyrevenue;
                targetincome = dbContext.BusinessModels.SingleOrDefault(m => m.Id == businessid).monthlytarget;
                var salesCount = dbContext.InvoiceModels.Where(m => m.businessid == businessid).Count();
                if (salesCount <= 0)
                {

                }
                else
                {
                    totalsales = dbContext.InvoiceModels.Where(m => m.businessid == businessid).Sum(m => m.amountdue);
                }
            }

            //check if user has added expected or target income
            if (expectedincome == 0 && targetincome == 0)
            {
                model.salessummary.message = "Please kindly update your monthly target or estimated monthly income in settings for sales summary projection";
            }
            else if (expectedincome <= totalsales || targetincome <= totalsales) //check if expected income/target income is less than totalsales
            {
                if (expectedincome > 0)
                {

                    int average = (totalsales + expectedincome) / 2;

                    var percentageaverage = (totalsales - expectedincome) / (float)totalsales * 100;

                    model.salessummary.percentage = (int)Math.Ceiling(percentageaverage);

                    if (percentageaverage >= 0)
                    {
                        model.salessummary.message = $"Your total revenue is {model.salessummary.percentage}% higher than your average {average}, your sales is higher than your expected income you can adjust your expected income in settings";
                        model.salessummary.isgood = true;
                    }
                    else
                    {
                        model.salessummary.message = $"Your total revenue is {model.salessummary.percentage}% lower than your average {average}";
                        model.salessummary.isgood = false;
                    }
                }
                else if (targetincome > 0)
                {
                    int average = (totalsales + targetincome) / 2;

                    var percentageaverage = (totalsales - targetincome) / (float)totalsales * 100;

                    model.salessummary.percentage = (int)Math.Ceiling(percentageaverage);

                    if (percentageaverage >= 0)
                    {
                        model.salessummary.message = $"Your total revenue is { model.salessummary.percentage}% higher than your average {average}, your sales is higher than your sales target <br/> you can adjust your sales target in settings";
                        model.salessummary.isgood = true;
                    }
                    else
                    {
                        model.salessummary.message = $"Your total revenue is { model.salessummary.percentage}% lower than your average {average}";
                        model.salessummary.isgood = false;
                    }
                }
                else { }
            }
            else if (expectedincome > totalsales || targetincome > totalsales) //check if expected income/target income is higher than total sales
            {
                if (expectedincome > 0)
                {

                    int average = (totalsales + expectedincome) / 2;

                    var percentageaverage = (expectedincome - totalsales) / (float)expectedincome * 100;

                    model.salessummary.percentage = (int)Math.Ceiling(percentageaverage);

                    if (percentageaverage >= 0)
                    {
                        model.salessummary.message = $"Your total revenue is {model.salessummary.percentage}% higher than your average {average}, your sales is higher than your expected income <br/> you can adjust your expected income in settings";
                        model.salessummary.isgood = true;

                    }
                    else
                    {
                        model.salessummary.message = $"Your total revenue is {model.salessummary.percentage}% lower than your average {average}";
                        model.salessummary.isgood = false;
                    }

                }
                else if (targetincome > 0)
                {
                    int average = (totalsales + targetincome) / 2;

                    var percentageaverage = (targetincome - totalsales) / (float)targetincome * 100;

                    model.salessummary.percentage = (int)Math.Ceiling(percentageaverage);

                    if (percentageaverage >= 0)
                    {
                        model.salessummary.message = $"Your total revenue is {model.salessummary.percentage}% higher than your average {average}, your sales is higher than your sales target <br/> you can adjust your sales target in settings";
                        model.salessummary.isgood = true;
                    }
                    else
                    {
                        model.salessummary.message = $"Your total revenue is {model.salessummary.percentage}% lower than your average {average}";
                        model.salessummary.isgood = false;

                    }
                }
                else { }
            }

            //model.salessummary.totalsales = model.salessummary.totalreceived + model.salessummary.totaloutstanding;

            return model;

        }

        private BusinessSummaryViewModel AnalyzeRevenueData(BusinessSummaryViewModel model, int businessid)
        {
            using (var dbContext = new ApplicationDbContext())
            {
                model.revenuesummary.due = dbContext.InvoiceModels.Where(m => m.businessid == businessid && m.invoicestatus.ToLower() == "due").Sum(m => m.amountdue);
                model.revenuesummary.overdue = dbContext.InvoiceModels.Where(m => m.businessid == businessid && m.invoicestatus.ToLower() == "overdue").Sum(m => m.amountdue);
            }

            return model;
        }

        private List<TotalProfitDashboardViewModel> GetTotalProfit(List<ExpenseModel> expenseModels, List<InvoicePaymentModel> paymentModels)
        {

            var paymentsSummaryViewModel = new List<InvoicePaymentViewModel>();

            var expensesSummaryViewModel = new List<ExpenseViewModel>();

            var totalProfitViewModel = new List<TotalProfitDashboardViewModel>();

            var paymentSummary = new List<TotalProfitDashboardViewModel>();

            var expenseSummary = new List<TotalProfitDashboardViewModel>();

            //add expenses to expense model list
            foreach (var item in expenseModels)
            {
                expensesSummaryViewModel.Add(new ExpenseViewModel { date_created = Convert.ToDateTime(item.date_created), totalamount = item.totalamount });
            }

            //add payments to payment model list
            foreach (var item in paymentModels)
            {
                paymentsSummaryViewModel.Add(new InvoicePaymentViewModel { date_created = Convert.ToDateTime(item.date_created), amount = item.amount });
            }

            //group payment by month
            var newPaymentSummary = paymentsSummaryViewModel.GroupBy(m => m.date_created.Month, (key, value) => new { month = key, total = value.Sum(x => x.amount) }).OrderBy(z => z.month).ToList();

            //group expense by month
            var newExpenseSummary = expensesSummaryViewModel.GroupBy(m => m.date_created.Month, (key, value) => new { month = key, total = value.Sum(x => x.totalamount) }).OrderBy(z => z.month).ToArray();

            //add grouped payment data to profit view model
            foreach (var item in newPaymentSummary)
            {
                paymentSummary.Add(new TotalProfitDashboardViewModel { month = item.month, total = item.total });
            };

            //add grouped expense data to profit view model
            foreach (var item in newExpenseSummary)
            {
                expenseSummary.Add(new TotalProfitDashboardViewModel { month = item.month, total = item.total });
            };

            //add missing months to payment summary
            var paymentDataSummary = GetDataSummaryData(paymentSummary).OrderBy(m => m.month).ToArray();

            //add missing months to expense summary
            var expenseDataSummary = GetDataSummaryData(expenseSummary).OrderBy(m => m.month).ToArray();

            //calculate profit
            for (int i = 0; i < paymentDataSummary.Count(); i++)
            {
                if (paymentDataSummary[i].month == expenseDataSummary[i].month)
                {
                    totalProfitViewModel.Add(new TotalProfitDashboardViewModel { month = paymentDataSummary[i].month, total = paymentDataSummary[i].total - expenseDataSummary[i].total });
                }
                else { }
            }


            return totalProfitViewModel;
        }

        private List<TotalProfitDashboardViewModel> GetDataSummaryData(List<TotalProfitDashboardViewModel> modeldata)
        {
            var model = modeldata.ToArray();

            for (int i = 1; i <= 12; i++)
            {
                if (modeldata.Any(m => m.month == i))
                {

                }
                else
                {
                    modeldata.Add(new TotalProfitDashboardViewModel { month = i, total = 0 });
                }
            }

            return modeldata;
        }

        public List<TotalProfitDashboardViewModel> ToTalProfit(string userid, int businessid, string sortby)
        {

            var paymentsSummary = new List<InvoicePaymentModel>();

            var expenseSummary = new List<ExpenseModel>();

            using (var dbContext = new ApplicationDbContext())
            {
                paymentsSummary = dbContext.InvoicePaymentModels.Where(m => m.invoice.businessid == businessid).ToList();

                expenseSummary = dbContext.ExpenseModels.Where(m => m.businessid == businessid).ToList();
            }

            var profit = GetTotalProfit(expenseSummary, paymentsSummary);


            return profit;
        }

        public List<IncomeSourcesViewModel> GetIncomeSources(string userid, int businessid, string sortby)
        {
            var invoicePayments = new List<InvoicePaymentModel>();

            var incomeSources = new List<IncomeSourcesViewModel>();

            using (var dbContext = new ApplicationDbContext())
            {
                var payments = dbContext.InvoicePaymentModels.Where(m => m.invoice.businessid == businessid).Join(dbContext.ItemsModels.Include("product"), inv => inv.invoiceid, pay => pay.invoiceid, (pay, items) => new { pay, items });

                foreach (var pay in payments)
                {
                    incomeSources.Add(new IncomeSourcesViewModel { itemid = pay.items.productid, total = pay.items.amount });
                }

                //invoicePayments = dbContext.InvoicePaymentModels.Include("invoice").Where(m => m.invoice.businessid == businessid).ToList();
            }

            var groupIncomeSource = GetIncomeSourcesData(incomeSources);

            return groupIncomeSource;
        }

        private List<IncomeSourcesViewModel> GetIncomeSourcesData(List<IncomeSourcesViewModel> model)
        {
            var incomeSourceModel = model.GroupBy(m => m.itemid).Select(g => new { name = g.Key, total = g.Sum(m => m.total) }).ToList();

            var newincomeSourceModel = new List<IncomeSourcesViewModel>();

            foreach (var item in incomeSourceModel)
            {
                
                newincomeSourceModel.Add(new IncomeSourcesViewModel { itemid = item.name, total = item.total });
            }

            return newincomeSourceModel;
        }


        public GoalViewModel GetGoal(string userid, int businessid)
        {
            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");
            GoalViewModel goalViewModel = new GoalViewModel();
            try
            {
                using (var dbContext = new ApplicationDbContext())
                {
                    int monthlytarget = dbContext.BusinessModels.Where(m => m.userid == userid && m.Id == businessid).SingleOrDefault().monthlytarget;
                    goalViewModel.monthlytarget = monthlytarget;
                    MyPayments = dbContext.InvoicePaymentModels.Where(m => m.invoice.businessid == businessid).ToList();
                    goalViewModel.TotalIncomeRecived = GetTotalincomereceived(MyPayments);
                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return goalViewModel;
        }
        
        public async Task<bool> AddGoal(AddGoalViewModel model)
        {
            bool isSuccessful = false;
            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");
            GoalViewModel goalViewModel = new GoalViewModel();
            try
            {
                using (var connection = new SqlConnection(ConnectionStringDal.ConnectionString))
                {
                    var sqlAdapter = new SqlDataAdapter();
                    var sqlCommand = new SqlCommand(StoredProcedureDal.AddGoal, connection);
                    connection.Open();
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.AddWithValue("@Id", model.businessid);
                    sqlCommand.Parameters.AddWithValue("@userid", model.userid);
                    sqlCommand.Parameters.AddWithValue("@estimatedmonthlyrevenue", model.estimatedmonthlyrevenue);
                    sqlCommand.Parameters.AddWithValue("@monthlytarget", model.monthlytarget);
                    sqlCommand.ExecuteNonQuery();
                    isSuccessful = true;
                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return isSuccessful;
        }


        public async Task<bool> updateGoal(UpdateGoalViewModel model)
        {
            var isSuccessful = false;
            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");
            GoalViewModel goalViewModel = new GoalViewModel();
            try
            {
                using (var connection = new SqlConnection(ConnectionStringDal.ConnectionString))
                {
                    var sqlAdapter = new SqlDataAdapter();
                    var sqlCommand = new SqlCommand(StoredProcedureDal.updateGoal, connection);
                    connection.Open();
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.AddWithValue("@Id", model.businessid);
                    sqlCommand.Parameters.AddWithValue("@userid", model.userid);
                    sqlCommand.Parameters.AddWithValue("@monthlytarget", model.monthlytarget);
                    await sqlCommand.ExecuteNonQueryAsync();
                    isSuccessful = true;
                }

            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return isSuccessful;
        }

        private int GetTotalincomereceived(List<InvoicePaymentModel> payments)
        {
            var PaymentData = new List<InvoicePaymentViewModel>();
            foreach (var item in payments)
            {
                PaymentData.Add(new InvoicePaymentViewModel
                {
                    id = item.id,
                    amount = item.amount,
                    date_created = Convert.ToDateTime(item.date_created).Date,
                    paidwith = item.paidwith,
                    date_paid = Convert.ToDateTime(item.date_paid),
                });
            }
             var currentMonth = DateTime.Now.ToString("MM");
             int totalincomereceived = PaymentData.Where(m => m.date_created.ToString("MM") == currentMonth).Sum(m => m.amount);
           
            return totalincomereceived;
        }
    }
}