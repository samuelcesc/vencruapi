﻿using RazorEngine;
using RazorEngine.Templating;
using SharpRaven;
using SharpRaven.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using VencruApi.Models.Classes;
using VencruApi.Models.ViewModels;

namespace VencruApi.Models.DalClasses
{
    public class AdminDal
    {
        //email templates folder location
        private static readonly string TemplateFolder = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Views/Shared");

        //get welcome email body
        private static string GetWelcomeEmailBody(ConfirmEmailModel model, string code)
        {

            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            var result = string.Empty;

            try
            {
                string emailTemplate = File.ReadAllText(Path.Combine(TemplateFolder, "activationemail-template.cshtml"));

                if (!string.IsNullOrEmpty(code))
                {
                    model.callbackurl = code;

                    model.ismobile = true;
                }
                else
                {
                    model.ismobile = false;
                }

                result = Engine.Razor.RunCompile(emailTemplate, "welcomekey", typeof(ConfirmEmailModel), model, null);
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return result;
        }


        //get user contact email body
        private string GetUserContactUsEmailBody()
        {
            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            string result = string.Empty;

            try
            {
                string emailTemplate = File.ReadAllText(Path.Combine(TemplateFolder, "contactusresponse-template.cshtml"));

                result = Engine.Razor.RunCompile(emailTemplate, "contactusresponse", null, null, null);
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }
            return result;
        }


        //get team member invite email body
        private static string GetTeamMemberInviteEmailBody(TeamMemberInviteViewModel model)
        {
            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            string result = string.Empty;

            try
            {
                string emailTemplate = File.ReadAllText(Path.Combine(TemplateFolder, "teammemberinvite-template.cshtml"));
                result = Engine.Razor.RunCompile(emailTemplate, "inviteteammemberkey", typeof(TeamMemberInviteViewModel), model, null);
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }
            return result;

        }

        //get password reset email body
        private static string GetPasswordResetEmailBody(PasswordResetModel model)
        {
            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            string result = string.Empty;

            try
            {
                string emailTemplate = File.ReadAllText(Path.Combine(TemplateFolder, "passwordreset-template.cshtml"));
                result = Engine.Razor.RunCompile(emailTemplate, "passwordresetkey", typeof(PasswordResetModel), model, null);
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return result;
        }

        //get invoice template
        private static string GetinvoiceTemplate(ConfirmInvoiceModel model)
        {
            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            string result = string.Empty;

            try
            {
                string emailTemplate = File.ReadAllText(Path.Combine(TemplateFolder, "invoice-emailtemplate.cshtml"));
                result = Engine.Razor.RunCompile(emailTemplate, "invoicekey", typeof(ConfirmInvoiceModel), model, null);
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return result;
        }

        //get feedback email body
        private static string GetFeeBackEmailBody(FeedbackModel model)
        {
            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            string result = string.Empty;

            try
            {
                string emailTemplate = File.ReadAllText(Path.Combine(TemplateFolder, "feedbackmodel-template.cshtml"));
                result = Engine.Razor.RunCompile(emailTemplate, "feedback", typeof(FeedbackModel), model, null);
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }
            return result;
        }

        //get contact us email body
        private static string GetAdminContactUsEmailBody(ContactUsModel model)
        {
            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            string result = string.Empty;

            try
            {
                string emailTemplate = File.ReadAllText(Path.Combine(TemplateFolder, "contactusmodel-template.cshtml"));

                result = Engine.Razor.RunCompile(emailTemplate, "contactus", typeof(ContactUsModel), model, null);
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }
            return result;
        }

        //get waitlist email body
        public static string GetWaitListEmailBody()
        {
            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            string result = string.Empty;
            try
            {
                string emailTemplate = File.ReadAllText(Path.Combine(TemplateFolder, "waitinglist-template.cshtml"));
                result = Engine.Razor.RunCompile(emailTemplate, "waitinglistwelcomekey", null, null, null);
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }
            return result;
        }

        //add waitlist
        public bool AddWaitList(WaitListModel model)
        {
            bool isSuccessful = false;
            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");
            try
            {
                using (var connection = new SqlConnection(ConnectionStringDal.ConnectionString))
                {
                    var sqlAdapter = new SqlDataAdapter();
                    var sqlCommand = new SqlCommand(StoredProcedureDal.AddWaitlist, connection);
                    connection.Open();
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.AddWithValue("@Email", model.Email);
                    sqlCommand.ExecuteNonQuery();
                    isSuccessful = true;
                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return isSuccessful;
        }

        //add plan
        public async Task<string> AddPlan(PlanModel model)
        {
            string isSuccessful = string.Empty;

            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            try
            {
                using (var connection = new SqlConnection(ConnectionStringDal.ConnectionString))
                {
                    var sqlAdapter = new SqlDataAdapter();
                    var sqlCommand = new SqlCommand(StoredProcedureDal.AddPlan, connection);
                    connection.Open();
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.Add("@Id", SqlDbType.Int).Direction = ParameterDirection.Output;
                    sqlCommand.Parameters.AddWithValue("@price", model.price);
                    sqlCommand.Parameters.AddWithValue("@name", model.name);
                    sqlCommand.Parameters.AddWithValue("@plantype", model.plantype);
                    await sqlCommand.ExecuteNonQueryAsync();
                    isSuccessful = sqlCommand.Parameters["@Id"].Value.ToString();
                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }
            return isSuccessful;
        }

        //get plan
        public static PlanViewModel GetPlan(int planid)
        {
            PlanViewModel planmodel = new PlanViewModel();

            var planinfo = new PlanViewModel();

            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            try
            {
                DataSet userDataSet = new DataSet();
                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter();
                using (SqlConnection connection = new SqlConnection(ConnectionStringDal.ConnectionString))
                {
                    SqlDataAdapter sqlAdapter = new SqlDataAdapter();
                    SqlCommand sqlCommand = new SqlCommand(StoredProcedureDal.GetPlan, connection);
                    connection.Open();
                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    sqlCommand.Parameters.AddWithValue("@id", planid);
                    sqlDataAdapter.SelectCommand = sqlCommand;
                    sqlDataAdapter.Fill(userDataSet);
                    if (userDataSet != null & userDataSet.Tables.Count > 0)
                    {
                        for (int index = 0; index <= userDataSet.Tables[0].Rows.Count - 1; index++)
                        {
                            planinfo.name = Convert.ToString(userDataSet.Tables[0].Rows[index]["name"]);
                            planinfo.id = (int)(userDataSet.Tables[0].Rows[index]["id"]);
                            planinfo.plantype = Convert.ToString(userDataSet.Tables[0].Rows[index]["plantype"]);
                            planinfo.price = Convert.ToInt32(userDataSet.Tables[0].Rows[index]["price"]);
                            planinfo.date_created = Convert.ToDateTime(userDataSet.Tables[0].Rows[index]["date_created"]);

                        }
                    }
                    planmodel = planinfo;
                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return planmodel;
        }

        //get all plan
        public List<PlanViewModel> GetAllPlans()
        {
            var planListViewModel = new List<PlanViewModel>();

            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            try
            {
                var userDataSet = new DataSet();

                var sqlDataAdapter = new SqlDataAdapter();

                using (var connection = new SqlConnection(ConnectionStringDal.ConnectionString))
                {
                    SqlDataAdapter sqlAdapter = new SqlDataAdapter();
                    SqlCommand sqlCommand = new SqlCommand(StoredProcedureDal.GetAllPlan, connection);
                    connection.Open();
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlDataAdapter.SelectCommand = sqlCommand;
                    sqlDataAdapter.Fill(userDataSet);
                    if (userDataSet != null & userDataSet.Tables.Count > 0)
                    {
                        for (int index = 0; index <= userDataSet.Tables[0].Rows.Count - 1; index++)
                        {
                            var planInfo = new PlanViewModel()
                            {
                                id = (int)(userDataSet.Tables[0].Rows[index]["id"]),
                                name = (string)(userDataSet.Tables[0].Rows[index]["name"]),
                                plantype = (string)(userDataSet.Tables[0].Rows[index]["plantype"]),
                                price = (int)(userDataSet.Tables[0].Rows[index]["price"]),
                            };

                            planListViewModel.Add(planInfo);
                        }



                    }

                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return planListViewModel;
        }

        //delete plan
        public async Task<bool> DeletePlan(int id)
        {
            bool isSuccessful = false;

            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");


            try
            {
                using (var connection = new SqlConnection(ConnectionStringDal.ConnectionString))
                {
                    var sqlAdapter = new SqlDataAdapter();
                    var sqlCommand = new SqlCommand(StoredProcedureDal.DeletePlan, connection);
                    connection.Open();
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.AddWithValue("@id", id);
                    await sqlCommand.ExecuteNonQueryAsync();
                    isSuccessful = true;
                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return isSuccessful;
        }

        //add feedback
        public async Task<string> AddFeedback(FeedbackModel model)
        {
            string isSuccessful = string.Empty;

            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");


            try
            {
                using (var connection = new SqlConnection(ConnectionStringDal.ConnectionString))
                {
                    var sqlAdapter = new SqlDataAdapter();
                    var sqlCommand = new SqlCommand(StoredProcedureDal.AddFeedback, connection);
                    connection.Open();
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.Add("@Id", SqlDbType.Int).Direction = ParameterDirection.Output;
                    sqlCommand.Parameters.AddWithValue("@details", model.details);
                    sqlCommand.Parameters.AddWithValue("@emailaddress", model.emailaddress);
                    sqlCommand.Parameters.AddWithValue("@netpromoterscore", model.netpromoterscore);
                    await sqlCommand.ExecuteNonQueryAsync();
                    isSuccessful = sqlCommand.Parameters["@Id"].Value.ToString();
                    //await SendFeedbackEmailAsync(model);
                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }
            return isSuccessful;
        }

        //get feedback
        public FeedbackModel GetFeedback(string feedbackid)
        {
            FeedbackModel feedbackModel = new FeedbackModel();

            var feedbackInfo = new FeedbackModel();

            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            try
            {
                DataSet userDataSet = new DataSet();
                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter();
                using (SqlConnection connection = new SqlConnection(ConnectionStringDal.ConnectionString))
                {
                    SqlDataAdapter sqlAdapter = new SqlDataAdapter();
                    SqlCommand sqlCommand = new SqlCommand(StoredProcedureDal.GetFeedback, connection);
                    connection.Open();
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.AddWithValue("@id", feedbackid);
                    sqlDataAdapter.SelectCommand = sqlCommand;
                    sqlDataAdapter.Fill(userDataSet);
                    if (userDataSet != null & userDataSet.Tables.Count > 0)
                    {
                        for (int index = 0; index <= userDataSet.Tables[0].Rows.Count - 1; index++)
                        {
                            feedbackInfo.details = (string)(userDataSet.Tables[0].Rows[index]["name"]);
                            feedbackInfo.emailaddress = (string)(userDataSet.Tables[0].Rows[index]["id"]);
                            feedbackInfo.netpromoterscore = Convert.ToInt32(userDataSet.Tables[0].Rows[index]["price"]);
                            feedbackInfo.id = (int)(userDataSet.Tables[0].Rows[index]["id"]);
                            feedbackInfo.date_created = Convert.ToDateTime(userDataSet.Tables[0].Rows[index]["date_created"]);

                        }
                    }

                    feedbackModel = feedbackInfo;
                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return feedbackModel;
        }

        //get all feedback
        public List<FeedbackModel> GetAllFeedback()
        {
            var feedbackListModel = new List<FeedbackModel>();

            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            try
            {
                var userDataSet = new DataSet();

                var sqlDataAdapter = new SqlDataAdapter();

                using (var connection = new SqlConnection(ConnectionStringDal.ConnectionString))
                {
                    SqlDataAdapter sqlAdapter = new SqlDataAdapter();
                    SqlCommand sqlCommand = new SqlCommand(StoredProcedureDal.GetAllFeedback, connection);
                    connection.Open();
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlDataAdapter.SelectCommand = sqlCommand;
                    sqlDataAdapter.Fill(userDataSet);
                    if (userDataSet != null & userDataSet.Tables.Count > 0)
                    {
                        for (int index = 0; index <= userDataSet.Tables[0].Rows.Count - 1; index++)
                        {
                            var feedbackInfo = new FeedbackModel()
                            {
                                id = (int)(userDataSet.Tables[0].Rows[index]["id"]),
                                details = (string)(userDataSet.Tables[0].Rows[index]["name"]),
                                emailaddress = (string)(userDataSet.Tables[0].Rows[index]["plantype"]),
                                netpromoterscore = (int)(userDataSet.Tables[0].Rows[index]["price"]),
                            };

                            feedbackListModel.Add(feedbackInfo);
                        }

                    }

                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }
            return feedbackListModel;
        }

        //add review
        public async Task<string> AddReview(ReviewsModel model)
        {
            string isSuccessful = string.Empty;

            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            try
            {
                using (var connection = new SqlConnection(ConnectionStringDal.ConnectionString))
                {
                    var sqlAdapter = new SqlDataAdapter();
                    var sqlCommand = new SqlCommand(StoredProcedureDal.AddReview, connection);
                    connection.Open();
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.Add("@Id", SqlDbType.Int).Direction = ParameterDirection.Output;
                    sqlCommand.Parameters.AddWithValue("@details", model.fullname);
                    sqlCommand.Parameters.AddWithValue("@emailaddress", model.reviewcomment);
                    sqlCommand.Parameters.AddWithValue("@netpromoterscore", model.isapproved = false);
                    await sqlCommand.ExecuteNonQueryAsync();
                    isSuccessful = sqlCommand.Parameters["@Id"].Value.ToString();
                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }
            return isSuccessful;
        }

        //get review
        public ReviewsModel GetReview(string reviewid)
        {
            ReviewsModel reviewModel = new ReviewsModel();

            var reviewInfo = new ReviewsModel();

            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            try
            {
                DataSet userDataSet = new DataSet();
                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter();
                using (SqlConnection connection = new SqlConnection(ConnectionStringDal.ConnectionString))
                {
                    SqlDataAdapter sqlAdapter = new SqlDataAdapter();
                    SqlCommand sqlCommand = new SqlCommand(StoredProcedureDal.GetReview, connection);
                    connection.Open();
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.AddWithValue("@id", reviewid);
                    sqlDataAdapter.SelectCommand = sqlCommand;
                    sqlDataAdapter.Fill(userDataSet);
                    if (userDataSet != null & userDataSet.Tables.Count > 0)
                    {
                        for (int index = 0; index <= userDataSet.Tables[0].Rows.Count - 1; index++)
                        {
                            reviewInfo.fullname = (string)(userDataSet.Tables[0].Rows[index]["fullname"]);
                            reviewInfo.isapproved = (bool)(userDataSet.Tables[0].Rows[index]["isapproved"]);
                            reviewInfo.reviewcomment = Convert.ToString(userDataSet.Tables[0].Rows[index]["reviewcomment"]);
                            reviewInfo.id = (int)(userDataSet.Tables[0].Rows[index]["id"]);
                            reviewInfo.date_created = Convert.ToDateTime(userDataSet.Tables[0].Rows[index]["date_created"]);

                        }
                    }

                    reviewModel = reviewInfo;
                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return reviewModel;
        }

        //get all review
        public List<ReviewsModel> GetAllReviews()
        {
            var reviewListModel = new List<ReviewsModel>();

            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            try
            {
                var userDataSet = new DataSet();

                var sqlDataAdapter = new SqlDataAdapter();

                using (var connection = new SqlConnection(ConnectionStringDal.ConnectionString))
                {
                    SqlDataAdapter sqlAdapter = new SqlDataAdapter();
                    SqlCommand sqlCommand = new SqlCommand(StoredProcedureDal.GetAllReviews, connection);
                    connection.Open();
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlDataAdapter.SelectCommand = sqlCommand;
                    sqlDataAdapter.Fill(userDataSet);
                    if (userDataSet != null & userDataSet.Tables.Count > 0)
                    {
                        for (int index = 0; index <= userDataSet.Tables[0].Rows.Count - 1; index++)
                        {
                            var reviewInfo = new ReviewsModel()
                            {
                                id = (int)(userDataSet.Tables[0].Rows[index]["id"]),
                                fullname = (string)(userDataSet.Tables[0].Rows[index]["name"]),
                                reviewcomment = (string)(userDataSet.Tables[0].Rows[index]["plantype"]),
                                date_created = (DateTime)(userDataSet.Tables[0].Rows[index]["price"]),
                                isapproved = (bool)(userDataSet.Tables[0].Rows[index]["price"]),

                            };

                            reviewListModel.Add(reviewInfo);
                        }

                    }

                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }
            return reviewListModel;
        }

        //get approved review
        public List<ReviewsModel> GetApprovedReviews()
        {
            var reviewListModel = new List<ReviewsModel>();

            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");


            try
            {
                var userDataSet = new DataSet();

                var sqlDataAdapter = new SqlDataAdapter();

                using (var connection = new SqlConnection(ConnectionStringDal.ConnectionString))
                {
                    SqlDataAdapter sqlAdapter = new SqlDataAdapter();
                    SqlCommand sqlCommand = new SqlCommand(StoredProcedureDal.GetApprovedReviews, connection);
                    connection.Open();
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlDataAdapter.SelectCommand = sqlCommand;
                    sqlDataAdapter.Fill(userDataSet);
                    if (userDataSet != null & userDataSet.Tables.Count > 0)
                    {
                        for (int index = 0; index <= userDataSet.Tables[0].Rows.Count - 1; index++)
                        {
                            var reviewInfo = new ReviewsModel()
                            {
                                id = (int)(userDataSet.Tables[0].Rows[index]["id"]),
                                fullname = (string)(userDataSet.Tables[0].Rows[index]["name"]),
                                reviewcomment = (string)(userDataSet.Tables[0].Rows[index]["plantype"]),
                                date_created = (DateTime)(userDataSet.Tables[0].Rows[index]["price"]),
                                isapproved = (bool)(userDataSet.Tables[0].Rows[index]["price"]),

                            };

                            reviewListModel.Add(reviewInfo);
                        }

                    }

                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }
            return reviewListModel;
        }

        public async Task<bool> ApproveReview(int id)
        {
            bool isSuccessful = false;

            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            try
            {
                using (var connection = new SqlConnection(ConnectionStringDal.ConnectionString))
                {
                    var sqlAdapter = new SqlDataAdapter();
                    var sqlCommand = new SqlCommand(StoredProcedureDal.ApproveReview, connection);
                    connection.Open();
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.AddWithValue("@id", id);
                    await sqlCommand.ExecuteNonQueryAsync();
                    isSuccessful = true;
                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return isSuccessful;
        }

        //get wait list
        public List<WaitListModel> GetWaitList(WaitListModel model)
        {
            var waitlistModels = new List<WaitListModel>();

            var waitLists = new List<WaitListModel>();

            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            try
            {
                DataSet waitlistDataSet = new DataSet();
                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter();
                using (SqlConnection connection = new SqlConnection(ConnectionStringDal.ConnectionString))
                {
                    SqlDataAdapter sqlAdapter = new SqlDataAdapter();
                    SqlCommand sqlCommand = new SqlCommand(StoredProcedureDal.GetallWaitlist, connection);
                    connection.Open();
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    //sqlCommand.Parameters.AddWithValue("@Userid", userid);
                    sqlDataAdapter.SelectCommand = sqlCommand;
                    sqlDataAdapter.Fill(waitlistDataSet);
                    if (waitlistDataSet != null & waitlistDataSet.Tables.Count > 0)
                    {
                        for (int index = 0; index <= waitlistDataSet.Tables[0].Rows.Count - 1; index++)
                        {
                            WaitListModel waitlistInfo = new WaitListModel()
                            {
                                Email = Convert.ToString(waitlistDataSet.Tables[0].Rows[index]["Email"]),
                                Date_added = Convert.ToString(waitlistDataSet.Tables[0].Rows[index]["Date_added"])

                            };
                            waitLists.Add(waitlistInfo);
                        }
                    }
                    waitlistModels = waitLists;
                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }
            return waitlistModels;
        }

        //check wait list
        public List<WaitListModel> CheckWaitList(WaitListModel model)
        {
            var waitlistModels = new List<WaitListModel>();

            var waitLists = new List<WaitListModel>();

            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            try
            {
                DataSet waitlistDataSet = new DataSet();
                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter();
                using (SqlConnection connection = new SqlConnection(ConnectionStringDal.ConnectionString))
                {
                    SqlDataAdapter sqlAdapter = new SqlDataAdapter();
                    SqlCommand sqlCommand = new SqlCommand(StoredProcedureDal.GetWaitlist, connection);
                    connection.Open();
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.AddWithValue("@Email", model.Email);
                    sqlDataAdapter.SelectCommand = sqlCommand;
                    sqlDataAdapter.Fill(waitlistDataSet);
                    if (waitlistDataSet != null & waitlistDataSet.Tables.Count > 0)
                    {
                        for (int index = 0; index <= waitlistDataSet.Tables[0].Rows.Count - 1; index++)
                        {
                            WaitListModel waitlistInfo = new WaitListModel()
                            {
                                Email = Convert.ToString(waitlistDataSet.Tables[0].Rows[index]["Email"]),
                                Date_added = Convert.ToString(waitlistDataSet.Tables[0].Rows[index]["Date_added"])

                            };
                            waitLists.Add(waitlistInfo);
                        }
                    }
                    waitlistModels = waitLists;
                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }
            return waitlistModels;
        }

        //send password reset email
        public static async Task<bool> SendPasswordResetEmailAsync(PasswordResetModel model)
        {
            bool isSuccessful = false;

            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            try
            {
                var messageBody = GetPasswordResetEmailBody(model);
                var messageTo = new MailAddressCollection();
                var message = new MailMessage
                {
                    IsBodyHtml = true,
                    From = new MailAddress(UtilitiesClass.mailFrom, UtilitiesClass.mailFromName),
                    Body = messageBody,
                    Subject = "Vencru Reset password",
                };

                message.To.Add(new MailAddress(model.Email));

                using (var client = new SmtpClient(UtilitiesClass.mailHost, UtilitiesClass.mailPort))
                {
                    client.Credentials = new NetworkCredential(UtilitiesClass.mailUsername, UtilitiesClass.mailPassword);
                    client.EnableSsl = true;
                    await client.SendMailAsync(message);
                    isSuccessful = true;
                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return isSuccessful;
        }

        //send welcome email
        public static async Task<bool> SendWelcomeEmailAsync(ConfirmEmailModel model, bool ismobile)
        {
            bool isSuccessful = false;

            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            var verificationCode = string.Empty;

            try
            {
                if (ismobile == true)
                {
                    verificationCode = await AddUserVerificationCode(model.email);
                }
                else
                {
                }

                var messageBody = GetWelcomeEmailBody(model, verificationCode);

                var messageTo = new MailAddressCollection();

                var message = new MailMessage
                {
                    IsBodyHtml = true,
                    From = new MailAddress(UtilitiesClass.mailFrom, UtilitiesClass.mailFromName),
                    Body = messageBody,
                    Subject = "Welcome To Vencru",
                };

                message.To.Add(new MailAddress(model.email));

                using (var client = new SmtpClient(UtilitiesClass.mailHost, UtilitiesClass.mailPort))
                {
                    client.Credentials = new NetworkCredential(UtilitiesClass.mailUsername, UtilitiesClass.mailPassword);
                    client.EnableSsl = true;
                    await client.SendMailAsync(message);
                    isSuccessful = true;
                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }


            return isSuccessful;
        }

        //send feedback email
        public static async Task<bool> SendFeedbackEmailAsync(FeedbackModel model)
        {
            bool isSuccessful = false;

            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            try
            {
                var messageBody = GetFeeBackEmailBody(model);
                var messageTo = new MailAddressCollection();
                var message = new MailMessage
                {
                    IsBodyHtml = true,
                    From = new MailAddress(UtilitiesClass.mailFrom, UtilitiesClass.mailFromName),
                    Body = messageBody,
                    Subject = "Feedback from VencruSMEBeta",
                };

                message.To.Add(new MailAddress(UtilitiesClass.mailFrom));

                using (var client = new SmtpClient(UtilitiesClass.mailHost, UtilitiesClass.mailPort))
                {
                    client.Credentials = new NetworkCredential(UtilitiesClass.mailUsername, UtilitiesClass.mailPassword);
                    client.EnableSsl = true;
                    await client.SendMailAsync(message);
                    isSuccessful = true;
                }

            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }


            return isSuccessful;
        }

        //send contact us email to admin
        private async Task<bool> SendContactUsEmailToAdminAsync(ContactUsModel model)
        {
            bool isSuccessful = false;

            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            try
            {
                var messageBody = GetAdminContactUsEmailBody(model);
                var messageTo = new MailAddressCollection();
                var message = new MailMessage
                {
                    IsBodyHtml = true,
                    From = new MailAddress(UtilitiesClass.mailFrom, UtilitiesClass.mailFromName),
                    Body = messageBody,
                    Subject = "Contact us from VencruSMEBeta",
                };

                message.To.Add(new MailAddress(UtilitiesClass.mailFrom));

                using (var client = new SmtpClient(UtilitiesClass.mailHost, UtilitiesClass.mailPort))
                {
                    client.Credentials = new NetworkCredential(UtilitiesClass.mailUsername, UtilitiesClass.mailPassword);
                    client.EnableSsl = true;
                    await client.SendMailAsync(message);
                    isSuccessful = true;
                }

            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }
            return isSuccessful;
        }

        //send contact us email
        private async Task<bool> SendContactUsEmailAutoResponseToUserAsync(ContactUsModel model)
        {
            bool isSuccessful = false;

            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            try
            {
                var messageBody = GetUserContactUsEmailBody();
                var messageTo = new MailAddressCollection();
                var message = new MailMessage
                {
                    IsBodyHtml = true,
                    From = new MailAddress(UtilitiesClass.mailFrom, UtilitiesClass.mailFromName),
                    Body = messageBody,
                    Subject = "Vencru Team",
                };

                message.To.Add(new MailAddress(model.emailaddress));

                using (var client = new SmtpClient(UtilitiesClass.mailHost, UtilitiesClass.mailPort))
                {
                    client.Credentials = new NetworkCredential(UtilitiesClass.mailUsername, UtilitiesClass.mailPassword);
                    client.EnableSsl = true;
                    await client.SendMailAsync(message);
                    isSuccessful = true;
                }

            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }


            return isSuccessful;
        }

        //send waiting list email
        public static async Task<bool> SendWaitingListWelcomeEmailAsync(WaitListModel model)
        {
            bool isSuccessful = false;

            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            try
            {
                var messageBody = GetWaitListEmailBody();
                var messageTo = new MailAddressCollection();
                var message = new MailMessage
                {
                    IsBodyHtml = true,
                    From = new MailAddress(UtilitiesClass.mailFrom, UtilitiesClass.mailFromName),
                    Body = messageBody,
                    Subject = "Welcome To Vencru",
                };

                message.To.Add(new MailAddress(model.Email));

                using (var client = new SmtpClient(UtilitiesClass.mailHost, UtilitiesClass.mailPort))
                {
                    client.Credentials = new NetworkCredential(UtilitiesClass.mailUsername, UtilitiesClass.mailPassword);
                    client.EnableSsl = true;
                    await client.SendMailAsync(message);
                    isSuccessful = true;
                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return isSuccessful;
        }

        //send invoice email
        public static async Task<bool> SendInvoiceEmail(ConfirmInvoiceModel model)
        {
            bool isSuccessful = false;

            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            try
            {
                var user = new ApplicationUser();

                using (var dbContext = new ApplicationDbContext())
                {
                    user = dbContext.Users.Find(model.invoice.business.ownerid);
                }

                var messageBody = GetinvoiceTemplate(model);

                var messageTo = new MailAddressCollection();

                var message = new MailMessage
                {
                    IsBodyHtml = true,
                    From = new MailAddress(UtilitiesClass.mailFrom, $"{ user.lastname  + user.firstname } via vencru"),
                    Body = messageBody,
                    Subject = $"{ user.lastname + user.firstname } has sent you an invoice",
                };

                if (!string.IsNullOrEmpty(model.invoice.email))
                {
                    message.To.Add(new MailAddress(model.invoice.email));

                }
                else
                {
                    message.To.Add(new MailAddress(model.invoice.client.companyemail));

                }

                using (var client = new SmtpClient(UtilitiesClass.mailHost, UtilitiesClass.mailPort))
                {
                    client.Credentials = new NetworkCredential(UtilitiesClass.mailUsername, UtilitiesClass.mailPassword);
                    client.EnableSsl = true;
                    await client.SendMailAsync(message);
                    isSuccessful = true;
                }
            }
            catch (Exception exception)
            {

                ravenClient.Capture(new SentryEvent(exception));
            }

            return isSuccessful;
        }

        //send team member invite email
        public static async Task<bool> SendTeamMemberInvite(TeamMemberInviteViewModel model)
        {

            bool isSuccessful = false;

            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            try
            {
                var messageBody = GetTeamMemberInviteEmailBody(model);
                var messageTo = new MailAddressCollection();
                var message = new MailMessage
                {
                    IsBodyHtml = true,
                    From = new MailAddress(UtilitiesClass.mailFrom, UtilitiesClass.mailFromName),
                    Body = messageBody,
                    Subject = "Welcome To Vencru",
                };

                message.To.Add(new MailAddress(model.memberemail));

                using (var client = new SmtpClient(UtilitiesClass.mailHost, UtilitiesClass.mailPort))
                {
                    client.Credentials = new NetworkCredential(UtilitiesClass.mailUsername, UtilitiesClass.mailPassword);
                    client.EnableSsl = true;
                    await client.SendMailAsync(message);
                    isSuccessful = true;
                }
            }
            catch (Exception exception)
            {

                ravenClient.Capture(new SentryEvent(exception));
            }

            return isSuccessful;
        }

        //add promo code
        public async Task<string> AddPromoCode(PromoCodeModel model)
        {
            string isSuccessful = string.Empty;

            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            try
            {
                using (var connection = new SqlConnection(ConnectionStringDal.ConnectionString))
                {
                    var sqlAdapter = new SqlDataAdapter();
                    var sqlCommand = new SqlCommand(StoredProcedureDal.AddPromoCode, connection);
                    connection.Open();
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.Add("@Id", SqlDbType.Int).Direction = ParameterDirection.Output;
                    sqlCommand.Parameters.AddWithValue("@code", model.code);
                    sqlCommand.Parameters.AddWithValue("@discount", model.discount);
                    sqlCommand.Parameters.AddWithValue("@limit", model.limit);
                    sqlCommand.Parameters.AddWithValue("@userid", model.created_by);

                    await sqlCommand.ExecuteNonQueryAsync();
                    isSuccessful = sqlCommand.Parameters["@Id"].Value.ToString();
                }

            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }
            return isSuccessful;
        }

        //get promocode count
        private int? GetPromoCodeCount(int promocodeid)
        {
            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            int? count = null;

            try
            {
                DataSet promoCodeDataSet = new DataSet();
                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter();
                using (SqlConnection connection = new SqlConnection(ConnectionStringDal.ConnectionString))
                {
                    SqlDataAdapter sqlAdapter = new SqlDataAdapter();
                    SqlCommand sqlCommand = new SqlCommand(StoredProcedureDal.GetPromoCodeCount, connection);
                    connection.Open();
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.AddWithValue("@promocodeid", promocodeid);
                    sqlDataAdapter.SelectCommand = sqlCommand;
                    sqlDataAdapter.Fill(promoCodeDataSet);
                    if (promoCodeDataSet != null & promoCodeDataSet.Tables.Count > 0)
                    {
                        for (int index = 0; index <= promoCodeDataSet.Tables[0].Rows.Count - 1; index++)
                        {
                            count = Convert.ToInt32(promoCodeDataSet.Tables[0].Rows[index]["count"]);
                        };
                    }
                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return count;
        }

        //apply promo code
        public async Task<bool> ApplyPromoCode(PromoCodeUserModel model)
        {
            bool isSuccessful = false;

            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            var promoCodeCount = GetPromoCodeCount(model.promocodeid);

            var promoCode = GetPromoCode(model.userid, null, model.promocodeid);

            if (promoCodeCount > promoCode.limit)
            {
                isSuccessful = false;
            }
            else
            {
                try
                {
                    using (var connection = new SqlConnection(ConnectionStringDal.ConnectionString))
                    {
                        var sqlAdapter = new SqlDataAdapter();
                        var sqlCommand = new SqlCommand(StoredProcedureDal.ApplyPromoCode, connection);
                        connection.Open();
                        sqlCommand.CommandType = CommandType.StoredProcedure;
                        sqlCommand.Parameters.AddWithValue("@promocodeid", model.promocodeid);
                        sqlCommand.Parameters.AddWithValue("@userid", model.userid);
                        await sqlCommand.ExecuteNonQueryAsync();
                        isSuccessful = true;
                    }

                }
                catch (Exception exception)
                {
                    ravenClient.Capture(new SentryEvent(exception));
                }
            }

            return isSuccessful;
        }

        //create user subscription
        private static SubscriptionModel CreateuserSubscriptionPlan(UserSubscriptionModel model)
        {
            var plan = GetPlan(model.planid);

            DateTime newExpireDate = DateTime.UtcNow;

            if (model.istrial == true)
            {
                newExpireDate = DateTime.UtcNow.AddDays(model.trialdays);
            }
            else { }

            if (plan.plantype.ToLower() == "monthly")
            {
                var subscriptionModel = new SubscriptionModel()
                {
                    userid = model.userid,
                    date_created = DateTime.UtcNow.Date,
                    date_expire = newExpireDate.AddMonths(1),
                    planid = model.planid,
                    istrial = model.istrial,
                    trialdays = model.trialdays,
                    referencenumber = model.referencenumber,
                    status = true,
                    promocode = model.promocode
                };

                return subscriptionModel;
            }
            else if (plan.plantype.ToLower() == "yearly")
            {
                var subscriptionModel = new SubscriptionModel()
                {
                    userid = model.userid,
                    date_created = DateTime.UtcNow.Date,
                    date_expire = newExpireDate.AddYears(1),
                    planid = model.planid,
                    istrial = model.istrial,
                    trialdays = model.trialdays,
                    referencenumber = model.referencenumber,
                    status = true,
                    promocode = model.promocode
                };

                return subscriptionModel;
            }
            else
            {
                return null;
            }

        }

        //add user to subscription plan
        public static async Task<string> AddUserSubscription(UserSubscriptionModel model)
        {
            string isSuccessful = string.Empty;

            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            var userSubscription = CreateuserSubscriptionPlan(model);

            try
            {
                if (userSubscription == null)
                {
                    return isSuccessful;
                }
                else
                {
                    using (var connection = new SqlConnection(ConnectionStringDal.ConnectionString))
                    {
                        var sqlAdapter = new SqlDataAdapter();
                        var sqlCommand = new SqlCommand(StoredProcedureDal.AddSubscription, connection);
                        connection.Open();
                        sqlCommand.CommandType = CommandType.StoredProcedure;
                        sqlCommand.Parameters.Add("@Id", SqlDbType.Int).Direction = ParameterDirection.Output;
                        sqlCommand.Parameters.AddWithValue("@date_expire", userSubscription.date_expire);
                        sqlCommand.Parameters.AddWithValue("@istrial", userSubscription.istrial);
                        sqlCommand.Parameters.AddWithValue("@planid", userSubscription.planid);
                        sqlCommand.Parameters.AddWithValue("@referencenumber", userSubscription.referencenumber);
                        sqlCommand.Parameters.AddWithValue("@status", userSubscription.status);
                        sqlCommand.Parameters.AddWithValue("@trialdays", userSubscription.trialdays);
                        sqlCommand.Parameters.AddWithValue("@userid", userSubscription.userid);
                        sqlCommand.Parameters.AddWithValue("@promocode", userSubscription.promocode);
                        await sqlCommand.ExecuteNonQueryAsync();
                        isSuccessful = sqlCommand.Parameters["@Id"].Value.ToString();
                    }

                }

            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }
            return isSuccessful;
        }

        //add user verification code
        private static async Task<string> AddUserVerificationCode(string email)
        {

            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            string code = GenerateVerificationCode();

            try
            {
                using (var connection = new SqlConnection(ConnectionStringDal.ConnectionString))
                {
                    var sqlAdapter = new SqlDataAdapter();
                    var sqlCommand = new SqlCommand(StoredProcedureDal.AddUserVerificationCode, connection);
                    connection.Open();
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.AddWithValue("@email", email);
                    sqlCommand.Parameters.AddWithValue("@code", code);

                    await sqlCommand.ExecuteNonQueryAsync();
                }

            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return code;
        }

        public static async Task<bool> VerifyUser(VerifyEmailModel model)
        {
            bool isSuccessful = false;

            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            try
            {
                using (var dbContext = new ApplicationDbContext())
                {
                    var user = dbContext.Users.Find(model.userid);
                    if (user != null)
                    {
                        if (model.code == user.verificationcode)
                        {
                            user.EmailConfirmed = true;
                            isSuccessful = true;
                            await dbContext.SaveChangesAsync();
                        }
                        else
                        {
                            isSuccessful = false;
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return isSuccessful;

        }

        //get promocode
        public PromoCodeModel GetPromoCode(string userid, string promocode, int promocodeid)
        {
            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            var promoCodeModel = new PromoCodeModel();

            try
            {
                DataSet promoCodeDataSet = new DataSet();
                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter();
                using (SqlConnection connection = new SqlConnection(ConnectionStringDal.ConnectionString))
                {
                    SqlDataAdapter sqlAdapter = new SqlDataAdapter();
                    SqlCommand sqlCommand = new SqlCommand(StoredProcedureDal.GetPromoCode, connection);
                    connection.Open();
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.AddWithValue("@id", promocodeid);
                    sqlCommand.Parameters.AddWithValue("@promocode", promocode);
                    sqlDataAdapter.SelectCommand = sqlCommand;
                    sqlDataAdapter.Fill(promoCodeDataSet);
                    if (promoCodeDataSet != null & promoCodeDataSet.Tables.Count > 0)
                    {
                        for (int index = 0; index <= promoCodeDataSet.Tables[0].Rows.Count - 1; index++)
                        {
                            PromoCodeModel promoCode = new PromoCodeModel()
                            {
                                id = Convert.ToInt32(promoCodeDataSet.Tables[0].Rows[index]["id"]),
                                code = Convert.ToString(promoCodeDataSet.Tables[0].Rows[index]["code"]),
                                discount = Convert.ToInt32(promoCodeDataSet.Tables[0].Rows[index]["discount"]),
                                limit = Convert.ToInt32(promoCodeDataSet.Tables[0].Rows[index]["limit"]),

                            };

                            promoCodeModel = promoCode;
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }

            return promoCodeModel;
        }

        //send contact us 
        public async Task<string> AddContactUs(ContactUsModel model)
        {
            string isSuccessful = string.Empty;

            var ravenClient = new RavenClient("https://12f7b8111ce84ed4befba84c693e909a@sentry.io/1316862");

            try
            {
                using (var connection = new SqlConnection(ConnectionStringDal.ConnectionString))
                {
                    var sqlAdapter = new SqlDataAdapter();
                    var sqlCommand = new SqlCommand(StoredProcedureDal.AddContactUs, connection);
                    connection.Open();
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.Add("@Id", SqlDbType.Int).Direction = ParameterDirection.Output;
                    sqlCommand.Parameters.AddWithValue("@emailaddress", model.emailaddress);
                    sqlCommand.Parameters.AddWithValue("@subject", model.subject);
                    sqlCommand.Parameters.AddWithValue("@question", model.question);
                    await sqlCommand.ExecuteNonQueryAsync();
                    isSuccessful = sqlCommand.Parameters["@Id"].Value.ToString();
                    await SendContactUsEmailToAdminAsync(model);
                    await SendContactUsEmailAutoResponseToUserAsync(model);
                }
            }
            catch (Exception exception)
            {
                ravenClient.Capture(new SentryEvent(exception));
            }
            return isSuccessful;
        }

        public static char GenerateLetter()
        {
            Random randomNumber = new Random();
            int number = randomNumber.Next(0, 26);
            char letter = (char)('A' + number);
            return letter;
        }

        public static string GenerateVerificationCode()
        {
            string code = GenerateLetter().ToString();

            {
                code += Guid.NewGuid().ToString("N").Substring(0, 5);

                code += GenerateLetter().ToString();

            }

            return code;
        }

        public static void GetAllUsers()
        {
            var users = new List<ApplicationUser>();
            using (var dbContext = new ApplicationDbContext())
            {
                users = dbContext.Users.g(m => m).ToList();
            }



        }


    }
}