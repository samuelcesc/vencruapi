﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace VencruApi.Models
{
    public class InvoiceModel
    {
        //public InvoiceModel()
        //{
        //    Products = new HashSet<ProductModel>();
        //}

        [Key]
        public int id { get; set; }

        [Required]
        public string invoicenumber { get; set; }

        public Guid invoiceguid { get { return Guid.NewGuid(); } set { } }

        public string description { get; set; }

        public int subtotal { get; set; }

        public string image { get; set; }

        public int discount { get; set; }

        public string paymentlink { get; set; }

        public int amountdue { get; set; }

        public int deposit { get; set; }

        public string paymenttype { get; set; }

        public string notes { get; set; }

        public int invoicestyle { get; set; }

        [Required]
        public string userid { get; set; }

        public string color { get; set; }

        public string font { get; set; }

        public string sendstyle { get; set; }

        public string personalmessage { get; set; }

        public string invoicestatus { get; set; }

        public bool isdeleted { get; set; }

        public int requireddeposit { get; set; }

        public string date_created { get; set; }

        public string invoicetype { get; set; }

        public string email { get; set; }

        [Required]
        public int businessid { get; set; }

        public BusinessModel business { get; set; }

        [Required]
        public int clientid { get; set; }

        public string due_date { get; set; }

        public string issue_date { get; set; }

        public ClientsModel client { get; set; }

        public ICollection<ItemsModel> items { get; set; }

        public ICollection<InvoicePaymentModel> payment { get; set; }

        //public virtual ICollection<ProductModel> Products { get; set; }
        
        public int utime
        {
            get { return getUtime(); }

            set { }
        }

        private int getUtime()
        {
            return utime = (Int32)DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1)).TotalSeconds;
        }
    }
}