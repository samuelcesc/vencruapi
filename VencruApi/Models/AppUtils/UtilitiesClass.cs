﻿using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using System;
using System.Configuration;
using System.IO;

namespace VencruApi.Models.Classes
{
    public class UtilitiesClass
    {
        private static readonly RegionEndpoint bucketRegion = RegionEndpoint.USEast1;

        private static IAmazonS3 s3Client;

        public static string mailFrom
        {
            get
            {
                return ConfigurationManager.AppSettings.Get("Mail");
            }
        }

        public static string mailFromName
        {
            get
            {
                return ConfigurationManager.AppSettings.Get("MailFromName");
            }
        }

        public static string mailHost
        {
            get
            {
                return ConfigurationManager.AppSettings.Get("MailHost");
            }
        }

        public static string mailPassword
        {
            get
            {
                return ConfigurationManager.AppSettings.Get("MailPassword");
            }
        }

        public static int mailPort
        {
            get
            {
                return Convert.ToInt32(ConfigurationManager.AppSettings.Get("MailPort"));
            }
        }

        public static string mailUsername
        {
            get
            {
                return ConfigurationManager.AppSettings.Get("MailUsername");
            }
        }

        public static string expenseBucketname
        {
            get
            {
                return ConfigurationManager.AppSettings.Get("ExpenseS3BucketName");
            }
        }

        public static string businesslogoBucketname
        {
            get
            {
                return ConfigurationManager.AppSettings.Get("BusinessS3BucketName");
            }
        }

        public static string profileImageBucketname
        {
            get
            {
                return ConfigurationManager.AppSettings.Get("ProfileS3BucketName");

            }
        }

        public static string PayStackSecret
        {
            get
            {
                return ConfigurationManager.AppSettings.Get("PayStackSecret");
            }
        }
        public static string PayStackPublic
        {
            get
            {
                return ConfigurationManager.AppSettings.Get("PayStackPublic");
            }
        }

        public static string sentryDSN
        {
            get { return ConfigurationManager.AppSettings.Get("SentryDSN"); }
        }

       

        public static bool CheckIfMobile(string url)
        {
            if (url.ToLower() == "ismobile")
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        //Get image url from s3 bucket on aws
        public static string GetImgUrl(string key, string bucketname)
        {
            if (string.IsNullOrEmpty(key))
            {
                return string.Empty;
            }
            else
            {
                s3Client = new AmazonS3Client(bucketRegion);

                var imgurlrequest = new GetPreSignedUrlRequest
                {
                    BucketName = bucketname,
                    Key = key,
                    Expires = DateTime.Now.AddDays(10)
                };

                return s3Client.GetPreSignedURL(imgurlrequest);
            }
        }

        public static bool IsBase64(string text)
        {
            try
            {
                var stream = new MemoryStream(Convert.FromBase64String(text));
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        
    }
}