﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VencruApi.Models
{
    public class SortDataModel
    {
        public string name { get; set; }
        public int totalamount { get; set; }
        public int totaloutstanding { get; set; }
        public string created_date { get; set; }
        public string sortorder { get; set; }
    }
}