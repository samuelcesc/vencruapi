﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Net.Http;
using System.Web;

namespace VencruApi.Models
{
    public class UpdateprofileModel
    {
        [Required]
        public string userid { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string profileimageurl { get; set; }
        [NotMapped]
        public string image { get; set; }
        public string emailaddress { get; set; }
    }
}