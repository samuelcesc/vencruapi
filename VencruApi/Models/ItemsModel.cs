﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace VencruApi.Models
{
    public class ItemsModel
    {
        [Key]
        public int id { get; set; }
       
        public string description { get; set; }
        public int price { get; set; }
        public int quantity { get; set; }
        public int amount { get; set; }
        public int invoiceid { get; set; }
        public InvoiceModel invoice { get; set; }
        public int productid { get; set; }
        public ProductModel product { get; set; }
        public string date_created { get; set; }
    }
}