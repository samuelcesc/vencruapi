﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VencruApi.Models
{
    public class DeleteMultipleClientModel
    {
        public string userid { get; set; }
        public List<DeleteClientModel> items { get; set; }
    }
}