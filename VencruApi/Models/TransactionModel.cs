﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VencruApi.Models
{
    public class TransactionModel
    {
        public string transactionreference { get; set; }
        public string emailaddress { get; set; }
        public int planid { get; set; }
        public string firstname { get; set; }
    }
}