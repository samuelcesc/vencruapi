﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VencruApi.Models
{
    public class OutStandingRevenueViewModel
    {
        public int overdue { get; set; }
        public int due { get; set; }
        public int totaloutstanding { get { return GetTotalOutstanding(due, overdue); } set { } }

        private int GetTotalOutstanding(int due, int overdue)
        {
            return due + overdue;
        }
    }
}