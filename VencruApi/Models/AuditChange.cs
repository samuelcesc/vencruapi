﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VencruApi.Models
{
    public class AuditChange
    {
        public string datetimestamp { get; set; }
        public AuditActionType auditactiontype { get; set; }
        public string auditactiontypeName { get; set; }
        public List<AuditData> changes { get; set; }
        public AuditChange()
        {
            changes = new List<AuditData>();
        }
    }

    public enum AuditActionType
    {
        Create = 1,
        Update,
        Delete,
        Restore
    }
}