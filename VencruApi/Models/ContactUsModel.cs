﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace VencruApi.Models
{
    public class ContactUsModel
    {
        [Key]
        public int id { get; set; }
        [Required]
        public string emailaddress { get; set; }
        public string subject { get; set; }
        [Required]
        public string question { get; set; }
        public string date_added { get; set; }
    }
}