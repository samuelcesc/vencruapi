﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace VencruApi.Models
{
    public class BusinessModel
    {
        public BusinessModel()
        {
            Users = new HashSet<ApplicationUser>();
        }

        [Key]
        public int Id { get; set; }

        public string city { get; set; }
        [Required]
        public string companyname { get; set; }

        public string firstname { get; set; }

        public string lastname { get; set; }

        public string country { get; set; }

        public string industry { get; set; }

        public byte isincorporated { get; set; }

        public string state { get; set; }

        //public string street {get;set;}

        public string address { get; set; }

        public string employeesize { get; set; }

        public string currency { get; set; }

        public byte onlinepayment { get; set; }

        public string phonenumber { get; set; }

        public string date_created { get; set; }

        [Required]
        public string userid { get; set; }

        public string domain { get; set; }

        public string facebookurl { get; set; }

        public string twitterurl { get; set; }

        public string linkdinurl { get; set; }

        public string instagramurl { get; set; }

        public string logourl { get; set; }
        [NotMapped]
        public string image { get; set; }

        public string service { get; set; }

        public int monthlytarget { get; set; }

        public int estimatedmonthlyrevenue { get; set; }

        public int tax { get; set; }

        public string vatnumber { get; set; }

        public int utime
        {
            get { return getUtime(); }

            set { }
        }

        public virtual ICollection<ApplicationUser> Users { get; set; }

        public ICollection<ClientsModel> Clients { get; set; }

        public ICollection<ExpenseModel> Expenses { get; set; }

        public ICollection<ProductModel> Products { get; set; }

        public ICollection<InvoiceModel> Invoices { get; set; }

        public ICollection<BankSettingsModel> Banks { get; set; }

        public ICollection<TeamMemberModel> TeamMembers { get; set; }

        public virtual PayStackSubAccountModel PayStackSubAccount { get; set; }



        private int getUtime()
        {
            return utime = (Int32)DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1)).TotalSeconds;
        }


    }
  
}