﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VencruApi.Models
{
    public class AuditData
    {
        public string fieldname { get; set; }
        public string valuebefore { get; set; }
        public string valueafter { get; set; }
    }
}