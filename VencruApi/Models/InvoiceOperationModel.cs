﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace VencruApi.Models
{
    public class InvoiceOperationModel
    {
        [Required]
        public int? invoiceid { get; set; }
        [Required]
        public int? businessid { get; set; }

        public int? itemid { get; set; }

        public string userid { get; set; }
    }
}