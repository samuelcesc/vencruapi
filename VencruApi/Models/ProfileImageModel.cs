﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace VencruApi.Models
{
    public class ProfileImageModel
    {
        [Required]
        public string userid { get; set; }
        [Required]
        public string image { get; set; }
        [Required]
        public int businessid { get; set; }
    }
}