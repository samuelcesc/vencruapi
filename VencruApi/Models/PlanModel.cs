﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace VencruApi.Models
{
    public class PlanModel
    {
        [Key]
        public int id { get; set; }
        [Required]
        public int price { get; set; }
        [Required]
        public string name { get; set; }
        [Required]
        public string plantype { get; set; }

        public DateTime date_created { get; set; }

        public ICollection<SubscriptionModel> subscriptions { get; set; }

    }
}