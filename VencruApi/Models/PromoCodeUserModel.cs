﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace VencruApi.Models
{
    public class PromoCodeUserModel
    {
        [Required]
        public int promocodeid { get; set; }
        [Required]
        public string userid { get; set; }
    }
}